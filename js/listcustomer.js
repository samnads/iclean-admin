if ($('#newCustomerList').length) {
	var customerlistDataTable = $('#newCustomerList').DataTable({
	'bFilter' : false,
	'bLengthChange': false,
	'pageLength' : 10,
	'processing': true,
	'serverSide': true,
	'bSort' : false,
	'serverMethod': 'post',
	'ajax': {
		'url':_base_url+'customer/list_ajax_customerlist',
		'data': function(data){
			if($('#customer_date').length)
			{
				var regdate = $('#customer_date').val();
				data.regdate = regdate;
			} else {
				data.regdate = '';
			}
			
			if($('#customer_date_to').length)
			{
				var regdatenew = $('#customer_date_to').val();
				data.regdatenew = regdatenew;
			} else {
				data.regdatenew = '';
			}
			data.searchcustomerstaus = $('#all-customers').val();
			data.searchpaytype = $('#payment_type').val();
			data.searchsource = $('#sort_source').val();
			data.searchcusttype = $('#sort_cust_type').val();
			data.searchcustomer = $('#keyword-search').val();
			data.searchservice = $('#servicetypes').val();
			
		},
		"complete": function(json, type) {
			var _resp = $.parseJSON(json.responseText);
		}
	},
	'columns': [
		{ data: 'slno' },
		{ data: 'name' },
		{ data: 'mobile' },
		{ data: 'area' },
		{ data: 'address' },
		{ data: 'source' },
		{ data: 'lastjob' },
		{ data: 'addedtime' },
		{ data: 'action' },
	]
	});
	$('#customer_date').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
	});
	
	$('#customer_date_to').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
	});

	$('#customer_date').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#customer_date_to').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#all-customers').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#payment_type').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#sort_source').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#sort_cust_type').change(function(){
		customerlistDataTable.draw();
	});
	
	$('#servicetypes').change(function(){
		customerlistDataTable.draw();
	});
	
	$("#keyword-search").keyup(function(){
		customerlistDataTable.draw();
	});
}

if ($('#newListCustomer').length) {
	var newcustomerlistDataTable = $('#newListCustomer').DataTable({
	'bFilter' : false,
	'bLengthChange': false,
	'pageLength' : 10,
	'processing': true,
	'serverSide': true,
	'bSort' : false,
	'serverMethod': 'post',
	'ajax': {
		'url':_base_url+'customer/list_ajax_newcustomerlist',
		'data': function(data){
			
		},
		"complete": function(json, type) {
			var _resp = $.parseJSON(json.responseText);
		}
	},
	'columns': [
		{ data: 'slno' },
		{ data: 'name' },
		{ data: 'mobile' },
		{ data: 'area' },
		{ data: 'address' },
		{ data: 'source' },
		{ data: 'lastjob' },
		{ data: 'addedtime' },
		{ data: 'action' },
	]
	});
}