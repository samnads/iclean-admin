$(document).ready(function()
{
	if($('.customers_vh_rep_new').length)
	{
		$(".customers_vh_rep_new").select2({
			ajax: { 
			 url: _base_url+"customer/report_srch_usr_news",
			 type: "post",
			 dataType: 'json',
			 delay: 150,
			 data: function (params) {
					return {
					  searchTerm: params.term, // search term
					};
				  },
			 processResults: function (response) {return {results: response};},
			 cache: true
			},
		});
    }
	
	$(".customers_vh_rep_new").change(function()
	{
		var cust_id = $(this).val();
		var href=_base_url+'customer/view/'+cust_id;
		window.open(href,"_blank");
	});
});

$.fn.blink = function(options)
 {
        var defaults = { delay:500 };
        var options = $.extend(defaults, options);
       
        return this.each(function()
        {
            var obj = $(this);
            setInterval(function()
            {
                if($(obj).css("visibility") == "visible")
                {
                    $(obj).css('visibility','hidden');
                }
                else
                {
                    $(obj).css('visibility','visible');
                }
            }, options.delay);
        });
 }
 
$('.blink').blink();

// Loading dots
function dloader()
{
	var dots = 0;    
	$(document).ready(function()
	{
		setInterval (type, 600);
	});

	function type()
	{
		if(dots < 5)
		{
			$('.dots_loader').append('.');
			dots++;
		}
		else
		{
			$('.dots_loader').html('');
			dots = 0;
		}
	}
}

dloader();

function highlight_autocomplete(text, $node) {
	var searchText = $.trim(text).toLowerCase(), currentNode = $node.get(0).firstChild, matchIndex, newTextNode, newSpanNode;
	while ((matchIndex = currentNode.data.toLowerCase().indexOf(searchText)) >= 0) {
		newTextNode = currentNode.splitText(matchIndex);
		currentNode = newTextNode.splitText(searchText.length);
		newSpanNode = document.createElement("span");
		newSpanNode.className = "highlight";
		currentNode.parentNode.insertBefore(newSpanNode, currentNode);
		newSpanNode.appendChild(newTextNode);
	}
}

var customers_s = [];

$('#head-search-f').autocomplete({
	source: customers_s,
	select: function(event, ui) {
		$('#head-search-f').val(ui.item.name);
		window.open( _base_url + 'customer/view/' + ui.item.value, "_blank");
		return false;
	}
}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	var re = new RegExp("(" + this.term + ")", "gi") ;
	var label = item.label.replace(re, "<u>$1</u>");
        $('#ui-id-1').css('z-index', '9999');
	return $('<li></li>')
		.data('item.autocomplete', item)
		.append('<a style="font-size:13px;">' + label + '</a>')
		.appendTo(ul);
        
};

var customers_s = [];
$.post( _base_url + 'customer', { action: 'get-customers' }, function(response) {
	customers_s = $.parseJSON(response);
	$('#head-search-f').autocomplete( "option", { source: customers_s } );
});
//For mobile
var customers_m = [];

$('#head-search-m').autocomplete({
	source: customers_m,
	select: function(event, ui) {
		$('#head-search-m').val(ui.item.name);
		window.open( _base_url + 'customer/view/' + ui.item.value, "_blank");
		return false;
	}
}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	var re = new RegExp("(" + this.term + ")", "gi") ;
	var label = item.label.replace(re, "<u>$1</u>");
        $('#ui-id-2').css('z-index', '9999');
	return $('<li></li>')
		.data('item.autocomplete', item)
		.append('<a style="font-size:13px;">' + label + '</a>')
		.appendTo(ul);
        
};

var customers_m = [];
$.post( _base_url + 'customer', { action: 'get-customers' }, function(response) {
	customers_m = $.parseJSON(response);
	$('#head-search-m').autocomplete( "option", { source: customers_m } );
});
//Ends
$('#paid-amount').show(); 
$('input[name="payment_type"]').each(function(){
    $(this).bind('click', function(){
        if($(this).val() == 1)
        {
           $('#paid-amount').show(); 
           $('#pinkid').show(); 
        }
        else
        {            
            $('#paid-amount').hide();
			$('#pinkid').hide(); 
        }
    });    
});
var _booking_id;
var _this;
function get_activity(activity_status, booking_id, $this)
{
    //if (typeof optionalArg === 'undefined') { optionalArg = ''; }
        
        $("#paid-amount").val('');
        $("#ps-no").val('');
        $('.transfer-details, .save-but').hide();
        $('#payment-details, #paid-amount, #pinkid').hide();
        _booking_id = booking_id;
        _this = $this;
        //$("#paid-amount").val(optionalArg);
        if(activity_status == 0) // not started
        {
                //$('.btn-warning, .save-but, #transfer-zone-id').hide();
                $('.btn-warning, .btn-primary, .save-but, #transfer-zone-id').hide();
                $('.btn-success, .btn-info').show();
        }
        if(activity_status == 1) // on going
        {
                $('.btn-warning, .btn-info').show();
                //$('#payment-details, #paid-amount, #frm-transfer, .btn-success, .save-but, .btn-info, #pinkid').hide();
                $('#payment-details, #paid-amount, #frm-transfer, .btn-success, .btn-primary .save-but, #pinkid').hide();
        }
        if(activity_status == 6) // finished not payment
        {
                //$('.btn-warning').hide();
                $('.btn-warning, .btn-success').hide();
                $('.btn-info, #frm-transfer').hide();
                //$('.btn-success, #payment-details, #paid-amount, .save-but, #pinkid').show();
                $('.btn-primary, #payment-details, #paid-amount, .save-but, #pinkid').show();
        }
        if(activity_status == 5) // finished with payment
        {
           //$('#frm-transfer, #transfer-zone-id, .btn-warning').hide(); 
           //$('.btn-success, #payment-details, #paid-amount, .save-but, #pinkid').show();
           $('#frm-transfer, #transfer-zone-id, .btn-warning, .btn-success').hide(); 
           $('.btn-primary, #payment-details, #paid-amount, .save-but, #pinkid').show();
        }  
        if(activity_status == 3) // service not done
        {
           //$('#frm-transfer, #transfer-zone-id, .btn-warning').hide(); 
           //$('.btn-success, .btn-info').show();
           $('#frm-transfer, #transfer-zone-id, .btn-warning, .btn-success').hide(); 
           $('.btn-primary, .btn-info').show();
        }  
        $('#activity-modal').modal({backdrop : true, keyboard : true, show : true});

}
function get_activity_new(activity_status, booking_id, $this, amt, statuss,paystatus,psno)
{
    //if (typeof optionalArg === 'undefined') { optionalArg = ''; }
        
        //$("#paid-amount").val('');
        $('.transfer-details, .save-but').hide();
        $('#payment-details, #paid-amount,#pinkid').hide();
        _booking_id = booking_id;
        _this = $this;
        if(paystatus == "1"){
            $("#paymnt").prop("checked", true)
        } else if(paystatus == "0"){
            $("#no-paymnt").prop("checked", true)
        } else if(statuss == "3"){
            $("#no-service").prop("checked", true)
        }
        $("#paid-amount").val(amt);
        if(psno == "0")
        {
            var psnum = "";
        } else {
            var psnum = psno;
        }
        $("#ps-no").val(psnum);
        if(activity_status == 0) // not started
        {
                //$('.btn-warning, .save-but, #transfer-zone-id').hide();
                $('.btn-warning, .btn-primary .save-but, #transfer-zone-id').hide();
                $('.btn-success, .btn-info').show();
        }
        if(activity_status == 1) // on going
        {
                $('.btn-warning,.btn-info').show();
                $('#payment-details, #paid-amount, #frm-transfer, .btn-success, .btn-primary, .save-but, #pinkid').hide();
        }
        if(activity_status == 6) // finished not payment
        {
                $('.btn-warning').hide();
                $('.btn-info, #frm-transfer').hide();
                $('.btn-success').hide();
                $('.btn-primary, #payment-details, #paid-amount, .save-but, #pinkid').show();
        }
        if(activity_status == 5) // finished with payment
        {
           $('#frm-transfer, #transfer-zone-id, .btn-warning').hide(); 
           $('.btn-success').hide();
           $('.btn-primary, #payment-details, #paid-amount, .save-but, #pinkid').show();
        }  
        if(activity_status == 3) // service not done
        {
           $('#frm-transfer, #transfer-zone-id, .btn-warning').hide(); 
           $('.btn-success').hide();
           $('.btn-primary, .btn-info').show();
        }  
        $('#activity-modal').modal({backdrop : true, keyboard : true, show : true});

}
function get_activity_new_job(activity_status, booking_id)
{
    $("#paid-amount-job").val('');
    $("#ps-no-job").val('');
    $('.transfer-details, .save-but-job').hide();
    $('#payment-details-job, #paid-amount-job,#pinkid-job').hide();
    _booking_id = booking_id;
    if(activity_status == 0) // not started
    {
            //$('.btn-warning, .save-but, #transfer-zone-id').hide();
            $('.mm-btn-warning, .btn-primary, .save-but-job, #transfer-zone-id-job').hide();
            $('.mm-btn-success, .mm-btn-info').show();
    }
    if(activity_status == 1) // on going
    {
            $('.mm-btn-warning').show();
            $('#payment-details-job, #paid-amount-job, #frm-transfer-job, .mm-btn-success, .btn-primary, .save-but-job, .mm-btn-info, #pinkid-job').hide();
    }
    if(activity_status == 6) // finished not payment
    {
            $('.mm-btn-warning').hide();
            $('.mm-btn-info, #frm-transfer-job').hide();
            $('.mm-btn-success').hide();
            $('.btn-primary, #payment-details-job, #paid-amount-job, .save-but-job, #pinkid-job').show();
    }
    if(activity_status == 5) // finished with payment
    {
       $('#frm-transfer-job, #transfer-zone-id-job, .mm-btn-warning').hide(); 
       $('.mm-btn-success').hide();
       $('.btn-primary, #payment-details-job, #paid-amount-job, .save-but-job, #pinkid-job').show();
    }  
    if(activity_status == 3) // service not done
    {
       $('#frm-transfer-job, #transfer-zone-id-job, .mm-btn-warning').hide(); 
       $('.mm-btn-success').hide();
       $('.btn-primary, .mm-btn-info').show();
    }
    $('#activity-modal_job').modal({backdrop : true, keyboard : true, show : true});
}
function get_activity_new_job_finish(activity_status, booking_id, amt, statuss,paystatus,psno)
{
    //if (typeof optionalArg === 'undefined') { optionalArg = ''; }
        
        //$("#paid-amount").val('');
        $('.transfer-details, .save-but-job').hide();
        $('#payment-details-job, #paid-amount-job,#pinkid-job').hide();
        _booking_id = booking_id;
        if(paystatus == "1"){
            $("#paymnt-job").prop("checked", true)
        } else if(paystatus == "0"){
            $("#no-paymnt-job").prop("checked", true)
        } else if(statuss == "3"){
            $("#no-service-job").prop("checked", true)
        }
        $("#paid-amount-job").val(amt);
        if(psno == "0")
        {
            var psnum = "";
        } else {
            var psnum = psno;
        }
        $("#ps-no-job").val(psnum);
        if(activity_status == 0) // not started
        {
                //$('.btn-warning, .save-but, #transfer-zone-id').hide();
                $('.mm-btn-warning, .btn-primary .save-but-job, #transfer-zone-id-job').hide();
                $('.mm-btn-success, .mm-btn-info').show();
        }
        if(activity_status == 1) // on going
        {
                $('.mm-btn-warning').show();
                $('#payment-details-job, #paid-amount-job, #frm-transfer-job, .mm-btn-success, .btn-primary, .save-but-job, .mm-btn-info, #pinkid-job').hide();
        }
        if(activity_status == 6) // finished not payment
        {
                $('.mm-btn-warning').hide();
                $('.mm-btn-info, #frm-transfer-job').hide();
                $('.mm-btn-success').hide();
                $('.btn-primary, #payment-details-job, #paid-amoun-jobt, .save-but-job, #pinkid-job').show();
        }
        if(activity_status == 5) // finished with payment
        {
           $('#frm-transfer-job, #transfer-zone-id-job, .mm-btn-warning').hide(); 
           $('.mm-btn-success').hide();
           $('.btn-primary, #payment-details-job, #paid-amount-job, .save-but-job, #pinkid-job').show();
        }  
        if(activity_status == 3) // service not done
        {
           $('#frm-transfer-job, #transfer-zone-id-job, .mm-btn-warning').hide(); 
           $('.mm-btn-success').hide();
           $('.btn-primary, .mm-btn-info').show();
        }  
        $('#activity-modal_job').modal({backdrop : true, keyboard : true, show : true});

}
$('.btn-warning').click(function(){
    // $('#payment-details, #paid-amount, .save-but, #pinkid').show();
    $('#payment-details, .save-but').show();
    $('#frm-transfer').hide();
});
$('.mm-btn-warning').click(function(){
    $('#payment-details-job, #paid-amount-job, .save-but-job, #pinkid-job').show();
    $('#frm-transfer-job').hide();
});
$('.btn-info').click(function(){
    $('#payment-details, #paid-amount, .btn-success,.btn-primary, #pinkid').hide();
    $('#frm-transfer, #transfer-zone-id, .save-but').show();
});

$('.mm-btn-info').click(function(){
    $('#payment-details-job, #paid-amount-job, .mm-btn-success,.btn-primary, #pinkid-job').hide();
    $('#frm-transfer-job, #transfer-zone-id-job, .save-but-job').show();
});

//$('.btn-danger').click(function(){
//    $('#payment-details, #paid-amount').hide();
//    $('#frm-transfer, .save-but').hide();
//});
$('.btn-success').click(function(e){
    
    $('.btn-success, .btn-info, .save-but').hide();
    
    $.post(_page_url, {action : 'service-start', booking_id : _booking_id, service_date : $('#vehicle_date').val()}, function(response){
        var _resp = $.parseJSON(response);
        if(_resp.status == 'success')
        {
            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(1, ' + _booking_id + ', this);"><span class="on-going"></span></a>';
            $('#activity-modal').modal('hide');
			$('#invbtnshide-'+_booking_id).attr('data-id',_resp.serviceid);
            $(_this).parent('td').html(_label);
           
        }
        else
        {
             alert(_resp.message);
        }
     
    });
   
});

$('#start_job').click(function(e){
    
    $('.mm-btn-success, .mm-btn-info, .save-but-job').hide();//$('#vehicle_date').val()
    var _service_date = $('#hiddenbookingdate').val();
    
    $.post(_page_url, {action : 'service-start', booking_id : _booking_id, service_date : _service_date}, function(response){
        var _resp = $.parseJSON(response);
        if(_resp.status == 'success')
        {
            //_label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(1, ' + _booking_id + ', this);"><span class="on-going">ON GOING</span></a>';
            $('#activity-modal_job').modal('hide');
            location.reload(); 
            //$(_this).parent('td').html(_label);
            //windows.reload();
           
        }
        else
        {
             alert(_resp.message);
        }
     
    });
   
});

$('.btn-primary').click(function(e){
    
    $('.btn-success, .btn-primary, .btn-info, .save-but').hide();
    
    $.post(_page_url, {action : 'service-start', booking_id : _booking_id, service_date : $('#vehicle_date').val()}, function(response){
        var _resp = $.parseJSON(response);
        if(_resp.status == 'success')
        {
            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(1, ' + _booking_id + ', this);"><span class="on-going"></span></a>';
            $('#activity-modal').modal('hide');
            $(_this).parent('td').html(_label);
           
        }
        else
        {
             alert(_resp.message);
        }
     
    });
   
});
function add_activity(){
    $(".save-but").hide();
    if($('#frm-transfer').length > 0 && $('#transfer-zone-id').is(":visible"))
    {
		if($('#transfer-zone-id').val() <= 0)
		{
			alert('Please select driver...');
			return false;
		}
         $.post(_page_url, {action : 'transfer', booking_id : _booking_id, tablet_id : $('#transfer-zone-id').val(), service_date : $('#vehicle_date').val()}, function(response){
             var _resp = $.parseJSON(response);
             if(_resp.status == 'success')
             {
                  $('#activity-modal').modal('hide');
                  //$('#frm-transfer').submit();
                  window.location.href = _page_url;
             }
             else
             {
                 alert(_resp.message);
             }
         });
         
    }
    
    else if($('#payment-details').length > 0 )
    {
        var _payment_status = $('input[name="payment_type"]:checked').val();
        var amount = 0;
        if($('#paid-amount').val() != '' && $.isNumeric($('#paid-amount').val()) && $('#paid-amount').val() > 0)
        {
            amount = $('#paid-amount').val();
        }
        
        if($('#ps-no').val() != '' && $('#ps-no').val() > 0)
        {
            psno = $('#ps-no').val();
        }
        else
        {
            psno = "";
        }
        //alert(psno);
            
               
            $.post(_page_url, {action : 'service-stop', booking_id : _booking_id, payment : _payment_status, amount : amount, psno : psno, service_date : $('#vehicle_date').val()}, function(response){
                    var _resp = $.parseJSON(response);
                    if(_resp.status == 'success')
                    {
                        $(".save-but").show();
                        $('#activity-modal').modal('hide');
                        var _label = '';
                        var _html = '';
                        if(_resp.service_status == 2)
                        {
                            //Finished
                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(2, ' + _booking_id + ', this);"><span class="finished">FINISHED</span></a>';
                            var paymenttype=$('#invbtnshide-'+_booking_id).data('paytype')
                            if(paymenttype!='M')//showing invoice create button for 
                            {                   //non monthly payment customers only
                                $('#invbtnshide-'+_booking_id).show();
                            }
							
						}
                        else if(_resp.service_status == 3)
                        {
                            //Cancelled
                             _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(3, ' + _booking_id + ', this);"><span class="cancelled">CANCELLED</span></a>';
                        }
                        if(_resp.payment_status == 1)
                        {
                            // Payed
                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(5, ' + _booking_id + ', this);"><span class="finished"></span></a>';
							var paymenttype=$('#invbtnshide-'+_booking_id).data('paytype');
                             var inserthtml=_resp.payment_amount+'<br/><div class="verify-payment text-center" data-paymntid="'+_resp.payment_id+'"  data-mpid="" data-bukid="'+_booking_id+'" data-vamt="'+amount+'" title="Verify Payment" id="verifpay">Verify Payment</div>';
                            if(paymenttype!='M')//showing invoice create button for 
                            {                   //non monthly payment customers only
                                $('#invbtnshide-'+_booking_id).show();
                            }
                            $('.clct_amt_'+_booking_id).html(inserthtml);
						}
                        else if(_resp.payment_status == 0)
                        {
                            // Not Payed
                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(6, ' + _booking_id + ', this);"><span class="finished"></span></a>';
							var paymenttype=$('#invbtnshide-'+_booking_id).data('paytype')
                            if(paymenttype!='M')//showing invoice create button for 
                            {                   //non monthly payment customers only
                                $('#invbtnshide-'+_booking_id).show();
                            }
						}
                        if (_resp.invoice_status == 1){
                            $('#invbtnshide-' + _booking_id).hide();
                            $('.invoiced-' + _booking_id).show();
                        }
                        $(_this).parent('td').html(_label);
//                        location.reload(); 
 //                       alert(_page_url);
                        //window.location.href=_page_url;
                    }
                    else
                    {
                         alert(_resp.message);
                    }
            });
           
                
    }
    
    
};

function add_activity_job(){
    var _service_date = $('#hiddenbookingdate').val();
    if($('#frm-transfer-job').length > 0 && $('#transfer-zone-id-job').is(":visible"))//$('#vehicle_date').val()
    {
         $.post(_page_url, {action : 'transfer', booking_id : _booking_id, tablet_id : $('#transfer-zone-id-job').val(), service_date : _service_date}, function(response){
             var _resp = $.parseJSON(response);
             if(_resp.status == 'success')
             {
                  $('#activity-modal_job').modal('hide');
                  //$('#frm-transfer').submit();
                  window.location.href = _page_url;
             }
             else
             {
                 alert(_resp.message);
             }
         });
         
    }
    
    else if($('#payment-details-job').length > 0 )
    {
        var _payment_status = $('input[name="payment_type"]:checked').val();
        var amount = 0;
        if($('#paid-amount-job').val() != '' && $.isNumeric($('#paid-amount-job').val()) && $('#paid-amount-job').val() > 0)
        {
            amount = $('#paid-amount-job').val();
        }
        
        if($('#ps-no-job').val() != '' && $('#ps-no-job').val() > 0)
        {
            psno = $('#ps-no-job').val();
        }
        else
        {
            psno = "";
        }
        //alert(psno);$('#vehicle_date').val()
            
               
            $.post(_page_url, {action : 'service-stop', booking_id : _booking_id, payment : _payment_status, amount : amount, psno : psno, service_date : _service_date}, function(response){
                    var _resp = $.parseJSON(response);
                    if(_resp.status == 'success')
                    {
                        $('#activity-modal_job').modal('hide');
//                        var _label = '';
//                        var _html = '';
//                        if(_resp.service_status == 2)
//                        {
//                            //Finished
//                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(2, ' + _booking_id + ', this);"><span class="finished">FINISHED</span></a>';
//                        }
//                        else if(_resp.service_status == 3)
//                        {
//                            //Cancelled
//                             _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(3, ' + _booking_id + ', this);"><span class="cancelled">CANCELLED</span></a>';
//                        }
//                        if(_resp.payment_status == 1)
//                        {
//                            // Payed
//                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(5, ' + _booking_id + ', this);"><span class="finished">FINISHED ' + amount + '</span></a>';
//                        }
//                        else if(_resp.payment_status == 0)
//                        {
//                            // Not Payed
//                            _label = '<a href="javascript:void" style="text-decoration:none;" onclick="get_activity(6, ' + _booking_id + ', this);"><span class="finished">FINISHED NP</span></a>';
//                        }
//                        $(_this).parent('td').html(_label);
//                        location.reload(); 
 //                       alert(_page_url);
                        window.location.href=_page_url;
                    }
                    else
                    {
                         alert(_resp.message);
                    }
            });
           
                
    }
    
    
};

function assignMaid(booking_id, no_of_maids)
{
    
    $('#free-maid-list-modal').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
    $('input[name="same_zone"]').attr('onclick', 'assignMaid(' + booking_id + ',' + no_of_maids + ')');    
    var _same_zone = $('input[name="same_zone"]:checked').val();
    $.post(_page_url, {action : 'get-free-maids', booking_id : booking_id, same_zone : _same_zone}, function(response){
            var _resp = $.parseJSON(response);
            var _options = '<option value="">Select Maid</option>';
            $('#no-maids-selected').html('');
            if(no_of_maids > 1) 
            {
                    _options = '';
                    $('#free-maid-id').prop('multiple', 'multiple');
                    $('#free-maid-id').css('width','180px');
            }
            else
            {
                $('#free-maid-id').removeAttr('multiple');
            }
            if(_resp.status !== 'error')
            {
                
                $.each(_resp, function(i, obj)
                {
                    _options += '<option value="' + obj.maid_id + '">';
                    _options += obj.maid_name + '</option>';
                    
                });
                
                $('#free-maid-id').html(_options);
                $('#assign-maid').attr('data-bind', booking_id);
                $('#free-maid-id').bind('click', function(){
                    
                    if($('#free-maid-id option:selected').length > no_of_maids)
                    {
                        var j=0;
                        $('#free-maid-id option:selected').each(function(index, i){
                            ++j;
                            if(j > no_of_maids)
                            {
                                $(this).removeAttr('selected');
                            }
                        });
                        //$('#free-maid-id option[value="'+ $(this).val()+'"]').prop('disabled', true);
                    }
                    $('#no-maids-selected').html('No of maids : ' + no_of_maids + ' <br />  Selected maids : ' + $('#free-maid-id option:selected').length);
                });
            }
            else
            {
                 $('#free-maid-id').html(_options);
                 $('#assign-maid').attr('data-bind', booking_id);
                 alert(_resp.message);
            }
        });
}
function deleteBooking(booking_id)
{
    if(confirm("Are you sure want to delete the booking request?"))
    {
        $.post(_page_url, {action : 'reject-booking', booking_id : booking_id}, function(response){
            var _resp = $.parseJSON(response);
            if(_resp.status == 'success')
            {
                window.location = _page_url;
            }
            else
            {
                 alert(_resp.message);
            }
        });
    }
}

$('#assign-maid').bind("click", function(e){    
        
        var _maid_id = $.trim($('#free-maid-id').val()); 
        var _booking_id = $.trim($('#assign-maid').attr('data-bind'))
        if(_maid_id != '')
        {
            $.post(_page_url, {action : 'assign-maid', booking_id : _booking_id, maid_id : _maid_id}, function(response){
                var _resp = $.parseJSON(response);
                if(_resp.status == 'success')
                {
                    alert('Maid added successfully!');
                    window.location = _page_url;
                }
                else
                {
                     alert(_resp.message);
                }
            });
        }
        else
        {
            alert("Please select a maid");
        }
});
$('#synch-to-odoo').unbind().click(function (){
    if(confirm('Are you sure want to Synchronize activity to Odoo?'))
    {
        window.location = _base_url + 'activity/synch_to_odoo/' + $.trim($('#formatted-date').val());
    }
    else
    {
        return false;
    }
});

$('#synch-to-odoo-new').unbind().click(function (){
    if(confirm('Are you sure want to Synchronize activity to Odoo?'))
    {
        window.location = _base_url + 'activity/synch_to_odoo_common/' + $.trim($('#formatted-date').val());
    }
    else
    {
        return false;
    }
});

function Maid_booking(book_id,no_of_maids,customerID)
{
    
 $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		content : _alert_html = '<div id="alert-popup"><div class="head">Assign<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure want to continue?</div><div class="bottom"><input type="button" value="Yes" data-id="'+customerID+'" data-bookID="'+book_id+'" class="assign_yes" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="assign_no pop_close"  /></div></div>',
		topRatio : 0.2,
		
	});     
    
}

function delete_booking(book_id,customerID)
{
    $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		content : _alert_html = '<div id="alert-popup"><div class="head">Delete<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:20px 0 5px !important;">Are you sure want to continue?</div><div class="content"><span id="deleteremarks" style="color:red; display:none;">Please enter remarks.</span><textarea name="remark" placeholder="Remarks" id="remarkss"></textarea></div><div class="bottom"><input type="button" value="Yes" data-id="'+customerID+'" data-bookID="'+book_id+'" class="delete_yes" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="assign_no pop_close" /></div></div>',
		topRatio : 0.2,
		
	}); 
}
function Maid_booking_view(book_id,no_of_maids,customerID)
{
    
   var customerId=customerID;
   var BookId=book_id;
  
   window.location='../customer/view/'+customerId+'/'+BookId;
    
}
$(document).on('click', '.assign_yes', function(){
    
   $.fancybox.close(); 
   var customerId=$(this).attr('data-id');
   var BookId=$(this).attr('data-bookID');
   
   $.post(_page_url, {action : 'assign-maid-status', booking_id : BookId}, function(response){
                var _resp = $.parseJSON(response);
                if(_resp.status == 'success')
                {
                   window.location='../customer/view/'+customerId+'/'+BookId; 
                }
                
            });
  
  
    
});

$(document).on('click', '.delete_yes', function(){
   var remarks = $.trim($('#remarkss').val());
   if(remarks == "")
   {
        $('#deleteremarks').css('display','block');
   } else {
        $('#deleteremarks').css('display','none');
        $.fancybox.close(); 
        var customerId=$(this).attr('data-id');
        var BookId=$(this).attr('data-bookID');
   
        $.post(_page_url, {action : 'delete-assign-maid-status', booking_id : BookId, remarks : remarks}, function(response){
            var _resp = $.parseJSON(response);
            if(_resp.status == 'success')
            {
                $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Deleted Successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });
            }
            else if(_resp.status == 'locked')
            {
                alert('Booking Locked by another user!');
            }

        });
    }
});

$(document).on('click', '.pop_close', function(){
    
   $.fancybox.close();  
    
}); 
$(document).on('click', '.pop_close2', function(){
    
   $.fancybox.close(); 
   $('#deleteassignmaidfilter').trigger('click');
    
});
$(document).on('change', '#customer_type_bulk', function(){

    var customertype=$(this).val();
    $('#mobnumber_list').tagsinput('removeAll');
    $('.mm-loader').css('display','block');
    $.post(_base_url+"customer/customer_bulk_ajax", { customertype: customertype})
  .done(function( data ) {
    var obj = jQuery.parseJSON(data);
    $.each(obj, function (index, value) {
      $('#mobnumber_list').tagsinput('add', value);  
     });
     $('.mm-loader').css('display','none');   
  });

   


    
});

$(document).on('change', '#customer_type_bulk_email', function(){

    var customertype=$(this).val();
    $('#email_list').tagsinput('removeAll');
    $('.mm-loader').css('display','block');
    $.post(_base_url+"customer/customer_bulk_email_ajax", { customertype: customertype})
  .done(function( data ) {
    var obj = jQuery.parseJSON(data);
    $.each(obj, function (index, value) {
      $('#email_list').tagsinput('add', value);  
     });
     $('.mm-loader').css('display','none');   
  });

   //$('#mobnumber_list').tagsinput('removeAll');
   //$('#mobnumber_list').tagsinput('add', '8907449846');



    
});

$(document).on('click', '.send_bulk', function(){
    
   if(confirm("Are you sure want to send message ?"))
     {

      return true;

     } 
     else
     {
      return false;  
     }
    
}); 



if($("#mobnumber_list").length >0)
{

$(document).keypress(function (e) {

 var key = e.which;
 if(key == 13)  // the enter key code
  {

    return false;  
  }
});



}
if($("#email_list").length >0)
{

$(document).keypress(function (e) {

 var key = e.which;
 if(key == 13)  // the enter key code
  {

    return false;  
  }
});



}




 

