$(document).ready(function(){
   $('.job-customer').select2({closeOnSelect: true}); 
});
$("#b-customer-id").select2({
	ajax: { 
	 url: _base_url+"customer/report_srch_usr_news",
	 type: "post",
	 dataType: 'json',
	 delay: 150,
	 data: function (params) {
			return {
			  searchTerm: params.term, // search term
			};
		  },
	 processResults: function (response) {
		 if(response.id === "")
		 { 
		
		 } else {
			return {results: response};
		 }
	 },
	 cache: true
	},
});
$('body').on('change', '#b-customer-id', function() {
    var _customer_id = $(this).val();
    $('#job-picked-address').html('');
    get_no_of_address(_customer_id);
    //alert(_customer_id);
});

$('.end_datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
        //startDate: new Date($('#repeate-end-start').val())
});

$('body').on('change', 'input[name="repeat_end"]', function() {
    if($(this).val() == 'ondate')
    {
        $('#job-repeat-end-date').removeAttr('disabled'); 
    }
    else
    {
        $('#job-repeat-end-date').attr('disabled', 'disabled'); 
    }
});

$('body').on('change', 'input[name="repeat_end"]', function() {
    if($('#job-repeat-end-never').is(':checked'))
    {
            $('#job-repeat-end-date').val('');
    }
	
//    var _booking_id = $('#booking-id').val();
//
//    if(_booking_id != '')
//    {
//            chk_booking_chg(_booking_id);
//    }
});

function get_no_of_address(customer_id)
{
    $.post(_page_url, { action: 'get-no-of-customer-address', customer_id: customer_id }, function(response) {
        if(response == 'refresh')
        {
            $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'fade',
                    openSpeed : 100,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                            }
                    },
                    padding : 0,
                    closeBtn : false,
                    content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
            });
        }
        else
        {
            //customer details on right side
            $.post(_page_url, { action: 'get-details-customer', customer_id: customer_id }, function(response) {
                var _respval = $.parseJSON(response);
                if(_respval.payment_type == "D")
                {
                    var paytype = "Daily";
                } else if(_respval.payment_type == "W")
                {
                    var paytype = "Weekly";
                } else if(_respval.payment_type == "M")
                {
                    var paytype = "Monthly";
                } else {
                    var paytype = "";
                }
                if(_respval.customer_booktype == '0')
                {
                    var cus_booktype = "Non Regular";
                } else if(_respval.customer_booktype == '1')
                {
                    var cus_booktype = "Regular";
                } else {
                    var cus_booktype = "";
                }
                $("#job-contact").val(_respval.mobile_number_1);
                $('#jobemail').val(_respval.email_address);
            });
            var _resp = $.parseJSON(response);				
            //var _address_html = '<div class="table">';

            if(_resp.no_of_address == 1)
            {
                $.each(_resp.address, function(key, val) {
                    var _address_id = val.customer_address_id;
                    $('#job-customer-address-id').val(_address_id);

                    var _addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
                    var _address = val.customer_address;

                    $('#job-picked-address').html('<strong>' + _addzone + '</strong> - ' + _address);
                    $('#selected-address').css('display','block');
                });
            }
            else
            {

                open_address_panel(customer_id)
            }

        }
    });
}

function open_address_panel(customer_id)
{
    $('#job-customer-address-id').val('');
    $('#job-customer-address-panel').hide();
    $('#job-customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#job-customer-address-panel').slideDown();
		
        $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response) {
            if(response == 'refresh')
            {
                $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'fade',
                        openSpeed : 100,
                        helpers : {
                                overlay : {
                                        css : {
                                                'background' : 'rgba(0, 0, 0, 0.3)'
                                        },
                                        closeClick: false
                                }
                        },
                        padding : 0,
                        closeBtn : false,
                        content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                });
            }
            else
            {
                var _resp = $.parseJSON(response);				
                var _address_html = '<div class="table">';                                
                                
                $.each(_resp, function(key, val) {
                    _address_html += '<div class="row"><div class="cell1"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                });				

                _address_html += '</div>';

                $('#job-customer-address-panel .inner').html(_address_html);
            }
        });
    }
}

$('body').on('click', '#job-customer-address-panel .close', function() {
	$('#job-customer-address-panel').slideUp( function() { $('#selected-address').show(); } );
});

$('body').on('click', '#job-customer-address-panel .pick_customer_address', function() {
	var _address_id = $(this).attr('id').replace('cadddress-', '');
	$('#job-customer-address-id').val(_address_id);
	var _addzone = $('#caddzone-' + _address_id).html();
	var _address = $('#cadd-' + _address_id).html();
	$('#job-customer-address-panel').slideUp(function() {
            $('#job-picked-address').html('<strong>' + _addzone + '</strong> - ' + _address);
            $('#selected-address').css('display','block');
            
            //$('#customer-picked-address').html('<div class="address"><strong>' + _addzone + '</strong> - ' + _address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
            //$('#customer-picked-address').show();
            //$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');

            //var _booking_id = $('#booking-id').val();

//           if(_booking_id != '')
//                    {
//                            chk_booking_chg(_booking_id);
//                    }
	});
});

$('body').on('click', '.pop_close', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error1', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error2', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_success', function() {
	parent.$.fancybox.close();
});

$('body').on('change', '#b-from-time', function() {
	refresh_to_time();
	//$('#b-time-slot').text('');
});

function refresh_to_time() {
	//$("#b-to-time").select2("val", "");
	$("#b-to-time").val('');
	var _selected_index = $("#b-from-time")[0].selectedIndex;
	
	var _time_to_options = '<option></option>';
	var _i = 0;
        var _last_index;
	$('#b-from-time option').each(function(index, option) {
		if(index > _selected_index)
		{
			_time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                        _last_index = index;
			_i++;
		}
	});
        
	
	if(_i == 0)
	{
		_time_to_options += '<option value="">No Time</option>';
	}
	
	$('#b-to-time').html(_time_to_options);
}

// $('body').on('change', '#b-to-time', function() {
	// if($("#b-from-time").val() != '' && $("#b-to-time").val() != '')
	// {
        // _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
        // _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
        // if (_end_time < _start_time) {
            // _end_time = parseAMDate(_end, 1);
        // }
        // var difference = _end_time - _start_time;
        // var hours = Math.floor(difference / 36e5),
                // minutes = Math.floor(difference % 36e5 / 60000);
        // if (parseInt(hours) >= 0) {
            // if (minutes == 0) {
                // minutes = "00";
            // }
            
           //var weekdayscount =  $('#total_week_days').val();
           //var customer_pre_book_hrs =  $('#total_prebook_hrs').val();
            //$('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs)+parseInt(hours*weekdayscount));
        //}

        //$('#b-time-slot').text('(' + $("#b-from-time option:selected").text() + ' to ' + $("#b-to-time option:selected").text() + ')');
        
        
//            $.ajax({
//            type: "POST",
//            url: _base_url + 'settings/check_hourly_price',
//            data: {total_week_hours: $('#b-hrs_per_week').val()},
//            cache: false,
//            success: function (response)
//            {
//                if(response==''){
//                    response = '40';
//                }
//             if(response != null) {
//                 
//        $('#b-rate_per_hr').val(response);
//             }
//            
//            }
//
//        });
        
        
        
//        var _booking_id = $('#booking-id').val();
//
//        if (_booking_id != '')
//        {
//            chk_booking_chg(_booking_id);
//        }
	// }
	// else
	// {
		//$('#b-time-slot').text('');
	// }
// });

$('body').on('change', '#b-booking-type', function () {
    var _booking_type = $(this).val();
    if (_booking_type == 'WE' || _booking_type == 'BW')
    {
        //$('#repeat-days').css('display', 'table-row');
        $('#jobweekday').css('display', 'table-row');
        $('#jobneverweekday').css('display', 'table-row');
        //$('#repeat-ends').css('display', 'table-row');
//        $('body').on('change', '.w_day', function () {
//                $('#total_week_days').val($('.w_day:checked').length);
//                _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
//                _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
//                if (_end_time < _start_time) {
//                    _end_time = parseAMDate(_end, 1);
//                }
//                var difference = _end_time - _start_time;
//                var hours = Math.floor(difference / 36e5),
//                        minutes = Math.floor(difference % 36e5 / 60000);
//                if (parseInt(hours) >= 0) {
//                    if (minutes == 0) {
//                        minutes = "00";
//                    }
//                    var weekdayscount = $('#total_week_days').val();
//                      var customer_pre_book_hrs =  $('#total_prebook_hrs').val();
//            $('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs)+parseInt(hours*weekdayscount));
//            
//            
////                    $('#b-hrs_per_week').val(hours * weekdayscount);
//                }
//                $.ajax({
//                    type: "POST",
//                    url: _base_url + 'settings/check_hourly_price',
//                    data: {total_week_hours: $('#b-hrs_per_week').val()},
//                    cache: false,
//                    success: function (response)
//                    {
//                        if (response == '') {
//                            response = '40';
//                        }
//                        if (response != null) {
//
//                            $('#b-rate_per_hr').val(response);
//                        }
//
//                    }
//
//                });
//            });
        } else
        {
            //$('#total_week_days').val('1');

            //$('#repeat-days').hide();
            $('#jobweekday').hide();
            $('#jobneverweekday').hide();
            //$('#repeat-ends').hide();
        }
//        _start_time = parseAMDate($("#b-from-time option:selected").text().toUpperCase());
//        _end_time = parseAMDate($("#b-to-time option:selected").text().toUpperCase());
//        if (_end_time < _start_time) {
//            _end_time = parseAMDate(_end, 1);
//        }
//        var difference = _end_time - _start_time;
//        var hours = Math.floor(difference / 36e5),
//                minutes = Math.floor(difference % 36e5 / 60000);
//        if (parseInt(hours) >= 0) {
//            if (minutes == 0) {
//                minutes = "00";
//            }
//
//            var weekdayscount = $('#total_week_days').val();
//            
//              var customer_pre_book_hrs =  $('#total_prebook_hrs').val();
//            $('#b-hrs_per_week').val(parseInt(customer_pre_book_hrs)+parseInt(hours*weekdayscount));
//            
//        }
//        $.ajax({
//            type: "POST",
//            url: _base_url + 'settings/check_hourly_price',
//            data: {total_week_hours: $('#b-hrs_per_week').val()},
//            cache: false,
//            success: function (response)
//            {
//                if (response == '') {
//                    response = '40';
//                }
//                if (response != null) {
//
//                    $('#b-rate_per_hr').val(response);
//                }
//
//            }
//
//        });
    });
    
$('body').on('click', '#save-booking', function() {
    //var _service_date = $('#service-date').val();
    var _maid_id = $('#jobmaidval').val();
    var _customer_id = $.trim($('#b-customer-id').val());
    var _customer_address_id = $.trim($('#job-customer-address-id').val());
    var _service_type_id = $.trim($('#b-service-type-id').val());
    var _from_time = $.trim($('#b-from-time').val());
    var _to_time = $.trim($('#b-to-time').val());
    var _lock_booking = $('input[name=lock_booking]:checked').val();
    var _booking_type = $.trim($('#b-booking-type').val());
    //var _hrly_amount = $.trim($('#b-rate_per_hr').val());
    //var _pending_amount = $.trim($('#b-pending_amount').val());
    //var _discount = $.trim($('#b-discount').val());
    //var _note = $.trim($('#booking-note').val());
    var _tot_amount = $.trim($('#n_totamt').val());
    var _cleaning_material = $('input[name=cleaning_materials]:checked').val();
    var _email_notifications = $('#job-notification-email-booking').is(':checked') ? 'Y' : 'N';
    var _sms_notifications = $('#job-notification-sms-booking').is(':checked') ? 'Y' : 'N';
    $('#job-b-error').text('');
    
    if($.isNumeric(_customer_id) == false)
    {
        $('#job-b-error').text('Select customer');
        return false;
    }
    
    if($.isNumeric(_customer_address_id) == false)
    {
        $('#job-b-error').text('Pick customer address');
        open_address_panel(_customer_id);
        return false;
    }
    
    if($.isNumeric(_service_type_id) == false)
    {
        $('#job-b-error').text('Select service type');
        return false;
    }

    if(_from_time == '' || _to_time == '')
    {
        $('#job-b-error').text('Select booking time');
        return false;
    }

    if(_booking_type == '')
    {
        $('#job-b-error').text('Select repeat type');
        return false;
    }
    if(_tot_amount == '')
    {
        $('#job-b-error').text('Enter total amount');
        return false;
    }
    
    if(_maid_id == '')
    {
        $('#job-b-error').text('Select Maid');
        return false;
    }
    
    var _repeat_days = [];
    var _repeat_end = '';
    var _repeat_end_date = '';
    
    if(_booking_type == 'WE' || _booking_type == 'BW')
    {	
        _repeat_days = $('input[id^="repeat-on-"]:checked').map(function() {
            return this.value;
        }).get();

        if(_repeat_days.length == 0)
        {
            $('#job-b-error').text('Select repeat days');
            return false;
        }

        _repeat_end = $('input[name="repeat_end"]:checked').val();
        if(_repeat_end == 'ondate')
        {
            _repeat_end_date = $('#job-repeat-end-date').val();
            if(_repeat_end_date == '')
            {
                $('#job-b-error').text('Enter an end date');
                return false;
            }
        }
    }
    $('.mm-loader').css('display','block');
    var maids = _maid_id.split(",");
    $('.mm-loader').css('display','block');
    for (i=0;i<maids.length;i++){
        //alert(maids[i]);
        $('.mm-loader').css('display','block');
        $.post( _page_url, { action: 'book-maid', customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: maids[i], service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, cleaning_material : _cleaning_material, email_notifications : _email_notifications, sms_notifications : _sms_notifications, total_amount : _tot_amount}, function(response) {//, sms_val : sms_val, sms_send_date : _sms_send_date,price_per_amount : _hrly_amount}, function(response) {
            //alert(response);
            var _alert_html = '';
            
            if(response == 'refresh')
            {
                $('.mm-loader').css('display','block');
                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close_error"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error"  /></div></div>';
                //_refresh_page = true;
                //$('.mm-loader').css('display','none');
                $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            closeClick  : false,
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
            }
            else
            {
                
                var _resp = $.parseJSON(response);
                if(typeof _resp.status && _resp.status == 'success')
                {
                    $('.mm-loader').css('display','block');
                        //$('#saving-booking').val('Done');
                        _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close_success"></span></div><div class="content padd20">Booking has been done successfully for maid '+_resp.maid_name+'.</div><div class="bottom"><input type="button" value="OK" class="pop_close_success" /></div></div>';
                        //_refresh_page = false
                        //$('.mm-loader').css('display','none');
                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            closeClick  : false,
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                }
                else if(typeof _resp.status && _resp.status == 'error')
                {
                    $('.mm-loader').css('display','block');
                        //$('#job-b-error').text(_resp.message);
                        _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close_error1"></span></div><div class="content padd20">'+_resp.message+'</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error1"  /></div></div>';
                        //$('#saving-booking').attr('id', 'save-booking');
                        //$('#save-booking').val('Save');
                        //$('.mm-loader').css('display','none');
                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            closeClick  : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                }
                else
                {
                    $('.mm-loader').css('display','block');
                        _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close_error2"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error2"  /></div></div>';
                        //_refresh_page = true
                        //$('.mm-loader').css('display','none');
                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            closeClick  : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                }
            }
            

//            if(_alert_html  != '')
//            {
//                    $.fancybox.open({
//                            autoCenter : true,
//                            fitToView : false,
//                            scrolling : false,
//                            openEffect : 'fade',
//                            openSpeed : 100,
//                            helpers : {
//                                    overlay : {
//                                            css : {
//                                                    'background' : 'rgba(0, 0, 0, 0.3)'
//                                            },
//                                            closeClick: false
//                                    }
//                            },
//                            padding : 0,
//                            closeBtn : false,
//                            content: _alert_html
//                    });
//                    
//            }
            $('.mm-loader').css('display','none');
            //$('#job_add_form')[0].reset();
            //location.reload(); 
        });
        $("#job_add_form select").select2("val", "");
       $('#job_add_form')[0].reset(); 
       $('#selected-address').hide();
    }
     
    
//    $.post( _page_url, { action: 'check-book-maid', customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, cleaning_material : _cleaning_material, email_notifications : _email_notifications, sms_notifications : _sms_notifications}, function(response) {//, sms_val : sms_val, sms_send_date : _sms_send_date,price_per_amount : _hrly_amount}, function(response) {
//        alert(response);
//    });
    
    
    
    
    
    
    
//    $.post( _page_url, { action: 'book-maid', customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, cleaning_material : _cleaning_material, email_notifications : _email_notifications, sms_notifications : _sms_notifications}, function(response) {//, sms_val : sms_val, sms_send_date : _sms_send_date,price_per_amount : _hrly_amount}, function(response) {
//        //alert(response);
//        var _alert_html = '';
//
//        if(response == 'refresh')
//        {
//            _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
//            //_refresh_page = true;
//        }
//        else
//        {
//            var _resp = $.parseJSON(response);
//            if(typeof _resp.status && _resp.status == 'success')
//            {	
//                    //$('#saving-booking').val('Done');
//                    _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
//                    //_refresh_page = false
//            }
//            else if(typeof _resp.status && _resp.status == 'error')
//            {
//                    $('#job-b-error').text(_resp.message);
//                    //$('#saving-booking').attr('id', 'save-booking');
//                    //$('#save-booking').val('Save');
//            }
//            else
//            {
//                    _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
//                    //_refresh_page = true
//            }
//        }
//		
//        if(_alert_html  != '')
//        {
//                $.fancybox.open({
//                        autoCenter : true,
//                        fitToView : false,
//                        scrolling : false,
//                        openEffect : 'fade',
//                        openSpeed : 100,
//                        helpers : {
//                                overlay : {
//                                        css : {
//                                                'background' : 'rgba(0, 0, 0, 0.3)'
//                                        },
//                                        closeClick: false
//                                }
//                        },
//                        padding : 0,
//                        closeBtn : false,
//                        content: _alert_html
//                });
//        }
//    });
});
