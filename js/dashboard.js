$(function() {

    //var start = moment().subtract(29, 'days');
    var start = moment();
    var end = moment();

    function cb(start, end) {
        //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //console.log(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        var startdate=start.format('YYYY-MM-DD');
        var enddate=end.format('YYYY-MM-DD');
        var _base_url=$('#burl').val();
        var reven_by_st_html='';
        $('#dash_revenue,#dash_crew_utilisation,#dash_new_cust,#dash_rating,#dash_rev_by_serv').html('<i class="fa fa-refresh fa-spin"></i>');
        $.ajax({
               type : "POST",
               url : _base_url + 'dashboard/get_dashboard_details' ,
               data : {start_date : startdate,end_date : enddate},
               dataType: "json",
               cache : false,
               success : function(response)
               {
                   //console.log('revenue='+response.total_revenue);
                   $('#dash_revenue').html('BHD '+response.total_revenue.toFixed(2));
                   $('#dash_crew_utilisation').html(response.total_hours+' hrs');
                   $('#dash_new_cust').html(response.new_customers);
                   $('#dash_rating').html(response.rating_details.average_rating);
                   $.each(response.rev_by_st, function(i, item) {
                        var rvnu=item.revenue;
                        if(rvnu > 0)
                        {
                          reven_by_st_html+='<div  class="dash-rght-box">'+item.name+' - BHD '+rvnu.toFixed(2)+'</div>';
                        }
                    });
                   $('#dash_rev_by_serv').html(reven_by_st_html);
               },
               error: function(response)
               {
                   $('#dash_revenue,#dash_crew_utilisation,#dash_new_cust,#dash_rating').html('');
               }
            });
    }

    $('#dashboard_datefilter').daterangepicker({
        startDate: start,
        endDate: end,
        locale: {
            format: 'DD/MM/YYYY'
        },
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});