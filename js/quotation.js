$(document).ready(function(){
   $('.job-customer').select2({closeOnSelect: true,allowClear: true}); 
});
$("#b-customer-id").select2({
	ajax: { 
	 url: _base_url+"customer/report_srch_usr_news",
	 type: "post",
	 dataType: 'json',
	 delay: 150,
	 data: function (params) {
			return {
			  searchTerm: params.term, // search term
			};
		  },
	 processResults: function (response) {
		 if(response.id === "")
		 { 
		
		 } else {
			return {results: response};
		 }
	 },
	 cache: true
	},allowClear: true
});
$('body').on('change', '#b-customer-id', function() {
    var _customer_id = $(this).val();
    $('#job-picked-address').html('');
    get_no_of_address(_customer_id);
});

$('.end_datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
});

$('body').on('change', 'input[name="repeat_end"]', function() {
    if($(this).val() == 'ondate')
    {
        $('#job-repeat-end-date').removeAttr('disabled'); 
    }
    else
    {
        $('#job-repeat-end-date').attr('disabled', 'disabled'); 
    }
});

$('body').on('change', 'input[name="repeat_end"]', function() {
    if($('#job-repeat-end-never').is(':checked'))
    {
            $('#job-repeat-end-date').val('');
    }
	

});

function get_no_of_address(customer_id)
{
    $.post(_page_url, { action: 'get-no-of-customer-address', customer_id: customer_id }, function(response) {
        if(response == 'refresh')
        {
            $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'fade',
                    openSpeed : 100,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                            }
                    },
                    padding : 0,
                    closeBtn : false,
                    content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
            });
        }
        else
        {
            
            $.post(_page_url, { action: 'get-details-customer', customer_id: customer_id }, function(response) {
                var _respval = $.parseJSON(response);
                if(_respval.payment_type == "D")
                {
                    var paytype = "Daily";
                } else if(_respval.payment_type == "W")
                {
                    var paytype = "Weekly";
                } else if(_respval.payment_type == "M")
                {
                    var paytype = "Monthly";
                } else {
                    var paytype = "";
                }
                if(_respval.customer_booktype == '0')
                {
                    var cus_booktype = "Non Regular";
                } else if(_respval.customer_booktype == '1')
                {
                    var cus_booktype = "Regular";
                } else {
                    var cus_booktype = "";
                }
                $("#job-contact").val(_respval.mobile_number_1);
                $('#jobemail').val(_respval.email_address);
            });
            var _resp = $.parseJSON(response);

            if(_resp.no_of_address == 1)
            {
                $.each(_resp.address, function(key, val) {
                    var _address_id = val.customer_address_id;
                    $('#job-customer-address-id').val(_address_id);

                    var _addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
                    var _address = val.customer_address;

                    $('#job-picked-address').html('<strong>' + _addzone + '</strong> - ' + _address);
                    $('#selected-address').css('display','block');
                });
            }
            else
            {

                open_address_panel(customer_id)
            }

        }
    });
}

function open_address_panel(customer_id)
{
    $('#job-customer-address-id').val('');
    $('#job-customer-address-panel').hide();
    $('#job-customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#job-customer-address-panel').slideDown();
		
        $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response) {
            if(response == 'refresh')
            {
                $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'fade',
                        openSpeed : 100,
                        helpers : {
                                overlay : {
                                        css : {
                                                'background' : 'rgba(0, 0, 0, 0.3)'
                                        },
                                        closeClick: false
                                }
                        },
                        padding : 0,
                        closeBtn : false,
                        content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                });
            }
            else
            {
                var _resp = $.parseJSON(response);				
                var _address_html = '<div class="table">';                                
                                
                $.each(_resp, function(key, val) {
                    _address_html += '<div class="row"><div class="cell1"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                });				

                _address_html += '</div>';

                $('#job-customer-address-panel .inner').html(_address_html);
            }
        });
    }
}

$('body').on('click', '#job-customer-address-panel .close', function() {
	$('#job-customer-address-panel').slideUp( function() { $('#selected-address').show(); } );
});

$('body').on('click', '#job-customer-address-panel .pick_customer_address', function() {
	var _address_id = $(this).attr('id').replace('cadddress-', '');
	$('#job-customer-address-id').val(_address_id);
	var _addzone = $('#caddzone-' + _address_id).html();
	var _address = $('#cadd-' + _address_id).html();
	$('#job-customer-address-panel').slideUp(function() {
            $('#job-picked-address').html('<strong>' + _addzone + '</strong> - ' + _address);
            $('#selected-address').css('display','block');
            
            
	});
});

$('body').on('click', '.pop_close', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error1', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_error2', function() {
	parent.$.fancybox.close();
});

$('body').on('click', '.pop_close_success', function() {
	parent.$.fancybox.close();
});

$('body').on('change', '#b-from-time', function() {
	refresh_to_time();
});

function refresh_to_time() {
	$("#b-to-time").val('');
	var _selected_index = $("#b-from-time")[0].selectedIndex;
	
	var _time_to_options = '<option></option>';
	var _i = 0;
        var _last_index;
	$('#b-from-time option').each(function(index, option) {
		if(index > _selected_index)
		{
			_time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                        _last_index = index;
			_i++;
		}
	});
        
	
	if(_i == 0)
	{
		_time_to_options += '<option value="">No Time</option>';
	}
	
	$('#b-to-time').html(_time_to_options);
}


$('body').on('change', '#b-booking-type', function () {
    var _booking_type = $(this).val();
    if (_booking_type == 'WE' || _booking_type == 'BW')
    {
        
        $('#jobweekday').css('display', 'table-row');
        $('#jobneverweekday').css('display', 'table-row');
        
        } else
        {            
            $('#jobweekday').hide();
            $('#jobneverweekday').hide();
        }
    });
    
$('body').on('click', '#save-booking', function() {
    
    
    var _customer_id = $.trim($('#b-customer-id').val());
    var _customer_address_id = $.trim($('#job-customer-address-id').val());
    //var _service_type_id = $.trim($('#b-service-type-id').val());
    var _from_time = $.trim($('#b-from-time').val());
    var _to_time = $.trim($('#b-to-time').val());
    var _quo_date = $.trim($('#quotation_time').val());

    var _lock_booking = $('input[name=lock_booking]:checked').val();
    var _booking_type = $.trim($('#b-booking-type').val());
    var _tot_amount = $.trim($('#n_totamt').val());
    var _email = $.trim($('#jobemail').val());
    var _phone = $.trim($('#job-contact').val());
    var _cleaning_material = $('input[name=cleaning_materials]:checked').val();
    var _sercount=$.trim($('#hid_serv_count').val());
    var _service_data = [];

    $('#job-b-error').text('');
    
    if($.isNumeric(_customer_id) == false)
    {
        $('#job-b-error').text('Select customer');
        return false;
    }
    
    if($.isNumeric(_customer_address_id) == false)
    {
        $('#job-b-error').text('Pick customer address');
        open_address_panel(_customer_id);
        return false;
    }
    
    // if($.isNumeric(_service_type_id) == false)
    // {
    //     $('#job-b-error').text('Select service type');
    //     return false;
    // }

    // if(_from_time == '' || _to_time == '')
    // {
    //     $('#job-b-error').text('Select booking time');
    //     return false;
    // }

    if(_quo_date == '')
    {
        $('#job-b-error').text('Select quotation Date');
        return false;
    }
    else
    {
         _quo_date = _quo_date.split("/").reverse().join("-");
    }

    // if(_tot_amount == '')
    // {
    //     $('#job-b-error').text('Enter total amount');
    //     return false;
    // }
    

    // if ($.isNumeric(_tot_amount) == false)
    // {
    //     $('#job-b-error').text('Check total amount entered');
    //     return false;
    // }

    for(var i=1;i<=_sercount;i++)
    {
        $('#serv_table_msg').text('');
        var line_serv_id=$('#b-service-type-'+i).val();
        var line_description=$('#description_'+i).val();
        var line_serv_rate=$('#service_rate_'+i).val();
        var line_serv_qty=$('#service_qty_'+i).val();
        var line_serv_price=$('#service_price_'+i).val();

        if($.isNumeric(line_serv_id) && $.isNumeric(line_serv_rate) && $.isNumeric(line_serv_qty) && $.isNumeric(line_serv_price))
        {
            var serarray={serv_id:line_serv_id,serv_description:line_description,serv_rate:line_serv_rate,serv_qty:line_serv_qty,serv_price:line_serv_price};
            _service_data.push(serarray);
            // _service_data[]=serarray;
            // _service_data[i].serv_id=line_serv_id;
            // _service_data[i].serv_rate=line_serv_rate;
            // _service_data[i].serv_qty=line_serv_qty;
            // _service_data[i].serv_price=line_serv_price;
        }
        else if($.isNumeric(line_serv_id) || $.isNumeric(line_serv_rate) || $.isNumeric(line_serv_qty) || $.isNumeric(line_serv_price))
        {
            $('#job-b-error').text('Enter all fields for service row '+i);
            return false;
        }
        else if(_sercount==1)
        {
            $('#job-b-error').text('Enter service details ');
            return false;
        }
        else if(i>1 && _service_data[0] == undefined)
        {
            $('#job-b-error').text('Enter service details ');
            return false;
        }

    }
    var _repeat_days = [];
    var _repeat_end = '';
    var _repeat_end_date = '';
    
    $('.mm-loader').css('display','block');
    //var maids = _maid_id.split(",");
    //$('.mm-loader').css('display','block');
    //console.log('start add quot');
        $.post( _page_url, { action: 'add-quotation', customer_id: _customer_id, customer_address_id: _customer_address_id, service_data: _service_data, time_from: _from_time, time_to: _to_time, quo_date:_quo_date,  cleaning_material : _cleaning_material, /*total_amount : _tot_amount,*/ mail : _email, phone : _phone}, function(response) {            
            var _alert_html = '';
            
            if(response == 'refresh')
            {
                $('.mm-loader').css('display','block');
                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close_error"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error"  /></div></div>';
                
                $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            closeClick  : false,
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
            }
            else
            {
                
                var _resp = $.parseJSON(response);
                if(typeof _resp.status && _resp.status == 'success')
                {
                    $('.mm-loader').css('display','block');
                        
                        _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close_success"></span></div><div class="content padd20">Quotation has been added successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close_success" /></div></div>';
                        

                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            closeClick  : false,
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                    $('#st_body select').val('').trigger("change");
                    $('#b-from-time,#b-to-time').val('').trigger("change");
                }
                else if(typeof _resp.status && _resp.status == 'error')
                {
                    $('.mm-loader').css('display','block');
                        
                        _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close_error1"></span></div><div class="content padd20">'+_resp.message+'</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error1"  /></div></div>';
                        
                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            closeClick  : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                }
                else
                {
                    $('.mm-loader').css('display','block');
                        _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close_error2"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close_error2"  /></div></div>';
                        
                        $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            closeClick  : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content: _alert_html
                    });
                }
            }
            
            console.log('end add quot');        

            $('.mm-loader').css('display','none');
            
        });
        $("#job_add_form select").select2("val", "");
       $('#job_add_form')[0].reset(); 
       $('#selected-address').hide();
    
  
});




var sercount=1;
var options_html=$("#hid_serv_optns").val();


$("#addser").click(function(){
    sercount=sercount+1;
     
    var row_html='<tr><td style="line-height: 18px; width: 20px">'+sercount+'</td><td style="line-height: 18px"><select name="service_type_'+sercount+'" id="b-service-type-'+sercount+'" data-placeholder="Select service type" class="job-customer" style="width:362px;">'+options_html+'</select></td><td style="line-height: 18px"><textarea name="description_'+sercount+'" id="description_'+sercount+'" ></textarea></td><td style="line-height: 18px"><input type="number" name="service_rate_'+sercount+'" id="service_rate_'+sercount+'"></td><td style="line-height: 18px"><input type="number" name="service_qty_'+sercount+'" id="service_qty_'+sercount+'"></td><td style="line-height: 18px"><input type="number" name="service_price_'+sercount+'" id="service_price_'+sercount+'"></td></tr> ';

    $("#st_body").append(row_html);
    $("#hid_serv_count").val(sercount);

    $('#b-service-type-'+sercount+'').select2({closeOnSelect: true,allowClear: true}); 
});

var ser_count = 1;
function addServiceRow(){
    ser_count = ser_count + 1;
    var row_html = `<tr id="row-` + ser_count +`">
    <td style="line-height: 18px; width: 20px">` + ser_count + `</td>
    <td style="line-height: 18px">
        <select name="service_type[]" id="b-service-type-` + sercount + `" data-placeholder="Select service type">` + options_html + `</select>
    </td>
    <td style="line-height: 18px"><textarea name="description[]" id="description_` + sercount + `" rows="1"></textarea></td>
    <td style="line-height: 18px"><input type="number" name="service_rate[]" id="service_rate_` + sercount + `" oninput="changeRate(this)"></td>
    <td style="line-height: 18px"><input type="number" name="service_qty[]" id="service_qty_`+ sercount + `" oninput="changeQty(this)"></td>
    <td style="line-height: 18px"><input type="number" name="service_vat[]" id="service_vat_`+ sercount + `" readonly></td>
    <td style="line-height: 18px"><input type="number" name="service_total[]" id="service_total_` + sercount + `" readonly></td>
    <td style="line-height: 18px;text-align:center;">
        <button type="button" class="text-danger" onclick="removeServiceRow(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </td>
    </tr>`;
    $("#service_rows").append(row_html);
}
function removeServiceRow(self){
    $(self).closest('tr').remove();
}



$(document).ready(function () {
    $("#quotation_add_form").validate({
        ignore: [],
        rules: {
            customer_id: {
                required: true
            },
            quotation_time: {
                required: true
            },
            'service_type[]': {
                required: true
            },
            'description[]': {
                required: true
            },
            'service_rate[]': {
                required: true
            },
            'service_qty[]': {
                required: true
            },
            'service_vat[]': {
                required: true
            },
            'service_price[]': {
                required: true
            },
        },
        errorPlacement: function (err, ele) {
            err.insertAfter(ele);
        },
        submitHandler: function (form, element) {
            $('.mm-loader').css('display', 'block');
            fm = new FormData(form);
            fm.append('action','add-quotation');
            $.ajax({
                url: _page_url,
                type: 'post',
                data: fm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        $('.mm-loader').css('display', 'none');
                        _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close_success"></span></div><div class="content padd20">Quotation has been added successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close_success" /></div></div>';
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'fade',
                            closeClick: false,
                            openSpeed: 100,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                                }
                            },
                            padding: 0,
                            closeBtn: false,
                            content: _alert_html
                        });
                        $('#quotation_add_form')[0].reset(); 
                    } else {
                        $('.mm-loader').css('display', 'none');
                    }
                },
                error: function (data) {
                    $('.mm-loader').css('display', 'none');
                }
            })
        }
    })
});

function changeRate(self){
    var service_rate = self.value;
    var quanity = $(self).closest('tr').find("input[name='service_qty[]']").val() || 0;
    var service_without_vat = service_rate * quanity;
    var vat_charge = (10 / 100) * service_without_vat;
    $(self).closest('tr').find("input[name='service_vat[]']").val(vat_charge);
    var row_total = service_without_vat + vat_charge;
    $(self).closest('tr').find("input[name='service_total[]']").val(row_total)
}
function changeQty(self) {
    var quanity = self.value;
    var service_rate = $(self).closest('tr').find("input[name='service_rate[]']").val() || 0;
    var service_without_vat = service_rate * quanity;
    var vat_charge = (10 / 100) * service_without_vat;
    $(self).closest('tr').find("input[name='service_vat[]']").val(vat_charge);
    var row_total = service_without_vat + vat_charge;
    $(self).closest('tr').find("input[name='service_total[]']").val(row_total)
}