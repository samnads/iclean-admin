
$(function () {
    var btnUpload = $('#me');
    var mestatus = $('#mestatus');
    var files = $('#files');
    var call_method = 'customer/bulkmailbannerimgupload';
    var img_fold = 'blk_mail_img';
    var fileObj = $('input[type="file"]');
    var up_archive = new AjaxUpload(btnUpload, {
        action: _base_url + call_method,
        name: 'uploadfile',
        onSubmit: function (file, ext) {
            if (!(ext && /^(jpg)$/.test(ext))) {
                alert('Only JPG files are allowed');
                mestatus.text('Only PNG files are allowed');
                return false;
            }

        },
        onChange: function (file, ext) {
            if (up_archive._input.files[0].size >= 1048576) { //1MB
                alert('Selected file is bigger than 1MB.');
                return false;
            }
        },
        onComplete: function (file, response) {
            //On completion clear the status
            mestatus.text('');
            //On completion clear the status
            $('#me span').html('<img src="' + _base_url + 'images/ajax-loader.gif" height="16" width="16">');
            //Add uploaded file to list
            if (response == "error") {
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').text(file).addClass('error');
            } else {
                $('#me span').html('');
                $('#img_name_resp').val(response);
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').html('<img style="width:300px;" src="' + _base_url + '' + img_fold + '/' + response + '"/><br />').addClass('success');
                $('#bnr_dsply').html('');
            }
        }
    });

});