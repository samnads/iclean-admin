$(document).on('change', '#offertype', function () {
	var offer_val = $("#offertype").val();
	if(offer_val == 'F')
	{
		$("#discount_price_new").show();
		$("#discount_percentage").show();
		var disctype=$("#discounttype").val();
		if(disctype=='0')
		{
			$("#discpric_txt").html('Percentage');
		}
		else
		{
			$("#discpric_txt").html('Price');
		}
	} else {
		$("#discount_percentage").hide();
		$("#discount_price_new").show();
		$("#discpric_txt").html('Price');
	}
});
$(document).on('change', '#discounttype', function () {
  var disctype=$(this).val();
  if(disctype=='0')
    {
      $("#discpric_txt").html('Percentage');
    }
    else
    {
      $("#discpric_txt").html('Price');
    }
});
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$( "#expirydate" ).datepicker({
	format: 'yyyy-mm-dd',
	autoclose: true,
	startDate: today
});
$("#contactdate").datepicker({
	format: 'dd/mm/yyyy',
	autoclose: true
	//startDate: today
});
$("#followupdate").datepicker({
	format: 'dd/mm/yyyy',
	autoclose: true
	//startDate: today
});
$("#nextfollowupdate").datepicker({
	format: 'dd/mm/yyyy',
	autoclose: true
	//startDate: today
});
$('#select-all').click(function(){
   if($(this).is(":checked")) 
   {
       $('input[name="permissions[]"]').prop('checked', true);
   }
   else
   {
       $('input[name="permissions[]"]').removeAttr('checked');
   }
});
$('input[name="permissions[]"]').click(function(){
   
   var _class = $(this).attr('class');
   var _sub_class = _class;
   var _parent_id = _class.replace('module',''); 
   var _sub_id = _sub_class.replace('sub',''); 
   
   if(_parent_id > 0)
   {
       if($(this).is(":checked"))
       {
            $('.child-' + _parent_id).prop('checked', true);
       }
       else
       {
           $('.child-' + _parent_id).removeAttr('checked');          
       }
   }
   
   if(_sub_id > 0)
   {
       var _checked = 0;
       
       $('.child-' + _sub_id).each(function(){
           if(!$(this).is(":checked"))
           {
               _checked = 1;
           }
       });
       
       if(_checked == 0)
       {
           $('.parent-' + _sub_id).prop('checked', true);
       }
       else
       {
           $('.parent-' + _sub_id).removeAttr('checked');           
       }
   }
   if($('input[name="permissions[]"]:checked').length == $('input[name="permissions[]"]').length)
   {
       $('#select-all').prop('checked', true);
   }
   else
   {
       $('#select-all').removeAttr('checked');
   }
});
function checkChildModules($this)
{   
        var parent = $($this).attr('class').replace('child-','');
        var flag = 0;
       $.each( $('.child-' + parent), function( key, element ) {
           
           if(!$(element).is(":checked"))
           {
               flag = 1;              
           }
           
       });
      
        if (flag == 0){ 
            $('#parent-' + parent).prop('checked', true);
        } else {
            $('#parent-' + parent).prop('checked', false);
        }
   
}
$(function(){
    $('.parent').click(function(){
        var sel = $(this).val();
        
            if ($(this).is(':checked')) {                
                $('.child-' + sel).prop('checked', true);
            } else {
                
                $('.child-' + sel).prop('checked', false);
            }
        
    });
   
    
});

function delete_active_user(userid)
{
    if (confirm('Are you sure you want to delete this user?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "users/remove_user",
            data: {user_id: userid},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'users');
            }
        });
    }
}

function delete_active_coupon(couponid)
{
	if (confirm('Are you sure you want to deactivate this coupon?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_coupon",
            data: {coupon_id: couponid},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'coupons');
            }
        });
    }
}