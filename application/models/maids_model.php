<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Maids_model Class 
  * 
  * @package	HM
  * @author     Geethu
  * @since	Version 1.0
  */
class Maids_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
        /** 
	 * Get maid attandence by date
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, str
	 * @return	array
	 */
	function get_maid_attandence_by_date($maid_id, $date)
	{
		$this->db->select('attandence_id, maid_id, zone_id, tablet_id, date, maid_in_time, maid_out_time, attandence_status')
				->from('maid_attandence')
				->where('maid_id', $maid_id)
				->where('date', $date)
				->order_by('maid_in_time', 'desc')
				->limit(1);
		
		$get_maid_attandence_by_date_qry = $this->db->get();
//	echo $this->db->last_query();	
		return $get_maid_attandence_by_date_qry->row();
	}
        /** 
	 * Get maids
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	//function get_maids($active_only = TRUE,$team_id = NULL)
        function get_maids()
	{
		//$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status,t.team_name')
		$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
				->from('maids m')
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
                                //->join('teams t','m.team_id = t.team_id','left')
				->order_by('m.maid_name','asc');//maid_added_datetime
				//->order_by('m.maid_name,m.team_id');//maid_added_datetime
                $this->db->where('maid_status', 1);
		
		
//                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}
                
		
		$get_maids_qry = $this->db->get();
		//echo $this->db->last_query();exit();
		return $get_maids_qry->result();
	}
    function get_all_maidss($status='')
	{
		//$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status,t.team_name')
		$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
				->from('maids m')
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
                                //->join('teams t','m.team_id = t.team_id','left')
				->order_by('m.maid_name','asc');//maid_added_datetime
				//->order_by('m.maid_name,m.team_id');//maid_added_datetime
                if($status!='')
                $this->db->where('maid_status', $status);
		
		
//                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}
                
		
		$get_maids_qry = $this->db->get();
		//echo $this->db->last_query();exit();
		return $get_maids_qry->result();
	}
        
        function get_total_maids_counts()
	{
		$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
				->from('maids m')
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
				->order_by('maid_name');//maid_added_datetime
                $this->db->where('maid_status', 1);
		
		$get_maids_qry = $this->db->get();
		//echo $this->db->query();exit();
		return $get_maids_qry->num_rows();
	}
        
        function get_maid_services_by_maid_id($maid_id)
	{
		$this->db->select('s.odoo_service_id')
				->from('service_types s')
				->join('maid_service_type_joins m', 's.service_type_id = m.service_type_id')
				->where('m.maid_id', $maid_id);//maid_added_datetime			
		
		$get_maids_qry = $this->db->get();
		//echo $this->db->query();exit();
		return $get_maids_qry->result();
	}
        /** 
	 * Add maids
	 * 
	 * @author	Betsy
	 * @acces	public 
	 * @param	
	 * @return	array
	 */
        function add_maids($data)
        {
            $this->db->set($data);
            $this->db->insert('maids');
            $result = $this->db->insert_id();
            return $result;
        }
        /** 
	 * Update Attachments
	 * 
	 * @author	Betsy
	 * @acces	public 
	 * @param	
	 * @return	
	 */
        function update_attachments($datas, $id)
        {
            $this->db->where('maid_id',$id);
            $this->db->update('maids',$datas);
        }
        function update_maid_services($datas, $id)
        {
            $this->db->where('maid_service_type_jon_id',$id);
            $this->db->update('maid_service_type_joins',$datas);
        }
        function update_services($dat, $id)
        {
            $this->db->set($dat);
            $this->db->insert('maid_service_type_joins');
            $result = $this->db->insert_id();
            return $result;
            
        }
        function get_all_maids($active = 2)
        {
            $this->db->select('maids.*, flats.flat_name')
		     ->from('maids')
                     ->join('flats', 'maids.flat_id = flats.flat_id', 'left')
                     ->order_by('maids.maid_name');
                     //->order_by('maids.maid_added_datetime');
            if ($active == 2) {
                $this->db->where('maids.maid_status', 1);
            } else if ($active == 3) {
                $this->db->where('maids.maid_status', 0);
            
            } else if ($active == 4) {
                $this->db->where('maids.maid_status', 2);
            }
            $query = $this->db->get();
            return $query->result_array();    
        }
        function delete_maid($maid_id)
        {
            $this->db->where('maid_id',$maid_id);
            $this->db->delete('maids');
        }
        function delete_maid_services($maid_id)
        {
            $this->db->where('maid_id',$maid_id);
            $this->db->delete('maid_service_type_joins');
        }
        function get_maid_details($maid_id)
        {
            $this->db->select('maids.*, flats.flat_name, teams.team_name')
                    ->from('maids')
                    ->join('flats','maids.flat_id = flats.flat_id','left')
                    ->join('teams','maids.team_id = teams.team_id','left')
                    ->where('maids.maid_id', $maid_id);
            $query = $this->db->get();
            return $query->result_array();   
        }
        function update_maids($data,$maid_id)
        {
            $this->db->where('maid_id',$maid_id);
            $this->db->update('maids',$data);
        }
        function get_maid_services($maid_id)
        {
            $qr = $this->db->select('maid_service_type_joins.service_type_id')
			->from('maid_service_type_joins')
                        ->where('maid_service_type_joins.maid_id',$maid_id);
            $query = $this->db->get();
            return $query->result();
        }
        function disable_status($maid_id,$data)
        {
            $this->db->where('maid_id',$maid_id);
            $this->db->update('maids',$data);
            
        }
        function activate_status($maid_id,$data)
        {
            $this->db->where('maid_id',$maid_id);
            $this->db->update('maids',$data);
        }
        function maid_services($maid_id)
        {
            $qr = $this->db->select('maid_service_type_joins.maid_service_type_jon_id, maid_service_type_joins.service_type_id, service_types.service_type_name, service_types.odoo_service_id')
			->from('maid_service_type_joins')
                        ->join('service_types','maid_service_type_joins.service_type_id = service_types.service_type_id')
                        ->where('maid_service_type_joins.maid_id',$maid_id);
            $query = $this->db->get();
            return $query->result_array();
        }
        /** 
	 * Get maid by id
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	int
	 * @return	array
	 */
	function get_maid_by_id($maid_id)
	{
		$this->db->select("maid_id, maid_login_token, maid_device_token, maid_name, maid_gender, maid_nationality, maid_present_address, maid_permanent_address, maid_mobile_1, maid_mobile_2, flat_id, maid_photo_file, maid_passport_number, maid_passport_expiry_date, maid_passport_file, maid_visa_number, maid_visa_expiry_date, maid_visa_file, maid_labour_card_number, maid_labour_card_expiry_date, maid_labour_card_file, maid_notes, odoo_maid_id, odoo_synch_status, odoo_new_maid_id, odoo_new_maid_status, maid_status, maid_added_datetime", FALSE)
				->from('maids')
				->where('maid_id', $maid_id)
				->limit(1);
		
		$get_maid_by_id_qry = $this->db->get();
		
		return $get_maid_by_id_qry->row();
	}
        /**
         * Get Maid Attendance by Date
         * 
         * @author Geethu
         * @access public
         * @param date $date search date
         * @return array 
         */
        
        function get_maid_attendance_by_date($date)
        {
            $this->db->select("m.maid_name, z.zone_name, a.attandence_status,a.maid_in_time, a.maid_out_time", FALSE)
                    ->from('maid_attandence a')
                    ->join('maids m', 'a.maid_id = m.maid_id')
                    ->join('tablets t', 'a.tablet_id = t.tablet_id')
                    ->join('zones z', 't.zone_id = z.zone_id')
                    ->where('a.attandence_id IN (SELECT MAX(attandence_id) FROM maid_attandence WHERE DATE(date) = "' . $date . '" GROUP BY tablet_id, maid_id)')
                    ->order_by('m.maid_name');
            
            $get_maid_attendance_by_date_qry = $this->db->get();
            
            return $get_maid_attendance_by_date_qry->result();
             
        }

    // **********************************************************************************************
    // function get_maid_idle_hours_by_date($date)
    // {
    //     $this->db->select('m.maid_name, m.maid_id, b.service_start_date, SEC_TO_TIME(SUM(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))) AS total_working_time_seconds');
    //     $this->db->from('maids m');
    //     $this->db->join('bookings b', 'b.maid_id = m.maid_id AND b.service_start_date = "' . $date . '"', 'left');
    //     $this->db->where('m.maid_status', 1);
    //     $this->db->group_by(array("m.maid_id", "m.maid_name", "b.service_start_date"));
    //     $this->db->order_by('m.maid_name');

    //     $query = $this->db->get();

    //     return $query->result();
    // }


    // ********************************************************************************
        function get_maid_vehicle_report($date = NULL)
        {
            $date = $date === NULL ? date('Y-m-d') : $date;
            
            $get_maid_vehicle_report =  $this->db->query("SELECT ma.maid_id, m.maid_name, t.zone_id, z.zone_name, ma.attendance_status
                            FROM  maid_barcode_attendance ma
                            JOIN tablets t ON ma.tablet_id = t.tablet_id
                            JOIN zones z ON t.zone_id = z.zone_id
                            JOIN maids m ON ma.maid_id = m.maid_id
                            WHERE DATE( ma.addeddate ) =  '$date'
                            AND ma.id
                            IN (

                            SELECT MAX( id ) 
                            FROM maid_barcode_attendance
                            WHERE maid_id = ma.maid_id
                            AND tablet_id = ma.tablet_id
                            AND DATE( addeddate ) =  '$date'
                            )
                            GROUP BY ma.tablet_id, ma.maid_id");
            
            return $get_maid_vehicle_report->result();
        }
        
        /* Betsy Bernard */

        function add_maid_leave($fields = array())
        {
            
            
            
            $this->db->select("maid_leave.*", FALSE)
                ->from('maid_leave')
                ->where('maid_leave.maid_id', $fields['maid_id'])
                ->where('maid_leave.leave_date', $fields['leave_date'])
                ->where('maid_leave.leave_status', 0)
                ->limit(1); 
        
            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                $data = array(
                    'leave_status' =>1,
                    'added_by' => $fields['added_by'],
                    'leave_type' => $fields['leave_type'],
                    'typeleaves' => $fields['typeleaves']
                );
                $this->db->where('leave_id', $query->row()->leave_id);
                $this->db->update('maid_leave', $data);
                $this->add_activity($fields['maid_id'], 5);
                return $query->row()->leave_id;
            }
            else
            {
                $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');
                $fields['added_ip_address'] = isset($fields['added_ip_address']) ? $fields['added_ip_address'] : $_SERVER['REMOTE_ADDR'];

                $this->db->set($fields);
                $this->db->insert('maid_leave'); 
                $this->add_activity($fields['maid_id'], 5);
                return $this->db->insert_id();
            }
            
            
        }
        
        function check_maid_leave_by_date($maid_id, $start_date, $end_date)
        {
            $this->db->select("maid_leave.*", FALSE)
                ->from('maid_leave')
                ->where('maid_leave.maid_id', $maid_id)
                ->where("((maid_leave.leave_date BETWEEN ". $this->db->escape($start_date) ." and ".$this->db->escape($end_date)." AND leave_status = 1))", NULL, FALSE); 
        
            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        function get_maid_leave($leave_date)
        {
            $this->db->select('maid_leave.*, maids.*, users.user_fullname')
                     ->from('maid_leave')
                     ->join('maids', 'maids.maid_id = maid_leave.maid_id')
                     ->join('users', 'users.user_id = maid_leave.added_by')
                     ->where('maid_leave.leave_date', $leave_date)
                     ->where('maid_leave.leave_status', 1);  
                
            $query = $this->db->get();
            
            return $query->result();
        }
        function get_maid_hours($date,$date_to)
        {
            $this->db->select('m.maid_name,SEC_TO_TIME(Sum(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from)))) AS hours,Sum(d.total_fee) AS revenue',FALSE)
                     ->from('maids m')
                     ->join('day_services d', 'm.maid_id = d.maid_id')
                     ->join('bookings b', 'b.booking_id = d.booking_id')
                     ->where('m.maid_status', 1)
                     ->where('d.service_date >=',"'$date'",FALSE)
                     ->where('d.service_date <=',"'$date_to'" ,FALSE)
                     ->group_by('m.maid_name')
                     ->order_by('m.maid_name', 'ASC');
                
            $query = $this->db->get();
            
            return $query->result();
        }
        function update_maid_leave($fields = array())
        {
            $fields['last_updated_ip_address'] = isset($fields['last_updated_ip_address']) ? $fields['last_updated_ip_address'] : $_SERVER['REMOTE_ADDR'];
            $fields['last_updated_date_time'] = isset($fields['last_updated_date_time']) ? $fields['last_updated_date_time'] : date('Y-m-d H:i:s');
        
            $this->db->where('leave_id',$fields['leave_id']);
            $this->db->update('maid_leave', $fields);
        }
        /*/ Betsy Bernard /*/
        
        
        /*
         * Maid Leave
         * @auther : Geethu
         */
        function get_maids_leave_by_date($service_date, $maid_id = NULL)
        {
            $this->db->select('maid_id')
                    ->from('maid_leave')
                    ->where('leave_date', $service_date)
                    ->where('leave_status', 1);
            
            if($maid_id)
            {
                $this->db->where('maid_id', $maid_id);
            }
            
            $get_maids_leave_by_date_qry = $this->db->get();
            
            return $get_maids_leave_by_date_qry->result();
        }
		
		function list_maids_leave_by_date($service_date)
		{
			$this->db->select('ml.maid_id,m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
                    ->from('maid_leave ml')
					->join('maids m','m.maid_id = ml.maid_id')
					->join('flats f', 'm.flat_id = f.flat_id', 'left')
                    ->where('ml.leave_date', $service_date)
                    ->where('ml.leave_status', 1)
					->order_by('m.maid_name','asc');
            $get_maids_leave_by_date_qry = $this->db->get();
            
            return $get_maids_leave_by_date_qry->result();
		}
        
        /*
         * Maid Booking module from customer page
         * @author : Jiby
         */
        function get_all_maid($filter = NULL){
          
        if(isset($filter)){
            $location_type  = $filter['location_type'];
            $value          = $filter['location_val'];
            $customer_id    = $filter['customer_id'];
            $area_id        = $filter['area_id'];
            $zone_id        = $filter['zone_id'];
            $province_id    = $filter['province_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];
            
            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
           }    
     
            if($location_type == 'area') {
                $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
                $this->db->from('maids m');
                $this->db->join('bookings as b', 'm.maid_id = b.maid_id','LEFT');
                $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id','LEFT');
                $this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT');
//                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
                $this->db->where('a.area_id', $area_id);
                
            }else if($location_type == 'zone') {
                $areaArray  = array();    
                $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";    
                $querySub   = $this->db->query($subQuery);
                foreach ($querySub->result_array() as $row)
                {
                    $areaArray[] = $row['area_id'];     

                }
        
                $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
                $this->db->from('maids m');
                $this->db->join('bookings as b', 'm.maid_id = b.maid_id','LEFT');
                $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id','LEFT');
                $this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT');
                $this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT');
//                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
                 $this->db->where_in('a.area_id',$areaArray);
//                 $this->db->where('z.zone_id', $value);
            }else if($location_type == 'province') {
                $areaArray  = array();   
                $subQuery   = "SELECT asq.area_id FROM `areas` as asq LEFT JOIN `zones` as zsq ON `zsq`.`zone_id` = `asq`.`zone_id` WHERE `zsq`.`province_id` = $province_id";               
                $querySub   = $this->db->query($subQuery);
                foreach ($querySub->result_array() as $row)
                {
                    $areaArray[] = $row['area_id'];     

                }         
                $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
                $this->db->from('maids m');
                $this->db->join('bookings as b', 'm.maid_id = b.maid_id','LEFT');
                $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id','LEFT');
                $this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT');
                $this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT');
                $this->db->join('province as p', 'p.province_id = z.province_id','LEFT');
//                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
                  $this->db->where_in('a.area_id',$areaArray);
//                 $this->db->where('p.province_id', $value);
            }
            else{
                $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
                $this->db->from('maids m');
            }
                $this->db->where('maid_status', 1);
                $this->db->group_by('m.maid_id');
                $this->db ->order_by('maid_name', 'ASC');
                  //  ->limit(10);
                
           
            $get_maids_leave_by_date_qry = $this->db->get();
            //echo $this->db->last_query();
            return $get_maids_leave_by_date_qry->result();
        }
        
        
        function get_maid_leave_report($data)
        {
            $maid_id=$data['maid_id'];
            $start_date=$data['start_date'];
            $end_date=$data['end_date'];
            $leave_type=$data['leave_type'];
            $this->db->select("l.*,m.maid_name", FALSE)
                ->from('maid_leave l');
                 $this->db->join('maids m', 'm.maid_id = l.maid_id')
                         
                ->where("((l.leave_date BETWEEN ". $this->db->escape($start_date) ." and ".$this->db->escape($end_date)." AND l.leave_status = 1))", NULL, FALSE); 
            if($maid_id)
            {
                $this->db->where('l.maid_id', $maid_id);
            }
            $this->db->order_by('l.leave_date','DESC');
            $query = $this->db->get();
            // echo $this->db->last_query();
            $results=$query->result();
            // print_r($results);
            
            return $results;
        }
        
        function delete_leavereport($leave_id, $status)
        {
            $this->db->set('leave_status', $status);
            $this->db->where('leave_id', $leave_id);
            $this->db->update('maid_leave');
        }
        
        function get_maid_leave_reportresults($data)
        {
            $maid_id=$data['maid_id'];
            $start_date=$data['start_date'];
            $end_date=$data['end_date'];
            $leave_type=$data['leave_type'];
            $this->db->select('m.maid_name,SUM( IF( l.leave_type = "1", 1, 0 ) ) AS fullday,SUM( IF( l.leave_type = "2", 1, 0 ) ) AS halfday',FALSE)
                    ->from('maid_leave l');
            $this->db->join('maids m', 'm.maid_id = l.maid_id');
            $this->db->where("((l.leave_date BETWEEN ". $this->db->escape($start_date) ." and ".$this->db->escape($end_date)." AND l.leave_status = 1))", NULL, FALSE);
            if($maid_id)
            {
                $this->db->where('l.maid_id', $maid_id);
            }
            $this->db->group_by('l.maid_id');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
        
        function add_maid_attandence($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('maid_attandence'); 
		return $this->db->insert_id();
	}
        
        function update_maid_attandence($attandence_id, $fields = array())
	{
		$this->db->where('attandence_id', $attandence_id);
		$this->db->update('maid_attandence', $fields); 

		return $this->db->affected_rows();
	}
        function add_activity($maid_id, $action_type=NULL)
        {
           $maid=$this->get_maid_by_id($maid_id); 
           $action=array("0"=>"disabled","1"=>"enabled","2"=>"deleted","3"=>"added","4"=>"edited","5"=>"marked_leave");
           if($action_type==3){
           $content_activity = "New maid ". $maid->maid_name . " is " ." ".$action[$action_type] ." "."by Admin user";
           }
           else if($action_type==5)
           {
            $content_activity = "Maid ". $maid->maid_name . " has been marked leave by Admin user";   
           }
          
           else {
           $content_activity = "Maid ". $maid->maid_name . " is " ." ".$action[$action_type] ." "."by Admin user";
            }
           $data_activity = array(
                'added_user' => user_authenticate(),
                'action_type' => "Maid_".$action[$action_type],
                'action_content' => $content_activity,
                'addeddate' => date('Y-m-d H:i:s'),
            );

           $this->db->set($data_activity);
           $this->db->insert('user_activity');
           $result = $this->db->insert_id();  
           return $result;
        }
        
    function get_all_maid_new($filter = NULL)
    {   
        if(isset($filter))
        {
            $customer_id    = $filter['customer_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];
            
            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
        } 
        $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
        $this->db->from('maids m');
            
        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db ->order_by('maid_name', 'ASC');
        $get_maids_leave_by_date_qry = $this->db->get();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }
    
    function get_maids_odoo()
    {
        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, m.maid_status, m.odoo_synch_status')
                ->from('maids m')
                ->order_by('m.maid_name','asc');
        $this->db->where('m.odoo_synch_status', 0);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }
	
	function list_inactivemaids()
	{
		$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
				->from('maids m')
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
				->order_by('m.maid_name','asc');
                $this->db->where('maid_status', 0);
		$get_maids_qry = $this->db->get();
		return $get_maids_qry->result();
	}
	
	function get_maids_for_odoo()
	{
		$this->db->select('m.maid_id, m.odoo_new_maid_id, m.maid_name, m.maid_full_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_new_flat_id, m.maid_status, m.odoo_new_maid_status')
				->from('maids m')
				->where('m.odoo_new_maid_status',0)
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
				->order_by('m.maid_name');
		$this->db->where('m.maid_status', 1);
		$get_maids_qry = $this->db->get();
		return $get_maids_qry->result();
	}
        
}