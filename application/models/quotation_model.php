<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Quotation_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function change_quotation_read_status($quo_id, $new_status)
    {
        $this->db->where('quo_id', $quo_id);
        $this->db->update('quotation', array('quo_read_status' => $new_status));
    }
	public function get_unread_quotation_count()
    {
		$this->db->select("count(q.quo_id) as total", false)
            ->from('quotation as q')
			->where('q.quo_read_status',null);
        $query = $this->db->get();
        return $query->row_array()['total'];
    }
}
