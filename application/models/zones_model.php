<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Zones_model Class 
  * 
  * @package	Emaid
  * @author		Habeeb Rahman 
  * @since		Version 1.0
  */
class Zones_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/** 
	 * Add zone
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function add_zone($fields = array())
	{
		$fields['zone_status'] = isset($fields['zone_status']) ? $fields['zone_status'] : 1;

		$this->db->set($fields);
		$this->db->insert('zones'); 
		return $this->db->insert_id();
	}
	/** 
	 * Update zone
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_zone($zone_id, $fields = array())
	{
		$this->db->where('zone_id', $zone_id);
		$this->db->update('zones', $fields); 

		return $this->db->affected_rows();
	}
        /** 
	 * Delete zone
	 * 
	 * @author   Geethu
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_zone($zone_id)
	{
            
                $this->db->where('zone_id', $zone_id);
		$this->db->delete('zones'); 

		return $this->db->affected_rows(); 
           
	}
	/** 
	 * Get zones
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool, bool
	 * @return	array
	 */
	function get_zones($active_only = TRUE, $no_spare = TRUE)
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				->from('zones')
				->order_by('zone_name');
		
		if($active_only)
		{
			$this->db->where('zone_status', 1);
		}
		
		if($no_spare)
		{
			$this->db->where('spare_zone', 'N');
		}
		
		$get_zones_qry = $this->db->get();
		
		return $get_zones_qry->result();
	}
        function get_all_zones()
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				->from('zones')
				->order_by('zone_id');
		
		
		
		$get_zones_qry = $this->db->get();
		
		return $get_zones_qry->result();
	}
	
	/** 
	 * Get zone by id
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str
	 * @return	obj
	 */
	function get_zone_by_id($zone_id)
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				 ->from('zones')
				 ->where('zone_id', $zone_id)
				 ->limit(1);
		
		$get_zone_by_id_qry = $this->db->get();
		
		return $get_zone_by_id_qry->row();
	}
	
	/** 
	 * Get zone by zone name
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str
	 * @return	obj
	 */
	function get_zone_by_zone_name($zone_name)
	{
		$this->db->select('zone_id')
				 ->from('zones')
				 ->where('zone_name', $zone_name)
				 ->limit(1);
		
		$get_zone_by_zone_name_qry = $this->db->get();
		
		return $get_zone_by_zone_name_qry->row();
	}
			
	
	/** 
	 * Get maximum of category order
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, int
	 * @return	int
	 */
	function get_max_category_order($parent_category_id)
	{
		$this->db->select('MAX(category_order) AS order_max')
				->from('categories')
				 ->where('parent_category_id', $parent_category_id);
				 
		$get_max_category_order_qry = $this->db->get();
		
		$result = $get_max_category_order_qry->row();

		return isset($result->order_max) && is_numeric($result->order_max) ? $result->order_max : 0;
	}
	
	/** 
	 * Get categories by englishname, and parent
	 * 
	 * @author   Habeeb Rahman
	 * @acces    public 
	 * @param    str, int
	 * @return   array
	 */
	function get_categories_by_englishname_and_parent($category_name_en, $parent_category_id)
	{
		$this->db->select('category_id')
				 ->from('categories')
				 ->where('category_name_en', $category_name_en)
				 ->where('parent_category_id', $parent_category_id);
				 
		$get_categories_by_englishname_and_parent_qry = $this->db->get();
		
		return $get_categories_by_englishname_and_parent_qry->result();
	}
	
	/** 
	 * Get categories by malayalamname and parent
	 * 
	 * @author   Habeeb Rahman
	 * @acces    public 
	 * @param    str, int
	 * @return   array
	 */
	function get_categories_by_malayalamname_and_parent($category_name_ml, $parent_category_id)
	{
		$this->db->select('category_id')
				 ->from('categories')
				 ->where('category_name_ml', $category_name_ml)
				 ->where('parent_category_id', $parent_category_id);
				 
		$get_categories_by_malayalamname_and_parent_qry = $this->db->get();
		
		return $get_categories_by_malayalamname_and_parent_qry->result();
	}
	
	/** 
	 * Get categories by parent id
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str, int, bool
	 * @return	array
	 */
	function get_categories_by_parent_id($parent_id, $active_only = TRUE)
	{
		$this->db->select('c.category_id, c.parent_category_id, c.category_nice_name, c.category_name_en, c.category_name_ml, c.category_order, c.category_show_menu, c.category_status, (SELECT COUNT(category_id) FROM categories WHERE parent_category_id = c.category_id) AS subcats_count, COUNT(n.news_id) AS news_count', FALSE)
				->from('categories c')
				->join('categories cs', 'c.category_id = cs.parent_category_id', 'left')
				->join('news n', 'c.category_id = n.category_id OR cs.category_id = n.category_id', 'left')
				->where('c.parent_category_id', $parent_id)
				->group_by('c.category_id')
				->order_by('c.category_order');
		
		if($active_only)
		{
			$this->db->where('c.category_status', 1);
		}
		
		$get_categories_by_parent_id_qry = $this->db->get();
		
		return $get_categories_by_parent_id_qry->result();
	}
	
	/** 
	 * Update category
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_category($category_id, $fields = array())
	{
		$this->db->where('category_id', $category_id);
		$this->db->update('categories', $fields); 

		return $this->db->affected_rows();
	}
	
	/** 
	 * Delete category
	 * 
	 * @author   Habeeb Rahman
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_category($category_id)
	{
		$this->db->where('category_id', $category_id);
		$this->db->delete('categories'); 

		return $this->db->affected_rows();
	}
	function get_all_zones_new()
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				->from('zones')
				->where('zone_status',1)
				->order_by('zone_id');
		
		
		
		$get_zones_qry = $this->db->get();
		
		return $get_zones_qry->result();
	}
}