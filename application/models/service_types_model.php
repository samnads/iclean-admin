<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Service_types_model Class 
  * 
  * @package	Emaid
  * @author		Azinova 
  * @since		Version 1.0
  */
class Service_types_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/** 
	 * Add service type
	 * 
	 * @author	Azinova
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function add_service_type($fields = array())
	{
		$fields['service_type_status'] = isset($fields['service_status']) ? $fields['service_status'] : 1;

		$this->db->set($fields);
		$this->db->insert('service_types'); 
		return $this->db->insert_id();
	}
	/** 
	 * Update service
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_service_type($service_type_id, $fields = array())
	{
		$this->db->where('service_type_id', $service_type_id);
		$this->db->update('service_types', $fields); 

		return $this->db->affected_rows();
	}
        /** 
	 * Delete service type
	 * 
	 * @author   Geethu
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_service_type($service_type_id)
	{
            
                $this->db->where('service_type_id', $service_type_id);
		$this->db->delete('service_types'); 

		return $this->db->affected_rows(); 
           
	}
        /** 
	 * Get service types
	 * 
	 * @author	Azinova
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_service_type_by_id($service_type_id)
	{
		$this->db->select('service_type_id, service_type_name, service_rate, service_type_status')
				->from('service_types')
                                ->where('service_type_id', $service_type_id)
				->limit(1);		
		
		
		$get_service_types_qry = $this->db->get();
		
		return $get_service_types_qry->row();
	}
	/** 
	 * Get service types
	 * 
	 * @author	Azinova
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_service_types($active_only = TRUE)
	{
		$this->db->select('service_type_id, service_type_name, service_rate, service_type_status, service_type, price_condition, enable_scrubbing')
				->from('service_types')
				->order_by('service_type_name');
		
		if($active_only)
		{
			$this->db->where('service_type_status', 1);
		}
		
		$get_service_types_qry = $this->db->get();
		
		return $get_service_types_qry->result();
	}
	
        
	/** 
	 * Get service type by name
	 * 
	 * @author	Azinova
	 * @acces	public 
	 * @param	str
	 * @return	obj
	 */
	function get_service_type_by_name($service_type_name)
	{
		$this->db->select('service_type_id')
				 ->from('service_types')
				 ->where('service_type_name', $service_type_name)
				 ->limit(1);
		
		$get_service_type_by_name_qry = $this->db->get();
		
		return $get_service_type_by_name_qry->row();
	}
        
        
        
          public function get_offers()
    {
       $this->db->select('*')
                ->from('offers')
                ->where('status','1');
        $offers = $this->db->get();
        
        return $offers->result(); 
        
    }
    
    function get_cleaning_service_types($active_only = TRUE)
    {
        $this->db->select('service_type_id, service_type_name, service_rate, service_type_status')
                        ->from('service_types')
                        ->where('service_category','C')
                        ->order_by('service_type_name');

        if($active_only)
        {
                $this->db->where('service_type_status', 1);
        }

        $get_service_types_qry = $this->db->get();

        return $get_service_types_qry->result();
    }
    
    function get_servicedetail_id($service_id)
    {
        $this->db->select('sc.*,c.service_category_name,ssc.sub_category_name,ssf.service_furnish_name')
                ->from('servicecategorycosts sc')
                ->join('servicecategory c','c.id = sc.category_id','left')
                ->join('servicesubcategory ssc','ssc.id = sc.sub_category_id','left')
                ->join('servicesubfurnishcategory ssf','ssf.id = sc.service_sub_funish_id','left')
                ->where('sc.service_id',$service_id)
                ->where('sc.status',1);
        
        $service_query = $this->db->get();
        return $service_query->result();
    }
    
    function add_service_mapping($fields = array())
    {
            $this->db->set($fields);
            $this->db->insert('bookingservicemapping'); 
            return $this->db->insert_id();
    }
}