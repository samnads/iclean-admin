<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Users_model Class 
  * 
  * @package	Emaid
  * @author		Habeeb Rahman 
  * @since		Version 1.0
  */
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/** 
	 * Add user
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function add_user($fields = array())
	{
		$fields['is_admin'] = isset($fields['is_admin']) ? $fields['is_admin'] : 'N';
		$fields['user_status'] = isset($fields['user_status']) ? $fields['user_status'] : 1;
		$fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

		$this->db->set($fields);
		$this->db->insert('users'); 
		return $this->db->insert_id();
	}
        
        function add_user_login_detail($fields = array())
	{
            $this->db->set($fields);
            $this->db->insert('user_login_details'); 
            return $this->db->insert_id();
	}
        
	function get_all_modules($active_only = TRUE)
	{
		$this->db->select('sm.module_id, sm.parent_id, sm.module_name AS sub_module_name, m.module_name AS module_name', FALSE)
				->from('modules sm')
                                ->join('modules m', 'm.module_id = sm.parent_id', 'left')
				->order_by('sm.parent_id');
		
		if($active_only)
		{
			$this->db->where('m.module_status', 1);
		}
               
		
		$get_modules_qry = $this->db->get(); 
		
		return $get_modules_qry->result();
	}
	/** 
	 * Get users
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_users($active_only = TRUE)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, user_status, last_loggedin_datetime, added_datetime, last_loggedin_ip')
				->from('users')
				->order_by('added_datetime', 'DESC');
		
		if($active_only)
		{
			$this->db->where('user_status', '1');
		}
		
		$get_users_qry = $this->db->get();
		
		return $get_users_qry->result();
	}
        
        function get_user_by_id($user_id)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, user_status, login_type')
				 ->from('users')
				 ->where('user_id', $user_id)
				 ->limit(1);
		
		$get_user_by_id_qry = $this->db->get();
		
		return $get_user_by_id_qry->row();
	}
        
        function get_user_login_info($user_id)
	{
            $this->db->select('ud.user_id, u.username,u.user_fullname, ud.login_ip, ud.user_agent, ud.login_date_time')
                    ->from('user_login_details ud')
                    ->join('users u','u.user_id = ud.user_id')
                    ->where('u.user_id', $user_id)
                    ->order_by('ud.login_date_time','desc');

            $get_user_by_id_qry = $this->db->get();

            return $get_user_by_id_qry->result();
	}
        
	function get_permission_by_user($user_id)
        {
//                $this->db->select('m.module_id, m.module_name')
//                         ->from('user_permissions up')
//                         ->join('modules m', 'up.module_id = m.module_id')
//                         ->where('up.user_id', $user_id);
//                
//                $get_permission_by_user_qry = $this->db->get();
//                
//                return $get_permission_by_user_qry->result();
            $this->db->select('up.module_id')
                    ->from('user_permissions up')
                    ->join('modules m', 'up.module_id = m.module_id')
                    ->where('up.user_id', $user_id)
                    ->where('up.permission', 'Y');        

            $get_permissions_qry = $this->db->get();       

            return $get_permissions_qry->result();
        }
	/** 
	 * Get user by username and password
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str, str
	 * @return	array
	 */
	function get_user_by_username_and_password($username, $password)
	{
		$this->db->select('user_id, username, password, user_fullname, permissions, is_admin, user_status, login_type')
				 ->from('users')
				 ->where('username', $username)
				 ->where('password', $password)
				 ->limit(1);
		
		$get_user_by_username_and_password_qry = $this->db->get();
		
		return $get_user_by_username_and_password_qry->row();
	}
		
	/** 
	 * Update user
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_user($user_id, $fields = array())
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $fields); 

		return $this->db->affected_rows();
	}
	
	/** 
	 * Delete user
	 * 
	 * @author   Habeeb Rahman
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_user($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('users'); 

		return $this->db->affected_rows();
	}
        function delete_permissions_by_user_id($user_id)
        {
            $this->db->where('user_id', $user_id);
            $this->db->delete('user_permissions'); 

            return $this->db->affected_rows();
        }
        function check_password($user_id, $password)
        {
            $this->db->select('user_id')
                ->from('users')
                ->where('user_id', $user_id)
                ->where('password', $password)
                ->limit(1);

            $check_password_qry = $this->db->get();

            if ($check_password_qry->num_rows() > 0) 
            {
                return TRUE;
            } else {
                return FALSE;
            }
    }
    function get_modules($active = TRUE)
    {
        $this->db->select('module_id, module_name, parent_id')
                ->from('modules')
                ->order_by('module_id,parent_id');
        if($active)
        {
            $this->db->where('module_status', 1);
        }
        
        $get_modules_qry = $this->db->get();
        
        return $get_modules_qry->result();
    }
    function get_module_id_by_name($module_name, $parent_id = 0)
    {
        $this->db->select('module_id')
                ->from('modules')
                ->where('module_name', $module_name)
                ->where('parent_id', $parent_id)
                ->where('module_status', 1);
        
        $get_module_id_by_name_qry = $this->db->get();
        
        return $get_module_id_by_name_qry->row();
    }
    function get_permissions($module_id, $user_id, $method_id)
    {
        $this->db->select('up.permission')
                ->from('user_permissions up')
                ->join('modules m', 'up.module_id = m.module_id')
                ->where('up.user_id', $user_id);
        if($method_id == 0){
            $this->db->where('up.module_id', $module_id)
                    ->where('m.parent_id', $method_id);  
        }else{
               $this->db->where('m.parent_id', $module_id)
                       ->where('up.module_id', $method_id);        
        }
        $get_permissions_qry = $this->db->get(); 
        //echo $this->db->last_query();
        return $get_permissions_qry->row();
        
    }
    function get_module_by_user_id($user_id)
    {
        $this->db->select('up.module_id')
                ->from('user_permissions up')
                ->join('modules m', 'up.module_id = m.module_id')
                ->where('up.user_id', $user_id)
                ->where('up.permission', 'Y');        
        
        $get_permissions_qry = $this->db->get();       
        
        return $get_permissions_qry->result();
        
    }
    
    function add_permission( $fields, $user_id)
    {
        
        $this->db->where('user_id', $user_id);
        $this->db->delete('user_permissions');
        
        $this->db->insert_batch('user_permissions', $fields); 
        
        return $this->db->affected_rows();
    }
    function check_user_permission($user_id, $module_id)
    {
        $this->db->select('user_permission_id')
                ->from('user_permissions')
                ->where('user_id', $user_id)
                ->where('module_id', $module_id)
                ->limit(1);
        
        $check_user_permission_qry = $this->db->get();
        
        return $check_user_permission_qry->row();
    }
    function get_ci_session($session_id)
    {
        $this->db->select('session_id')
                ->from('ci_sessions')
                ->where('session_id', $session_id);

        $get_ci_session_qry = $this->db->get();

        return $get_ci_session_qry->num_rows();
    }
    function add_sessions($fields = array())
    {
            $session_id = $fields['session_id'];
            if($this->get_ci_session($session_id) > 0)
            {
                $update_fields = array();
                $update_fields['last_activity'] = $fields['last_activity'];
                $update_fields['user_data'] = $fields['user_data'];

                $this->update_sessions($session_id, $update_fields);
            }
            else
            {
                $this->db->set($fields);
                $this->db->insert('ci_sessions'); 
            }


    }
    function update_sessions($session_id, $fields = array())
    {
            $this->db->where('session_id', $session_id);
            $this->db->update('ci_sessions', $fields); 

            return $this->db->affected_rows();
    }
    function get_user_activity_by_date($date, $date_to = NULL)
    {
        
        $this->db->select('u.user_fullname, a.action_type, a.booking_type, a.shift, a.action_content, DATE_FORMAT(a.addeddate, "%d/%m/%Y %H:%i:%s") AS added', FALSE)
                ->from('user_activity a')
                ->join('users u', 'a.added_user = u.user_id')                
                ->order_by('a.addeddate', 'desc');
        if($date_to != NULL)
        {
            $this->db->where("(DATE(a.addeddate) BETWEEN  '$date' AND '$date_to')", NULL, FALSE);
        }
        else
        {
            $this->db->where('DATE(a.addeddate)', $date);
        }
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
    function get_user_activity()
    {
        
        $this->db->select('user_activity.*,users.user_fullname', FALSE)
                ->from('user_activity')
                ->join('users','user_activity.added_user = users.user_id','left')
                ->order_by('user_activity.addeddate', 'desc')
                ->limit(25);
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
    function delete_module($module_id)
    {
            $this->db->where('module_id', $module_id);
            $this->db->delete('modules'); 

            $affected = $this->db->affected_rows();
            $this->db->where('module_id', $module_id);
            $this->db->delete('user_permissions');

            return $affected;
    }
    
    function check_passwords($user_id, $password)
    {
        $this->db->select('user_id')
                ->from('users')
            ->where('user_id', $user_id)
            ->where('password', $password)
            ->limit(1);

        $check_password_qry = $this->db->get();

        if ($check_password_qry->num_rows() > 0) 
        {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function update_users($user_id, $fields = array())
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $fields); 

        return $this->db->affected_rows();
    }
    
    function get_user_by_user_name($username)
    {
        $this->db->select('user_id, username, password, user_fullname, permissions, is_admin, user_status')
                ->from('users')
                ->where('username', $username)				 
                ->limit(1);

        $get_user_by_username_and_password_qry = $this->db->get();

        return $get_user_by_username_and_password_qry->row();
    }
    
   
}