<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model{
    
    function get_zones()
    {
        $qr = $this->db->select('*')->from('zones')->where('zone_status',1);
	$query = $this->db->get();
	return $query->result_array();
    }
    function add_zones($data)
    {
        $this->db->set($data);
	$this->db->insert('zones');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_zone_details($zone_id)
    {
        $qr = $this->db->select('*')
                ->from('zones')
                ->where('zone_id',$zone_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_team_details($team_id)
    {
        $qr = $this->db->select('*')
                ->from('teams')
                ->where('team_id',$team_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_zones($data,$zone_id)
    {
        $this->db->where('zone_id',$zone_id);
        $this->db->update('zones',$data);
    }
	function delete_zone_new($data,$zone_id)
    {
        $qr = $this->db->select('*')
                ->from('areas')
                ->where('areas.zone_id',$zone_id);
	$query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return 1;
        }
        else{
            $this->db->where('zone_id',$zone_id);
			$this->db->update('zones',$data);
            return 0;
        }
        
    }
    function update_teams($data,$team_id)
    {
        $this->db->where('team_id',$team_id);
        $this->db->update('teams',$data);
    }
    function delete_zone($zone_id)
    {
        $qr = $this->db->select('*')
                ->from('areas')
                ->where('areas.zone_id',$zone_id);
	$query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return 1;
        }
        else{
            $this->db->where('zone_id',$zone_id);
            $this->db->delete('zones');
            return 0;
        }
        
    }
    function add_teams($fields = array())
    {
        $fields['team_status'] = isset($fields['team_status']) ? $fields['team_status'] : 1;

        $this->db->set($fields);
        $this->db->insert('teams'); 
        return $this->db->insert_id();
    }
    function get_teams($active_only = TRUE)
    {
        $this->db->select('team_id, team_name, team_status')
                        ->from('teams')
                        ->order_by('team_name');

        if($active_only)
        {
                $this->db->where('team_status', 1);
        }

        $get_teams_qry = $this->db->get();

        return $get_teams_qry->result();
    }
    function get_teams_for_count($team_id = NULL)
    {
        if($team_id == NULL)
        {
            $this->db->select('team_id, team_name, team_status')
                        ->from('teams')
                        ->order_by('team_name');
            $this->db->where('team_status',1);
            $get_teams_qry = $this->db->get();
            $results = $get_teams_qry->result();
            $res_array = array();
            foreach ($results as $result)
            {
                $this->db->select('maid_id')
                        ->from('maids')
                        ->where('team_id',$result->team_id);
                $get_maid_qry = $this->db->get();
                $res_count = $get_maid_qry->num_rows();
                
                if($res_count != 0)
                {
                    $data = array();
                    $data['team_id'] = $result->team_id;
                    array_push($res_array, $data);
                }
            }
            return $res_array;
        } else {
            $this->db->select('team_id, team_name, team_status')
                            ->from('teams')
                            ->order_by('team_name');
            $this->db->where('team_status',1);

            if($team_id != NULL)
            {
                    $this->db->where('team_id', $team_id);
            }

            $get_teams_qry = $this->db->get();

            return $get_teams_qry->result();
        }
    }
    function get_areas()
    {
        $qr = $this->db->select('*')
                ->from('areas')
				->where('area_status', 1)
                ->join('zones','zones.zone_id = areas.zone_id');
	$query = $this->db->get();
	return $query->result_array();
    }
    function add_area($data)
    {
        $this->db->set($data);
	$this->db->insert('areas');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_area_details($area_id)
    {
        $qr = $this->db->select('*')
                ->from('areas')
                ->where('area_id',$area_id);
        $query = $this->db->get();
        return $query->result_array();
    }
	function check_area($area_name)
    {
        $qr = $this->db->select('*')
                ->from('areas')
                ->where('area_name',$area_name);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_area($data,$area_id)
    {
        $this->db->where('area_id',$area_id);
        $this->db->update('areas',$data);
    }
    function delete_area($area_id)
    {
        $this->db->where('area_id',$area_id);
        $this->db->delete('areas');
    }
    function get_flats()
    {
        $qr = $this->db->select('*')->from('flats');
	$query = $this->db->get();
	return $query->result_array();
    }
    function add_flats($data)
    {
        $this->db->set($data);
        $this->db->insert('flats');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_flat_details($flat_id)
    {
        $qr = $this->db->select('*')
                ->from('flats')
                ->where('flat_id',$flat_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_flats($data,$flat_id)
    {
        $this->db->where('flat_id',$flat_id);
        $this->db->update('flats',$data);
    }
    function delete_flat($flat_id)
    {
        $this->db->where('flat_id',$flat_id);
        $this->db->delete('flats');
    }
    function get_services()
    {
        $qr = $this->db->select('*')->from('service_types')->where('service_category','C');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_all_services()
    {
        $this->db->select('*')
            ->from('service_types')
            ->where('service_category','C')
            ->where('service_type_status','1');
        $query = $this->db->get();
        return $query->result();
    }
    function add_services($data)
    {
        $this->db->set($data);
        $this->db->insert('service_types');
        $this->db->insert_id();
    }
    function get_service_details($service_id)
    {
        $this->db->select('*')->from('service_types')->where('service_type_id',$service_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_services($data,$service_id)
    {
        $this->db->where('service_type_id',$service_id);
        $this->db->update('service_types',$data);
    }
	function update_souqmaid_price($data,$service_id)
    {
        $this->db->where('id',$service_id);
        $this->db->update('offer_price',$data);
    }
    function delete_services($service_id)
    {
        $this->db->where('service_type_id',$service_id);
        $this->db->delete('service_types');
    }
    function get_tablets()
    {
        $this->db->select('*')->from('tablets')->join('zones','zones.zone_id = tablets.zone_id');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_tablet($data)
    {
        $this->db->set($data);
	$this->db->insert('tablets');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_tablet_details($tablet_id)
    {
        $qr = $this->db->select('*')
                ->from('tablets')
                ->where('tablet_id',$tablet_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_tablet($data,$tablet_id)
    {
        $this->db->where('tablet_id',$tablet_id);
        $this->db->update('tablets',$data);
    }
    function disable_status($tablet_id,$data)
    {
        $this->db->where('tablet_id',$tablet_id);
        $this->db->update('tablets',$data);
    }
    function activate_status_check($tablet_id)
    {
        $qr = $this->db->select('zone_id')
        ->from('tablets')
        ->where('tablet_id',$tablet_id);
        $query = $this->db->get();
        $qry = $query->result_array();
        // return $qry;
        //die();
        $q = $this->get_active_tablets($qry[0]['zone_id']);
        return $q;
    }
    function activate_status($tablet_id,$data)
    {
        $this->db->where('tablet_id',$tablet_id);
        $this->db->update('tablets',$data);
    }
      function get_prices()
    {
        $qr = $this->db->select('*')->from('payment_settings');
        $query = $this->db->get();
//        return $query->result_array();
        return $query->result();
    }
    
     function get_hourly_price_details($service_id)
    {
        $this->db->select('*')->from('payment_settings')->where('ps_id',$service_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
     function update_hourly_charges($data,$service_id)
    {
        $this->db->where('ps_id',$service_id);
        $this->db->update('payment_settings',$data);
    }
    
      function add_hrly_price($data)
    {
        $this->db->set($data);
        $this->db->insert('payment_settings');
        $this->db->insert_id();
    }
    
    
     function delete_hrly_price($service_id)
    {
        $this->db->where('ps_id',$service_id);
        $this->db->delete('payment_settings');
    }
    
         function get_sms_info()
    {
        $qr = $this->db->select('*')->from('sms_settings');
        $query = $this->db->get();
        return $query->result_array();
    }
        function get_sms_details($sms_id)
    {
        $this->db->select('*')->from('sms_settings')->where('id',$sms_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
       function update_sms_settings($data,$sms_id)
    {
        $this->db->where('id',$sms_id);
        $this->db->update('sms_settings',$data);
    }
  
    function update_email_settings($data,$e_id)
    {
        $this->db->where('id',$e_id);
        $this->db->update('email_settings',$data);
    }
    
    function update_tax_settings($data,$t_id)
    {
        $this->db->where('tax_id',$t_id);
        $this->db->update('tax_settings',$data);
    }
    
    function get_email_info()
    {
        $qr = $this->db->select('*')->from('email_settings');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_tax_info()
    {
        $qr = $this->db->select('*')->from('tax_settings');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_email_details($e_id)
    {
        $this->db->select('*')->from('email_settings')->where('id',$e_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_tax_details($t_id)
    {
        $this->db->select('*')->from('tax_settings')->where('tax_id',$t_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_timezone_info()
    {
                $qr = $this->db->select('*')->from('time_zone_settings');
        $query = $this->db->get();
        return $query->result();
     
    }
    
    function get_customer_per_hrprices($customerid)
    {
        $this->db->select('*')->from('customers');
        $this->db->where('customer_id',$customerid);
        $query = $this->db->get();
        return $query->row();
    }
    
    /*odoo*/
    function get_all_zones_odoo()
    {
        $this->db->select('*')
                ->from('zones')
                ->where('zone_status',1)
                ->where('odoo_sync_status',0);
        
	$query = $this->db->get();
	return $query->result();
    }
    
    function get_areas_odoo()
    {
        $qr = $this->db->select('a.*,z.odoo_zone_id')
                ->from('areas a')
                ->join('zones z','z.zone_id = a.zone_id')
                ->where('a.area_status',1)
                ->where('a.odoo_sync_status',0);
	$query = $this->db->get();
	return $query->result();
    }
    
    function get_zone_odoo_id($zoneid)
    {
        $this->db->select('*')
                ->from('zones')
                ->where('zone_id',$zoneid);
        
	$query = $this->db->get();
	return $query->row();
    }
	
	function get_coupons()
    {
        $this->db->select('*')
                ->from('coupon_code')
				->where('type','C')
				->order_by('added_date','DESC');
        
		$query = $this->db->get();
		return $query->result();
    }
	
	function get_coupon_by_id($e_coupon_id)
    {
        $this->db->select('*')
                ->from('coupon_code')
                ->where('coupon_id',$e_coupon_id);
        
		$query = $this->db->get();
		return $query->row();
    }
	
	function add_coupon($data)
	{
		$this->db->set($data);
		$this->db->insert('coupon_code');
        $result = $this->db->insert_id();
        return $result;
	}
	
	function update_coupon($coupon_id,$data)
	{
		$this->db->where('coupon_id', $coupon_id);
        $this->db->update('coupon_code', $data);
        return $this->db->affected_rows();
	}

    function check_coupon_customer($customer_id,$coupon_id)
    {
        $this->db->select('*')
                ->from('customer_coupons')
                ->where('customer_id',$customer_id)
                ->where('coupon_id',$coupon_id);

        
        return $this->db->affected_rows();
    }
	
	function get_souqmaid_pricelist()
    {
        $qr = $this->db->select('*')->from('offer_price')->order_by('offer_date','DESC')->limit(20);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	function get_offer_list($date)
	{
		$this->db->select('*')
                ->from('offer_price')
                ->where('offer_date',$date);
        
		$query = $this->db->get();
		return $query->row();
	}
	
	function add_souqmaid_price($data)
	{
		$this->db->set($data);
		$this->db->insert('offer_price');
        $result = $this->db->insert_id();
        return $result;
	}
	
	function get_souqmaid_pricelist_byid($price_id)
    {
        $this->db->select('*')->from('offer_price')->where('id',$price_id);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	function get_area_id_name($area)
    {
        $this->db->select('area_id')
                ->from('areas')
                ->where('area_name',$area)
                ->where('area_status',1);
        $get_area_id_name_qry = $this->db->get();

        return $get_area_id_name_qry->row();
    }
	
	function get_zones_api()
    { 
		$qr = $this->db->select('*')
			->where('odoo_new_zone_status',0)
			->from('zones');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function get_areas_api()
    { 
		$qr = $this->db->select('areas.*,zones.odoo_new_zone_id as odoo_zone_id,zones.odoo_new_zone_status as statuss')
						->from('areas')
						->where('areas.odoo_new_area_status',0) 
						->join('zones','zones.zone_id = areas.zone_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function get_flats_api()
    {
        $qr = $this->db->select('*')
                    ->from('flats')
                    ->where('odoo_new_syncflat_status',0)
                    ->where('flat_status',1);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_categories()
    {
        $qr = $this->db->select('*')->from('complaint_category')->where('status',1);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function add_complaint_category($data)
    {
        $this->db->set($data);
        $this->db->insert('complaint_category');
        $result = $this->db->insert_id();
        return $result;
    }

    function add_complaint($data)
    {
        $this->db->set($data);
        $this->db->insert('complaint');
        $result = $this->db->insert_id();
        return $result;
    }
    function update_complaint($data,$com_id)
    {
        $this->db->where('cmp_id',$com_id);
        $this->db->update('complaint',$data);

    }
    function update_complaint_category($data,$com_cat_id)
    {
        $this->db->where('category_id',$com_cat_id);
        $this->db->update('complaint_category',$data);
    }
    
    function get_category_details($category_id)
    {
        $qr = $this->db->select('*')
                ->from('complaint_category')
                ->where('category_id',$category_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function delete_category_new($data,$category_id)
    {
        $this->db->where('category_id',$category_id);
        $this->db->update('complaint_category',$data);
        return 0;
    }
    function get_complaints_by_date($vehicle_date, $vehicle_date_to,$complaint_status,$customer_id){
        $this->db->select("complaint.*,customers.customer_name,users.user_fullname,day_services.service_status,bookings.time_from,bookings.time_to,complaint_category.category_name", FALSE)
        ->from('complaint')
        ->join('complaint_category', 'complaint_category.category_id = complaint.cmp_ticket_cat')
        ->join('bookings', 'bookings.booking_id = complaint.cmp_booking_id')
        ->join('customers', 'customers.customer_id = complaint.cmp_customer_id')
        ->join('users', 'users.user_id = complaint.cmp_added_by','left')
        ->join('day_services', 'day_services.day_service_id = complaint.cmp_day_service_id','left');

        if ($complaint_status!='') {
            $this->db->where('complaint.cmp_ticket_status', $complaint_status);
        }

        if (is_numeric($customer_id)&&$customer_id!=0) {
            $this->db->where('complaint.cmp_customer_id', $customer_id);
        }

        $this->db->order_by('complaint.cmp_ticket_service_date');

        $query = $this->db->get();
        
        return $query->result();
    }

    function get_complaints_details($complaint_id){
        $this->db->select("complaint.*,customers.customer_name,customers.email_address,customers.mobile_number_1,customer_addresses.customer_address,users.user_fullname,day_services.service_status,bookings.booking_type,bookings.time_from,bookings.time_to,complaint_category.category_name,areas.area_name", FALSE)
        ->from('complaint')
        ->join('complaint_category', 'complaint_category.category_id = complaint.cmp_ticket_cat')
        ->join('bookings', 'bookings.booking_id = complaint.cmp_booking_id')
        ->join('customers', 'customers.customer_id = complaint.cmp_customer_id')
        ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
        ->join('areas', 'areas.area_id = customer_addresses.area_id')
        ->join('users', 'users.user_id = complaint.cmp_added_by','left')
        ->join('day_services', 'day_services.day_service_id = complaint.cmp_day_service_id','left');
        $this->db->where('complaint.cmp_id', $complaint_id);

        $query = $this->db->get();
        
        return $query->row();
    }
    function update_complaint_stat($complaint_id,$complaint_status)
    {
        $this->db->set('cmp_ticket_status', $complaint_status);
        $this->db->where('cmp_id', $complaint_id);
        $this->db->update('complaint');
        return $this->db->affected_rows();
    }

    function get_complaint_comments($complaint_id)
    {
        $this->db->select("complaint_comments.*,users.user_fullname", FALSE)
        ->from('complaint_comments')
        ->join('users', 'users.user_id = complaint_comments.cmnt_added_by');
        $this->db->where('complaint_comments.cmnt_complaint_id', $complaint_id);
        $query = $this->db->get();        
        return $query->result();
    }

    function add_ticket_comment($data)
    {
        $this->db->set($data);
        $this->db->insert('complaint_comments');
        $result = $this->db->insert_id();
        return $result;
    }

    function add_aggregator($data)
    {
        $this->db->set($data);
        $this->db->insert('aggregator');
        $result = $this->db->insert_id();
        return $result;
    }

    function get_aggregators()
    {
        $qr = $this->db->select('*')
                ->from('aggregator');
                //->where('area_status', 1)
                //->join('zones','zones.zone_id = areas.zone_id');
    $query = $this->db->get();
    return $query->result_array();
    }

    function get_aggregator_details($agg_id)
    {
        $qr = $this->db->select('*')
                ->from('aggregator')
                ->where('agg_id',$agg_id);
        $query = $this->db->get();
        return $query->row();
    }

    function update_aggregator($data,$agg_id)
    {
        $this->db->where('agg_id',$agg_id);
        $this->db->update('aggregator',$data);

        return true;
    }

    function get_active_aggregators()
    {
        $qr = $this->db->select('*')
                ->from('aggregator')
                ->where('agg_status', 1);
                //->join('zones','zones.zone_id = areas.zone_id');
        $query = $this->db->get();
        return $query->result();
    }

    function add_incentive($data)
    {
        $this->db->set($data);
        $this->db->insert('incentives');
        $result = $this->db->insert_id();
        return $result;
    }


    function get_active_incentives_by_date($date = NULL,$weekly_booking_ids) {
        if (!$date) {
            $date = date('Y-m-d');
        }

        
        $this->db->select('*');
        $this->db->from("incentives");
        $this->db->where('incentive_service_date', $date);
        $this->db->where('incentive_status', '1');
        $query1 = $this->db->get()->result();

        if(!$weekly_booking_ids)
        return [];
        $this->db->select('*')
                ->from('incentives')
                ->where('incentive_for', '1')
                ->where('incentive_status', '1')
                ->where_in('incentive_booking_id', $weekly_booking_ids);
        $query2 = $this->db->get()->result();
                
        $query = array_merge($query1, $query2);
        return $query;
    }

    function get_active_incentives_by_date_new($date = NULL,$date_to = NULL,$weekly_booking_ids,$incentive_for = NULL) {
        if (!$date) {
            $date = date('Y-m-d');
        }

        
        $this->db->select('*');
        $this->db->from("incentives");
        $this->db->where('incentive_service_date >=', $date);
        $this->db->where('incentive_service_date <=', $date_to);
        $this->db->where('incentive_status', '1');
        if (is_numeric($incentive_for)) {
            $this->db->where('incentive_for', $incentive_for);
        }
        $query1 = $this->db->get()->result();


        if(!$weekly_booking_ids)
        return [];
        if (is_numeric($incentive_for)) {
            $this->db->select('*')
                ->from('incentives')
                ->where('incentive_status', '1')
                ->where('incentive_for',  $incentive_for)
                ->where_in('incentive_booking_id', $weekly_booking_ids);
            
        }
        else
        {
            $this->db->select('*')
                ->from('incentives')
                ->where('incentive_status', '1')
                //->where('incentive_for', '1')
                ->where_in('incentive_booking_id', $weekly_booking_ids);
        }
        $query2 = $this->db->get()->result();


        if (is_numeric($incentive_for)&&$incentive_for==0) {
            return $query1;
        }  
              
        $query = array_merge($query1, $query2);
        return $query;
    }


    function get_incentives_by_bookingid($bookingid) {
        

        $qr = $this->db->select('*')
                ->from('incentives')
                ->where('incentive_booking_id', $bookingid);

        $query = $this->db->get();
        return $query->result();
    }

    function get_active_incentives_by_bookingid($bookingid) {
        

        $qr = $this->db->select('*')
                ->from('incentives')
                ->where('incentive_booking_id', $bookingid)
                ->where('incentive_status', '1');

        $query = $this->db->get();
        return $query->result();
    }

    function get_crew_incentive_by_bid($bookingid) {
        

        $qr = $this->db->select('*')
                ->from('incentives')
                ->where('incentive_for', '0')
                ->where('incentive_booking_id', $bookingid);

        $query = $this->db->get();
        return $query->result();
    }

    function get_aggregator_incentive_by_bid($bookingid) {
        

        $qr = $this->db->select('*')
                ->from('incentives')
                ->where('incentive_for', '1')
                ->where('incentive_booking_id', $bookingid);

        $query = $this->db->get();
        return $query->result();
    }

    function update_incentive($data,$inc_id)
    {
        $this->db->where('incentive_id',$inc_id);
        $this->db->update('incentives',$data);

        return true;
    }

    function update_crew_incentive_by_bid($incentive_data,$booking_id)
    {
        $this->db->where('incentive_booking_id',$booking_id);
        $this->db->where('incentive_for','0');
        $this->db->update('incentives',$incentive_data);
    }

    function update_aggregator_incentive_by_bid($incentive_data,$booking_id)
    {
        $this->db->where('incentive_booking_id',$booking_id);
        $this->db->where('incentive_for','1');
        $this->db->update('incentives',$incentive_data);
    }
    function get_active_tablets($zoneId)
    {
        $qr = $this->db->select('*')
        ->from('tablets')
        ->where('tablet_status', 1)
        ->where('zone_id', $zoneId);
        $query = $this->db->get();
        return $query->num_rows();
      
    }
    function get_active_edit_tablets($zoneId,$tabletId)
    {
        $qr = $this->db->select('*')
        ->from('tablets')
        ->where('tablet_status', 1)
        ->where('tablet_id','!=', $tabletId)
        ->where('zone_id', $zoneId);
        $query = $this->db->get();
        return $query->num_rows();
      
    }
	
	function get_tablet_list()
    {
        $qr = $this->db->select('tablet_id,tablet_driver_name')
					->from('tablets')
					->where('tablet_status',1);
		$query = $this->db->get();
		return $query->result();
    }
	
	function get_tablets_by_status($status)
    {
        $this->db->select('*')->from('tablets')->join('zones','zones.zone_id = tablets.zone_id')->where('tablets.tablet_status',$status);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_complaints_by_datee($vehicle_date, $vehicle_date_to,$complaint_status,$customer_id){
        $this->db->select("complaint.*,customers.customer_name,users.user_fullname,day_services.service_status,bookings.time_from,bookings.time_to,complaint_category.category_name", FALSE)
        ->from('complaint')
        ->join('complaint_category', 'complaint_category.category_id = complaint.cmp_ticket_cat')
        ->join('bookings', 'bookings.booking_id = complaint.cmp_booking_id')
        ->join('customers', 'customers.customer_id = complaint.cmp_customer_id')
        ->join('users', 'users.user_id = complaint.cmp_added_by','left')
        ->join('day_services', 'day_services.day_service_id = complaint.cmp_day_service_id','left');

        if ($complaint_status!='') {
            $this->db->where('complaint.cmp_ticket_status', $complaint_status);
        }

        if (is_numeric($customer_id)&&$customer_id!=0) {
            $this->db->where('complaint.cmp_customer_id', $customer_id);
        }
        if( (isset($vehicle_date) != '')) {
            $this->db->where('complaint.cmp_created_datetime >=', $vehicle_date);
        }
         if( (isset($vehicle_date_to) != '')) {
            $this->db->where('complaint.cmp_created_datetime <=', $vehicle_date_to);
        }              

        $this->db->order_by('complaint.cmp_ticket_service_date');

        $query = $this->db->get();
        
        return $query->result();
    }

}