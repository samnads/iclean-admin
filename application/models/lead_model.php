<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lead_model extends CI_Model{
	
	function get_leads_list($date_from,$date_to,$leadtype,$timelines,$service,$source,$leadstatus)
    {
        $this->db->select('l.*,s.service_type_name')
                ->from('lead_management_list l')
				->join('service_types s','l.service_type = s.service_type_id')
				->order_by('added_date_time','DESC');
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("l.added_date_time BETWEEN '$date_from' AND '$date_to'");
		}
		if($leadtype != NULL)
		{
			$this->db->where('l.lead_type', $leadtype);
		}
		if($timelines != NULL)
		{
			$this->db->where('l.timeline', $timelines);
		}
		if($service != NULL)
		{
			$this->db->where('l.service_type', $service);
		}
		if($source != NULL)
		{
			$this->db->where('l.source', $source);
		}
		if($leadstatus != NULL)
		{
			if($leadstatus == 2)
			{
				$this->db->where('l.lead_status', 1);
			} else {
				$this->db->where('l.status', $leadstatus);
			}
		}
        
		$query = $this->db->get();
		return $query->result();
    }
	
	function get_lead_details($lead_id)
	{
		$this->db->select('l.*,s.service_type_name')
                ->from('lead_management_list l')
				->join('service_types s','l.service_type = s.service_type_id')
				->where('l.lead_id',$lead_id)
				->order_by('l.added_date_time','DESC');
        
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_services()
	{
		$this->db->select('*')
                ->from('service_types')
				->where('service_type_status',1);
        
		$query = $this->db->get();
		return $query->result();
	}
	
	function add_lead($data)
	{
		$this->db->set($data);
        $this->db->insert('lead_management_list');
        return $this->db->insert_id();
	}
	
	function update_lead($lead_id,$lead_fields = array())
	{
		$this->db->where('lead_id', $lead_id);
		$this->db->update('lead_management_list', $lead_fields);
		return $this->db->affected_rows();
	}

	function delete_lead($lead_id)
	{
		$this->db->where('lead_id',$lead_id);
        $this->db->delete('lead_management_list');
    }
}