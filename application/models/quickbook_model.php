<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickbook_model extends CI_Model{
    function add_quick_customers($data)
    {
        $this->db->set($data);
		$this->db->insert('quickbook_customer');
        $result = $this->db->insert_id();
        return $result;
    }
	
	function update_quickbookdetails($data,$id){
        $this->db->where('id', $id);
        $this->db->update('quickbook_details', $data);
    }
	
	function get_quickbookdetails($id)
	{
		$this->db->select('access_token,refresh_token')
                ->from('quickbook_details')
				->where('id',$id);
		$get_qry = $this->db->get();
        return $get_qry->row();
	}
	
	function get_customers_detail($customer_id)
    {
        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.is_company, c.company_name, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.email_address, c.contact_person, c.customer_trn, c.balance, c.quickbook_id, c.quickbook_sync_status, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.quickbook_sync_status', 0);
        $this->db->where('c.customer_id', $customer_id);
        $this->db->where('ca.default_address', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function update_quickbook_cust_sync_msg($data,$id){
        $this->db->where('tmp_msg_id', $id);
        $this->db->update('qb_sync_tmp_msg', $data);
    }

    function get_quickbook_cust_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 1);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_inv_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 2);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_inv_pay_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 4);
        $this->db->where('tmp_msg_stat', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    function get_quickbook_quo_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 3);
        $this->db->where('tmp_msg_stat', 1);
        $get_quickbook_quo_sync_msg_qry = $this->db->get();
        return $get_quickbook_quo_sync_msg_qry->row();
    }

    function get_invoice_detailbyid($invoice_id)
    {
        $this->db->select('c.total_invoice_amount,c.balance,c.total_paid_amount,c.customer_trn,c.email_address,c.quickbook_id,i.invoice_id,i.invoice_num,i.customer_name,i.customer_id,i.bill_address,i.invoice_status, i.invoice_paid_status, i.added, i.service_date, i.invoice_date, i.invoice_due_date, i.invoice_total_amount, i.invoice_tax_amount, i.invoice_net_amount, i.balance_amount, i.received_amount,,i.invoice_qb_id,i.invoice_qb_sync_stat, l.maid_name, l.service_from_time, l.service_to_time, l.line_bill_address, l.description, l.line_amount, l.line_vat_amount, l.line_net_amount,l.invoice_line_id,l.service_hrs,l.monthly_product_service, u.user_fullname,st.service_type_name,st.qb_sync_id,st.qb_sync_stat,c.customer_id as custid')
                ->from('invoice i')
                ->join('invoice_line_items l','i.invoice_id = l.invoice_id','INNER')
                ->join('day_services ds','ds.day_service_id = l.day_service_id','LEFT')
                ->join('bookings b','b.booking_id = ds.booking_id','LEFT')
                ->join('service_types st','st.service_type_id = b.service_type_id','LEFT')
                ->join('customers c','c.customer_id = i.customer_id')
                ->join('users u','i.invoice_added_by = u.user_id','LEFT')
                ->where('i.invoice_id',$invoice_id)
                ->where('l.line_status',1);
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_invoice_payment_by_invnum($invnum)
    {
        $this->db->select('ipm.*')
                ->from('InvoicePaymentMapping ipm')
                ->where('ipm.inv_reference',$invnum)
                ->where('ipm.qb_sync_stat','0');
        $get_invoice_payment_by_invnum_qry = $this->db->get();
        return $get_invoice_payment_by_invnum_qry->result();
    }

    function update_invoice_payment_by_ipmid($invoice,$fields = array())
    {
        $this->db->where('inv_map_id', $invoice);
        $this->db->update('InvoicePaymentMapping', $fields);
        return $this->db->affected_rows();
    }

    function update_invoicepay_detail($invoice,$fields = array())
    {
        $this->db->where('invoice_id', $invoice);
        $this->db->update('invoice', $fields);
        return $this->db->affected_rows();
    }

    function get_customer_invoices_for_qb_sync($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        //$this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_customer_invoices_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        //$this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        //$this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }

    function get_customer_reciepts_for_qb_sync($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
    }


    function get_customer_reciepts_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('i.*,c.email_address')
                ->from('invoice i')
                ->join('customers c','c.customer_id = i.customer_id');

        $this->db->where('i.invoice_type', 1);
        if($date_from != NULL)
        {
            $this->db->where('i.invoice_date', $date_from);
        }        
        $this->db->where('i.invoice_qb_sync_stat', '0');
        $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        $this->db->where('i.invoice_status !=', '0');
        //$this->db->limit(10); 

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }

    function get_customer_quotations_for_qb_sync($date_from= NULL)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_qb_sync_stat', 0);
        if($date_from != NULL)
        {
            $this->db->where('q.quo_date', $date_from);
        }        
        // $this->db->where('i.invoice_qb_sync_stat', '0');
        // $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        // $this->db->where('i.invoice_status !=', '0');
        $this->db->limit(10); 

        $get_quotation_qry = $this->db->get();
        return $get_quotation_qry->result();
    }


    function get_customer_quotations_for_qb_sync_count($date_from= NULL)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_qb_sync_stat', 0);
        if($date_from != NULL)
        {
            $this->db->where('q.quo_date', $date_from);
        }        
        // $this->db->where('i.invoice_qb_sync_stat', '0');
        // $this->db->where('i.invoice_status !=', '2');//to avoid loading cancelled invoices on default load
        // $this->db->where('i.invoice_status !=', '0');
        

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->num_rows();
    }



    function get_quotation_detailbyid($quotation_id)
    {
        $this->db->select('q.*,c.email_address,c.customer_name,c.quickbook_id,st.service_type_name')
                ->from('quotation q')
                ->join('customers c','c.customer_id = q.quo_customer_id')
                ->join('service_types st','st.service_type_id = q.quo_servicetype_id','LEFT');

        $this->db->where('q.quo_id', $quotation_id);        

        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->row();
    }

    function update_quotation($quo_id,$fields = array())
    {
        $this->db->where('quo_id', $quo_id);
        $this->db->update('quotation', $fields);
        return $this->db->affected_rows();
    }

    function get_invoice_payments_for_qb_sync($date)
    {

        $this->db->select('ipm.*,cp.*,c.customer_name')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id');
        

        
        $this->db->where("date(ipm.payment_added_datetime)", $date);
        $this->db->where("ipm.qb_sync_stat", 0);
        $this->db->where("ipm.map_status", 1);
        $this->db->group_by('ipm.inv_map_id');
        
        $get_pay_qry = $this->db->get();
        return $get_pay_qry->result();
    }



    function get_invoice_payments_for_qb_sync_count($date)
    {

        $this->db->select('ipm.*,cp.*,c.customer_name')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id');
        

        
        $this->db->where("date(ipm.payment_added_datetime)", $date);
        $this->db->where("ipm.qb_sync_stat", 0);
        $this->db->where("ipm.map_status", 1);
        $this->db->group_by('ipm.inv_map_id');
        
        
        $get_pay_qry = $this->db->get();
        return $get_pay_qry->num_rows();
    }

    function get_invoice_payment_detailbyid($ipm_id)
    {
        $this->db->select('ipm.*,cp.*,c.customer_name,c.quickbook_id as cust_quickbook_id ,inv.invoice_qb_sync_stat,inv.invoice_qb_id')
                ->from('InvoicePaymentMapping ipm')
                ->join('customer_payments cp','cp.customer_id = ipm.inv_customer_id')
                ->join('customers c','c.customer_id = ipm.inv_customer_id')
                ->join('invoice inv','inv.invoice_id = ipm.invoiceId');

        $this->db->where("ipm.inv_map_id", $ipm_id);
        $this->db->group_by('ipm.inv_map_id');

        $get_pay_qry = $this->db->get();
        return $get_pay_qry->row();
    }


    public function fetch_service_types_for_quickbook_sync() {

        $this->db->select("st.*")
                ->from('service_types st')
                ->where('st.service_type_status','1')
                ->where('st.qb_sync_stat','0');

        $this->db->limit(10); 
        $query = $this->db->get();

        return $query->result();
    }


    public function fetch_service_types_for_quickbook_count() {

        $this->db->select("st.*")
                ->from('service_types st')
                ->where('st.service_type_status','1')
                ->where('st.qb_sync_stat','0');

        
        $query = $this->db->get();

        return $query->num_rows();
    }

    function get_service_details($service_id)
    {
        $this->db->select('*')->from('service_types')->where('service_type_id',$service_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_quickbook_servicetype_sync_msg()
    {
        $this->db->select('*')
                ->from('qb_sync_tmp_msg');

        $this->db->where('tmp_msg_id', 5);
        $this->db->where('tmp_msg_stat', 1);
        $get_quickbook_servicetype_sync_msg_qry = $this->db->get();
        return $get_quickbook_servicetype_sync_msg_qry->row();
    }
}