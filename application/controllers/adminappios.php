<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminappios extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('adminappios_model');
		$this->load->helper('string');
    }

    public function index() {
        $response               = array();
        $response['status']     = 'error';
        $response['error_code'] = '101';
        $response['message']    = 'Invalid request';

        echo json_encode($response);
        exit();
    }
	
	public function userlogin()
	{ 
		if($this->input->get('user_name') && $this->input->get('password') && strlen(trim($this->input->get('user_name'))) > 0  && strlen(trim($this->input->get('password'))) > 0 )
		{
                   
			$user_name = trim($this->input->get('user_name'));
			$password = trim($this->input->get('password'));
			$user = $this->adminappios_model->check_ipad_user($user_name, $password);			
				
			if(!empty($user) )
			{
				echo json_encode(array('status' => 'success','user_id'=>$user->user_id));
				exit();
			} 
			else 
			{
				$response = array();
				$response['status'] = 'error';
				$response['error_code'] = '103';
				$response['message'] = 'Invalid user!';

				echo json_encode($response);
				exit();	 
			}
			
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';
			
			echo json_encode($response);
			exit();	
		}
			
	}
	
	public function gettodayplanoractivity()
	{
		$response = array(); 
		$response['status'] = 'success'; 
            
		$response['plan']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d'), 1);
		$response['plan']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d'), 1);
		$response['plan']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d'), 1);
		$response['plan']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d'), 1);
					
		$response['activity']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d'), 2);
		$response['activity']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d'), 2);
		$response['activity']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d'), 2);
		$response['activity']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d'), 2);
		$response['activity']['Total_Cancellation'] = $this->adminappios_model->get_total_cancellation(date('Y-m-d'));
		//$response['activity']['Total_Invoice_amount'] = $this->adminappios_model->get_total_invoice_amount(date('Y-m-d'));
		$response['activity']['Day_Collection'] = $this->adminappios_model->get_day_collection(date('Y-m-d'));
		$response['activity']['Total_PNR'] = abs($response['activity']['Total_Booking_Hours'] - $response['activity']['Day_Collection']);
		//$response['activity']['Pending_Received'] = 'NA';
		//$response['activity']['Total_Pending'] = $this->adminappios_model->get_total_pending_amount(date('Y-m-d'));
		$response['activity']['New_Customer'] = $this->adminappios_model->get_new_customer(date('Y-m-d'));
         
		echo json_encode($response);
		exit();	
	}
	
	public function getpastactivity()
	{
		if($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0 )
	    {
			$date = trim($this->input->get('date'));
			$response = array();
			$response['status'] = 'success';
                              
			$response['activity']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 2);
			$response['activity']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d', strtotime($date)), 2);
			$response['activity']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 2);
			$response['activity']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 2);
			$response['activity']['Total_Cancellation'] = $this->adminappios_model->get_total_cancellation(date('Y-m-d', strtotime($date)));
			//$response['activity']['Total_Invoice_amount'] = $this->adminappios_model->get_total_invoice_amount(date('Y-m-d', strtotime($date)));
			$response['activity']['Day_Collection'] = $this->adminappios_model->get_day_collection(date('Y-m-d', strtotime($date)));
			$response['activity']['Total_PNR'] = abs($response['activity']['Total_Booking_Hours'] - $response['activity']['Day_Collection']);
			//$response['activity']['Pending_Received'] = 'NA';
			//$response['activity']['Total_Pending'] = $this->adminappios_model->get_total_pending_amount(date('Y-m-d', strtotime($date)));
			$response['activity']['New_Customer'] = $this->adminappios_model->get_new_customer(date('Y-m-d', strtotime($date)));
			echo json_encode($response);
			exit();	
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';

			echo json_encode($response);
			exit();	
		}
	}
	
	public function getfutureplan()
	{
		if($this->input->get('date') && strlen(trim($this->input->get('date'))) > 0 )
	    {
			$date = trim($this->input->get('date'));
			$response = array();  
			$response['status'] = 'success';  
			
			$response['plan']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d', strtotime($date)), 1);
			$response['plan']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d', strtotime($date)), 1);
			$response['plan']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d', strtotime($date)), 1);
			$response['plan']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d', strtotime($date)), 1);
			echo json_encode($response);
			exit();	
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';

			echo json_encode($response);
			exit();	
		}
	}
	
	public function getreportbwdates()
	{
		if($this->input->get('fromdate') && $this->input->get('todate') && strlen(trim($this->input->get('fromdate')))  && strlen(trim($this->input->get('todate'))) > 0 )
		{
			$fromdate = trim($this->input->get('fromdate'));
			$todate = trim($this->input->get('todate'));
			
			$response = array();            

			$response['status'] = 'success';               
			
			$response['activity']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));  
			$response['activity']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['Total_Cancellation'] = $this->adminappios_model->get_total_cancellation(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			//$response['activity']['Total_Invoice_amount'] = $this->adminappios_model->get_total_invoice_amount(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['Day_Collection'] = $this->adminappios_model->get_day_collection(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['Total_PNR'] = abs($response['activity']['Total_Booking_Hours'] - $response['activity']['Day_Collection']);
			//$response['activity']['Pending_Received'] = 'NA';
			//$response['activity']['Total_Pending'] = $this->adminappios_model->get_total_pending_amount(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['New_Customer'] = $this->adminappios_model->get_new_customer(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));


			echo json_encode($response);
			exit();	
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';

			echo json_encode($response);
			exit();	
		}
	}
	
	public function changepassword()
	{
		if($this->input->get('old_password') && $this->input->get('new_password') && strlen(trim($this->input->get('old_password'))) > 0  && strlen(trim($this->input->get('new_password'))) > 0 )
		{
			$old_password = trim($this->input->get('old_password'));
			$new_password = trim($this->input->get('new_password'));
			$user = $this->adminappios_model->change_password($old_password, $new_password);			
				
			if($user > 0)
			{
				echo json_encode(array('status' => 'success'));
				exit();
			} 
			else 
			{
				$response = array();
				$response['status'] = 'error';
				$response['error_code'] = '103';
				$response['message'] = 'Invalid password!';

				echo json_encode($response);
				exit();	 
			}
			
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';
			
			echo json_encode($response);
			exit();	
		}
			
	}
	
	function gettabletlocations() 
	{
		$location_dtls = $this->adminappios_model->get_tablet_locations();
		$responseArr['locations'] = array();
		if (!empty($location_dtls)) 
		{
			foreach ($location_dtls as $row)
			{
				$data = array();
				
				$data['tablet_id'] = $row->tablet_id;
				//$data['zone'] = $row->zone_name;
				$data['zone'] = $row->zone_nick;
				$data['latitude'] = $row->latitude;                    
				$data['longitude'] = $row->longitude;
				$data['speed'] = $row->speed;
				$data['added'] = $row->added;                  
			   
				array_push( $responseArr['locations'], $data);
			} 
			echo json_encode(array('status' => 'success', 'locations' =>  $responseArr['locations']));
			exit();
		} else {
			echo json_encode(array('status' => 'success', 'message' => 'No locations'));
			exit();
		}
	}
	
	public function getmonthlysummary()
	{
		if($this->input->get('month') && strlen(trim($this->input->get('month')))> 0 )
	    {
			$response = array(); 
			$response['status'] = 'success';
			$param = explode("-",$this->input->get('month'));
			$month = $param[0];
			$year = $param[1];        
			//$month = is_numeric($this->input->get('month')) ? $this->input->get('month') : date('m', strtotime($this->input->get('month')));                                
			$month = date('F', mktime(0,0,0,$month, 12));                
			$fromdate = date("Y-m-d", strtotime("First DAY OF $month ".$year));                
			$todate = date("Y-m-d", strtotime("last DAY OF $month ".$year));
                
			$response = array(); 
			$response['status'] = 'success';               
			
			$response['activity']['Total_Booking_Hours'] = $this->adminappios_model->get_total_booking_hours(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['Total_Bookings'] = $this->adminappios_model->get_total_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['One_Day_Booking'] = $this->adminappios_model->get_one_day_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['Weekend_Booking'] = $this->adminappios_model->get_week_day_bookings(date('Y-m-d', strtotime($fromdate)), 2, date('Y-m-d', strtotime($todate)));
			$response['activity']['Total_Cancellation'] = $this->adminappios_model->get_total_cancellation(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			//$response['activity']['Total_Invoice_amount'] = $this->adminappios_model->get_total_invoice_amount(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['Day_Collection'] = $this->adminappios_model->get_day_collection(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['Total_PNR'] = abs($response['activity']['Total_Booking_Hours'] - $response['activity']['Day_Collection']);
			//$response['activity']['Pending_Received'] = 'NA';
			//$response['activity']['Total_Pending'] = $this->adminappios_model->get_total_pending_amount(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			$response['activity']['New_Customer'] = $this->adminappios_model->get_new_customer(date('Y-m-d', strtotime($fromdate)), date('Y-m-d', strtotime($todate)));
			echo json_encode($response);
			exit();	
		}
		else
		{
			$response = array();
			$response['status'] = 'error';
			$response['error_code'] = '101';
			$response['message'] = 'Invalid request';

			echo json_encode($response);
			exit();	
		}
	}
	
	public function maid_tracking()
	{
		$location_dtls = $this->adminappios_model->get_maid_locations();
		if (!empty($location_dtls)) {
			echo json_encode(array('status' => 'success', 'locations' =>  $location_dtls));
			exit();
		} else {
			echo json_encode(array('status' => 'success', 'message' => 'No locations'));
			exit();
		}	
	}
}