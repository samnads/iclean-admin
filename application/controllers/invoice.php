<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        /*
        if(!is_user_loggedin())
        {
                if($this->input->is_ajax_request())
                {
                        echo 'refresh';
                        exit();
                }
                else
                {
                        redirect('logout');
                }
        }
        */
//                if(!user_permission(user_authenticate(), 5))
//                {
//                    show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
//                }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('invoice_model');
        $this->load->model('zones_model');
        $this->load->model('bookings_model');
        $this->load->model('day_services_model');
        $this->load->helper('google_api_helper');
        $this->load->model('tablets_model');
        $this->load->model('service_types_model');
        $this->load->model('quotation_model');
        //$this->load->helper('odoo_helper');
    }
    
    public function index()
    {
        if($this->uri->segment(2) && strlen(trim($this->uri->segment(2) > 0)))
        {
            $schedule_date = $this->uri->segment(2);
        } else {
            redirect('activity/jobs');
            exit();
        }
        if($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0)))
        {
            $booking_id = $this->uri->segment(3);		
        }
        else
        {
            redirect('activity/jobs');
            exit();
        }
        
        if($this->uri->segment(4) && strlen(trim($this->uri->segment(4) > 0)))
        {
            $customer_id = $this->uri->segment(4);		
        } else {
            redirect('activity/jobs');
            exit();
        }
        
        $get_total_booking_by_customer = $this->bookings_model->get_total_booking_by_customer($schedule_date,$customer_id);
        
        foreach ($get_total_booking_by_customer as $booking)
        { 
            $chk_day_service = $this->day_services_model->get_day_service_by_booking_id($schedule_date, $booking->booking_id);
            
            if($chk_day_service->service_status != 2)
            {
                redirect('activity/job_view/'.$schedule_date.'/'.$booking_id);
                exit();
            } else {
                //echo 'hai';
                $checkinvoiceexist = $this->invoice_model->checkinvoiceexistbydayserviceid($chk_day_service->day_service_id);
                if(count($checkinvoiceexist) > 0)
                {
                    echo 'hai';
                    //redirect('activity/job_view/'.$schedule_date.'/'.$booking_id);
                    redirect('invoice/view_invoice/'.$checkinvoiceexist->invoice_num);
                    exit();
                }
            }
        }
        
        $all_details = array();
        $i = 0;
        foreach ($get_total_booking_by_customer as $bookings)
        {
            $jobdetail = $this->day_services_model->get_activity_by_date_plan_job_invoice($schedule_date,$bookings->booking_id);
            
            $all_details[$bookings->booking_id] = new stdClass();
            $all_details[$bookings->booking_id]->booking_id = $jobdetail->booking_id;
            $all_details[$bookings->booking_id]->day_service_id = $jobdetail->day_service_id;
            $all_details[$bookings->booking_id]->customer_id = $jobdetail->customer_id;
            $all_details[$bookings->booking_id]->customer_name = $jobdetail->customer_name;
            $all_details[$bookings->booking_id]->customer_address = $jobdetail->customer_address;
            $all_details[$bookings->booking_id]->latitude = $jobdetail->latitude;
            $all_details[$bookings->booking_id]->longitude = $jobdetail->longitude;
            $all_details[$bookings->booking_id]->maid_name = $jobdetail->maid_name;
            $all_details[$bookings->booking_id]->mobile_number_1 = $jobdetail->mobile_number_1;
            $all_details[$bookings->booking_id]->email_address = $jobdetail->email_address;
            $all_details[$bookings->booking_id]->user_fullname = $jobdetail->user_fullname;
            $all_details[$bookings->booking_id]->customer_address_id = $jobdetail->customer_address_id;
            $all_details[$bookings->booking_id]->maid_id = $jobdetail->maid_id;
            $all_details[$bookings->booking_id]->booking_note = $jobdetail->booking_note;
            $all_details[$bookings->booking_id]->time_from = $jobdetail->time_from;
            $all_details[$bookings->booking_id]->time_to = $jobdetail->time_to;
            $all_details[$bookings->booking_id]->start_time = $jobdetail->start_time;
            $all_details[$bookings->booking_id]->end_time = $jobdetail->end_time;
            $all_details[$bookings->booking_id]->booking_type = $jobdetail->booking_type;
            $all_details[$bookings->booking_id]->booking_status = $jobdetail->booking_status;
            $all_details[$bookings->booking_id]->total_fee = $jobdetail->total_fee;
            $all_details[$bookings->booking_id]->material_fee = $jobdetail->material_fee;
            $all_details[$bookings->booking_id]->starting_time = $jobdetail->starting_time;
            $all_details[$bookings->booking_id]->ending_time = $jobdetail->ending_time;
            $all_details[$bookings->booking_id]->collected_amount = $jobdetail->collected_amount;
            $all_details[$bookings->booking_id]->service_status = $jobdetail->service_status;
            $all_details[$bookings->booking_id]->payment_status = $jobdetail->payment_status;
            $all_details[$bookings->booking_id]->ps_no = $jobdetail->ps_no;
            $all_details[$bookings->booking_id]->service_date = $jobdetail->service_date;
            
            $i++;
        }
//        echo '<pre>';
//        print_r($all_details);
//        echo '</pre>';
//        exit();
        
        //$jobdetail = $this->day_services_model->get_activity_by_date_plan_job_view($schedule_date,$booking_id);
        
        $data = array();
        $data['jobdetail'] = $all_details;
        $data['jobdate'] = date('d F Y', strtotime($schedule_date));
        $data['scheduledates'] = $schedule_date;
        $data['bookings_id'] = $booking_id;
        $layout_data['invoice_active'] = '1';
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('add_invoice', $data, TRUE);		
        $layout_data['page_title'] = 'Invoice';
        $layout_data['meta_description'] = 'Invoice';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
        
        
    }
	
	function list_customer_invoices() 
	{
		$data = array();
        $date_from = NULL;
        $date_to = NULL;
		$customer_id = NULL;
		$inv_status = NULL;
		$data['search_date_from'] = NULL;
		$data['search_date_to'] = NULL;
        $source = $this->input->post('source')!=''?$this->input->post('source'):NULL;
        $company_vh_rep = $this->input->post('company_vh_rep')!=''?$this->input->post('company_vh_rep'):NULL;
		
		if($this->input->post())
		{
			if($this->input->post('customers_vh_rep') > 0)
			{
				$customer_id = $this->input->post('customers_vh_rep');
			} else {
				$customer_id = NULL;
			}
			
			if($this->input->post('inv_status') != "")
			{
				$inv_status = $this->input->post('inv_status');
			} else {
				$inv_status = NULL;
			}
			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			} else {
				$date_from = NULL;
				$data['search_date_from'] = NULL;
			}
			
			if($this->input->post('vehicle_date_to') != "")
			{
				$to_date = $this->input->post('vehicle_date_to');
				$date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
				$data['search_date_to'] = date('d/m/Y', strtotime($date_to));
			} else {
				$date_to = NULL;
				$data['search_date_to'] = NULL;
			}
		}
        
		$data['customer_id'] = $customer_id;
		$data['inv_status'] = $inv_status;
        $data['company_vh_rep'] = $company_vh_rep;
        $data['source'] = $source;
        $data['invoice_report']=$this->invoice_model->get_customer_invoices($date_from,$date_to,$customer_id,$inv_status,$company_vh_rep,$source);
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('customer_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function add_monthly_invoice() 
    {
        $data=array();
        if($this->input->post())
        {
            $customer_id    = $this->input->post('customer');
            $month          = $this->input->post('invoice_month');
            $month          = sprintf("%02d", $month);
            $year          = $this->input->post('invoice_year');
            $service_date   =$year."-".$month."-"."01";
            $service_end_date   = date("Y-m-t", strtotime($service_date));
            $product_name   = $this->input->post('product_name');
            $description    = $this->input->post('description');
            $quantity       = $this->input->post('quantity');
            $amount      = $this->input->post('unit_cost');
            $issue_date     = $this->input->post('issue_date');
            $due_date       = $this->input->post('due_date');
            $linearray=array();

          

            $cust_details=$this->customers_model->get_customers_byid($customer_id);
              // echo $cust_details; die;

            $added_date_time = date('Y-m-d H:i:s');
          
            //$invoice_id = $this->day_services_model->add_main_invoice($main_inv_array);
            $line_items = $this->invoice_model->get_monthly_bookings_for_invoice($customer_id,$service_date,$service_end_date);
           // $line_items = $this->invoice_model->get_monthly_bookings_for_invoice($cust_details,$service_date,$service_end_date);
            $line_item_count=count($line_items);
            

            // echo $line_item_count;
            // exit();

        

            if($line_item_count > 0)
            {
                foreach ($line_items as $key => $line_item) 
                {
                    $ds_id=$line_item->day_service_id;
                    $total_time = ((strtotime($line_item->end_time) - strtotime($line_item->start_time))/ 3600);
                    $total_fee_l = $line_item->total_fee;
                    $serviceamount_l = $line_item->serviceamount;
                    $vat_fee_l = $line_item->vatamount;
                    if(!is_numeric($vat_fee_l)){$vat_fee_l=0.10*$serviceamount_l;$vat_fee_l=sprintf("%0.2f",$vat_fee_l);}
                    $unit_price = $line_item->price_hourly;
                    $discount=$line_item->discount;
                    $discount=is_numeric($discount)?$discount:'0';

                    $linearray[$key]['day_service_id'] = $ds_id;
                    $linearray[$key]['booking_id'] = $line_item->booking_id;
                    $linearray[$key]['day_service_reference'] = $line_item->day_service_reference;
                    $linearray[$key]['maid_id'] = $line_item->maid_id;
                    $linearray[$key]['maid_name'] = $line_item->maid_name;
                    $linearray[$key]['service_from_time'] = $line_item->start_time;
                    $linearray[$key]['service_to_time'] = $line_item->end_time;
                    $linearray[$key]['line_bill_address'] = $line_item->customer_address;
                    $linearray[$key]['description'] = "Service from ".$line_item->time_from." to ".$line_item->time_to." For ".$line_item->customer_address;
                    $linearray[$key]['service_hrs'] = $total_time;
                    $linearray[$key]['inv_unit_price'] = (float)$unit_price;
                    $linearray[$key]['line_discount'] = (float)$discount;
                    $linearray[$key]['line_amount'] = (float)$serviceamount_l; 
                    $linearray[$key]['line_vat_amount'] = (float)$vat_fee_l;
                    $linearray[$key]['line_net_amount'] = (float)$total_fee_l;
                       
                    array_push($dayservice_id_array,$ds_id);
                    $i++;
                }


                $main_inv_array = array();
               $main_inv_array['customer_id'] = $line_items[0]->customer_id;
                $main_inv_array['customer_name'] = $line_items[0]->customer_name;
                $main_inv_array['bill_address'] = $line_items[0]->customer_address;
                $main_inv_array['added'] = $added_date_time;
                $main_inv_array['service_date'] = $issue_date;
                $main_inv_array['invoice_date'] = $issue_date;
                $main_inv_array['invoice_due_date'] = $due_date;
                $main_inv_array['invoice_type'] = 2;
                $main_inv_array['invoice_month'] = $month; 
                $main_inv_array['invoice_added_by'] = user_authenticate();
                    
                $invoice_id = $this->day_services_model->add_main_invoice($main_inv_array);

                if($invoice_id > 0)
                    {
                        $tot_bill_amount = 0;
                        $total_vat_amt = 0;
                        $total_net_amt = 0;
                        $dayids = array();
                        foreach($linearray as $line_val)
                        {
                            $tot_bill_amount += $line_val['line_amount'];
                            $total_vat_amt += $line_val['line_vat_amount'];
                            $total_net_amt += $line_val['line_net_amount'];
                            $main_inv_line = array();
                            $main_inv_line['invoice_id'] = $invoice_id;
                            $main_inv_line['day_service_id'] = $line_val['day_service_id'];
                            $main_inv_line['day_service_reference'] = $line_val['day_service_reference'];
                            $main_inv_line['maid_id'] = $line_val['maid_id'];
                            $main_inv_line['maid_name'] = $line_val['maid_name'];
                            $main_inv_line['service_from_time'] = $line_val['service_from_time'];
                            $main_inv_line['service_to_time'] = $line_val['service_to_time'];
                            $main_inv_line['line_bill_address'] = $line_val['line_bill_address'];
                            $main_inv_line['description'] = $line_val['description'];
                            $main_inv_line['service_hrs'] = $line_val['service_hrs'];
                            $main_inv_line['inv_unit_price'] = $line_val['inv_unit_price'];
                            $main_inv_line['line_discount'] = $line_val['line_discount'];
                            $main_inv_line['line_amount'] = $line_val['line_amount'];
                            $main_inv_line['line_vat_amount'] = $line_val['line_vat_amount'];
                            $main_inv_line['line_net_amount'] = $line_val['line_net_amount'];
                            
                            $invoice_line_id = $this->day_services_model->add_line_invoice($main_inv_line);
                            
                            if($invoice_line_id > 0)
                            {
                                $ds_fields = array();
                                $ds_fields['serv_invoice_id'] = $invoice_id;
                                $ds_fields['invoice_status'] = 1;
                                
                                $this->day_services_model->update_day_service($line_val['day_service_id'], $ds_fields);
                                array_push($dayids,$line_val['booking_id']);
                            }
                            
                        }
                        
                        $update_main_inv = array();
                        $update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
                        //$update_main_inv['billed_amount'] = $tot_bill_amount;
                        $update_main_inv['invoice_total_amount'] = $tot_bill_amount;
                        $update_main_inv['invoice_tax_amount'] = $total_vat_amt;
                        $update_main_inv['invoice_net_amount'] = $total_net_amt;
                        $update_main_inv['balance_amount'] = $total_net_amt;
                        
                        $updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
                        if($updateinv)
                        {
                            $return = array();
                            $data['message'] = 'success';
                            $return['dayids'] = $dayids;

                            
                        } 
                        else 
                        {
                            $return = array();
                            $data['message'] = 'error';
                            $return['dayids'] = $dayids;                            
                        }
                    }






                

                //$data['message'] = "success";
            }
          
        }


        $data['customerlist'] = $this->invoice_model->get_monthly_customer_list();
        $layout_data['content_body'] = $this->load->view('add_monthly_invoice', $data, TRUE);
        $layout_data['page_title'] = 'Add Monthly Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['accounting_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function list_customer_monthly_invoices() 
    {
        $data = array();
        $date_from = NULL;
        $date_to = NULL;
        $customer_id = NULL;
        $inv_status = NULL;
        $data['search_date_from'] = NULL;
        $data['search_date_to'] = NULL;
        $source = $this->input->post('source')!=''?$this->input->post('source'):NULL;
        $company_vh_rep = $this->input->post('company_vh_rep')!=''?$this->input->post('company_vh_rep'):NULL;
        
        if($this->input->post())
        {
            if($this->input->post('customers_vh_rep') > 0)
            {
                $customer_id = $this->input->post('customers_vh_rep');
            } else {
                $customer_id = NULL;
            }
            
            if($this->input->post('inv_status') != "")
            {
                $inv_status = $this->input->post('inv_status');
            } else {
                $inv_status = NULL;
            }
            
            if($this->input->post('vehicle_date') != "")
            {
                $from_date = $this->input->post('vehicle_date');
                $date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
                $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
            } else {
                $date_from = NULL;
                $data['search_date_from'] = NULL;
            }
            
            if($this->input->post('vehicle_date_to') != "")
            {
                $to_date = $this->input->post('vehicle_date_to');
                $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
                $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
            } else {
                $date_to = NULL;
                $data['search_date_to'] = NULL;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['inv_status'] = $inv_status;
        $data['company_vh_rep'] = $company_vh_rep;
        $data['source']=$source;

        $data['invoice_report']=$this->invoice_model->get_customer_monthly_invoices_new($date_from,$date_to,$customer_id,$inv_status,$company_vh_rep,$source);
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('customer_monthly_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Monthly Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    public function add_invoice()
    {
        $data_table = $this->input->post(NULL, TRUE);
        
        $d_array = $this->input->post('day_service_id');
        $dayservice_var = implode(",",$d_array);
        
        $b_array = $this->input->post('booking_id');
        $booking_var = implode(",",$b_array);
        
        $issuedate = $this->input->post('invoiceissuedate');
        $s_date = explode("/", $issuedate);
        $jobinvoiceissuedate = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

        $duedate = $this->input->post('invoiceduedate');
        $s_date1 = explode("/", $duedate);
        $jobinvoiceduedate = $s_date1[2] . '-' . $s_date1[1] . '-' . $s_date1[0];
        
        $data = array(
            'invoice_num' => $this->input->post('invoice_num'),
            'customer_id' => $this->input->post('customer_id'),
            'day_service_id' => $dayservice_var,
            'invoice_status' => $this->input->post('invoice_status'),
            'added' => date('Y-m-d H:i:s'),
            'booking_id' => $booking_var,
            'service_date' => $this->input->post('service_date'),
            'invoice_date' => $jobinvoiceissuedate,
            'invoice_due_date' => $jobinvoiceduedate,
            'billed_amount' => $this->input->post('billed_amount'),
            'received_amount' => $this->input->post('received_amount'),
            'balance_amount' => $this->input->post('balance_amount'),
            'invoice_notes' => $this->input->post('invoice_note')
        );
        
        $invoices_id = $this->invoice_model->add_invoice($data);
        if(!empty($invoices_id))
        {
            redirect('invoice/view_invoice/'.$this->input->post('invoice_num'));
            exit();
        } else {
            redirect('activity/jobs');
            exit();
        }
    }
    
    public function view_invoice($invoice_id)
	{
		$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
		$data = array();
		$data['invoice_detail'] = $invoice_detail;
		
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_invoice_new', $data, TRUE);		
        $layout_data['page_title'] = 'Invoice View';
        $layout_data['meta_description'] = 'Invoice View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
	}


    public function view_invoice_monthly($invoice_id)
    {
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_invoice_monthly', $data, TRUE);      
        $layout_data['page_title'] = 'Invoice View';
        $layout_data['meta_description'] = 'Invoice View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function update_quo_addr()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $address = $this->input->post('address');
        $quotationid = $this->input->post('quotationid');
        $data=array();
        $data['quo_bill_address']=$address;
        $result=$this->invoice_model->update_quotation($quotationid,$data);
        exit();
    }

    public function update_quotation_amt()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $quotation_amount = $this->input->post('quotation_amount');
        $quotation_amount=round($quotation_amount,2);
        $tax_amount=0.10*$quotation_amount;
        $tax_amount=round($tax_amount,2);
        $total_amount=$quotation_amount+$tax_amount;
        $quotation_id = $this->input->post('quotation_id');
        $data=array();
        $data['quo_net_amount']=$quotation_amount;
        $data['quo_tax_amount']=$tax_amount;
        $data['quo_total_amount']=$total_amount;
        $result=$this->invoice_model->update_quotation($quotation_id,$data);
        print_r(json_encode($data));
        exit();
    }

    public function generatequotation($quotation_id)
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);

        $this->load->library('pdf');
        $quotation_detail = $this->invoice_model->get_quotation_detailbyid($quotation_id);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quotation_id);
        //$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
        $html_content = $this->load->view('quotationpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        $this->pdf->stream("".$quotation_id.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
        exit();
    }

    public function update_line_desc()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $description = $this->input->post('line_description');
        $lineid = $this->input->post('lineid');
        $data=array();
        $data['description']=$description;
        $result=$this->invoice_model->update_invoicelineitems($lineid,$data);
        exit();
    }

    public function update_line_amt()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);

        
        $invoice_id = $this->input->post('invoice_id');
        $line_id = $this->input->post('lineid');
        $line_amount = $this->input->post('line_amount');
        $line_old_net_amount = $this->input->post('line_old_net_amount');
        $invstat = $this->input->post('invstat');
        $line_vat_amount=0.10*$line_amount;
        $line_vat_amount=sprintf('%.2f', $line_vat_amount);;
        $line_net_amount=$line_amount + $line_vat_amount;

        $line_old_amount = $this->input->post('line_old_amount');
        $line_old_vat_amount=0.10*$line_old_amount;
        $line_old_vat_amount=sprintf('%.2f', $line_old_vat_amount);
        $line_old_net_amount=$line_old_vat_amount + $line_old_amount;



        $lineid = $this->input->post('lineid');
        $data=array();
        $data['line_amount']=$line_amount;
        $data['line_vat_amount']=$line_vat_amount;
        $data['line_net_amount']=$line_net_amount;
        $result=$this->invoice_model->update_invoicelineitems($line_id,$data);

        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
        $invoice_total_amount=$invoice_detail[0]->invoice_total_amount;;
        $invoice_tax_amount=$invoice_detail[0]->invoice_tax_amount;
        $invoice_net_amount=$invoice_detail[0]->invoice_net_amount;

        $invoice_new_total_amount=$invoice_total_amount - ($line_old_amount - $line_amount);
        $invoice_new_tax_amount=$invoice_tax_amount - ($line_old_vat_amount - $line_vat_amount);
        $invoice_new_net_amount=$invoice_net_amount - ($line_old_net_amount - $line_net_amount);
        $data_invoice=array();
        $data_invoice['invoice_total_amount']=$invoice_new_total_amount;
        $data_invoice['invoice_tax_amount']=$invoice_new_tax_amount;
        $data_invoice['invoice_net_amount']=$invoice_new_net_amount;
        $result=$this->invoice_model->update_invoicepay_detail($invoice_id,$data_invoice);


        $data['invoice_total_amount']=$invoice_new_total_amount;
        $data['invoice_tax_amount']=$invoice_new_tax_amount;
        $data['invoice_net_amount']=$invoice_new_net_amount;



        if($invstat=='1')
        {
            $custid = $this->input->post('custid');
            $line_old_net_amount = $this->input->post('line_old_net_amount');
            $get_customer_detail = $this->invoice_model->get_customer_detail($custid);
            $inv_amt = $get_customer_detail->total_invoice_amount;
            $bal_amount = $get_customer_detail->balance;
            $new_inv_amt=$inv_amt-$line_old_net_amount+$line_net_amount;
            $new_bal_amount = $bal_amount-$line_old_net_amount+$line_net_amount;

            $custdata=array();
            $custdata['total_invoice_amount']=$new_inv_amt;
            $custdata['balance']=$new_bal_amount;
            $custdata['last_invoice_date']=date('Y-m-d');
            $this->invoice_model->update_customer_detail($custid,$custdata);
            $txt="$bal_amount-$line_old_net_amount+$line_net_amount";
            $data['txt']=$txt;
            $data['dtl']=$get_customer_detail;

        }
        
        print_r(json_encode($data));
        exit();
    }
	
	public function validate_invoice()
	{
		$invoice_id = $this->input->post('invoice_id');
		$invoice_amt = $this->input->post('invoice_amt');
		$cust_id = $this->input->post('cust_id');
		$get_customer_detail = $this->invoice_model->get_customer_detail($cust_id);
		if(!empty($get_customer_detail))
		{
			$inv_amt = $get_customer_detail->total_invoice_amount;
			$bal_amount = $get_customer_detail->balance;
			$total_invoice = ($inv_amt + $invoice_amt);
			$total_balance = ($bal_amount + $invoice_amt);
			$update_invoice = $this->invoice_model->update_invoice_detail($invoice_id,$cust_id,$total_invoice,$total_balance);
			if($update_invoice)
			{
				echo "success";
				exit();
			} else {
				echo "error";
				exit();
			}
		} else {
			echo "error";
			exit();
		}
	}
	
	public function generateinvoice($invoiceid)
	{
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
		$this->load->library('pdf');
		$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
		$data = array();
		$data['invoice_detail'] = $invoice_detail;
		$html_content = $this->load->view('invoicepdf', $data, TRUE);
		$this->pdf->loadHtml($html_content);
		$this->pdf->set_paper("a4", "portrait");
		$this->pdf->render();
        
		$this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
		exit();
	}

    public function generateinvoice_monthly($invoiceid)
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_monthly', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        //file_put_contents(base_url().''.$invoiceid.".pdf", $this->pdf->output());
        exit();
    }
    public function generateinvoice_monthly_test($invoiceid)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        // $this->load->view('invoicepdf_monthly_test', $data);
        $html_content = $this->load->view('invoicepdf_monthly_test', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        exit();
    }

    public function send_quotation_email()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $quoid=$this->input->post('quoid');
        $quotationcontent=$this->input->post('quotationcontent');
         $quotationemail=$this->input->post('quotationemail');
         $this->load->library('pdf');
        $quotation_detail = $this->invoice_model->get_quotation_detailbyid($quoid);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quoid);
        $data = array();
        $data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
        $html_content = $this->load->view('quotationpdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$quoid.".pdf", $this->pdf->output());
        
        //$this->load->library('email');
        $config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://gator3313.hostgator.com',
			'smtp_port' => 465,
			'smtp_user' => 'support@iclean.bh',
			'smtp_pass' => 'IClean@123',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		
		$this->email->from('support@iclean.bh', 'iClean');
		$this->email->to($quotationemail);
		$this->email->subject('iClean Quotation Email');
		$this->email->message($quotationcontent);
		$this->email->attach('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$quoid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($quotationemail)<2)
            {echo "email_error";}
            else
            {echo "error";} 
           // echo $this->email->print_debugger();           
        }
        unlink('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$quoid.".pdf");
        //$this->email->print_debugger();
        exit();
    }


    public function send_invoice_email()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $invoiceid=$this->input->post('invoiceid');
        $invoicecontent=$this->input->post('invoicecontent');
       // print_r($invoiceid); die;
        $customer_email=$this->input->post('invoiceemail');
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf", $this->pdf->output());
        
        //$this->load->library('email');
        $config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://gator3313.hostgator.com',
			'smtp_port' => 465,
			'smtp_user' => 'support@iclean.bh',
			'smtp_pass' => 'IClean@123',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		
		$this->email->from('support@iclean.bh', 'iClean');
		$this->email->to($customer_email);
		$this->email->subject('iClean Invoice Email');
		$this->email->message($invoicecontent);
		$this->email->attach('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf");
        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($customer_email)<2)
            {echo "email_error";}
            else
            {echo "error";}            
        }
        unlink('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf");
        //$this->email->print_debugger();
        exit();
    }


    public function send_invoice_email_monthly()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $invoiceid=$this->input->post('invoiceid');
        $invoicecontent=$this->input->post('invoicecontent');
        $customer_email=$this->input->post('invoiceemail');
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_monthly', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();
        
        //$customer_email=$invoice_detail[0]->email_address;
        
        file_put_contents('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf", $this->pdf->output());
        
        //$this->load->library('email');
        $config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://gator3313.hostgator.com',
			'smtp_port' => 465,
			'smtp_user' => 'support@iclean.bh',
			'smtp_pass' => 'IClean@123',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		
		$this->email->from('support@iclean.bh', 'iClean');
		$this->email->to($customer_email);
		$this->email->subject('iClean Invoice Email');
		$this->email->message($invoicecontent);
		$this->email->attach('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf");
		$this->email->send();

        if($this->email->send())
        {
            echo "success";
        }
        else
        {
            if(strlen($customer_email)<2)
            {echo "email_error";}
            else
            {echo "error";}            
        }
        unlink('/var/www/booking.emaid.info/public_html/iclean/invoices/'.$invoiceid.".pdf");
        //$this->email->print_debugger();
        exit();
    }

    public function update_invoice_issue_date()
    {
        $invoiceid=$this->input->post('invoice_id');
        $new_issue_date=$this->input->post('new_issue_date');
        list($day, $month, $year) = explode("/", $new_issue_date);
        $new_issue_date = "$year-$month-$day";
        $data=array();
        $data['invoice_date']=$new_issue_date;
        $invoice_detail = $this->invoice_model->update_invoicepay_detail($invoiceid,$data);
        if($invoice_detail){echo "success";}else{echo "fail";}
        exit();
    }

    public function update_invoice_due_date()
    {
        $invoiceid=$this->input->post('invoice_id');
        $new_due_date=$this->input->post('new_due_date');
        list($day, $month, $year) = explode("/", $new_due_date);
        $new_due_date = "$year-$month-$day";
        $data=array();
        $data['invoice_due_date']=$new_due_date;
        $invoice_detail = $this->invoice_model->update_invoicepay_detail($invoiceid,$data);
        if($invoice_detail){echo "success";}else{echo "fail";}
        exit();
    }

    public function generateinvoice_test($invoiceid)
    {
        
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_test', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        
        exit();
    }


    public function generateinvoice_test2($invoiceid)
    {
        
        $this->load->library('pdf');
        $invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoiceid);
        $data = array();
        $data['invoice_detail'] = $invoice_detail;
        $html_content = $this->load->view('invoicepdf_test2', $data, TRUE);
        $this->pdf->loadHtml($html_content);
        $this->pdf->set_paper("a4", "portrait");
        $this->pdf->render();        
        $this->pdf->stream("".$invoiceid.".pdf", array("Attachment"=>0));
        
        exit();
    }
	
	public function cancel_invoice()
	{
		$invoice_id = $this->input->post('invoice_id');
		$invoice_amt = $this->input->post('invoice_amt');
		$cust_id = $this->input->post('cust_id');
		$invoice_status = $this->input->post('inv_status');
		
		if($invoice_status == 0)
		{
			$update_invoice = $this->invoice_model->update_invoice_status($invoice_id);
			if($update_invoice)
			{
				echo "success";
				exit();
			} else {
				echo "error";
				exit();
			}
		} else if($invoice_status == 1)
		{
			$get_customer_detail = $this->invoice_model->get_customer_detail($cust_id);
			if(!empty($get_customer_detail))
			{
				$inv_amt = $get_customer_detail->total_invoice_amount;
				$last_inv_date = $get_customer_detail->last_invoice_date;
				$last_date = date('Y-m-d', strtotime('-1 day', strtotime($last_inv_date)));
				$bal_amount = $get_customer_detail->balance;
				$total_invoice = ($inv_amt - $invoice_amt);
				$total_balance = ($bal_amount - $invoice_amt);
				$update_invoice = $this->invoice_model->update_invoice_status_detail($invoice_id,$cust_id,$total_invoice,$total_balance,$last_date);
				if($update_invoice)
				{
					echo "success";
					exit();
				} else {
					echo "error";
					exit();
				}
			} else {
				echo "error";
				exit();
			}
		} else {
			echo "error";
			exit();
		}
	}
	
	public function create_invoice()
	{
		//$invoice_detail = $this->invoice_model->get_invoice_detailbyid($invoice_id);
		$data = array();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
		//$data['invoice_detail'] = $invoice_detail;
		
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('create_invoice_new', $data, TRUE);		
        $layout_data['page_title'] = 'Create Invoice';
        $layout_data['meta_description'] = 'Create Invoice';
        $layout_data['css_files'] = array('datepicker.css','jquery.fancybox.css','demo.css','toastr.min.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js','ajaxupload.3.5.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
	}
	
	public function add_new_invoice()
	{
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		$issuedate = $this->input->post('invoiceissuedate');
        $s_date = explode("/", $issuedate);
        $jobinvoiceissuedate = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

        $duedate = $this->input->post('invoiceduedate');
        $s_date1 = explode("/", $duedate);
        $jobinvoiceduedate = $s_date1[2] . '-' . $s_date1[1] . '-' . $s_date1[0];
		
		$customer_id = $this->input->post('customers_vh_rep_new_inv');
		
		$get_customer_address = $this->invoice_model->get_customer_invoice_address($customer_id);
		
		$address = $get_customer_address->customer_address;
		$description = $this->input->post('invoicedescription');
		$amount = $this->input->post('invoiceamount');
		$vatamount = $this->input->post('invoicevatamount');
		$netamount = $this->input->post('invoicenetamount');
		$added_date_time = date('Y-m-d H:i:s');
		
		$invoice_array = array();
		$invoice_array['customer_id'] = $customer_id;
		$invoice_array['customer_name'] = $get_customer_address->customer_name;
		$invoice_array['bill_address'] = $get_customer_address->customer_address;
		$invoice_array['added'] = $added_date_time;
		$invoice_array['service_date'] = $jobinvoiceissuedate;
		$invoice_array['invoice_date'] = $jobinvoiceissuedate;
		$invoice_array['invoice_due_date'] = $jobinvoiceduedate;
		$invoice_array['invoice_added_by'] = user_authenticate();
		$invoice_array['invoice_status'] = 1;
		$invoice_array['invoice_total_amount'] = $amount;
		$invoice_array['invoice_tax_amount'] = $vatamount;
		$invoice_array['invoice_net_amount'] = $netamount;
		$invoice_array['balance_amount'] = $netamount;
		$invoice_array['invoice_notes'] = $description;
		
		$invoice_id = $this->day_services_model->add_main_invoice($invoice_array);
		
		if($invoice_id > 0)
		{
			$main_inv_line = array();
			$main_inv_line['invoice_id'] = $invoice_id;
			$main_inv_line['day_service_id'] = 0;
			$main_inv_line['maid_id'] = 0;
			$main_inv_line['line_bill_address'] = $get_customer_address->customer_address;
			$main_inv_line['description'] = $description;
			$main_inv_line['line_amount'] = $amount;
			$main_inv_line['line_vat_amount'] = $vatamount;
			$main_inv_line['line_net_amount'] = $netamount;
							
			$invoice_line_id = $this->day_services_model->add_line_invoice($main_inv_line);
			
			$update_main_inv = array();
			$update_main_inv['invoice_num'] = "INV-".date('Y')."-".sprintf('%04s', $invoice_id);
						
			$updateinv = $this->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
			if($updateinv)
			{
				redirect('invoice/view_invoice/'.$invoice_id);
				exit();
			}
		}
	}
	
	
	
    
    public function add_quotation()
    {
        $schedule_date = date('d-M-Y');
        $day_number = date('w', strtotime($schedule_date));
        $times = array();
        $current_hour_index = 0;
        $time = '12:00 am';  
        $time_stamp = strtotime($time);

        for ($i = 0; $i < 24; $i++)
        {
                if(!isset($times['t-' . $i]))
                {
                        $times['t-' . $i] = new stdClass();
                }

                $times['t-' . $i]->stamp = $time_stamp;
                $times['t-' . $i]->display = $time;

                
                if(date('H') == $i)
                {
                        $current_hour_index = 't-' . ($i - 1);
                }

                $time_stamp = strtotime('+60mins', strtotime($time));
                $time = date('g:i a', $time_stamp);
        }
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses))); 
                //echo 'refresh';
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-details-customer' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_details = $this->customers_model->get_customer_by_id($customer_id);
                echo json_encode($customer_details); 
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode($customer_addresses); 
                exit();
            }

            if($this->input->post('action') && $this->input->post('action') == 'add-quotation' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));
                $customer_address_id = trim($this->input->post('customer_address_id'));
                $service_type_id = trim($this->input->post('service_type_id'));
                $time_from =  date('H:i', trim($this->input->post('time_from')));
                $time_to = date('H:i', trim($this->input->post('time_to')));
                //$quo_date = trim($this->input->post('quotation_time'));
                $quo_date = DateTime::createFromFormat('d/m/Y', $this->input->post('quotation_time'));
                $quo_date = $quo_date->format('Y-m-d');
                $cleaning_material = $this->input->post('cleaning_material');
                $total_amount = 0;
                $mail = $this->input->post('mail');
                $phone = $this->input->post('phone');
                $service_data = $this->input->post('service_data');
                
                
                foreach ($service_data as $sdt) {
                    $total_amount+=$sdt['serv_price'];
                }
                
                $get_customer_detail = $this->customers_model->get_customer_details($customer_id);
                $get_customer_addr_detail = $this->customers_model->get_customer_address_by_id($customer_address_id);
                $addressss='';
                if($get_customer_addr_detail->customer_address == "")
                {
                    $addressss = 'Building - '.$get_customer_addr_detail->building.', '.$get_customer_addr_detail->unit_no.''.$get_customer_addr_detail->street;
                } 
                else 
                {
                    if($get_customer_addr_detail->building != "")
                    {
                        $addressss = "Apt No: ".$get_customer_addr_detail->building.", ".$get_customer_addr_detail->customer_address;
                    } else {
                        $addressss = $get_customer_addr_detail->customer_address;
                    }
                    
                }
                //echo 'custname='.$get_customer_detail[0]['customer_name'];
                //print_r($get_customer_detail);exit();
                $net_amount=round(($total_amount/1.10),2);
                $tax_amount=$total_amount - $net_amount;
                $data=array();
                $data['quo_customer_id']=$customer_id;
                $data['quo_customer_addr_id']=$customer_address_id;
                $data['quo_customer_name']=$get_customer_detail[0]['customer_name'];
                $data['quo_bill_address']=$addressss;
                $data['quo_status']=1;
                $data['quo_added_datetime']=date("Y-m-d H::i:s");
                //$data['quo_date']=date("Y-m-d");
                $data['quo_date']=$quo_date;
                $data['quo_total_discount']=0;
                $data['quo_total_amount']=$total_amount;
                $data['quo_time_from']=$time_from;
                $data['quo_time_to']=$time_to;
                $data['quo_tax_amount']=$tax_amount;
                $data['quo_net_amount']=$net_amount;
                $data['quo_added_by']=user_authenticate();
                $data['quo_qb_id']='0';
                $data['quo_qb_sync_stat']='0';
                $data['quo_servicetype_id']=$service_data[0]['serv_id'];
                $this->db->trans_begin();
                $add_quotation = $this->invoice_model->add_quotation($data);
                if($add_quotation)
                {
                    $main['quo_total_amount'] = $main['quo_tax_amount'] = $main['quo_net_amount'] = 0;
                    foreach ($this->input->post('service_type') as $key => $value){
                        $line_array=array();
                        $line_array['qli_quotation_id'] = $add_quotation;
                        $line_array['qli_service_type_id'] = $this->input->post('service_type')[$key];
                        $line_array['qli_description'] = $this->input->post('description')[$key];
                        $line_array['qli_rate'] = $this->input->post('service_rate')[$key];
                        $line_array['qli_quantity'] = $this->input->post('service_qty')[$key];
                        $quantity_value = $line_array['qli_rate'] * $line_array['qli_quantity'];
                        $vat_charge = ($this->config->item('service_vat_percentage')/100)*$quantity_value;
                        $line_array['qli_vat_charge'] = $vat_charge;
                        $line_array['qli_price'] = $quantity_value+$vat_charge;
                        $main['quo_total_amount'] += $quantity_value;
                        $main['quo_tax_amount'] += $vat_charge;
                        $main['quo_net_amount'] = $main['quo_total_amount'] +  $main['quo_tax_amount'];
                        $add_quotation_line_item = $this->invoice_model->add_quotation_line_item($line_array);
                    }
                    /*foreach ($service_data as $sdt)
                    {

                        $line_array=array();
                        $line_array['qli_service_type_id']=$sdt['serv_id'];
                        $line_array['qli_description']=$sdt['serv_description'];
                        $line_array['qli_quotation_id']=$add_quotation;
                        $line_array['qli_rate']=$sdt['serv_rate'];
                        $line_array['qli_quantity']=$sdt['serv_qty'];
                        $line_array['qli_price']=$sdt['serv_price'];
                        $add_quotation_line_item = $this->invoice_model->add_quotation_line_item($line_array);
                    }*/
					$idlength = strlen((string)$add_quotation);
					$ref= ($idlength >= 4) ? $add_quotation : sprintf('%04u', $add_quotation);
                    $main['quo_reference'] = 'QTN'.$ref;
                	$this->invoice_model->update_quotation($add_quotation,$main);
                    $this->db->trans_commit();
                }
                else{
                    $this->db->trans_rollback();
                }
                $response = array();
                if($add_quotation){$response['status'] = 'success';}
                else{$response['status'] = 'error';}
                echo json_encode($response); 
                exit();
            }
            
            
        }
        
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date(date('Y-m-d'));
        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave)
                {
                    array_push($leave_maid_ids, $leave->maid_id);
                }
                
        
        
        $maids = $this->maids_model->get_maids();
        $service_types = $this->service_types_model->get_service_types();
        $data = array();
        $data['maids'] = $maids;
        $data['service_types'] = $service_types;
        $data['times'] = $times;
        $data['day_number'] = $day_number;
        $data['leave_maid_ids'] = $leave_maid_ids;
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('add_quotation', $data, TRUE);       
        $layout_data['page_title'] = 'New Quotation';
        $layout_data['meta_description'] = 'New Quotation';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'bootstrap-datetimepicker.min.css');
        //$layout_data['jobs_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.validate.min.js','jquery.fancybox.pack.js', 'bootstrap-datetimepicker.min.js', 'bootstrap-datepicker.js', 'quotation.js?v='.time(), 'moment.min.js','bootstrap.js');

        $this->load->view('layouts/default_job', $layout_data);
    }

    function list_customer_quotations() 
    {
        $data = array();
        $date_from = NULL;
        $date_to = NULL;
        $customer_id = NULL;
        $data['search_date_from'] = NULL;
        $data['search_date_to'] = NULL;

        if($this->input->post())
        {
            if($this->input->post('customers_vh_rep') > 0)
            {
                $customer_id = $this->input->post('customers_vh_rep');
            } else {
                $customer_id = NULL;
            }            
            
            
            if($this->input->post('vehicle_date') != "")
            {
                $from_date = $this->input->post('vehicle_date');
                $date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
                $data['search_date_from'] = date('d/m/Y', strtotime($date_from));
            } else {
                $date_from = NULL;
                $data['search_date_from'] = NULL;
            }
            
            if($this->input->post('vehicle_date_to') != "")
            {
                $to_date = $this->input->post('vehicle_date_to');
                $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
                $data['search_date_to'] = date('d/m/Y', strtotime($date_to));
            } else {
                $date_to = NULL;
                $data['search_date_to'] = NULL;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['inv_status'] = $inv_status;
        $quotations=$this->invoice_model->get_customer_quotations($date_from,$date_to,$customer_id);
        $data['quotations']=$quotations;
        $data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('customer_quotation_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Quotations';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function view_quotation($quotation_id)
	{
        if($this->input->is_ajax_request()){
            if($this->input->post('action') == 'update-quotation'){
                $line_items = array();
                $line_item = array();
                $main = array();
                $main['quo_total_amount'] = 0;
                $main['quo_tax_amount'] = 0;
                $main['quo_net_amount'] = 0;
                foreach($this->input->post('line_id') as $key => $value){
                    $line_item['qli_rate'] = $this->input->post('service_rate')[$key];
                    $line_item['qli_quantity'] = $this->input->post('service_quantity')[$key];
                    $quantity_value = $line_item['qli_rate'] * $line_item['qli_quantity'];
                    $main['quo_total_amount'] += $quantity_value;
                    $vat_charge = ($this->config->item('service_vat_percentage')/100)*$quantity_value;
                    $main['quo_tax_amount'] += $vat_charge;
                    $line_item['qli_vat_charge'] = $vat_charge;
                    $line_item['qli_price'] = $quantity_value+$vat_charge;
                    $main['quo_net_amount'] = $main['quo_total_amount'] + $main['quo_tax_amount'];
                    $line_items[] = $line_item;
                    $this->db->trans_begin();
                    $update_quotation_line_item = $this->invoice_model->update_quotation_line_item($quotation_id,$this->input->post('line_id')[$key],$line_item);
                    $update_quotation = $this->invoice_model->update_quotation($quotation_id,$main);
                    $this->db->trans_commit();
                }
                $response = array();
                $response['status'] = 'success';
                die(json_encode($response));
            }
            return;
        }
        $this->quotation_model->change_quotation_read_status($quotation_id,1); // change read status
		$quotation_detail = $this->invoice_model->get_quotation_detailbyid($quotation_id);
        $quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quotation_id);
		$data = array();
		$data['quotation_detail'] = $quotation_detail;
        $data['quotation_line_items'] = $quotation_line_items;
		$layout_data = array();
        $layout_data['content_body'] = $this->load->view('view_quotation_new', $data, TRUE);		
        $layout_data['page_title'] = 'Quotation View';
        $layout_data['meta_description'] = 'Quotation View';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.validate.min.js','jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','toastr.min.js','mymaids.js','ajaxupload.3.5.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
	}
}