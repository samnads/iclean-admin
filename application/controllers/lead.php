<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lead extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if(!is_user_loggedin())
		{
			redirect('logout');
		}

		$this->load->model('lead_model');
		$this->load->helper('google_api_helper');
		
	}

	public function index()
	{
		$data = array();
		$date_from = NULL;
        $date_to = NULL;
		$leadtype = NULL;
		$timelines = NULL;
		$leadstatus = NULL;
		$data['search_date_from'] = NULL;
		$data['search_date_to'] = NULL;
		
		if($this->input->post())
		{
			if($this->input->post('leadtype') != "")
			{
				$leadtype = $this->input->post('leadtype');
			} else {
				$leadtype = NULL;
			}
			
			if($this->input->post('timelines') != "")
			{
				$timelines = $this->input->post('timelines');
			} else {
				$timelines = NULL;
			}

			if($this->input->post('service') != "")
			{
				$service = $this->input->post('service');
			} else {
				$service = NULL;
			}

			if($this->input->post('source') != "")
			{
				$source = $this->input->post('source');
			} else {
				$source = NULL;
			}
			
			if($this->input->post('leadstatus') != "")
			{
				$leadstatus = $this->input->post('leadstatus');
			} else {
				$leadstatus = NULL;
			}
			
			if($this->input->post('schedule_date_from') != "")
			{
				$from_date = $this->input->post('schedule_date_from');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			} else {
				$date_from = NULL;
				$data['search_date_from'] = NULL;
			}
			
			if($this->input->post('schedule_date_to') != "")
			{
				$to_date = $this->input->post('schedule_date_to');
				$date_to = date('Y-m-d', strtotime(str_replace("/", "-", $to_date)));
				$data['search_date_to'] = date('d/m/Y', strtotime($date_to));
			} else {
				$date_to = NULL;
				$data['search_date_to'] = NULL;
			}
		}
		$data['leadtype'] = $leadtype;
		$data['timelines'] = $timelines;
		$data['service'] = $service;
		$data['source'] = $source;
		$data['leadstatus'] = $leadstatus;
		$data['services'] = $this->lead_model->get_services();
        $data['leads'] = $this->lead_model->get_leads_list($date_from,$date_to,$leadtype,$timelines,$service,$source,$leadstatus);
        $layout_data['content_body'] = $this->load->view('lead-list', $data, TRUE);
		$layout_data['page_title'] = 'Lead Management';
		$layout_data['meta_description'] = 'Lead Management';
		$layout_data['css_files'] = array('datepicker.css','demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js');
		$layout_data['account_active'] = '1'; 
		$this->load->view('layouts/default', $layout_data);
	}
	
	public function add()
	{
		$e_lead = array();
        if($this->input->post('lead_sub'))
        {
			//echo 'God is love'; die;
            $errors = array();
			
            $this->load->library('form_validation');
            
			
			//$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');
			$this->form_validation->set_error_delimiters('<div class="error" style="text-align:center;">', '</div>');

            $this->form_validation->set_rules('customername', 'Name', 'required');
            //$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

            $this->form_validation->set_rules('customeremail', 'Email', 'required|valid_email');
            //$this->form_validation->set_message('required', 'Select a date', 'expirydate');
			
			$this->form_validation->set_rules('customermobile', 'Phone', 'required|min_length[10]|max_length[10]');
			
            if ($this->form_validation->run() == TRUE)
            {
                $customername = $this->input->post('customername');
                $customeremail = $this->input->post('customeremail');
				$customermobile = $this->input->post('customermobile');
				$contactdate = $this->input->post('contactdate');
				$customeraddress = $this->input->post('customeraddress');
				$servicetype = $this->input->post('servicetype');
				$customersource = $this->input->post('customersource');
				$leadtypes = $this->input->post('leadtypes');
				$servicetimeline = $this->input->post('servicetimeline');
				$quote = $this->input->post('quote');
				$comments = $this->input->post('comments');
				$status = $this->input->post('status');
				$added_date     = date('Y-m-d H:i:s');
				
				list($day, $month, $year) = explode("/", $contactdate);
                $contact_date = "$year-$month-$day";
				
				$lead_fields = array();
				$lead_fields['customer_name']         = $customername; 
				$lead_fields['customer_email']        = $customeremail; 
				$lead_fields['customer_phone']        = $customermobile;
				$lead_fields['customer_address']      = $customeraddress;
				$lead_fields['contacted_date']        = $contact_date;
				$lead_fields['service_type']          = $servicetype;
				$lead_fields['source']     			  = $customersource;
				$lead_fields['lead_type']             = $leadtypes;
				$lead_fields['timeline']              = $servicetimeline;
				$lead_fields['status']                = $status;
				$lead_fields['added_date_time']       = date('Y-m-d H:i:s'); 
				$lead_fields['quote']         		  = $quote; 
				$lead_fields['comments']         	  = $comments; 
				
				$lead_add                        = $this->lead_model->add_lead($lead_fields);
				if($lead_add > 0)
				{
					redirect(base_url() . 'lead');
					exit();
				}
            } 
            else
            {
                $errors = $this->form_validation->error_array();
            }
        }
        $data = array();
        $data['e_lead'] = $e_lead;
        $data['error'] = isset($errors) ? array_shift($errors) : '';
		$data['services'] = $this->lead_model->get_services();
        $layout_data['content_body'] = $this->load->view('add_lead', $data, TRUE);
		$layout_data['page_title'] = 'Lead Management';
		$layout_data['meta_description'] = 'Lead Management';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('base.js','bootstrap-datepicker.js','users.js');
		$this->load->view('layouts/default', $layout_data);
	}
	
	function lead_view($lead_id) {
        $data['lead_details']       = $this->lead_model->get_lead_details($lead_id);
        $data['link_lead_id']      = $lead_id;
        //print_r($data);exit;
        $layout_data['content_body']= $this->load->view('view_lead', $data, TRUE);
        $layout_data['page_title']  = 'Lead Management';
        $layout_data['meta_description'] = 'Lead Management';
        $layout_data['css_files']   = array('jquery.lighter.css');
        $layout_data['external_js_files'] = array();
          $layout_data['maids_active'] = '1';
        $layout_data['js_files']    = array('base.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function edit($lead_id)
	{
		$e_lead = array();
        if($this->input->post('lead_sub'))
        {
            $errors = array();
			
            $this->load->library('form_validation');
            
			
			$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('customername', 'Name', 'required');
            //$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

            $this->form_validation->set_rules('customeremail', 'Email', 'required|valid_email');
            //$this->form_validation->set_message('required', 'Select a date', 'expirydate');
			
			$this->form_validation->set_rules('customermobile', 'Phone', 'required|min_length[10]|max_length[10]');
            
            if ($this->form_validation->run() == TRUE)
            {
                $customername = $this->input->post('customername');
                $customeremail = $this->input->post('customeremail');
				$customermobile = $this->input->post('customermobile');
				$contactdate = $this->input->post('contactdate');
				$followupdate = $this->input->post('followupdate');
				$followupcomment = $this->input->post('followupcomment');
				$nextfollowupdate = $this->input->post('nextfollowupdate');
				$customeraddress = $this->input->post('customeraddress');
				$servicetype = $this->input->post('servicetype');
				$customersource = $this->input->post('customersource');
				$leadtypes = $this->input->post('leadtypes');
				$servicetimeline = $this->input->post('servicetimeline');
				$status = $this->input->post('status');
				$added_date     = date('Y-m-d H:i:s');
				$quote = $this->input->post('quote');
				$comments = $this->input->post('comments');
				
				list($day, $month, $year) = explode("/", $contactdate);
                $contact_date = "$year-$month-$day";
				if($followupdate != "")
				{
					list($day, $month, $year) = explode("/", $followupdate);
					$followup_date = "$year-$month-$day";
				} else {
					$followup_date = "";
				}
				if($nextfollowupdate != "")
				{
					list($day, $month, $year) = explode("/", $nextfollowupdate);
					$nextfollowup_date = "$year-$month-$day";
				} else {
					$nextfollowup_date = "";
				}
				
				$lead_fields = array();
				$lead_fields['customer_name']        = $customername; 
				$lead_fields['customer_email']       = $customeremail; 
				$lead_fields['customer_phone']       = $customermobile;
				$lead_fields['customer_address']     = $customeraddress;
				$lead_fields['contacted_date']       = $contact_date;
				$lead_fields['followup_date']        = $followup_date;
				$lead_fields['followup_comment']     = $followupcomment;
				$lead_fields['next_follow_update']   = $nextfollowup_date;
				$lead_fields['service_type']         = $servicetype;
				$lead_fields['source']     			 = $customersource;
				$lead_fields['lead_type']            = $leadtypes;
				$lead_fields['timeline']             = $servicetimeline;
				$lead_fields['status']             	 = $status;
				$lead_fields['updated_datetime']     = date('Y-m-d H:i:s'); 
				$lead_fields['quote']         		 = $quote; 
				$lead_fields['comments']         	 = $comments;
				
				$lead_add = $this->lead_model->update_lead($lead_id,$lead_fields);
				if($lead_add)
				{
					redirect(base_url() . 'lead');
					exit();
				}
            } 
            else
            {
                $errors = $this->form_validation->error_array();
            }
        }
        $data = array();
        $data['e_lead'] = $e_lead;
        $data['leadid'] = $lead_id;
        $data['error'] = isset($errors) ? array_shift($errors) : '';
		$data['services'] = $this->lead_model->get_services();
		$data['lead_details']       = $this->lead_model->get_lead_details($lead_id);
        $layout_data['content_body'] = $this->load->view('edit_lead', $data, TRUE);
		$layout_data['page_title'] = 'Lead Management';
		$layout_data['meta_description'] = 'Lead Management';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('base.js','bootstrap-datepicker.js','users.js');
		$this->load->view('layouts/default', $layout_data);
	}

	public function delete($lead_id)
	{
		$lead_details= $this->lead_model->get_lead_details($lead_id);
		$data_activity = array();
	    $data['added_user'] = user_authenticate();
	    $data['action_type'] = 'Lead_delete';
	    $data['action_content'] = 'Lead Deleted.Name - '.$lead_details->customer_name.',Email - '.$lead_details->customer_email.',Phone - '.$lead_details->customer_phone;
	    $data['addeddate'] = date('Y-m-d H:i:s');
	    $this->db->set($data);
		$this->db->insert('user_activity');
		$this->lead_model->delete_lead($lead_id);
		redirect('lead');
	}
	
}