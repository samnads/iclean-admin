<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maidappapi extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('maidapp_api_model');

		$this->load->helper('string');
        $this->load->helper('google_api_helper');
    }

    public function index() {
        $response = array();
        $response['status'] = 'error';
        $response['error_code'] = '101';
        $response['message'] = 'Invalid request';

        echo json_encode($response);
        exit();
    }
	
	public function maidapp_login() 
	{
        if ($this->config->item('ws') == $this->input->get('ws')) 
		{
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
           // $jsonInput = $_POST;
           // $jsonInput = json_encode($_POST);
            $json_arr = json_decode($jsonInput, true);
          // print_r($json_arr['username']);exit;

//            $json_arr['username']    = 'username';
//            $json_arr['password']       = 'password';
//            $json_arr['logged_status'] = 1;
            if (isset($json_arr['username']) && strlen($json_arr['username']) > 0 && isset($json_arr['password']) && strlen($json_arr['password']) > 0) 
			{
                $username = trim($json_arr['username']);
                $password = trim($json_arr['password']);
                $logged_status = trim($json_arr['logged_status']);

                //$chk_maid_already_login = $this->pickmaid_model->chk_maid_already_login($username, $password);

                $chk_maiduser = $this->maidapp_api_model->get_maid_login($username, $password);
                      
                if (!empty($chk_maiduser)) 
				{
                    if ($chk_maiduser->maid_login_status == 1) 
					{
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '105';
                        $response['message'] = 'Already logged in another device';

                        echo json_encode($response);
                        exit();
                    } else {
                        $maid_id = $chk_maiduser->maid_id;
                        $update_login_status = $this->maidapp_api_model->update_login_status($maid_id, $logged_status);

                        $maid_dtls = $this->maidapp_api_model->get_maid_detail_by_id($maid_id);
                       // print_r($maid_dtls);exit;
                        if (!empty($chk_maiduser->maid_photo_file)) 
						{
                            $photoName = $chk_maiduser->maid_photo_file;
                            $photoUrl = base_url() . 'assets/maid/photos/' . $photoName;
                        }
                        echo json_encode(array('status' => 'success', 'maid_details' => $maid_dtls, 'photoUrl' => @$photoUrl));
                        exit();
                    }
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'Invalid login details';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function maidapp_logout() 
	{
        if ($this->config->item('ws') == $this->input->get('ws')) 
		{
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            $json_arr = json_decode($jsonInput, true);

            //$json_arr['maid_id'] = 101;
            //$json_arr['logged_status'] = 0;
            if (isset($json_arr['maid_id']) && strlen($json_arr['maid_id']) > 0) 
			{
                $maid_id = trim($json_arr['maid_id']);
                $logged_status = trim($json_arr['logged_status']);
                $update_login_status = $this->maidapp_api_model->update_login_status($maid_id, $logged_status);
                echo json_encode(array('status' => 'success'));
                exit();
//                if($update_login_status > 0)
//                {
//                    echo json_encode(array('status' => 'success'));
//                    exit();
//                } else {
//                    echo json_encode(array('status' => 'error'));
//                    exit();
//                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function job_by_maid_id() 
	{
        if ($this->config->item('ws') == $this->input->get('ws')) 
		{
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            //$jsonInput = json_encode($_POST);
               

            $json_arr = json_decode($jsonInput, true);
             
            //$json_arr['date']    = '2018-05-15';
            //$json_arr['maid_id']       = 28;
            if (isset($json_arr['date']) && strlen($json_arr['date']) > 0 && isset($json_arr['maid_id']) && strlen($json_arr['maid_id']) > 0) {
                $date = trim($json_arr['date']);
                $maid_id = trim($json_arr['maid_id']);
               
                $get_maid_details = $this->maidapp_api_model->get_maid_job_details($date, $maid_id);

                if (!empty($get_maid_details)) 
				{
                    $schedule = array();
                    $i = 0;
                    foreach ($get_maid_details as $booking) {
                        $day_service = $this->maidapp_api_model->get_day_service_by_booking_id($date, $booking->booking_id);
                        if (!empty($day_service)) {
                            $service_status = $day_service->service_status;
                            if ($day_service->payment_status == 1) {
                                $amount_status = "Paid - AED " . number_format($day_service->paid_amount, 2);
                            } else {
                                $amount_status = "Not Paid";
                            }
                        } else {
                            $service_status = 0;
                            $amount_status = "Not Paid";
                        }

                        // $check_buk_exist = $this->maidapp_api_model->get_booking_exist($booking->booking_id, $service_date);
                        // if (!empty($check_buk_exist)) {
                            // $mop = $check_buk_exist->mop;
                            // $ref = $check_buk_exist->just_mop_ref;
                        // } else {
                            // $mop = $booking->payment_mode;
                            // $ref = "";
                        // }
						$mop = $booking->payment_mode;
						$ref = "";

                        if ($booking->booking_note == "") {
                            $desc = "Not Specified";
                        } else {
                            $desc = $booking->booking_note;
                        }

                        $schedule[$i]['booking_id'] = $booking->booking_id;
                        $schedule[$i]['customer_id'] = $booking->customer_id;
                        $schedule[$i]['customer_address_id'] = $booking->customer_address_id;
                        $schedule[$i]['maid_id'] = $booking->maid_id;
                        $schedule[$i]['service_type_id'] = $booking->service_type_id;
                        $schedule[$i]['service_start_date'] = $booking->service_start_date;
                        $schedule[$i]['service_week_day'] = $booking->service_week_day;
                        $schedule[$i]['is_locked'] = $booking->is_locked;
                        $schedule[$i]['time_from'] = $booking->time_from;
                        $schedule[$i]['time_to'] = $booking->time_to;
                        $schedule[$i]['ftime'] = $booking->ftime;
                        $schedule[$i]['ttime'] = $booking->ttime;
                        $schedule[$i]['booking_type'] = $booking->booking_type;
                        $schedule[$i]['service_end'] = $booking->service_end;
                        $schedule[$i]['service_end_date'] = $booking->service_end_date;
                        $schedule[$i]['service_actual_end_date'] = $booking->service_actual_end_date;
                        $schedule[$i]['booking_note'] = $booking->booking_note;
                        $schedule[$i]['is_locked'] = $booking->is_locked;
                        $schedule[$i]['pending_amount'] = $booking->pending_amount;
                        $schedule[$i]['discount'] = $booking->discount;
                        $schedule[$i]['total_amount'] = $booking->total_amount;
                        $schedule[$i]['booking_status'] = $booking->booking_status;
                        $schedule[$i]['customer_name'] = $booking->customer_name . ' (' . $booking->customer_source . ')';
                        $schedule[$i]['price_hourly'] = $booking->price_hourly;
                        $schedule[$i]['balance'] = $booking->balance;
                        $schedule[$i]['signed'] = $booking->signed;
                        $schedule[$i]['customer_nick_name'] = $booking->customer_nick_name;
                        $schedule[$i]['mobile_number_1'] = $booking->mobile_number_1;
                        $schedule[$i]['key_given'] = $booking->key_given;
                        $schedule[$i]['payment_type'] = $booking->payment_type;
                        $schedule[$i]['longitude'] = $booking->longitude;
                        $schedule[$i]['latitude'] = $booking->latitude;
                        $schedule[$i]['customer_source'] = $booking->customer_source;
                        $schedule[$i]['zone_id'] = $booking->zone_id;
                        $schedule[$i]['zone_name'] = $booking->zone_name;
                        $schedule[$i]['area_name'] = $booking->area_name;
                        $schedule[$i]['customer_address'] = $booking->customer_address;
                        $schedule[$i]['maid_name'] = $booking->maid_name;
                        $schedule[$i]['maid_nationality'] = $booking->maid_nationality;
                        $schedule[$i]['maid_mobile_1'] = $booking->maid_mobile_1;
                        $schedule[$i]['maid_photo_file'] = $booking->maid_photo_file;
                        $schedule[$i]['user_fullname'] = $booking->user_fullname;
                        $schedule[$i]['service_status'] = $service_status;
                        $schedule[$i]['amount_status'] = $amount_status;
                        $schedule[$i]['service_type_name'] = $desc;
                        $schedule[$i]['mop'] = $mop;

                        $i++;
                    }
                    echo json_encode(array('status' => 'success', 'maid_details' => $schedule));
                    exit();
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '103';
                    $response['message'] = 'No details found!';

                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function job_list_by_maid_id() 
	{
        if ($this->config->item('ws') == $this->input->get('ws')) {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            $json_arr = json_decode($jsonInput, true);

//            $json_arr['date']    = '2017-11-28';
//            $json_arr['maid_id']       = 101;
            if (isset($json_arr['date']) && strlen($json_arr['date']) > 0 && isset($json_arr['maid_id']) && strlen($json_arr['maid_id']) > 0) {
                $date = trim($json_arr['date']);
                $maid_id = trim($json_arr['maid_id']);
                $get_maid_details = $this->maidapp_api_model->get_maid_job_details($date, $maid_id);

                $times = array();
                $current_hour_index = 0;
                $time = '12:00 am';
                $time_stamp = strtotime($time);
                for ($i = 0; $i < 24; $i++) {
                //for ($i = 0; $i < 24; $i++)
                    $time_stamp = strtotime('+60mins', strtotime($time));
                    $time = date('h:i A', $time_stamp);

                    foreach ($get_maid_details as $job) {
                        if ($time == $job->time_from) {
                            if (!isset($times[$time])) {
                                $times[$time] = new stdClass();
                            }
                            $hr = $this->get_time_difference($job->ftime, $job->ttime);

                            $times[$time]->booking_id = $job->booking_id;
                            $times[$time]->customer_id = $job->customer_id;
                            $times[$time]->customer_address_id = $job->customer_address_id;
                            $times[$time]->maid_id = $job->maid_id;
                            $times[$time]->service_type_id = $job->service_type_id;
                            $times[$time]->service_start_date = $job->service_start_date;
                            $times[$time]->service_week_day = $job->service_week_day;
                            $times[$time]->is_locked = $job->is_locked;
                            $times[$time]->time_from = $job->time_from;
                            $times[$time]->time_to = $job->time_to;
                            $times[$time]->starttime = $job->starttime;
                            $times[$time]->endtime = $job->endtime;
                            $times[$time]->booking_type = $job->booking_type;
                            $times[$time]->booking_status = $job->booking_status;

                            $times[$time]->discount = $job->discount;
                            $times[$time]->customer_name = $job->customer_name;
                            $times[$time]->mobile_number_1 = $job->mobile_number_1;
                            $times[$time]->email_address = $job->email_address;
                            $times[$time]->latitude = $job->latitude;
                            $times[$time]->longitude = $job->longitude;
                            $times[$time]->customer_address = $job->customer_address;
                            $times[$time]->payment_type = $job->payment_type;
                            $times[$time]->maid_name = $job->maid_name;
                            $times[$time]->zone_id = $job->zone_id;
                            $times[$time]->area_name = $job->area_name;
                            $times[$time]->zone_name = $job->zone_name;
                            $times[$time]->total_fee = $job->total_amount;
                            $times[$time]->actual_zone = $job->actual_zone;
                            $times[$time]->service_type_name = $job->service_type_name;
                            $times[$time]->total_hour = $hr;
                        } else {
                            $times[$time] = new stdClass();
                        }
                    }
                }
                echo json_encode(array('status' => 'success', 'maid_job_details' => $times));
                exit();
//                if (!empty($get_maid_details)) {
//                    echo json_encode(array('status' => 'success', 'maid_details' => $get_maid_details));
//                    exit();
//                } else {
//                    $response = array();
//                    $response['status'] = 'error';
//                    $response['error_code'] = '103';
//                    $response['message'] = 'No details found!';
//
//                    echo json_encode($response);
//                    exit();
//                }
            } else {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit();
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function location_update()
    {
        if ($this->config->item('ws') == $this->input->get('ws'))
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            $json_arr = json_decode($jsonInput, true);

            //$json_arr['maid_id'] = 101;
            //$json_arr['lat'] = '25.2854';
            //$json_arr['lng'] = '51.5310';
            if(isset($json_arr['maid_id']) && strlen($json_arr['maid_id'])>0 && isset($json_arr['lat']) && strlen($json_arr['lat'])>0 && isset($json_arr['lng']) && strlen($json_arr['lng'])>0) {
                $maid_id = trim($json_arr['maid_id']);
                $latitude = trim($json_arr['lat']);
                $longitude = trim($json_arr['lng']);
                $update_location = $this->maidapp_api_model->update_location($maid_id,$latitude,$longitude);
                echo json_encode(array('status' => 'success'));
                exit();
            }
            else
            {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit(); 
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function device_id_update()
    {
        if ($this->config->item('ws') == $this->input->get('ws'))
        {
            $handle = fopen('php://input', 'r');
            $jsonInput = fgets($handle);
            $json_arr = json_decode($jsonInput, true);

            //$json_arr['maid_id'] = 101;
            //$json_arr['lat'] = '25.2854';
            //$json_arr['lng'] = '51.5310';
            if(isset($json_arr['maid_id']) && strlen($json_arr['maid_id'])>0 && isset($json_arr['device_id']) && strlen($json_arr['device_id'])>0) {
                $maid_id = trim($json_arr['maid_id']);
                $device_id = trim($json_arr['device_id']);
                $update_device_id = $this->maidapp_api_model->update_device_id($maid_id,$device_id);
                echo json_encode(array('status' => 'success'));
                exit();
            }
            else
            {
                $response = array();
                $response['status'] = 'error';
                $response['error_code'] = '101';
                $response['message'] = 'Missing Parameter';

                echo json_encode($response);
                exit(); 
            }
        } else {
            $response = array();
            $response['status'] = 'error';
            $response['error_code'] = '101';
            $response['message'] = 'Invalid request';

            echo json_encode($response);
            exit();
        }
    }
	
	public function update_service_status()
    {
        if ($this->config->item('ws') == $this->input->get('ws'))
        {
			if($this->input->get('booking_id') && $this->input->get('status') && is_numeric($this->input->get('booking_id')) && $this->input->get('booking_id') > 0 && is_numeric($this->input->get('status')))
			{
				$booking_id = trim($this->input->get('booking_id'));
				$status = trim($this->input->get('status'));
				$service_date = date('Y-m-d');
				
				$chk_day_service = $this->maidapp_api_model->get_day_service_by_booking_id($service_date, $booking_id);

				switch ($status)
				{
					case 1: // service started
					if(isset($chk_day_service->day_service_id))
					{
						$response = array();
						$response['status'] = 'error';
						$response['error_code'] = '106';
						$response['message'] = $chk_day_service->service_status == 1 ? 'Service already started' : 'Service already finished';
						echo json_encode($response);
						exit();
					}
					else
					{
						$booking = $this->maidapp_api_model->get_booking_by_id($booking_id);
						// if($booking->cleaning_materials == 1)
						// {
							// $getdefault_fee = $this->maidapp_api_model->get_default_fee();
							// $materialfee = $getdefault_fee->cleaning_material_fee;
						// } else {
							// $materialfee = 0;
						// }

						// if($booking->sofa_cleaning == 1)
						// {
							// $getdefault_fee = $this->maidapp_api_model->get_default_fee();
							// $sofafee = $getdefault_fee->sofa_cleaning;
						// } else {
							// $sofafee = 0;
						// }

						$normal_hours = 0;
						$extra_hours = 0;
						$weekend_hours = 0;

						$normal_from = strtotime('06:00:00');
						$normal_to = strtotime('21:00:00');

						$shift_from = strtotime($booking->time_from . ':00');
						$shift_to = strtotime($booking->time_to . ':00');
						
						$total_hours = ($shift_to - $shift_from) / 3600;
						if($booking->cleaning_material == 'Y')
						{
							$materialfee = ($total_hours * 5);
						} else {
							$materialfee = 0;
						}
							
						//if(date('w') > 0 && date('w') < 5)
						if(date('w') != 5 )
						{					
							if($shift_from < $normal_from)
							{
								if($shift_to <= $normal_from)
								{
									$extra_hours = ($shift_to - $shift_from) / 3600;
								}

								if($shift_to > $normal_from && $shift_to <= $normal_to)
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$normal_hours = ($shift_to - $normal_from) / 3600;
								}

								if($shift_to > $normal_to)
								{
									$extra_hours = ($normal_from - $shift_from) / 3600;
									$extra_hours += ($shift_to - $normal_to) / 3600;
									$normal_hours = ($normal_to - $normal_from) / 3600;
								}
							}

							if($shift_from >= $normal_from && $shift_from < $normal_to)
							{
								if($shift_to <= $normal_to)
								{
									$normal_hours = ($shift_to - $shift_from) / 3600;
								}

								if($shift_to > $normal_to)
								{
									$normal_hours = ($normal_to - $shift_from) / 3600;
									$extra_hours = ($shift_to - $normal_to) / 3600;
								}
							}

							if($shift_from > $normal_to)
							{
								$extra_hours = ($shift_to - $shift_from) / 3600;
							}
						}
						else
						{
							$weekend_hours = ($shift_to - $shift_from) / 3600;
						}

						$service_description = array();

						$service_description['normal'] = new stdClass();
						$service_description['normal']->hours = $normal_hours;
						$service_description['normal']->fees = $normal_hours * $booking->price_hourly;

						$service_description['extra'] = new stdClass();
						$service_description['extra']->hours = $extra_hours;
						$service_description['extra']->fees = $extra_hours * $booking->price_extra;

						$service_description['weekend'] = new stdClass();
						$service_description['weekend']->hours = $weekend_hours;
						$service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

						/*
						 * Location charge
						 */
						// $location_charge = 0;

						// $customer_address = $this->pickmaid_model->get_customer_address_by_id($booking->customer_address_id);
						// $cus_detail = $this->pickmaid_model->get_customer_by_id($booking->customer_id);
						// if($customer_address){
							// $area = $this->pickmaid_model->get_area_by_id($customer_address->area_id);
							// if($area){
								// $location_charge = $area->area_charge;
							// }
						// }
						//added by vishnu
						//$total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $booking->discount;
						if($booking->customer_address == "")
						{
							$booking->customer_address = 'Building - '.$booking->building.', '.$booking->unit_no.''.$booking->street;
						} else {
							if($booking->building != "")
							{
								$addressss = "Apt No: ".$booking->building.", ".$booking->customer_address;
							} else {
								$addressss = $booking->customer_address;
							}
							$booking->customer_address = $addressss;
						}
						$total_fee = $booking->total_amount;
						$ds_fields = array();
						$ds_fields['booking_id'] = $booking_id;
						$ds_fields['customer_id'] = $booking->customer_id;
						$ds_fields['customer_name'] = $booking->customer_name;
						$ds_fields['customer_address'] = $booking->customer_address;
						$ds_fields['customer_payment_type'] = $booking->payment_type;
						$ds_fields['maid_id'] = $booking->maid_id;
						$ds_fields['service_description'] = serialize($service_description);
						$ds_fields['total_fee'] = $total_fee;
						$ds_fields['material_fee'] = $materialfee;
						$ds_fields['service_date'] = $service_date;
						$ds_fields['start_time'] = date('H:i:s');
						$ds_fields['service_status'] = 1;
						$ds_fields['service_added_by'] = 'M';
						$ds_fields['service_added_by_id'] = 0;
                    //$ds_fields['service_added_by_id'] = $tablet->tablet_id;

						$day_service = $this->maidapp_api_model->add_day_service($ds_fields);

						if($day_service)
						{
							$ds_fields_new = array();
							$ds_fields_new['booking_service_id'] = "SM-".date('Y')."-".sprintf('%04s', $day_service);
									
								$this->day_services_model->update_day_service($day_service, $ds_fields_new);
                            
							$response = array();
							$response['status'] = 'success';
							$response['booking_id'] = $booking_id;
							$response['service_status'] = 1;

							echo json_encode($response);
							exit();
						}
						else
						{
							$response = array();
							$response['status'] = 'error';
							$response['error_code'] = '105';
							$response['message'] = 'Unexpected error';

							echo json_encode($response);
							exit();
						}
					}
					break;
					
					case 2: // service finished						
					if($this->input->get('payment') !== FALSE && ($this->input->get('payment') == 0 ||  $this->input->get('payment') == 1))
					{
						$payment_status = $this->input->get('payment');
					}
					else
					{       
						$zone_id = trim($this->input->get('zone_id'));
						$response = array();
						$response['status'] = 'error';
						$response['error_code'] = '101';
						$response['message'] = 'Invalid request. Invalid payment status.';

						echo json_encode($response);
						exit();
					}
												
					if(isset($chk_day_service->day_service_id) && isset($payment_status))
					{
						if($chk_day_service->service_status == 1)
						{
							if($payment_status == 1)
							{
								if($this->input->get('amount') && is_numeric($this->input->get('amount')) &&  $this->input->get('amount') > 0 || $this->input->get('amount') == 0)
								{
									$amount = $this->input->get('amount');
									if($this->input->get('outstanding_amount') && is_numeric($this->input->get('outstanding_amount')) &&  $this->input->get('outstanding_amount') > 0)
									{
										$outstanding_amount = $this->input->get('outstanding_amount');
									} else {
										$outstanding_amount = 0;
									}

									if($this->input->get('receipt_no') && is_numeric($this->input->get('receipt_no')) &&  $this->input->get('receipt_no') > 0)
									{
										$receipt_no = $this->input->get('receipt_no');
									} else {
										$receipt_no = "";
									}

									$payment_method = 0;
									if($this->input->get('method') && is_numeric($this->input->get('method')) &&  $this->input->get('method') > 0)
									{
										//$payment_method = 1;
										$payment_method = $this->input->get('method');
									}

                                

									$payment_fields = array();
									$payment_fields['customer_id'] = $chk_day_service->customer_id;
									$payment_fields['day_service_id'] = $chk_day_service->day_service_id;//offline
									$payment_fields['paid_amount'] = $amount;
									$payment_fields['balance_amount'] = $amount;
									//$payment_fields['outstanding_amt'] = $outstanding_amount;
									$payment_fields['receipt_no'] = $receipt_no;
									$payment_fields['paid_at'] = 'M';
									//$payment_fields['paid_at_id'] = $tablet->tablet_id;
									$payment_fields['payment_method'] = $payment_method;
									$payment_fields['paid_datetime'] = date('Y-m-d H:i:s');

									$add_payment = $this->maidapp_api_model->add_customer_payment($payment_fields);
                                                                                
                               
								}
								else
								{
									$response = array();
									$response['status'] = 'error';
									$response['error_code'] = '101';
									$response['message'] = 'Invalid request. Invalid amount.';

									echo json_encode($response);
									exit();
								}
							}
							else if($chk_day_service->payment_type == 'D')
							{
								// Daily paying custmer, not paid. Send notification to manager
								//$this->push_notification(3);
							}
                                                                
							if($payment_status == 0)
							{
								
							}

							$ds_fields = array();
							$ds_fields['end_time'] = date('H:i:s');
							$ds_fields['service_status'] = 2;
							$ds_fields['payment_status'] = isset($add_payment) && $add_payment ? 1 : 0;

							$this->maidapp_api_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
							$response = array();
							$response['status'] = 'success';
							//$response['statuss'] = $res;
							$response['booking_id'] = $booking_id;
							$response['service_status'] = 2;

                            echo json_encode($response);
                            exit();
                        }
                        else
                        {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = 'Service already finished';

                            echo json_encode($response);
                            exit();
                        }
                    }
                    else
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '106';
                        $response['message'] = 'Service not started';

                        echo json_encode($response);
                        exit();
                    }
                    break;
					
                    case 3: // service not done
                    if(isset($chk_day_service->day_service_id))
                    {
                        if($chk_day_service->service_status == 1)
                        {
                            $ds_fields = array();
                            $ds_fields['end_time'] = date('H:i:s');
                            $ds_fields['service_status'] = 3;

                            $this->maidapp_api_model->update_day_service($chk_day_service->day_service_id, $ds_fields);
                            //$this->push_notification(2);

                            $response = array();
                            $response['status'] = 'success';
                            $response['booking_id'] = $booking_id;
                            $response['service_status'] = 3;

                            echo json_encode($response);
                            exit();
                        }
                        else
                        {
                            $response = array();
                            $response['status'] = 'error';
                            $response['error_code'] = '106';
                            $response['message'] = 'Service already finished';

                            echo json_encode($response);
                            exit();
                        }
                    }
                    else
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['error_code'] = '106';
                        $response['message'] = 'Service not started';

                        echo json_encode($response);
                        exit();
                    }
                    break;
					
                    default:
                    $response = array();
                    $response['status'] = 'error';
                    $response['error_code'] = '104';
                    $response['message'] = 'Invalid status';
                    break;
                }
				
        echo json_encode($response);
        exit();
    }
    else
    {
        $response = array();
        $response['status'] = 'error';
        $response['error_code'] = '101';
        $response['message'] = 'Invalid request';

        echo json_encode($response);
        exit();			
    }
    } else 
    {
        $response = array();
        $response['status'] = 'error';
        $response['error_code'] = '101';
        $response['message'] = 'Invalid request';

        echo json_encode($response);
        exit();
    }
    }
	
	public function maid_tracking()
	{
		$location_dtls = $this->maidapp_api_model->get_maid_locations();
		if (!empty($location_dtls)) {
			echo json_encode(array('status' => 'success', 'locations' =>  $location_dtls));
			exit();
		} else {
			echo json_encode(array('status' => 'success', 'message' => 'No locations'));
			exit();
		}	
	}
	
	
	function get_time_difference($time1, $time2) 
	{
		$time1 = strtotime("1/1/1980 $time1");
		$time2 = strtotime("1/1/1980 $time2");

		if ($time2 < $time1) {
			$time2 = $time2 + 86400;
		}

		return ($time2 - $time1) / 3600;
	}

}
