<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
            if(!is_user_loggedin())
            {
		redirect('logout');
            }
            $this->load->model('settings_model');
            $this->load->model('bookings_model');
            $this->load->helper('google_api_helper');
			$this->load->helper('curl_helper');
	
    }
    public function index()
    {
        if(!user_permission(user_authenticate(), 7))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        //$data = array();
        if($this->input->post('zone_sub'))
        {
            $zone_name = $this->input->post('zonename');
            $drivername = $this->input->post('drivername');
            $spare = $this->input->post('spare');

            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
            );
			
			$zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $this->input->post('zonename');
            $zone_odoo_fields['driver_name'] = $this->input->post('drivername');
            $zone_odoo_fields['spare_zone'] = $spare;
			
            $zone_id = $this->settings_model->add_zones($data);
            //$odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields);
            //$this->settings_model->update_zones(array('odoo_new_zone_id' => $odoo_zone_id,'odoo_new_zone_status' => 1),$zone_id);
        }
        if($this->input->post('zone_edit'))
        {
            $zone_id = $this->input->post('edit_zoneid');
            $zone_name = $this->input->post('edit_zonename');
            $drivername = $this->input->post('edit_drivername');
            $spare = $this->input->post('edit_spare');
             $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
            );
            $this->settings_model->update_zones($data,$zone_id);
            
            $get_zone_by_id = $this->settings_model->get_zone_details($zone_id);
            
            $zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $zone_name;
            $zone_odoo_fields['driver_name'] = $drivername;
            $zone_odoo_fields['spare_zone'] = $spare;
            //$odo_zone_affected = $this->update_zone_inodoo($zone_odoo_fields, $get_zone_by_id[0]['odoo_new_zone_id']);
        }
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('zone_list', $data, TRUE);
	$layout_data['page_title'] = 'zones';
	$layout_data['meta_description'] = 'zones';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();  
        $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    public function edit_team()
    {
        $team_id = $this->input->post('team_id');
        $result = $this->settings_model->get_team_details($team_id);
        echo json_encode($result);
    }
    public function edit_zone()
    {
        $zone_id = $this->input->post('zone_id');
        $result = $this->settings_model->get_zone_details($zone_id);
        echo json_encode($result);
    }
    public function remove_zone()
    {
        $zone_id = $this->input->post('zone_id');
		$data = $this->settings_model->delete_zone_new(array('zone_status' => 0),$zone_id);
        //$data = $this->settings_model->delete_zone($zone_id);
        echo $data;
    }
    public function team()
    {
        //$data = array();
        if($this->input->post('team_sub'))
        {
            $team_name = $this->input->post('teamname');

            $data = array(
                'team_name' => $team_name,
                'team_status' => 1,
            );
            $this->settings_model->add_teams($data);
        }
        if($this->input->post('team_edit'))
        {
            $team_id = $this->input->post('edit_teamid');
            $team_name = $this->input->post('edit_teamname');
            //$drivername = $this->input->post('edit_drivername');
            //$spare = $this->input->post('edit_spare');
             $data = array(
                'team_name' => $team_name,
                //'driver_name' => $drivername,
                //'spare_zone' => $spare,
                'team_status' => 1,
            );
             $this->settings_model->update_teams($data,$team_id);
        }
        $data['teams'] = $this->settings_model->get_teams();
        $layout_data['content_body'] = $this->load->view('team_list', $data, TRUE);
	$layout_data['page_title'] = 'Teams';
	$layout_data['meta_description'] = 'Teams';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
         $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js');
	$this->load->view('layouts/default', $layout_data);
    }
    public function area()
    {
        if(!user_permission(user_authenticate(), 8))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('area_sub'))
        {
            $areaname = $this->input->post('areaname');
            $zone_id = $this->input->post('zone_id');
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
            );
            $area_id = $this->settings_model->add_area($data);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
			
			$zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_new_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
			
			//$odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            //$this->settings_model->update_area(array('odoo_new_area_id' => $odoo_area_id,'odoo_new_area_status' => 1),$area_id);
        }
        if($this->input->post('area_edit'))
        {
            $areaname = $this->input->post('edit_areaname');
            $area_id = $this->input->post('edit_areaid');
            $zone_id = $this->input->post('edit_zone_id');
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
            );
            $this->settings_model->update_area($data,$area_id);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
			$zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_new_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
			//$odo_area_affected = $this->update_area_inodoo($zone_area_fields, $get_area_by_id[0]['odoo_new_area_id']);
        }
        $data['areas'] = $this->settings_model->get_areas();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('area_list', $data, TRUE);
	$layout_data['page_title'] = 'area';
	$layout_data['meta_description'] = 'area';
	$layout_data['css_files'] = array('demo.css');
	$layout_data['external_js_files'] = array();
         $layout_data['settings_active'] = '1'; 
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function edit_area()
    {
        $area_id = $this->input->post('area_id');
        $result = $this->settings_model->get_area_details($area_id);
        echo json_encode($result);
    }
    public function remove_area()
    {
        $area_id = $this->input->post('area_id');
		$data = $this->settings_model->update_area(array('area_status' => 0),$area_id);
        //$data = $this->settings_model->delete_area($area_id);
    }
    public function flats()
    {
        if(!user_permission(user_authenticate(), 9))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('flat_sub'))
        {
            $flatname = $this->input->post('flatname');
            $tablet_imei = $this->input->post('tablet_imei');
            $data = array(
                'flat_name' => $flatname,
                'tablet_imei' => $tablet_imei,
                'flat_status' => 1,
            );
            $flat_id = $this->settings_model->add_flats($data);
			$flat_odoo_fields = array();
            $flat_odoo_fields['name'] = $this->input->post('flatname');
            $flat_odoo_fields['code'] = $this->input->post('tablet_imei');
			
			//$odoo_flat_id = $this->do_flat_odoo_api($flat_odoo_fields);
            //$this->settings_model->update_flats(array('odoo_new_flat_id' => $odoo_flat_id,'odoo_new_syncflat_status' => 1),$flat_id);
        }
        if($this->input->post('flat_edit'))
        {
            $flat_id = $this->input->post('edit_flatid');
            $flat_name = $this->input->post('edit_flatname');
            $tablet_imei = $this->input->post('edit_tablet_imei');
            $data = array(
                'flat_name' => $flat_name,
                'tablet_imei' => $tablet_imei,
                'flat_status' => 1,
            );
            $this->settings_model->update_flats($data,$flat_id);
			$get_flat_by_id = $this->settings_model->get_flat_details($flat_id);
			$flat_odoo_fields = array();
            $flat_odoo_fields['name'] = $flat_name;
            $flat_odoo_fields['code'] = $tablet_imei;
            //$odo_flat_affected = $this->update_flat_inodoo($flat_odoo_fields, $get_flat_by_id[0]['odoo_new_flat_id']); 
        }
        $data['flats'] = $this->settings_model->get_flats();
        $layout_data['content_body'] = $this->load->view('flat_list', $data, TRUE);
	$layout_data['page_title'] = 'flats';
	$layout_data['meta_description'] = 'flats';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
         $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    public function edit_flat()
    {
        $flat_id = $this->input->post('flat_id');
        $result = $this->settings_model->get_flat_details($flat_id);
        echo json_encode($result);
    }
    public function remove_flat()
    {
        $flat_id = $this->input->post('flat_id');
        $data = $this->settings_model->delete_flat($flat_id);
    }
    public function services()
    {
        if(!user_permission(user_authenticate(), 11))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('services_sub'))
        {
            $service_type_name = $this->input->post('service_type_name');
            $service_rate = $this->input->post('service_rate');
            $data = array(
                'service_type_name' => $service_type_name,
                'service_rate' => $service_rate,
                'service_type_status' => 1,
            );
            $this->settings_model->add_services($data);
        }
        if($this->input->post('services_edit'))
        {
            $service_id = $this->input->post('edit_service_type_id');
            $service_type_name = $this->input->post('edit_service_type_name');
            $service_rate = $this->input->post('edit_service_rate');
            $data = array(
                'service_type_name' => $service_type_name,
                'service_rate' => $service_rate,
                'service_type_status' => 1,
            );
             $this->settings_model->update_services($data,$service_id);
        }
        $data['services'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('services_list', $data, TRUE);
	$layout_data['page_title'] = 'services';
	$layout_data['meta_description'] = 'services';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    public function edit_services()
    {
        $service_id = $this->input->post('service_id');
        $result = $this->settings_model->get_service_details($service_id);
        echo json_encode($result);
    }
    function remove_services()
    {
        $service_id = $this->input->post('service_id');
        $data = $this->settings_model->delete_services($service_id);
    }
    public function tablets()
    {
        if(!user_permission(user_authenticate(), 10))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('tablet_sub'))
        {
            $zone_id = $this->input->post('zone_id');
            $access_code = $this->input->post('access_code');
            $imei = $this->input->post('imei');
            $drivername = $this->input->post('drivernameval');
			$username = $this->input->post('username');
            $password_tab = $this->input->post('password_tab');		
            $data = array(
                'zone_id' => $zone_id,
                'access_code' => $access_code,
                'imei' => $imei,
				'tablet_username' => $username,
                'tablet_password' => $password_tab,
                'tablet_driver_name' => $drivername,
                'tablet_status' => 1,
            );
			$this->settings_model->add_tablet($data);
			$errors = '';
            $data['error'] = $errors;
        }
        if($this->input->post('tablet_edit'))
        {
            $access_code = $this->input->post('edit_access_code');
            $tablet_id = $this->input->post('edit_tabletid');
            $imei = $this->input->post('edit_imei');
            $username = $this->input->post('edit_username_tab');
            $password_tab = $this->input->post('edit_password_tab');
            $zone_id = $this->input->post('edit_zone_id');
			$drivername = $this->input->post('editdrivernameval');
            $data = array(
                'zone_id' => $zone_id,
                'tablet_driver_name' => $drivername,
                'imei' => $imei,
				'tablet_username' => $username,
                'tablet_password' => $password_tab,
            );
            
			$errors = '';
            $this->settings_model->update_tablet($data,$tablet_id);
            $data['error'] = $errors;

        }
        $data['tablets'] = $this->settings_model->get_tablets();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('tablets_list', $data, TRUE);
	$layout_data['page_title'] = 'tablets';
	$layout_data['meta_description'] = 'tablets';
	$layout_data['css_files'] = array('demo.css');
	$layout_data['external_js_files'] = array();
         $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js','jquery.dataTables.min.js');
	$this->load->view('layouts/default', $layout_data);
    }
    function edit_tablet()
    {
        $tablet_id = $this->input->post('tablet_id');
        $result = $this->settings_model->get_tablet_details($tablet_id);
        echo json_encode($result);
    }
    function tablet_status()
    {
        $tablet_id = $this->input->post('tablet_id');
        $status = $this->input->post('status');
        if($status == 1)
        {
            $data = array(
                'tablet_status' => 0
            );
            $this->settings_model->disable_status($tablet_id,$data);
        } else {
            // $dataa = $this->settings_model->activate_status_check($tablet_id);
            // print_r($dataa);
            
            if($this->settings_model->activate_status_check($tablet_id) < 1 ) {
                $error = '';
                $data = array(
                    'tablet_status' => 1
                );
                $this->settings_model->activate_status($tablet_id,$data);
            } else {
                $error = "Can't activate this tablet.Tablet already available in the zone.";
            }
            echo $error;
        }
    }
    /*
    * Modules
    */
    public function modules()
    {
            $e_module_id = 0;
            $e_module = array();

            if($this->uri->segment(3) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0)
            {
                    $e_module_id = $this->uri->segment(3);
                    $e_module = $this->modules_model->get_module_by_id($e_module_id);
            }

            if($this->input->post('add_module'))
            {
                    $errors = array();

                    $this->load->library('form_validation');
                    $this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');		

                    $this->form_validation->set_rules('module_name', 'Module Name', 'required');
                    $this->form_validation->set_message('required', 'Please enter module name', 'module_name');

                    if($this->form_validation->run())
                    {
                            $module_fields = array();

                            $module_fields['module_name'] = strtolower(trim($this->input->post('module_name')));
                            $module_fields['parent_id'] = trim($this->input->post('parent_id'));

                            $chk_module = $this->users_model->get_module_by_module_name($module_fields['module_name'], $module_fields['parent_id']);

                            if($e_module_id > 0)
                            {
                                if(isset($chk_module->module_id) && $chk_module->module_id != $e_module->module_id)
                                {
                                        $errors['module_name_error'] = 'This module already exists';
                                }
                                else
                                {
                                        $update_module = $this->users_model->update_module($e_module->module_id, $module_fields);

                                        if(is_numeric($update_module) && $update_module > 0)
                                        {
                                                $this->session->set_flashdata('success_msg', 'Module details has been updated successfully.');
                                                redirect(base_url() . 'settings/modules');
                                                exit();
                                        }
                                }
                            }
                            else
                            {
                                if(isset($chk_module->module_id))
                                {
                                        $errors['module_name_error'] = 'This module already exists';
                                }
                                else
                                {
                                        $module_id = $this->users_model->add_module($module_fields);

                                        if(is_numeric($module_id) && $module_id > 0)
                                        {
                                                $this->session->set_flashdata('success_msg', 'Module has been added successfully.');
                                                redirect(base_url() . 'settings/modules');
                                                exit();
                                        }
                                }
                            }
                    }
                    else
                    {
                            $errors = $this->form_validation->error_array();
                    }
            }

            $modules = $this->users_model->get_all_modules(FALSE);

            $parent_modules = $this->users_model->get_parent_modules(FALSE);

            $module_array = array();
            $module_array['0'] = 'Select Module';
            foreach($parent_modules as $module)
            {
                    $module_array[$module->module_id] = ucwords($module->module_name);
            }

            $data = array();
            $data['error'] = isset($errors) ? array_shift($errors) : '';
            $data['modules'] = $modules;
            $data['sel_modules'] = $module_array;
            $data['e_module'] = $e_module;
            $layout_data['content_body'] = $this->load->view('modules', $data, TRUE);

            $layout_data['page_title'] = 'Modules';
            $layout_data['meta_description'] = 'Modules';
            $layout_data['css_files'] = array();
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js');

            $this->load->view('layouts/default', $layout_data);
    }
    function delete_module($id)
    {
        if ($this->users_model->delete_module($id))
        {

            $this->session->set_flashdata('success_msg', 'Module has been deleted successfully.');
        }
        else
        {
            $this->session->set_flashdata('failure_msg', 'Can\'t delete the module.');
        }
        redirect('/settings/modules/','refresh');
    }
    function backpayment()
    {
        
        $this->load->model('customers_model');
        
        if($this->input->post('payment_date') && strlen($this->input->post('payment_date')) > 0)
        {
            $payment_date = trim($this->input->post('payment_date'));
        }
        else
        {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        
        
        $payments = $this->customers_model->get_back_payment($date);
        $back_payment = array();
        foreach ($payments as $payment)
        {
            $customer_balance = $this->customers_model->get_customer_balance($payment->customer_id);
            $payment->balance = ($customer_balance->amount ? $customer_balance->amount : 0) - $payment->collected_amount;
            array_push($back_payment, $payment);
        }
        
        $data = array();
        $data['back_payment'] = array();
        $data['payment_date'] = $payment_date;
        $data['back_payment'] = $back_payment;
        
        $layout_data['content_body'] = $this->load->view('back_payment', $data, TRUE);

        $layout_data['page_title'] = 'Back Payment';
        $layout_data['meta_description'] = 'Back Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array(); 
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js','mymaids.js' );
 $layout_data['reports_active'] = '1'; 
        $this->load->view('layouts/default', $layout_data);
    }
    function add_backpayment()
    {
        
        $this->load->model('customers_model');
        $this->load->model('tablets_model');
        
        if($this->input->is_ajax_request())
        {
                if($this->input->post('action') && $this->input->post('action') == 'get-balance-amount')
                {
                    if($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
                    {
                        $customer_id = $this->input->post('customer_id');
                        
                        $customer_balance = $this->customers_model->get_customer_balance($customer_id);
                        
                        $response = array();
                        $response['status'] = 'success';
                        $response['balance'] = $customer_balance->amount ? $customer_balance->amount : 0;
                        
                        echo json_encode($response);
                        exit();
                    }
                    else
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Please select customer';
                        
                        echo json_encode($response);
                        exit();
                    }
                }
        }
        $errors = array();
        if($this->input->post('add_back_payment'))
        {
            $customer_id = trim($this->input->post('customer_id'));
            $ids = explode("-", trim($this->input->post('tablet_id')));
            $tablet_id = $ids[0];
            $zone_id = $ids[1];
            //edited by vishnu
            //$collected_amount = trim($this->input->post('collected_amount'));
            //$requested_amount = trim($this->input->post('requested_amount'));
            $requested_amount = trim($this->input->post('collected_amount'));
            //ends
            $collected_date = date("Y-m-d", strtotime(str_replace("/", "-", trim($this->input->post('collected_date')))));
            
            $back_payment_fields = array();
            $back_payment_fields['customer_id'] = $customer_id;
            $back_payment_fields['tablet_id'] = $tablet_id;
            $back_payment_fields['zone_id'] = $zone_id;
            //edited by vishnu
            //$back_payment_fields['collected_amount'] = $collected_amount;
            //$back_payment_fields['requested_amount'] = $requested_amount;
            $back_payment_fields['requested_amount'] = $requested_amount;
            //ends
            $back_payment_fields['collected_date'] = $collected_date;
            $back_payment_fields['collected_time'] = date('H:i:s');
            
            $back_payment_id = $this->customers_model->add_backpayment($back_payment_fields);
            $customer_name = $this->customers_model->getcustomernamebyid($customer_id);
            
            $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone_id);
					
            if(isset($tzone_tablet->google_reg_id))
            {
                //edited by vishnu
                //$messages = "Back Payment added for customer ".$customer_name->customer_name." an amount of ".$collected_amount;
                $messages = "Back Payment added for customer ".$customer_name->customer_name." an amount of ".$requested_amount;
                //ends
                $push_fields = array();
                $push_fields['tab_id'] = $tablet_id;
                $push_fields['type'] = 4;
                $push_fields['message'] = $messages;
                $push = $this->bookings_model->add_push_notifications($push_fields);
                
                $return = android_push(array($tzone_tablet->google_reg_id), array('message' => $messages));
            }
            
            if($back_payment_id > 0)
            {                
                $errors['class'] = 'success';
                $errors['message'] = '<strong>Success!</strong> Back Payment added Successfully.';
            }
            else
            {
                $errors['class'] = 'warning';
                $errors['message'] = '<strong>Error!</strong> Something went wrong!';
            }
            /*
            $payment_fields = array();
            $payment_fields['customer_id'] = $customer_id;
            $payment_fields['tablet_id'] = $tablet_id;
            $payment_fields['zone_id'] = $zone_id;
            $payment_fields['collected_amount'] = $collected_amount;
            $payment_fields['requested_amount'] = $requested_amount;
            $this->customers_model->add_customer_payment($payment_fields);
            */
            
        }
        
        $data = array();
        $data['errors'] = $errors;
        $data['collected_date'] = date('d/m/Y');
        $data['customers'] = $this->customers_model->get_customers();
        $data['tablets'] = $this->tablets_model->get_all_tablets(TRUE, FALSE);
        
        $layout_data['content_body'] = $this->load->view('add_back_payment', $data, TRUE);

        $layout_data['page_title'] = 'Add Back Payment';
        $layout_data['meta_description'] = 'Add Back Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array(); 
        $layout_data['js_files'] = array('mymaids.js' );

        $this->load->view('layouts/default', $layout_data);
    }
    
    
    /*
     * Payment settings 
     * Author ; Jiby
     * Date : 09-10-17
     */
    
     public function payment_settings()
    {
        if(!user_permission(user_authenticate(), 11))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('hrly_price_sub'))
        {
            $from_hrly_name = $this->input->post('from_hrly_name');
            $to_hrly_name = $this->input->post('to_hrly_name');
            $service_rate = $this->input->post('hrly_price');
            $data = array(
                  'from_hr' => $from_hrly_name,
                  'to_hr' => $to_hrly_name,
                'price' => $service_rate,
                'status' => 1,
            );
            $this->settings_model->add_hrly_price($data);
        }
        if($this->input->post('price_charges_edit'))
        {
            $service_id = $this->input->post('edit_ps_id');
            
             $from_hrly_name = $this->input->post('edit_from_hrly_name');
            $to_hrly_name = $this->input->post('edit_to_hrly_name');
            
            $service_rate = $this->input->post('edit_hourly_rate');
            $data = array(
                  'from_hr' => $from_hrly_name,
                  'to_hr' => $to_hrly_name,
                'price' => $service_rate,
                'status' => 1,
            );
             $this->settings_model->update_hourly_charges($data,$service_id);
        }
        $data['price_charges'] = $this->settings_model->get_prices();
        $layout_data['content_body'] = $this->load->view('price_list', $data, TRUE);
	$layout_data['page_title'] = 'Price settings';
	$layout_data['meta_description'] = 'price settings';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
         $layout_data['settings_active'] = '1'; 
	$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
    
    public function edit_hourly_price()
    {
        $service_id = $this->input->post('ps_id');
        $result = $this->settings_model->get_hourly_price_details($service_id);
        echo json_encode($result);
    }
    
    
       function remove_hrly_charge()
    {
        $service_id = $this->input->post('ps_id');
        $data = $this->settings_model->delete_hrly_price($service_id);
    }
    
    
     public function sms_settings()
    {
        if(!user_permission(user_authenticate(), 11))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
       
        if($this->input->post('sms_edit_sub'))
        {
            $sms_id = $this->input->post('edit_sms_id');
             $user_name = $this->input->post('edit_user');
            $pass = $this->input->post('edit_pass');
            
            $sender_id = $this->input->post('edit_sender_id');
            $api = $this->input->post('edit_api');
            $data = array(
                  'sender_id' => $sender_id,
                  'api_url' => $api,
                'user' => $user_name,
                'pass' => $pass,
            );
             $this->settings_model->update_sms_settings($data,$sms_id);
        }
        $data['sms_settings']           = $this->settings_model->get_sms_info();
        $layout_data['content_body']    = $this->load->view('sms_list', $data, TRUE);
	$layout_data['page_title']      = 'SMS settings';
	$layout_data['meta_description'] = 'SMS settings';
	$layout_data['css_files']       = array();
	$layout_data['external_js_files'] = array();
         $layout_data['account_active'] = '1'; 
	$layout_data['js_files']        = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
     public function edit_sms_settings()
    {
        $sms_id = $this->input->post('sms_id');
        $result = $this->settings_model->get_sms_details($sms_id);
        echo json_encode($result);
    }
    
    public function email_settings()
    {
        if(!user_permission(user_authenticate(), 11))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
       
        if($this->input->post('email_edit_sub'))
        {
            $email_id = $this->input->post('edit_email_id');
            $header_name = $this->input->post('edit_header_name');
            $from = $this->input->post('edit_from');
            $subject = $this->input->post('edit_subject');
            
            $data = array(
                  'id' => $email_id,
                  'header_name' => $header_name,
                'subject' => $subject,
                'from_address' => $from,
            );
             $this->settings_model->update_email_settings($data,$email_id);
        }
        $data['email_settings']           = $this->settings_model->get_email_info();
        $layout_data['content_body']    = $this->load->view('email_list', $data, TRUE);
	$layout_data['page_title']      = 'Email settings';
	$layout_data['meta_description'] = 'Email settings';
	$layout_data['css_files']       = array();
	$layout_data['external_js_files'] = array();
           $layout_data['account_active'] = '1'; 
	$layout_data['js_files']        = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
    public function edit_email_settings()
    {
        $email_id = $this->input->post('e_id');
        $result = $this->settings_model->get_email_details($email_id);
        echo json_encode($result);
    }
    
      public function check_hourly_price() {
        $hrs    = $this->input->post('total_week_hours');
        $result = $this->settings_model->get_prices();
        $rate   = '';
        foreach ($result as $res) {
            if (($hrs >= $res->from_hr) && ($hrs <= $res->to_hr)) {
                $rate = $res->price;
                break;
            }
        }
        echo $rate;
        exit;
    }
    
    public function check_hourly_price_new() {
        $customerid    = $this->input->post('customer_id');
        $result = $this->settings_model->get_customer_per_hrprices($customerid);
        $rate   = $result->price_hourly;
//        foreach ($result as $res) {
//            if (($hrs >= $res->from_hr) && ($hrs <= $res->to_hr)) {
//                $rate = $res->price;
//                break;
//            }
//        }
        echo $rate;
        exit;
    }
    
    public function tax_settings()
    {
        if(!user_permission(user_authenticate(), 11))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
       
        if($this->input->post('tax_edit_sub'))
        {
            $tax_id = $this->input->post('edit_taxt_id');
            $percentage = $this->input->post('edit_percentage');
            
            $data = array(
                  'tax_id' => $tax_id,
                  'percentage' => $percentage,
                'status' => 1,
            );
            $this->settings_model->update_tax_settings($data,$tax_id);
        }
        $data['tax_settings']           = $this->settings_model->get_tax_info();
        $layout_data['content_body']    = $this->load->view('tax_list', $data, TRUE);
	$layout_data['page_title']      = 'Tax settings';
	$layout_data['meta_description'] = 'Tax settings';
	$layout_data['css_files']       = array();
	$layout_data['external_js_files'] = array();
        $layout_data['account_active'] = '1'; 
	$layout_data['js_files']        = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
    public function edit_tax_settings()
    {
        $tax_id = $this->input->post('t_id');
        $result = $this->settings_model->get_tax_details($tax_id);
        echo json_encode($result);
    }
	
	public function coupons()
	{
		$data = array();
        $data['coupons'] = $this->settings_model->get_coupons();
        $layout_data['content_body'] = $this->load->view('coupons', $data, TRUE);
		$layout_data['page_title'] = 'Coupons';
		$layout_data['meta_description'] = 'Coupons';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('users.js');
		$layout_data['account_active'] = '1'; 
		$this->load->view('layouts/default', $layout_data);
	}
	
	public function add_coupons()
    {
		$e_coupon = array();
        if($this->input->post('coupon_sub'))
        {
            $errors = array();
			
            $this->load->library('form_validation');
            
			
			//$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('couponname', 'Coupon Name', 'required');
            //$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

            $this->form_validation->set_rules('expirydate', 'Expiry Date', 'required');
            //$this->form_validation->set_message('required', 'Select a date', 'expirydate');
			
            $this->form_validation->set_rules('service_type', 'Expiry Date', 'required');

			$this->form_validation->set_rules('coupontype', 'Coupon Type', 'required');
			
			//$this->form_validation->set_rules('offertype', 'Offer Type', 'required');
            //$this->form_validation->set_message('required', 'Select offer type', 'offertype');
			
			$this->form_validation->set_rules('discount_price', 'Price', 'required');
            //$this->form_validation->set_message('required', 'Enter Price', 'discountprice');
			
			//$this->form_validation->set_rules('discounttype', 'Discount Type', 'required');
            //$this->form_validation->set_message('required', 'Select an option', 'discounttype');
			
			$this->form_validation->set_rules('minhour', 'Minimum Hour', 'required');
			
			$this->form_validation->set_rules('w_day', 'Week Day', 'required');
            //$this->form_validation->set_message('required', 'Please select the days', 'w_day');
			
			$this->form_validation->set_rules('status', 'Status', 'required');
            // $this->form_validation->set_message('required', 'Select status', 'status');
            
            if ($this->form_validation->run() == TRUE)
            {
                $coupon_name = $this->input->post('couponname');
                $expirydate = $this->input->post('expirydate');
                $service_type = $this->input->post('service_type');
				$coupontype = $this->input->post('coupontype');
                //$offertype = 'P';
                $offertype = $this->input->post('offertype');
				if($this->input->post('offertype') == 'F')
				{
					//$discountprice = $this->input->post('discountprice');
					$discount_type = $this->input->post('discounttype'); 
				} else {
					//$discountprice = $this->input->post('discount_price');
					$discount_type  = 1; 
				}
				$discountprice = $this->input->post('discount_price');
				//$discount_type  = 1;
				$valid_week_day = implode(",", $this->input->post('w_day'));	
				//$valid_week_day = '0,1,2,3,4,6';	
				$status      = $this->input->post('status');
				$added_date     = date('Y-m-d');
				$min_hr = $this->input->post('minhour');
				
				$time_fields = array();
				$time_fields['coupon_name']        = $coupon_name; 
                $time_fields['service_id']        = $service_type; 
				$time_fields['expiry_date']        = $expirydate; 
				$time_fields['type']        	   = "C"; 
				$time_fields['coupon_type']         = $coupontype;
				$time_fields['offer_type']         = $offertype;
				$time_fields['percentage']         = $discountprice;
                $time_fields['discount_type']      = $discount_type; 
				$time_fields['valid_week_day']     = $valid_week_day;
				$time_fields['status']             = $status;
				$time_fields['min_hrs']             = $min_hr;
				$time_fields['added_date']         = date('Y-m-d H:i:s'); 
				
				$coupon_add                        = $this->settings_model->add_coupon($time_fields);
				if($coupon_add > 0)
				{
					redirect(base_url() . 'coupons');
					exit();
				}
            } 
            else
            {
                $errors = $this->form_validation->error_array();
            }
        }
        $data = array();
        $data['e_coupon'] = $e_coupon;
        $data['services'] = $this->settings_model->get_services();
        $data['error'] = isset($errors) ? array_shift($errors) : '';
        $layout_data['content_body'] = $this->load->view('add_coupon', $data, TRUE);
		$layout_data['page_title'] = 'Coupons';
		$layout_data['meta_description'] = 'Coupons';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('base.js','bootstrap-datepicker.js','users.js');
		$this->load->view('layouts/default', $layout_data);
    }
	
    public function check_coupon_customer()
    {
        $customer_id=$this->input->post('customer_id');
        $coupon_id=$this->input->post('coupon_id');
        $customer_coupon_count=$this->settings_model->check_coupon_customer($customer_id,$coupon_id);
        echo $customer_coupon_count;
        exit();
    }

	public function edit_coupon()
	{
		$e_coupon_id = 0;
        $e_coupon = array();
        if($this->uri->segment(3) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0)
        {
			if($this->input->post('coupon_sub'))
			{
				$errors = array();
				
				$this->load->library('form_validation');
				
				
				//$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

				$this->form_validation->set_rules('couponname', 'Coupon Name', 'required');
				//$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

				$this->form_validation->set_rules('expirydate', 'Expiry Date', 'required');
				//$this->form_validation->set_message('required', 'Select a date', 'expirydate');

                $this->form_validation->set_rules('service_type', 'Expiry Date', 'required');
				
				$this->form_validation->set_rules('discountprice', 'Price', 'required');
				//$this->form_validation->set_message('required', 'Enter Price', 'discountprice');
				
				$this->form_validation->set_rules('minhour', 'Minimum Hour', 'required');
				
				$this->form_validation->set_rules('w_day', 'Week Day', 'required');
				//$this->form_validation->set_message('required', 'Please select the days', 'w_day');
				
				$this->form_validation->set_rules('status', 'Status', 'required');
				// $this->form_validation->set_message('required', 'Select status', 'status');
				
				if ($this->form_validation->run() == TRUE)
				{
					$e_coupon_id = $this->uri->segment(3);
					$coupon_name = $this->input->post('couponname');
					$expirydate = $this->input->post('expirydate');
                    $service_type = $this->input->post('service_type');
					$discountprice = $this->input->post('discountprice');
					$valid_week_day = implode(",", $this->input->post('w_day'));	
					$status      = $this->input->post('status');
					$min_hr = $this->input->post('minhour');
					$added_date     = date('Y-m-d');
					
					$time_fields = array();
					$time_fields['coupon_name']        = $coupon_name;
                    $time_fields['service_id']        = $service_type; 
					$time_fields['expiry_date']        = $expirydate;
					$time_fields['percentage']         = $discountprice;
					$time_fields['valid_week_day']     = $valid_week_day;
					$time_fields['status']             = $status;
					$time_fields['min_hrs']             = $min_hr;
					$time_fields['added_date']         = date('Y-m-d H:i:s'); 
					
					$coupon_add                        = $this->settings_model->update_coupon($e_coupon_id,$time_fields);
					if($coupon_add)
					{
						redirect(base_url() . 'coupons');
						exit();
					}
				} 
				else
				{
					$errors = $this->form_validation->error_array();
				}
			}
		
            $e_coupon_id = $this->uri->segment(3);
			$data = array();
			$data['coupon_info'] = $this->settings_model->get_coupon_by_id($e_coupon_id);
            $data['services'] = $this->settings_model->get_services();
			$data['coupon_id'] = $e_coupon_id;
			$data['error'] = isset($errors) ? array_shift($errors) : '';
			$layout_data['content_body'] = $this->load->view('edit_coupon', $data, TRUE);
			$layout_data['page_title'] = 'Coupons';
			$layout_data['meta_description'] = 'Coupons';
			$layout_data['css_files'] = array('datepicker.css');
			$layout_data['external_js_files'] = array();
			$layout_data['js_files'] = array('base.js','bootstrap-datepicker.js','users.js');
			$this->load->view('layouts/default', $layout_data);
        } else {
			redirect(base_url() . 'coupons');
			exit();
		}
	}
	
	public function remove_coupon()
    {
        $coupon_id = $this->input->post('coupon_id');
        $data = array(
            'status' => '0',
        );
        $datas = $this->settings_model->update_coupon($coupon_id, $data);
		echo "success";
    }
	
	public function souqmaid_price()
    {
        if($this->input->post('services_rate_sub'))
        {
            $service_date_val = $this->input->post('service_date_val');
            $service_rate = $this->input->post('service_rate');
			list($day, $month, $year) = explode("/", $service_date_val);
			$service_date = "$year-$month-$day";
            $checkexist = $this->settings_model->get_offer_list($service_date);
			if(count($checkexist) > 0)
			{
				echo '<script language="javascript">';
				echo 'alert("Already added...")';
				echo '</script>';
			} else {
				$data = array(
					'offer_date' => $service_date,
					'offer_price' => $service_rate,
					'added_date_time' => date('Y-m-d h:i:s'),
					'added_by' => user_authenticate(),
				);
				$this->settings_model->add_souqmaid_price($data);
			}
        }
        if($this->input->post('services_rate_edit'))
        {
            $service_price_id = $this->input->post('edit_service_offer_id');
            $service_rate = $this->input->post('edit_service_rate');
            $data = array(
                'offer_price' => $service_rate,
                'updated_date_time' => date('Y-m-d h:i:s'),
                'updated_by' => user_authenticate(),
            );
             $this->settings_model->update_souqmaid_price($data,$service_price_id);
        }
        $data['prices'] = $this->settings_model->get_souqmaid_pricelist();
        $layout_data['content_body'] = $this->load->view('souq_price_list', $data, TRUE);
		$layout_data['page_title'] = 'Souq Maids Price';
		$layout_data['meta_description'] = 'Souq Maids Price';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1'; 
		$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
		$this->load->view('layouts/default', $layout_data);
    }
	
	public function edit_souqprice()
    {
        $price_id = $this->input->post('price_id');
        $result = $this->settings_model->get_souqmaid_pricelist_byid($price_id);
        echo json_encode($result);
    }
	
	public function do_zone_odoo_api($data = array())
    {
        if($data['spare_zone'] == 'Y')
		{
			$spare = TRUE;
		} else {
			$spare = FALSE;
		}
		$post['params']['user_id'] = 1;
		$post['params']['name'] = $data['zone_name'];
		$post['params']['driver_name'] = $data['driver_name'];
		$post['params']['spare_zone'] = $spare;
		$post_values=json_encode($post);

		$url = $this->config->item('odoo_url')."zone_creation";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_zone_id = $returnData->result->response->id;
		}
    }
	
	public function update_zone_inodoo($data,$zone_id)
	{
		if($data['spare_zone'] == 'Y')
		{
			$spare = TRUE;
		} else {
			$spare = FALSE;
		}
		$post['params']['user_id'] = 1;
		$post['params']['id'] = $zone_id;
		$post['params']['name'] = $data['zone_name'];
		$post['params']['driver_name'] = $data['driver_name'];
		$post['params']['spare_zone'] = $spare;
		$post_values=json_encode($post);

		$url = $this->config->item('odoo_url')."zone_edition";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_zone_id = $zone_id;
		}
	}
	
	public function do_area_odoo_api($data = array())
    {
		$post['params']['user_id'] = 1;
		$post['params']['name'] = $data['area_name'];
		$post['params']['zone_id'] = $data['zone_id'];
		$post['params']['location_charge'] = $data['area_charge'];
		$post_values=json_encode($post);
		$url = $this->config->item('odoo_url')."area_creation";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_area_id = $returnData->result->response->id;
			
		}
    }
	
	public function update_area_inodoo($data,$area_id)
	{
		$post['params']['user_id'] = 1;
		$post['params']['id'] = $area_id;
		$post['params']['name'] = $data['area_name'];
		$post['params']['zone_id'] = $data['zone_id'];
		$post['params']['location_charge'] = $data['area_charge'];;
		$post_values=json_encode($post);

		$url = $this->config->item('odoo_url')."area_edition";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_area_id = $area_id;
		}
	}
	
	public function do_flat_odoo_api($data = array())
    {	
		$post['params']['user_id'] = 1;
		$post['params']['name'] = $data['name'];
		$post['params']['code'] = $data['code'];
		$post_values=json_encode($post);

		$url = $this->config->item('odoo_url')."flat_creation";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_flat_id = $returnData->result->response->id;
		}
    }
	
	public function update_flat_inodoo($data,$flat_id)
	{
		$post['params']['user_id'] = 1;
		$post['params']['id'] = $flat_id;
		$post['params']['name'] = $data['name'];
		$post['params']['code'] = $data['code'];
		$post_values=json_encode($post);

		$url = $this->config->item('odoo_url')."flat_edition";
		$login_check = curl_api_service($post_values,$url);
		$returnData=json_decode($login_check);
		
		if($returnData->result->status=="success")
		{
			return $odoo_flat_id = $flat_id;
		}
	}
	
	public function export_zone_to_odoo_api()
    {
		$zones = $this->settings_model->get_zones_api();
		$i = 0;
        foreach ($zones as $zone)
        {
            if($zone['spare_zone'] == 'Y')
			{
				$spare = TRUE;
			} else {
				$spare = FALSE;
			}
			
            $post['params']['user_id'] = 1;
			$post['params']['name'] = $zone['zone_name'];
			$post['params']['driver_name'] = $zone['driver_name'];
			$post['params']['spare_zone'] = $spare;
			$post_values=json_encode($post);
			$url = $this->config->item('odoo_url')."zone_creation";
			$login_check = curl_api_service($post_values,$url);
			$returnData=json_decode($login_check);
			
			if($returnData->result->status=="success")
			{
				$odoo_zone_id = $returnData->result->response->id;
				if($odoo_zone_id > 0)
				{
					$response = $this->settings_model->update_zones(array('odoo_new_zone_id' => $odoo_zone_id,'odoo_new_zone_status' => 1),$zone['zone_id']);
				}
				echo $i++.'-'.$odoo_zone_id.'<br>';
			}
        }
		exit();
	}
	
	public function export_areas_to_odoo_api()
    {
		$areas = $this->settings_model->get_areas_api();
		$i = 0;
        foreach ($areas as $area)
        {
            $post['params']['user_id'] = 1;
			$post['params']['name'] = $area['area_name'];
			$post['params']['zone_id'] = $area['odoo_zone_id'];
			$post['params']['location_charge'] = $area['area_charge'];
			$post_values=json_encode($post);
			$url = $this->config->item('odoo_url')."area_creation";
			$login_check = curl_api_service($post_values,$url);
			$returnData=json_decode($login_check);
			
			if($returnData->result->status=="success")
			{
				$odoo_area_id = $returnData->result->response->id;
				if($odoo_area_id > 0)
				{
					$response = $this->settings_model->update_area(array('odoo_new_area_id' => $odoo_area_id,'odoo_new_area_status' => 1),$area['area_id']);
				}
				echo $i++.'-'.$odoo_area_id.'<br>';
			}
        }
		exit();
	}
	
	public function export_flats_to_odoo_api()
    {
		$flats = $this->settings_model->get_flats_api();
		$i = 0;
        foreach ($flats as $flat)
        {
            $post['params']['user_id'] = 1;
			$post['params']['name'] = $flat['flat_name'];
			$post['params']['code'] = $flat['tablet_imei'];
			$post_values=json_encode($post);
			$url = $this->config->item('odoo_url')."flat_creation";
			$login_check = curl_api_service($post_values,$url);
			$returnData=json_decode($login_check);
			
			if($returnData->result->status=="success")
			{
				$odoo_flat_id = $returnData->result->response->id;
				if($odoo_flat_id > 0)
				{
					$response = $this->settings_model->update_flats(array('odoo_new_flat_id' => $odoo_flat_id,'odoo_new_syncflat_status' => 1),$flat['flat_id']);
				}
				echo $i++.'-'.$odoo_flat_id.'<br>';
			}
        }
		exit();
	}
	
	public function complaint_category()
    {
        //$data = array();
        if($this->input->post('cat_sub'))
        {
			$datetime = date('Y-m-d H:i:s');
            $cat_name = $this->input->post('categoryname');

            $data = array(
                'category_name' => $cat_name,
                'createdDate' => $datetime,
                'updatedDate' => $datetime,
                'status' => 1,
            );
            $cat_id = $this->settings_model->add_complaint_category($data);
        }
        if($this->input->post('cat_edit'))
        {
			$datetime = date('Y-m-d H:i:s');
            $com_cat_id = $this->input->post('edit_catid');
            $cat_name = $this->input->post('edit_catname');
             $data = array(
                'category_name' => $cat_name,
                'updatedDate' => $datetime,
            );
            $this->settings_model->update_complaint_category($data,$com_cat_id);
        }
        $data['categories'] = $this->settings_model->get_categories();
        $layout_data['content_body'] = $this->load->view('complaint_category', $data, TRUE);
		$layout_data['page_title'] = 'Complaint Category';
		$layout_data['meta_description'] = 'Complaint Category';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();  
		$layout_data['settings_active'] = '1'; 
		$layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js','ajaxupload.3.5.js');
		$this->load->view('layouts/default', $layout_data);
    }
	
	public function edit_complaint_category()
    {
        $category_id = $this->input->post('category_id');
        $result = $this->settings_model->get_category_details($category_id);
        echo json_encode($result);
    }
	
	public function remove_complaint_category()
    {
		$datetime = date('Y-m-d H:i:s');
        $category_id = $this->input->post('category_id');
		$data = $this->settings_model->delete_category_new(array('updatedDate'=>$datetime,'status' => 0),$category_id);
        echo $data;
    }


    public function complaints()
    {
        if ($this->input->post('vehicle_report')) 
        {
            $date = $this->input->post('vehicle_date');
            $date_to = $this->input->post('vehicle_date_to');
            $customer_id= $this->input->post('customers_vh_rep');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day_to = "";
            }
            
            $complaint_status=$this->input->post('complaint_status');
        }
        else
        {
            $date = date('d/m/Y', strtotime('-7 day'));
            $date_to = date('d/m/Y');
            $customer_id='';
            $start_date= date('d/m/Y', strtotime('-7 day'));
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day = "";
            }
            $complaint_status='';
        }
        $search = array('search_date' => $date,'search_date_to' => $date_to,'customer' => $customer_id, 'search_day' => $day, 'status' => $complaint_status);
        $data['complaint_report'] = $this->settings_model->get_complaints_by_datee($vehicle_date, $vehicle_date_to,$complaint_status,$customer_id);
        $data['search'] = $search;
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('complaints', $data, TRUE);
        $layout_data['page_title'] = 'Complaints';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    public function view_complaint($complaint_id)
    {
        $data['complaints_details'] = $this->settings_model->get_complaints_details($complaint_id);
        $data['complaint_comments'] = $this->settings_model->get_complaint_comments($complaint_id);
        $layout_data['content_body'] = $this->load->view('view_complaint', $data, TRUE);
        $layout_data['page_title'] = 'Complaints';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function update_complaint_status()
    {
        $complaint_id=$this->input->post('complaint_id');
        $complaint_status=$this->input->post('complaint_status');

        $update_stat=$this->settings_model->update_complaint_stat($complaint_id,$complaint_status);
        if($update_stat){echo 'success';}
        else{echo 'error';}
        exit();
    }

    public function add_ticket_comment()
    {
        $complaint_id=$this->input->post('hid_cmp_id');
        $complaint_description=$this->input->post('ticket_reply');

        if(is_numeric($complaint_id))
        {
            $data = array(
                'cmnt_text' => $complaint_description,
                'cmnt_complaint_id' => $complaint_id,
                'cmnt_added_by' => user_authenticate(),
                'cmnt_added_datetime' => date('Y-m-d H:i:s')
            );
            $comment_id = $this->settings_model->add_ticket_comment($data);
            redirect('settings/view_complaint/'.$complaint_id);
        }
        else
        {
            redirect('settings/complaints');
        }
        exit();
    }

    public function add_aggregator()
    {
        $agg_add_stat='';
        if ($this->input->post('agg_sub')) {
            $agg_name      = $this->input->post('agg_name');
            $agg_status    = $this->input->post('agg_status');
            $added_by      = user_authenticate();
            $added_datetime     = date("Y-m-d H:i:s");

            $data = array(
                'agg_name' => $agg_name,
                'agg_added_by' => $added_by,
                'agg_added_datetime' => $added_datetime,
                'agg_status' => $agg_status
            );
            $result = $this->settings_model->add_aggregator($data);
            if($result)
            {
               $agg_add_stat='success'; 
               redirect('aggregators');
            }
        }
        $data=array();
        $data['message']=$agg_add_stat;
        $layout_data['content_body'] = $this->load->view('add_aggregator', $data, TRUE);
        $layout_data['page_title'] = 'Add Aggregator';
        $layout_data['meta_description'] = 'Add Aggregator';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }



    public function aggregators()
    {
        if(!user_permission(user_authenticate(), 8))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if($this->input->post('area_sub'))
        {
            $areaname = $this->input->post('areaname');
            $zone_id = $this->input->post('zone_id');
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
            );
            $area_id = $this->settings_model->add_area($data);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_new_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
            
            $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            $this->settings_model->update_area(array('odoo_new_area_id' => $odoo_area_id,'odoo_new_area_status' => 1),$area_id);
        }
        if($this->input->post('area_edit'))
        {
            $areaname = $this->input->post('edit_areaname');
            $area_id = $this->input->post('edit_areaid');
            $zone_id = $this->input->post('edit_zone_id');
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
            );
            $this->settings_model->update_area($data,$area_id);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_new_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
            $odo_area_affected = $this->update_area_inodoo($zone_area_fields, $get_area_by_id[0]['odoo_new_area_id']);
        }
        $data['aggregators'] = $this->settings_model->get_aggregators();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('aggregator_list', $data, TRUE);
        $layout_data['page_title'] = 'aggregators';
        $layout_data['meta_description'] = 'aggregators';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1'; 
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    public function edit_aggregator($agg_id)
    {
        $agg_add_stat='';
        if ($this->input->post('agg_sub')) {
            $agg_name      = $this->input->post('agg_name');
            $agg_status    = $this->input->post('agg_status');
            $added_by      = user_authenticate();
            $added_datetime     = date("Y-m-d H:i:s");

            $data = array(
                'agg_name' => $agg_name,
                'agg_status' => $agg_status
            );
            $result = $this->settings_model->update_aggregator($data,$agg_id);
            if($result)
            {
               $agg_add_stat='success'; 
               redirect('aggregators');
            }
        }
        $data=array();
        $data['message']=$agg_add_stat;
        $data['aggregator_details']=$this->settings_model->get_aggregator_details($agg_id);
        $layout_data['content_body'] = $this->load->view('edit_aggregator', $data, TRUE);
        $layout_data['page_title'] = 'Edit Aggregator';
        $layout_data['meta_description'] = 'Edit Aggregator';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function enable_aggregator($agg_id)
    {
        $data = array(
                'agg_status' => 1
            );
        $result = $this->settings_model->update_aggregator($data,$agg_id);
        redirect('aggregators');
    }

    public function disable_aggregator($agg_id)
    {
        
        $data = array(
                'agg_status' => 0
            );
        $result = $this->settings_model->update_aggregator($data,$agg_id);
        redirect('aggregators');
    }



    function incentive_reports() 
    {        

        if ($this->input->post('incentives_report')) {
            $data['search_date'] = $this->input->post('search_date');
            $data['search_date_to'] = $this->input->post('search_date_to');
            $incentive_for=$this->input->post('slct_incentive_for');
            if(!is_numeric($incentive_for)){$incentive_for=NULL;}
            list($day, $month, $year) = explode('/', $this->input->post('search_date'));
            $search_date = "$year-$month-$day";
            list($day, $month, $year) = explode('/', $this->input->post('search_date_to'));
            $search_date_to = "$year-$month-$day";
            
            $search_date_temp=$search_date;$search_date_to_temp=$search_date_to;$all_bookings=array();
            while (strtotime($search_date_temp) <= strtotime($search_date_to_temp)) 
            {  
                $bookings_by_date=$this->bookings_model->get_schedule_by_date($search_date_temp);
                array_push($all_bookings, $bookings_by_date);
                $search_date_temp = date ("Y-m-d", strtotime("+1 day", strtotime($search_date_temp)));
            }
            $weekly_booking_ids=array();
            foreach ($all_bookings as $bookings_by_date) 
            {
                foreach ($bookings_by_date as $booking) 
                {
                    if ($booking->booking_type!='OD' && $booking->service_start_date!=$search_date && $booking->booking_status==1) {
                        $weekly_booking_ids[]=$booking->booking_id;
                    }
                }
            }

            $data['incentives'] = $this->settings_model->get_active_incentives_by_date_new($search_date,$search_date_to,$weekly_booking_ids,$incentive_for);
            $data['incentive_for'] = $this->input->post('slct_incentive_for');
        } else {
            $search_date=date('Y-m-d');
            $search_date_to=date('Y-m-d');
            $search_date_temp=$search_date;$search_date_to_temp=$search_date_to;$all_bookings=array();$weekly_booking_ids=array();

            while (strtotime($search_date_temp) <= strtotime($search_date_to_temp)) 
            {  
                $bookings_by_date=$this->bookings_model->get_schedule_by_date($search_date_temp);
                array_push($all_bookings, $bookings_by_date);
                $search_date_temp = date ("Y-m-d", strtotime("+1 day", strtotime($search_date_temp)));
            }

            //$bookings_by_date=$this->bookings_model->get_schedule_by_date($search_date);
            $i=0;
            foreach ($all_bookings as $bookings_by_date) 
            {
                foreach ($bookings_by_date as $booking) 
                {
                    if($booking->booking_type!='OD')
                    {
                        $i++;
                    }
                    if ($booking->booking_type!='OD' && $booking->service_start_date!=$search_date && $booking->booking_status==1) {
                        $weekly_booking_ids[]=$booking->booking_id;
                    }
                }
            }
            $incentive_for=NULL;
            $data['incentives'] = $this->settings_model->get_active_incentives_by_date_new($search_date,$search_date_to,$weekly_booking_ids,$incentive_for);
            $data['incentive_for'] ='';
        }
        //print_r($data['incentive_for']);exit();
        $layout_data['content_body'] = $this->load->view('incentives_list', $data, TRUE);
        $layout_data['page_title'] = 'Incentive Reports';
        $layout_data['meta_description'] = 'Incentive';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

}