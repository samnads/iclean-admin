<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('bookings_model');
        $this->load->model('reports_model');
        $this->load->model('maids_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('users_model');
        $this->load->model('lead_model');
        $this->load->model('customers_model');
		$this->load->helper('google_api_helper');	
        ini_set("memory_limit","256M");
    }

    function index() {
        
        if(!user_permission(user_authenticate(), 13))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['zone_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;

        if ($this->input->post('zone_report')) {
            $date = $this->input->post('zone_date');

            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_zone_reports($zone_date, $zone);
            //echo "<pre>";
            //print_r($zone_result);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {

                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 

				$zone_report['booking_id'] = $zone_res->booking_id;
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['maid_name'] = $zone_res->maid_name;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_paytype'] = $zone_res->payment_type;
				$zone_report['price'] = $zone_res->price_hourly;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;

                $data['zone_report'][] = $zone_report;
            }
            //echo "<pre>";
            //print_r($data['zone_report']);
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids($zone_date, $zone);
        } else {
            $date = date('d/m/Y');

            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_zone_reports($zone_date, $zone);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {

                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 

				$zone_report['booking_id'] = $zone_res->booking_id;
                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_paytype'] = $zone_res->payment_type;
				$zone_report['price'] = $zone_res->price_hourly;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;

                $data['zone_report'][] = $zone_report;
            }
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids($zone_date, $zone);
        }
        //echo '<pre>';print_r($data);exit;
        //$data['maids'] = $this->reports_model->get_all_maids();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('zone_reports', $data, TRUE);
        $layout_data['page_title'] = 'Zone Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    public function zonereporttoExcel($date,$zone)
    {
    	//echo $date.$zone;
    	$zone=($zone=='All')?'':$zone;
    	list($day, $month, $year) = explode("-", $date);
        $zone_date = "$year-$month-$day";
    	$zone_result = $this->reports_model->get_zone_reports($zone_date, $zone);
    	
        $zone_report = array();
        $data = array();
        foreach ($zone_result as $zone_res) {

            $firstTime = $zone_res->time_from;
            $lastTime = $zone_res->time_to;
            $firstTime = strtotime($firstTime);
            $lastTime = strtotime($lastTime);
            $timeDiff = $lastTime - $firstTime;
            $timedif = ($timeDiff / 60) / 60;
            //echo $timedif; 

			$zone_report['booking_id'] = $zone_res->booking_id;
            $zone_report['maid_id'] = $zone_res->maid_id;
            $zone_report['customer'] = $zone_res->customer_name;
            $zone_report['customer_paytype'] = $zone_res->payment_type;
			$zone_report['price'] = $zone_res->price_hourly;
            $zone_report['zone'] = $zone_res->zone_name;
            $zone_report['area'] = $zone_res->area_name;
            $zone_report['booking_type'] = $zone_res->booking_type;
            $zone_report['time_from'] = $zone_res->time_from;
            $zone_report['time_to'] = $zone_res->time_to;
            $zone_report['time_diff'] = $timedif;

            $data['zone_report'][] = $zone_report;
        }
        $data['maids'] = $this->reports_model->get_maids($zone_date, $zone);
        $data['zones'] = $this->settings_model->get_zones();
        $this->load->view('zonereport_spreadsheetview', $data);
    }

    public function newreport()
    {
        
        
        if($this->input->post('cstatemnet-date-from')!=""){
            $d_from = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $d_from);
            $d_from_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
            $data['search_date_from_statement'] = $this->input->post('cstatemnet-date-from');
            $data['date_from_job'] = $d_from_date;
            
        } else {
            $date_from_job = date('Y-m-d');
            $data['search_date_from_statement'] = date('d/m/Y', strtotime($date_from_job));
            $data['date_from_job'] = $date_from_job;
        }
		$service_date = $data['date_from_job'];
		
		if($this->input->post('zones')!=""){
			$zone_id = $this->input->post('zones');
		} else {
			$zone_id = "";
		}
		$morning_avail='';
		$eve_avail='';
		$tot_avail='';
		$leave_maid='';
		$inactive_maid='';
		
		if($this->input->post('freemaids') && strlen(trim($this->input->post('freemaids') > 0)))
		{
			if($this->input->post('freemaids')==1)
			{
				$freemaid_id = 	$this->input->post('freemaids');	
			} else if($this->input->post('freemaids')==2)
			{
				$morning_avail = $this->input->post('freemaids');	   
			} else if($this->input->post('freemaids')==3)
			{
				$eve_avail   = $this->input->post('freemaids');
			} else if($this->input->post('freemaids')==4)
			{
				$tot_avail = $this->input->post('freemaids');
			} else if($this->input->post('freemaids')==5)
			{
				$leave_maid = $this->input->post('freemaids');
			} else if($this->input->post('freemaids')==6)
			{
				$inactive_maid = $this->input->post('freemaids');
			}
		}
		else
		{
			$freemaid_id = '';
			$morning_avail='';
			$eve_avail='';
			$tot_avail='';
			$leave_maid='';
			$inactive_maid='';
		}
		//starts
		$get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);
		$leave_maid_ids_new = array();
		foreach ($get_leave_maids as $leave)
		{
			array_push($leave_maid_ids_new, $leave->maid_id);
		}
		if($freemaid_id == 1)
		{
			//$maidss = $this->maids_model->get_maids($team_id);
			$maidss = $this->maids_model->get_maids();
			$maids = array();
			foreach ($maidss as $maid)
			{		
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$shifts = $this->bookings_model->get_schedule_by_date_new_count($service_date,$maid->maid_id);
					if($shifts == 0)
					{
						//$newdata[] = new stdClass();
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						//$newdata['team_name'] = $maid->team_name;
						array_push($maids, (object) $newdata);
						//$maids = ;
					}
				}
                        
			}
			$view_id = $freemaid_id;
			
		} else if($morning_avail==2)
		{
			$view_id = $morning_avail;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=2;
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date,$maid->maid_id);
					$count = count($get_avilable_before_noon);
					$hour_array = array();
					for($i=0; $i<$count; $i++)
					{
						if($i==0)
						{ 
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
							array_push($hour_array,$gap);
							if($count == 1)
							{
								$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
								array_push($hour_array,$gap2);
							}
						} else if($i == ($count - 1))
						{
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap); 
							$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
							array_push($hour_array,$gap2);
						} else {
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap);
						}
					}
							
					foreach($hour_array as $list)
					{
					   if($list >= 3)
					   {
							$newdata['maid_id'] = $maid->maid_id;
							$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
							$newdata['maid_name'] = $maid->maid_name;
							$newdata['maid_nationality'] = $maid->maid_nationality;
							$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
							$newdata['maid_photo_file'] = $maid->maid_photo_file;
							$newdata['flat_name'] = $maid->flat_name;
							$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
							$newdata['maid_status'] = $maid->maid_status;
							$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
							$newdata['total_hrs'] = $total_hrs;
							array_push($maids, (object) $newdata);
					   }
					}
							
					if($count == 0)
					{
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						$newdata['total_hrs'] = $total_hrs;
						array_push($maids, (object) $newdata);
					}	
				}
			}
			//print_r(json_encode($maids));
		}
		else if($eve_avail==3)
		{
			$view_id = $eve_avail;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=3;
		  
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$get_avilable_after_noon = $this->bookings_model->get_avilable_after_noon($service_date,$maid->maid_id);
					$count = count($get_avilable_after_noon);
					$hour_array = array();
					for($i=0; $i<$count; $i++)
					{
						if($i==0)
						{ 
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime('13:00:00')) / 3600;
							array_push($hour_array,$gap);
							if($count == 1)
							{
								$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
								array_push($hour_array,$gap2);
							}
						} else if($i == ($count - 1))
						{
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap); 
							$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
							array_push($hour_array,$gap2);
						} else {
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap);
						}
					}
					
					foreach($hour_array as $list)
					{
					   if($list >= 3)
					   {
							$newdata['maid_id'] = $maid->maid_id;
							$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
							$newdata['maid_name'] = $maid->maid_name;
							$newdata['maid_nationality'] = $maid->maid_nationality;
							$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
							$newdata['maid_photo_file'] = $maid->maid_photo_file;
							$newdata['flat_name'] = $maid->flat_name;
							$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
							$newdata['maid_status'] = $maid->maid_status;
							$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
							$newdata['total_hrs'] = $total_hrs;
							array_push($maids, (object) $newdata);
					   }
					}
					
					if($count == 0)
					{
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						$newdata['total_hrs'] = $total_hrs;
						array_push($maids, (object) $newdata);
					}
				}
			}
		 
		} else if($tot_avail == 4)
		{
			$view_id = $tot_avail;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=4;
		  
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					//$newdata[] = new stdClass();
					$newdata['maid_id'] = $maid->maid_id;
					$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
					$newdata['maid_name'] = $maid->maid_name;
					$newdata['maid_nationality'] = $maid->maid_nationality;
					$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
					$newdata['maid_photo_file'] = $maid->maid_photo_file;
					$newdata['flat_name'] = $maid->flat_name;
					$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
					$newdata['maid_status'] = $maid->maid_status;
					$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
					//$newdata['team_name'] = $maid->team_name;
					array_push($maids, (object) $newdata);
					//$maids = ;
				}
			}
		} else if($leave_maid == 5)
		{
			$view_id = $leave_maid;
			$list_leave_maidss = $this->maids_model->list_maids_leave_by_date($service_date);
			$maids  = array();
			foreach ($list_leave_maidss as $maid)
			{
				$newdata['maid_id'] = $maid->maid_id;
				$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
				$newdata['maid_name'] = $maid->maid_name;
				$newdata['maid_nationality'] = $maid->maid_nationality;
				$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
				$newdata['maid_photo_file'] = $maid->maid_photo_file;
				$newdata['flat_name'] = $maid->flat_name;
				$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
				$newdata['maid_status'] = $maid->maid_status;
				$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
				array_push($maids, (object) $newdata);
			}
		} else if($inactive_maid == 6)
		{
			$view_id = $inactive_maid;
			$inactiveemaidss = $this->maids_model->list_inactivemaids();
			$maids  = array();
			foreach ($inactiveemaidss as $maid)
			{
				$newdata['maid_id'] = $maid->maid_id;
				$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
				$newdata['maid_name'] = $maid->maid_name;
				$newdata['maid_nationality'] = $maid->maid_nationality;
				$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
				$newdata['maid_photo_file'] = $maid->maid_photo_file;
				$newdata['flat_name'] = $maid->flat_name;
				$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
				$newdata['maid_status'] = $maid->maid_status;
				$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
				array_push($maids, (object) $newdata);
			}
		} else {
			$view_id = 0;
			//$maids = $this->maids_model->get_maids($team_id);
			$maids = $this->maids_model->get_maids();
		}
		
		
		
		//ends
        
        
        $data['freemaid_ids'] = $view_id;
		
		//$data['maids'] = $this->reports_model->get_maids_for_report();
		$data['maids'] = $maids;
        $data['zones'] = $this->settings_model->get_zones();
		$data['zone_id'] = $zone_id;
        $layout_data['content_body'] = $this->load->view('zone_reports_new', $data, TRUE);
        $layout_data['page_title'] = 'Zone Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    
    public function newreporttoExcel()
    {
        $date_from_job = $this->uri->segment(3);
		if($this->uri->segment(5) != "")
		{
			$zone_id = $this->uri->segment(5);
		} else {
			$zone_id = "";
		}
		
		if($this->uri->segment(4) != "")
		{
			$freemaidid = $this->uri->segment(4);
		} else {
			$freemaidid = "";
		}
		$service_date = $date_from_job;
		
		//starts
		$get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);
		$leave_maid_ids_new = array();
		foreach ($get_leave_maids as $leave)
		{
			array_push($leave_maid_ids_new, $leave->maid_id);
		}
		if($freemaidid == 1)
		{
			//$maidss = $this->maids_model->get_maids($team_id);
			$maidss = $this->maids_model->get_maids();
			$maids = array();
			foreach ($maidss as $maid)
			{		
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$shifts = $this->bookings_model->get_schedule_by_date_new_count($service_date,$maid->maid_id);
					if($shifts == 0)
					{
						//$newdata[] = new stdClass();
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						//$newdata['team_name'] = $maid->team_name;
						array_push($maids, (object) $newdata);
						//$maids = ;
					}
				}
                        
			}
			$view_id = $freemaidid;
			
		} else if($freemaidid==2)
		{
			$view_id = $freemaidid;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=2;
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date,$maid->maid_id);
					$count = count($get_avilable_before_noon);
					$hour_array = array();
					for($i=0; $i<$count; $i++)
					{
						if($i==0)
						{ 
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
							array_push($hour_array,$gap);
							if($count == 1)
							{
								$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
								array_push($hour_array,$gap2);
							}
						} else if($i == ($count - 1))
						{
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap); 
							$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
							array_push($hour_array,$gap2);
						} else {
							$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap);
						}
					}
							
					foreach($hour_array as $list)
					{
					   if($list >= 3)
					   {
							$newdata['maid_id'] = $maid->maid_id;
							$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
							$newdata['maid_name'] = $maid->maid_name;
							$newdata['maid_nationality'] = $maid->maid_nationality;
							$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
							$newdata['maid_photo_file'] = $maid->maid_photo_file;
							$newdata['flat_name'] = $maid->flat_name;
							$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
							$newdata['maid_status'] = $maid->maid_status;
							$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
							$newdata['total_hrs'] = $total_hrs;
							array_push($maids, (object) $newdata);
					   }
					}
							
					if($count == 0)
					{
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						$newdata['total_hrs'] = $total_hrs;
						array_push($maids, (object) $newdata);
					}	
				}
			}
			//print_r(json_encode($maids));
		}
		else if($freemaidid==3)
		{
			$view_id = $freemaidid;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=3;
		  
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					$get_avilable_after_noon = $this->bookings_model->get_avilable_after_noon($service_date,$maid->maid_id);
					$count = count($get_avilable_after_noon);
					$hour_array = array();
					for($i=0; $i<$count; $i++)
					{
						if($i==0)
						{ 
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime('13:00:00')) / 3600;
							array_push($hour_array,$gap);
							if($count == 1)
							{
								$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
								array_push($hour_array,$gap2);
							}
						} else if($i == ($count - 1))
						{
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap); 
							$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
							array_push($hour_array,$gap2);
						} else {
							$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
							array_push($hour_array,$gap);
						}
					}
					
					foreach($hour_array as $list)
					{
					   if($list >= 3)
					   {
							$newdata['maid_id'] = $maid->maid_id;
							$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
							$newdata['maid_name'] = $maid->maid_name;
							$newdata['maid_nationality'] = $maid->maid_nationality;
							$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
							$newdata['maid_photo_file'] = $maid->maid_photo_file;
							$newdata['flat_name'] = $maid->flat_name;
							$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
							$newdata['maid_status'] = $maid->maid_status;
							$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
							$newdata['total_hrs'] = $total_hrs;
							array_push($maids, (object) $newdata);
					   }
					}
					
					if($count == 0)
					{
						$newdata['maid_id'] = $maid->maid_id;
						$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
						$newdata['maid_name'] = $maid->maid_name;
						$newdata['maid_nationality'] = $maid->maid_nationality;
						$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
						$newdata['maid_photo_file'] = $maid->maid_photo_file;
						$newdata['flat_name'] = $maid->flat_name;
						$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
						$newdata['maid_status'] = $maid->maid_status;
						$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
						$newdata['total_hrs'] = $total_hrs;
						array_push($maids, (object) $newdata);
					}
				}
			}
		 
		} else if($freemaidid == 4)
		{
			$view_id = $freemaidid;
			$maidss = $this->maids_model->get_maids();
			$maids  = array();
			$filter_by=4;
		  
			foreach ($maidss as $maid)
			{
				if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
				{
					//$newdata[] = new stdClass();
					$newdata['maid_id'] = $maid->maid_id;
					$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
					$newdata['maid_name'] = $maid->maid_name;
					$newdata['maid_nationality'] = $maid->maid_nationality;
					$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
					$newdata['maid_photo_file'] = $maid->maid_photo_file;
					$newdata['flat_name'] = $maid->flat_name;
					$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
					$newdata['maid_status'] = $maid->maid_status;
					$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
					//$newdata['team_name'] = $maid->team_name;
					array_push($maids, (object) $newdata);
					//$maids = ;
				}
			}
		} else if($freemaidid == 5)
		{
			$view_id = $freemaidid;
			$list_leave_maidss = $this->maids_model->list_maids_leave_by_date($service_date);
			$maids  = array();
			foreach ($list_leave_maidss as $maid)
			{
				$newdata['maid_id'] = $maid->maid_id;
				$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
				$newdata['maid_name'] = $maid->maid_name;
				$newdata['maid_nationality'] = $maid->maid_nationality;
				$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
				$newdata['maid_photo_file'] = $maid->maid_photo_file;
				$newdata['flat_name'] = $maid->flat_name;
				$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
				$newdata['maid_status'] = $maid->maid_status;
				$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
				array_push($maids, (object) $newdata);
			}
		} else if($freemaidid == 6)
		{
			$view_id = $freemaidid;
			$inactiveemaidss = $this->maids_model->list_inactivemaids();
			$maids  = array();
			foreach ($inactiveemaidss as $maid)
			{
				$newdata['maid_id'] = $maid->maid_id;
				$newdata['odoo_maid_id'] = $maid->odoo_maid_id;
				$newdata['maid_name'] = $maid->maid_name;
				$newdata['maid_nationality'] = $maid->maid_nationality;
				$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
				$newdata['maid_photo_file'] = $maid->maid_photo_file;
				$newdata['flat_name'] = $maid->flat_name;
				$newdata['odoo_flat_id'] = $maid->odoo_flat_id;
				$newdata['maid_status'] = $maid->maid_status;
				$newdata['odoo_synch_status'] = $maid->odoo_synch_status;
				array_push($maids, (object) $newdata);
			}
		} else {
			$view_id = 0;
			//$maids = $this->maids_model->get_maids($team_id);
			$maids = $this->maids_model->get_maids();
		}
		
		
		
		//ends
        
        
        $data['freemaid_ids'] = $view_id;
		
		
		
		
        $data['date_from_job'] = $date_from_job;
		$data['zone_id'] = $zone_id;
        //$data['maids'] = $this->reports_model->get_maids_for_report();
        $data['maids'] = $maids;
        //$data['zones'] = $this->settings_model->get_zones();
        
        $this->load->view('zone_reports_new_excel', $data);
    }

    public function leadsreporttoExcel($frm_date,$to_date,$leadtype,$timelines,$source,$service,$leadstatus)
    {
    	//echo "$frm_date , $to_date , $leadtype , ".urldecode($timelines)." , $source , ".urldecode($service);exit();
    	$timelines=urldecode($timelines);
    	$service=urldecode($service);
    	//ini_set('display_errors', 1);
		//ini_set('display_startup_errors', 1);
		//error_reporting(E_ALL);
    		if($leadtype== "nil")
			{
				$leadtype = NULL;
			} 
			
			if($timelines== "nil")
			{
				$timelines = NULL;
			}

			if($service== "nil")
			{
				$service = NULL;
			}
			
			if($leadstatus== "nil")
			{
				$leadstatus = NULL;
			}

			if($source== "nil")
			{
				$source = NULL;
			}
			
			if($frm_date== "1970-01-01")
			{
				$frm_date = NULL;
			}
			
			if($to_date== "1970-01-01")
			{
				$to_date = NULL;
			}
		$data=array();
		$data['leadtype'] = $leadtype;
		$data['timelines'] = $timelines;
		$data['service'] = $service;
		$data['source'] = $source;
		$data['services'] = $this->lead_model->get_services();
        $data['leads'] = $this->lead_model->get_leads_list($frm_date,$to_date,$leadtype,$timelines,$service,$source,$leadstatus);
        //printf(json_encode($data));exit();
        $this->load->view('leads_report_excel', $data);
    	
    }
    function vehicle_report_test() {
        if(!user_permission(user_authenticate(), 14))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['vehicle_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;

        if ($this->input->post('vehicle_report')) {
            $date = $this->input->post('vehicle_date');
            $date_to = $this->input->post('vehicle_date_to');
            $customer_id= $this->input->post('customers_vh_rep');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day_to = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            
            
            $loop_start_date=$vehicle_date;
            $loop_end_date=$vehicle_date_to;
            $vehicle_report = array();

            $vehicle_result= array();
            
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                
                    
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
                    
                    $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
                    
            }


            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;

                foreach ($vehicle_result as $vehicle_res) 
				{
					
					$check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id,$vehicle_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						if($vehicle_res->pay_by != "")
						{
							$mop = $vehicle_res->pay_by;
							$ref = $vehicle_res->justmop_reference;;
						} else {
							$mop = $vehicle_res->payment_mode;
							$ref = $vehicle_res->justmop_reference;;
						}
						//$mop = $vehicle_res->payment_mode;
						//$ref = $vehicle_res->justmop_reference;
					}
					
					if($vehicle_res->customer_address == "")
					{
						$a_address = 'Building - '.$vehicle_res->building.', '.$vehicle_res->unit_no.''.$vehicle_res->street;
					} else {
						$a_address = $vehicle_res->customer_address;
					}

                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
							$vehicle_report[$i]['payment_mode'] = $mop;
							$vehicle_report[$i]['justmop_reference'] = $ref;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
						//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
						$vehicle_report[$i]['payment_mode'] = $mop;
						$vehicle_report[$i]['justmop_reference'] = $ref;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }


            $search = array('search_date' => $date,'search_date_to' => $date_to, 'search_zone' => $zone, 'customer' => $customer_id, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        } else {
            $date = date('d/m/Y', strtotime('-3 day'));
            $date_to = date('d/m/Y');
            $customer_id='';
            $start_date= date('d/m/Y', strtotime('-3 day'));
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day = "";
            }
            
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            
            $loop_start_date=$vehicle_date;
            $loop_end_date=$vehicle_date_to;
            $vehicle_report = array();

            $vehicle_result= array();
            
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                
                    
                    $vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
                    
                    $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
                    
            }
            
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;


                foreach ($vehicle_result as $vehicle_res) 
				{
					$check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id,$vehicle_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						if($vehicle_res->pay_by != "")
						{
							$mop = $vehicle_res->pay_by;
							$ref = $vehicle_res->justmop_reference;;
						} else {
							$mop = $vehicle_res->payment_mode;
							$ref = $vehicle_res->justmop_reference;;
						}
						//$mop = $vehicle_res->payment_mode;
						//$ref = $vehicle_res->justmop_reference;
					}
					if($vehicle_res->customer_address == "")
					{
						$a_address = 'Building - '.$vehicle_res->building.', '.$vehicle_res->unit_no.''.$vehicle_res->street;
					} else {
						$a_address = $vehicle_res->customer_address;
					}

                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
							//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
							$vehicle_report[$i]['payment_mode'] = $mop;
							$vehicle_report[$i]['justmop_reference'] = $ref;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
						//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
						$vehicle_report[$i]['payment_mode'] = $mop;
						$vehicle_report[$i]['justmop_reference'] = $ref;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }

            $search = array('search_date' => $date,'search_date_to' => $date_to,'customer' => $customer_id, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        }
        
        //$data['customers'] = $this->customers_model->get_customers_for_search();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('vehicle_reports', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	function vehicle_report() {
        if(!user_permission(user_authenticate(), 14))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['vehicle_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;
		$current_date = date('Y-m-d');

        if ($this->input->post('vehicle_report')) {
            $date = $this->input->post('vehicle_date');
            $date_to = $this->input->post('vehicle_date_to');
            $customer_id= $this->input->post('customers_vh_rep');
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day_to = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            
            
            $loop_start_date=$vehicle_date;
            $loop_end_date=$vehicle_date_to;
            $vehicle_report = array();

            $vehicle_result= array();
            
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                
                    if($loop_start_date < $current_date)
					{
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone,$customer_id));
					} else {
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
					}
                    
					//$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
                    
                    $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
                    
            }


            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;

                foreach ($vehicle_result as $vehicle_res) 
				{
					
					$check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id,$vehicle_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						if($vehicle_res->pay_by != "")
						{
							$mop = $vehicle_res->pay_by;
							$ref = $vehicle_res->justmop_reference;;
						} else {
							$mop = $vehicle_res->payment_mode;
							$ref = $vehicle_res->justmop_reference;;
						}
						//$mop = $vehicle_res->payment_mode;
						//$ref = $vehicle_res->justmop_reference;
					}
					
					if($vehicle_res->customer_address == "")
					{
						$a_address = 'Building - '.$vehicle_res->building.', '.$vehicle_res->unit_no.''.$vehicle_res->street;
					} else {
						$a_address = $vehicle_res->customer_address;
					}

                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
							$vehicle_report[$i]['payment_mode'] = $mop;
							$vehicle_report[$i]['justmop_reference'] = $ref;
							$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
						//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
						$vehicle_report[$i]['payment_mode'] = $mop;
						$vehicle_report[$i]['justmop_reference'] = $ref;
						$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }


            $search = array('search_date' => $date,'search_date_to' => $date_to, 'search_zone' => $zone, 'customer' => $customer_id, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        } else {
            $date = date('d/m/Y', strtotime('-3 day'));
            $date_to = date('d/m/Y');
            $customer_id='';
            $start_date= date('d/m/Y', strtotime('-3 day'));
            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $vehicle_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($vehicle_date));
            } else {
                $vehicle_date = "";
                $day = "";
            }
            
            if ($date_to != "") {
                list($day, $month, $year) = explode("/", $date_to);
                $vehicle_date_to = "$year-$month-$day";
                $day_to = strftime("%A", strtotime($vehicle_date_to));
            } else {
                $vehicle_date_to = "";
                $day = "";
            }
            
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            
            $loop_start_date=$vehicle_date;
            $loop_end_date=$vehicle_date_to;
            $vehicle_report = array();

            $vehicle_result= array();
            
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
					
					if($loop_start_date < $current_date)
					{
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone,$customer_id));
					} else {
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
					}
                    
                    
                    
                    $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
                    
            }
            
            if (count($vehicle_result) > 0) {
                $i = 0;
                $MaidId = 0;
                $CustId = 0;
                $time_To = 0;


                foreach ($vehicle_result as $vehicle_res) 
				{
					$check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id,$vehicle_date);
					if(!empty($check_buk_exist))
					{
						$mop = $check_buk_exist->mop;
						$ref = $check_buk_exist->just_mop_ref;
					} else {
						if($vehicle_res->pay_by != "")
						{
							$mop = $vehicle_res->pay_by;
							$ref = $vehicle_res->justmop_reference;;
						} else {
							$mop = $vehicle_res->payment_mode;
							$ref = $vehicle_res->justmop_reference;;
						}
						//$mop = $vehicle_res->payment_mode;
						//$ref = $vehicle_res->justmop_reference;
					}
					if($vehicle_res->customer_address == "")
					{
						$a_address = 'Building - '.$vehicle_res->building.', '.$vehicle_res->unit_no.''.$vehicle_res->street;
					} else {
						$a_address = $vehicle_res->customer_address;
					}

                    if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) {
                        if ($time_To == $vehicle_res->time_from) {
                            $j++;
                            $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                        } else {
                            $j = 0;
                            $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                            $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                            $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                            $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                            $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                            $vehicle_report[$i]['customer_address'] = $a_address;
                            $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                            $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                            $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                            $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                            $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                            $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                            $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                            $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                            $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                            $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                            $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                            $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                            $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                            $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                            $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                            $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                            $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                            //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
							//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
							$vehicle_report[$i]['payment_mode'] = $mop;
							$vehicle_report[$i]['justmop_reference'] = $ref;
							$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                        }
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
                        $vehicle_report[$i]['mobile_number'] = $vehicle_res->mobile_number_1;
                        //$vehicle_report[$i]['payment_mode'] = $vehicle_res->payment_mode;
						//$vehicle_report[$i]['justmop_reference'] = $vehicle_res->justmop_reference;
						$vehicle_report[$i]['payment_mode'] = $mop;
						$vehicle_report[$i]['justmop_reference'] = $ref;
						$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                    }
                    $time_To = $vehicle_res->time_to;
                    $data['vehicle_report'] = $vehicle_report;
                    $MaidId = $vehicle_res->maid_id;
                    $CustId = $vehicle_res->customer_id;
                    $i++;
                }
            }

            $search = array('search_date' => $date,'search_date_to' => $date_to,'customer' => $customer_id, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        }
        
        //$data['customers'] = $this->customers_model->get_customers_for_search();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('vehicle_reports', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    function report_srch_usr()
    {
        $searchTerm = $this->input->post('searchTerm');
        $search_result=$this->customers_model->report_srch_usr($searchTerm);
        
        $data=array();
        foreach ($search_result as $user) {
            $data[] = array("id"=>$user->customer_id, "text"=>$user->customer_name);
        }
        print_r(json_encode($data));
        exit();
    }
    
            function vehiclereporttoExcel() 
    {
        if(!user_permission(user_authenticate(), 14))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        
        $vehicle_date = $this->uri->segment(3);
        $vehicle_date_to = $this->uri->segment(5);
        $customer_id=$this->uri->segment(6);
        $zone = $this->uri->segment(4);
        //$vehicle_result = $this->reports_model->get_vehicle_reports($vehicle_date, $zone);
        $vehicle_report = array();
		$current_date = date('Y-m-d');
        
        $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } 
            else if ($zone == "All") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
        
        $loop_start_date=$vehicle_date;
            $loop_end_date=$vehicle_date_to;
            

            $vehicle_result= array();
            
            while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
                
                    if($loop_start_date < $current_date)
					{
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new_test($loop_start_date, $zone,$customer_id));
					} else {
						$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
					}
                    
					
					
					//$vehicle_result = array_merge($vehicle_result, $this->reports_model->get_vehicle_reports_new($loop_start_date, $zone,$customer_id));
                    
                    $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
                    
            }

        
        if (count($vehicle_result) > 0)
        {
            $i = 0;
            $MaidId = 0;
            $CustId = 0;
            $time_To = 0;

            foreach ($vehicle_result as $vehicle_res) 
            {
				$check_buk_exist = $this->bookings_model->get_booking_exist($vehicle_res->booking_id,$vehicle_date);
				if(!empty($check_buk_exist))
				{
					$mop = $check_buk_exist->mop;
					$ref = $check_buk_exist->just_mop_ref;
				} else {
					if($vehicle_res->pay_by != "")
					{
						$mop = $vehicle_res->pay_by;
						$ref = $vehicle_res->justmop_reference;
					} else {
						$mop = $vehicle_res->payment_mode;
						$ref = $vehicle_res->justmop_reference;
					}
					//$mop = $vehicle_res->payment_mode;
					//$ref = $vehicle_res->justmop_reference;
				}
				if($vehicle_res->customer_address == "")
					{
						$a_address = 'Building - '.$vehicle_res->building.', '.$vehicle_res->unit_no.''.$vehicle_res->street;
					} else {
						$a_address = $vehicle_res->customer_address;
					}
                if ($vehicle_res->maid_id == $MaidId && $vehicle_res->customer_id == $CustId) 
                {
                    if ($time_To == $vehicle_res->time_from)
                    {
                        $j++;
                        $vehicle_report[$i - $j]['time_to'] = $vehicle_res->time_to;
                    } else {
                        $j = 0;
                        $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                        $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                        $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                        $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                        $vehicle_report[$i]['mobile'] = $vehicle_res->mobile_number_1;
                        $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                        $vehicle_report[$i]['customer_address'] = $a_address;
                        $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                        $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                        $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                        $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                        $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                        $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                        $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                        $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                        $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                        $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                        $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                        $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                        $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                        $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                        $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                        $vehicle_report[$i]['payment_mode'] = $mop;
                        $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
						$vehicle_report[$i]['justmop_reference'] = $ref;
						$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                    }
                } else {
                    $j = 0;
                    $vehicle_report[$i]['maid_id'] = $vehicle_res->maid_id;
                    $vehicle_report[$i]['maid'] = $vehicle_res->maid_name;
                    $vehicle_report[$i]['customer_id'] = $vehicle_res->customer_id;
                    $vehicle_report[$i]['customer'] = $vehicle_res->customer_name;
                    $vehicle_report[$i]['mobile'] = $vehicle_res->mobile_number_1;
                    $vehicle_report[$i]['customer_paytype'] = $vehicle_res->payment_type;
                    $vehicle_report[$i]['customer_address'] = $a_address;
                    $vehicle_report[$i]['zone_name'] = $vehicle_res->zone_name;
                    $vehicle_report[$i]['booking_type'] = $vehicle_res->booking_type;
                    $vehicle_report[$i]['time_from'] = $vehicle_res->time_from;
                    $vehicle_report[$i]['time_to'] = $vehicle_res->time_to;
                    $vehicle_report[$i]['cleaning_material'] = $vehicle_res->cleaning_material;
                    $vehicle_report[$i]['booking_note'] = $vehicle_res->booking_note;
                    $vehicle_report[$i]['booked_from'] = $vehicle_res->booked_from;
                    $vehicle_report[$i]['price_per_hr'] = $vehicle_res->price_per_hr;
                    $vehicle_report[$i]['discount'] = $vehicle_res->discount;
                    $vehicle_report[$i]['total_amount'] = $vehicle_res->total_amount;
                    $vehicle_report[$i]['price_hourly'] = $vehicle_res->price_hourly;
                    $vehicle_report[$i]['price_extra'] = $vehicle_res->price_extra;
                    $vehicle_report[$i]['price_weekend'] = $vehicle_res->price_weekend;
                    $vehicle_report[$i]['driver_name'] = $vehicle_res->driver_name;
                    $vehicle_report[$i]['customer_source'] = $vehicle_res->customer_source;
                    $vehicle_report[$i]['payment_mode'] = $mop;
                    $vehicle_report[$i]['service_type_name'] = $vehicle_res->service_type_name;
					$vehicle_report[$i]['justmop_reference'] = $ref;
					$vehicle_report[$i]['veh_date'] = $vehicle_res->start_date;
                }
                $time_To = $vehicle_res->time_to;
                $data['vehicle_report'] = $vehicle_report;
                $MaidId = $vehicle_res->maid_id;
                $CustId = $vehicle_res->customer_id;
                $i++;
            }
        }
        $data['searchdate'] = $this->uri->segment(3);
        $data['searchzone'] = $this->uri->segment(4);
        

        $this->load->view('vehiclereport_spreadsheetview', $data);
    }

    function payment_report() {
         if(!user_permission(user_authenticate(), 15))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $data['payment_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;

        if ($this->input->post('payment_report')) {
            $date = $this->input->post('payment_date');

            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $payment_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($payment_date));
            } else {
                $payment_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $payment_result = $this->reports_model->get_payment_reports($payment_date, $zone);
            //$payment_result = $this->reports_model->get_payment_reports($payment_date);
            //echo "<pre>";
            //print_r($payment_result);

            $payment_report = array();
            foreach ($payment_result as $payment_res) {


                $payment_report['customer'] = $payment_res->customer_name;
                $payment_report['payment_type'] = $payment_res->payment_type;
                $payment_report['paid_amount'] = $payment_res->paid_amount;
                $payment_report['zone_name'] = $payment_res->zone_name;
                $payment_report['paid_datetime'] = date('d/m/Y H:i:s', strtotime($payment_res->paid_datetime));

                $data['payment_report'][] = $payment_report;
            }

            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        } else {
            $date = date('d/m/Y');

            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $payment_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($payment_date));
            } else {
                $payment_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $payment_result = $this->reports_model->get_payment_reports($payment_date, $zone);
            //$payment_result = $this->reports_model->get_payment_reports($payment_date);

            //echo "<pre>"; print_r($payment_result);exit;
            
            
            $payment_report = array();
            foreach ($payment_result as $payment_res) {


                $payment_report['customer'] = $payment_res->customer_name;
                $payment_report['payment_type'] = $payment_res->payment_type;
                $payment_report['paid_amount'] = $payment_res->paid_amount;
                $payment_report['zone_name'] = $payment_res->zone_name;
                $payment_report['paid_datetime'] = date('d/m/Y H:i:s', strtotime($payment_res->paid_datetime));

                $data['payment_report'][] = $payment_report;
            }
            
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
        }

        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('payment_reports', $data, TRUE);
        $layout_data['page_title'] = 'Payment Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
//        $layout_data['reports_active'] = '1';
        $layout_data['accounts_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    //Edited By Aparna
    function one_day_cancel() {
         if(!user_permission(user_authenticate(), 16))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('ondeday_report')) {
            $data['search_date'] = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $this->input->post('search_date'));
            $search_date = "$year-$month-$day";
            $data['reports'] = $this->reports_model->get_onedaycancel_report($search_date);
        } else {
            $data['reports'] = $this->reports_model->get_onedaycancel_report();
        }
        $layout_data['content_body'] = $this->load->view('one_day_cancel_report', $data, TRUE);
        $layout_data['page_title'] = 'One Day Cancel Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


     function one_day_cancel_to_excel($search_date) {
         // if(!user_permission(user_authenticate(), 16))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        $data=array();
        $data['reports'] = $this->reports_model->get_onedaycancel_report($search_date);
        //print_r($data);exit();
        $this->load->view('one_day_cancel_report_spreadsheetview', $data);
        
    }

    function delete_onedaycancel() {
        if ($this->input->post('id')) {
            echo $this->reports_model->delete_onedaycancel($this->input->post('id'));
        }
    }

    function employee_work() {
         if(!user_permission(user_authenticate(), 17))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $reports = array();
        $search_conditions = array();
        if ($this->input->post('activity_report')) {
            if ($this->input->post('maid') != "" && $this->input->post('month') != "" && $this->input->post('year') != "") {
                $results = $this->reports_model->get_employee_work_report($this->input->post('maid'), $this->input->post('month'), $this->input->post('year'));
                $search_conditions = array(
                    'maid_id' => $this->input->post('maid'),
                    'month' => $this->input->post('month'),
                    'year' => $this->input->post('year')
                );
//                if (!empty($results)) {
//                    foreach ($results as $result) {
//                        $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
//                        $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
//                        $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
//                        $time_diff = (strtotime($result->time_to) - strtotime($result->time_from)) / 3600;
//                        if ($time_diff > 1) {
//                            for ($j = 0; $j < $time_diff; $j++) {
//                                $start_time = date('H:i:s', strtotime($result->time_from) + 3600);
//                                $reports[$result->service_date][$start_time]['time_to'] = $result->time_to;
//                                $reports[$result->service_date][$start_time]['customer'] = $result->customer_name;
//                                $reports[$result->service_date][$start_time]['type'] = $result->booking_type;
//                            }
//                        }
//                    }
//                }
                
                if (!empty($results)) {
                    foreach ($results as $result) {
                        $reports[$result->service_date][$result->time_from]['time_from'] = $result->time_from;
                        $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
                        $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
                        $reports[$result->service_date][$result->time_from]['payment_type'] = $result->payment_type;
                        $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
                        $time_diff = (strtotime($result->time_to) - strtotime($result->time_from)) / 3600;
                        $reports[$result->service_date][$result->time_from]['time_diff'] = $time_diff;
                        if ($time_diff > 1) {
                            for ($j = 0; $j < $time_diff; $j++) {
                                $reports[$result->service_date][$result->time_from]['time_from'] = $result->time_from;
                                $reports[$result->service_date][$result->time_from]['time_to'] = $result->time_to;
                                $reports[$result->service_date][$result->time_from]['customer'] = $result->customer_name;
                                $reports[$result->service_date][$result->time_from]['payment_type'] = $result->payment_type;
                                $reports[$result->service_date][$result->time_from]['type'] = $result->booking_type;
                                $reports[$result->service_date][$result->time_from]['time_diff'] = $time_diff;
                            }
                        }
                    }
                }
                
            }
        }
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
        $data['maids'] = $this->reports_model->get_all_maids();
        $layout_data['content_body'] = $this->load->view('employee_work_report', $data, TRUE);
        $layout_data['page_title'] = 'Employee Work Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function employee_work_all() {
         if(!user_permission(user_authenticate(), 17))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $reports = array();
        $search_conditions = array();
        if ($this->input->post('activity_report')) {
            if ($this->input->post('month') != "" && $this->input->post('year') != "") {
                
                $search_conditions = array(
                    
                    'month' => $this->input->post('month'),
                    'year' => $this->input->post('year')
                );
                
            }
        }
        else
        {
            $currentMonth = date('m');
            $currentNewMonth = Date('m', strtotime($currentMonth . " last month"));
            $search_conditions = array(
                    
                    'month' => $currentNewMonth,
                    'year' => date('Y')
                );
        }
        
        $reports = $this->reports_model->get_employee_work_all($search_conditions['month'], $search_conditions['year']);
        
        $source_ref = $this->reports_model->get_customer_refereces();
        $maid_ref = array();
        if(!empty($source_ref))
        {
            foreach ($source_ref as $ref)
            {
                $maid_ref[$ref->customer_source_id] = $ref->reference;
            }
        }
        $data['month']=$search_conditions['month'];
        $data['year']=$search_conditions['year'];
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['maid_references'] = $maid_ref;
        
        $layout_data['content_body'] = $this->load->view('employee_work_report_all', $data, TRUE);
        $layout_data['page_title'] = 'All Employees Work Report';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    function empworkreporttoExcel($month,$year) {
         // if(!user_permission(user_authenticate(), 17))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        $reports = array();
        
        
        $reports = $this->reports_model->get_employee_work_all($month, $year);
        
        $source_ref = $this->reports_model->get_customer_refereces();
        $maid_ref = array();
        if(!empty($source_ref))
        {
            foreach ($source_ref as $ref)
            {
                $maid_ref[$ref->customer_source_id] = $ref->reference;
            }
        }
        $data['month']=$month;
        $data['year']=$year;
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
        $data['maids'] = $this->reports_model->get_all_maids();
        $data['maid_references'] = $maid_ref;
        
        $this->load->view('empwork_rpt_all_spreadsheetview', $data);
    }


    function activity_summary() {
         // if(!user_permission(user_authenticate(), 21))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        if ($this->input->post('activity_report')) {
            $data['from_date'] = $this->input->post('from_date');
            $data['to_date'] = $this->input->post('to_date');
            list($day, $month, $year) = explode('/', $this->input->post('from_date'));
            $from_date = "$year-$month-$day";
            list($day, $month, $year) = explode('/', $this->input->post('to_date'));
            $to_date = "$year-$month-$day";
            //$reports = $this->reports_model->get_activity_summary_report($from_date, $to_date);
        } else {
            $to_date = date('Y-m-d');
            $from_date = date('Y-m-d', strtotime('-30 days', strtotime($to_date)));
            //$reports = $this->reports_model->get_activity_summary_report($from_date, $to_date);
        }
        
        $datetime1 = new DateTime($from_date);
        $datetime2 = new DateTime($to_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        
        $service_date = $from_date;
        
        
        $final_report = array();
        for($i = 0; $i <= $days; ++$i)
        {            
            $reports = $this->reports_model->get_activity_summary_report_by_date($service_date);
            if (!empty($reports)) {
                $data_to_check = "";
                foreach ($reports as $report) {
                    if ($report->service_date == $data_to_check) {
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] += $report->booking_type == 'OD' ? 1 : 0;
                        $final_report[$report->service_date]['WE'] += $report->booking_type == 'WE' ? 1 : 0;
                        $final_report[$report->service_date]['OD_hrs'] += $report->booking_type == 'OD' ? 1 : 0;
                        $final_report[$report->service_date]['WE_hrs'] += $report->booking_type == 'WE' ? 1 : 0;
                        $final_report[$report->service_date]['hours'] += $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    } else {
                        $activity_report[$report->service_date] = array();
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD']) ? ($final_report[$report->service_date]['OD'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE']) ? ($final_report[$report->service_date]['WE'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD_hrs']) ? ($final_report[$report->service_date]['OD_hrs'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE_hrs']) ? ($final_report[$report->service_date]['WE_hrs'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['hours'] = isset($final_report[$report->service_date]['hours']) ? ($final_report[$report->service_date]['hours'] + $report->Total_hours ) : $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    }
                    /*if ($report->service_date == $data_to_check) {
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->service_date]['OD']) ? $final_report[$report->service_date]['OD'] : 0);
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->service_date]['WE']) ? $final_report[$report->service_date]['WE'] : 0);
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->service_date]['OD_hrs']) ? $final_report[$report->service_date]['OD_hrs'] : 0);
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->service_date]['WE_hrs']) ? $final_report[$report->service_date]['WE_hrs'] : 0);
                        $final_report[$report->service_date]['hours'] += $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    } else {
                        $activity_report[$report->service_date] = array();
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->service_date]['OD']) ? $final_report[$report->service_date]['OD'] : 0);
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->service_date]['WE']) ? $final_report[$report->service_date]['WE'] : 0);
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->service_date]['OD_hrs']) ? $final_report[$report->service_date]['OD_hrs'] : 0);
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->service_date]['WE_hrs']) ? $final_report[$report->service_date]['WE_hrs'] : 0);
                        $final_report[$report->service_date]['hours'] = $report->Total_hours;
                        //$final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    }*/
                    $final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    $data_to_check = $report->service_date;
                }
            }
            
            $service_date = date('Y-m-d', strtotime('+1 day', strtotime($service_date)));    
        }
        
        
        $data['reports'] = $final_report;
        $data['from_date'] = date('d/m/Y', strtotime($from_date));
        $data['to_date'] = date('d/m/Y', strtotime($to_date));
        $layout_data['content_body'] = $this->load->view('activity_summary_report', $data, TRUE);
        $layout_data['page_title'] = 'Activity Summary Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }



    function activity_summary_report_toexcel($from_date,$to_date) {
         
        if ($from_date!=''&&$to_date!='') {
            
        } else {
            $to_date = date('Y-m-d');
            $from_date = date('Y-m-d', strtotime('-30 days', strtotime($to_date)));
            
        }
        
        $datetime1 = new DateTime($from_date);
        $datetime2 = new DateTime($to_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        
        $service_date = $from_date;
        
        
        $final_report = array();
        for($i = 0; $i <= $days; ++$i)
        {            
            $reports = $this->reports_model->get_activity_summary_report_by_date($service_date);
            if (!empty($reports)) {
                $data_to_check = "";
                foreach ($reports as $report) {
                    if ($report->service_date == $data_to_check) {
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] += $report->booking_type == 'OD' ? 1 : 0;
                        $final_report[$report->service_date]['WE'] += $report->booking_type == 'WE' ? 1 : 0;
                        $final_report[$report->service_date]['OD_hrs'] += $report->booking_type == 'OD' ? 1 : 0;
                        $final_report[$report->service_date]['WE_hrs'] += $report->booking_type == 'WE' ? 1 : 0;
                        $final_report[$report->service_date]['hours'] += $report->Total_hours;
                        
                    } else {
                        $activity_report[$report->service_date] = array();
                        $final_report[$report->service_date]['date'] = $report->service_date;
                        $final_report[$report->service_date]['OD'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD']) ? ($final_report[$report->service_date]['OD'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['WE'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE']) ? ($final_report[$report->service_date]['WE'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['OD_hrs'] = $report->booking_type == 'OD' ? (isset($final_report[$report->service_date]['OD_hrs']) ? ($final_report[$report->service_date]['OD_hrs'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['WE_hrs'] = $report->booking_type == 'WE' ? (isset($final_report[$report->service_date]['WE_hrs']) ? ($final_report[$report->service_date]['WE_hrs'] + 1) : 0) : 0;
                        $final_report[$report->service_date]['hours'] = isset($final_report[$report->service_date]['hours']) ? ($final_report[$report->service_date]['hours'] + $report->Total_hours ) : $report->Total_hours;
                        
                    }
                    

                    $final_report[$report->service_date]['payment'] = $this->reports_model->get_payment_by_date($report->service_date);
                    $data_to_check = $report->service_date;
                }
            }
            
            $service_date = date('Y-m-d', strtotime('+1 day', strtotime($service_date)));    
        }
        
        
        $data['reports'] = $final_report;
        $this->load->view('activity_summary_report_excel', $data);
    }




    
    function activity_summary_view() {
        
        // if(!user_permission(user_authenticate(), 13))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        $data['zone_report'] = array();
        $data['search'] = array();
        $date = "";
        $zone = "";
        $zone_name = "All";
        $day = strftime("%A", strtotime(date('Y-m-d')));
        $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);
        $data['search'] = $search;

        if ($this->input->post('zone_report')) {
            $date = $this->input->post('zone_date');

            if ($date != "") {
                list($day, $month, $year) = explode("/", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_activity_zone_report($zone_date, $zone);
            //echo "<pre>";
            //print_r($zone_result);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {

                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 

                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['maid_name'] = $zone_res->maid_name;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_number'] = $zone_res->mobile_number_1;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;

                $data['zone_report'][] = $zone_report;
            }
            //echo "<pre>";
            //print_r($data['zone_report']);
            $search = array('search_date' => $date, 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids_report($zone_date, $zone);
        } else {
            $date = $this->uri->segment(3);//date('d/m/Y');

            if ($date != "") {
                list($year, $month, $day) = explode("-", $date);
                $zone_date = "$year-$month-$day";
                $day = strftime("%A", strtotime($zone_date));
            } else {
                $zone_date = "";
                $day = "";
            }
            $zone = $this->input->post('zones');
            if ($zone == "") {
                $zone_name = "All";
            } else {
                $zone_name = $this->reports_model->get_zone_name($zone);
            }
            $zone_result = $this->reports_model->get_activity_zone_report($zone_date, $zone);
            $zone_report = array();
            foreach ($zone_result as $zone_res) {

                $firstTime = $zone_res->time_from;
                $lastTime = $zone_res->time_to;
                $firstTime = strtotime($firstTime);
                $lastTime = strtotime($lastTime);
                $timeDiff = $lastTime - $firstTime;
                $timedif = ($timeDiff / 60) / 60;
                //echo $timedif; 

                $zone_report['maid_id'] = $zone_res->maid_id;
                $zone_report['customer'] = $zone_res->customer_name;
                $zone_report['customer_number'] = $zone_res->mobile_number_1;
                $zone_report['zone'] = $zone_res->zone_name;
                $zone_report['area'] = $zone_res->area_name;
                $zone_report['booking_type'] = $zone_res->booking_type;
                $zone_report['time_from'] = $zone_res->time_from;
                $zone_report['time_to'] = $zone_res->time_to;
                $zone_report['time_diff'] = $timedif;

                $data['zone_report'][] = $zone_report;
            }
            $search = array('search_date' => date('d/m/Y', strtotime($date)), 'search_zone' => $zone, 'search_zone_name' => $zone_name, 'search_day' => $day);

            $data['search'] = $search;
            $data['maids'] = $this->reports_model->get_maids_report($zone_date, $zone);
        }
        
        
        //$data['maids'] = $this->reports_model->get_all_maids();
        $data['zones'] = $this->settings_model->get_zones();
        $layout_data['content_body'] = $this->load->view('activity_summary_view', $data, TRUE);
        $layout_data['page_title'] = 'Activity Summary Report View';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function maidattendance() {
         if(!user_permission(user_authenticate(), 18))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('attendance_date')) {
            $attendance_date = $this->input->post('attendance_date');
            $s_date = explode("/", $attendance_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $attendance_date = date('d/m/Y');
        }

        $data['attendance_date'] = $attendance_date;
        $data['attendance_report'] = $this->maids_model->get_maid_attendance_by_date($date);

        $layout_data['content_body'] = $this->load->view('maid_attendance_report', $data, TRUE);

        $layout_data['page_title'] = 'Maid Attendance Report';
        $layout_data['meta_description'] = 'Maid Attendance Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    // *********************************************************************************
    function maid_idle_report()
    {
        if (!user_permission(user_authenticate(), 18)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        // log_message('error','post idle report'.json_encode($this->input->post()));
        if ($this->input->post('idle_date')) {
            $idle_date = $this->input->post('idle_date');
            $s_date = explode("/", $idle_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $idle_date = date('d/m/Y');
        }

        $data['idle_date'] = $idle_date;
        $data['maids'] = $this->maids_model->get_maids();

        // print_r($data['non_working_maids']);die();

        $layout_data['content_body'] = $this->load->view('maid_idle_report', $data, TRUE);

        $layout_data['page_title'] = 'Maid Idle Report';
        $layout_data['meta_description'] = 'Maid Idle Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
   
    // *********************************************************************

    function maidattendance_rptto_xcl($date) {
        
        $data['attendance_date'] = $attendance_date;
        $data['attendance_report'] = $this->maids_model->get_maid_attendance_by_date($date);

        $this->load->view('maid_attendance_report_excel', $data);
    }


    function driveractivity() {
         if(!user_permission(user_authenticate(), 20))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_date')) {
            $activity_date = $this->input->post('activity_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $activity_date = date('d/m/Y');
        }

        $data['activity_date'] = $activity_date;
        $data['driver_activity_report'] = $this->tablets_model->get_driver_activity_by_date($date);

        $layout_data['content_body'] = $this->load->view('driver_activity_report', $data, TRUE);

        $layout_data['page_title'] = 'Driver Activity Report';
        $layout_data['meta_description'] = 'Driver Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function driveractivity_report_to_excel($date) {
    	//echo $date;
    	$data['driver_activity_report'] = $this->tablets_model->get_driver_activity_by_date($date);

        $this->load->view('driver_activity_report_excel', $data);
    }

	/*
     * Maid attendance by vehicle
     * @author : Geethu
     * @date : 27-08-2015
     */
    function vehicleattendance() 
    {
        if(!user_permission(user_authenticate(), 21))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('vehicle_report')) 
        {
            $search_date = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $search_date);
            $date = "$year-$month-$day";
            $reports = $this->maids_model->get_maid_vehicle_report($date);
        } 
        else 
        {
            $search_date = date('d/m/Y');
            $reports = $this->maids_model->get_maid_vehicle_report();
        }
        $zones = $this->zones_model->get_all_zones();
        
        $maid_attendance = array();
        $i = 0;
        $zone_id = 0;
        foreach ($reports as $rpt)
        {
            foreach($zones as $zone)
            {
                
                    if($zone_id != $rpt->zone_id)
                    {
                        $zone_id = $rpt->zone_id;
                        $i = 0;
                    }
                    
                    $maid_attendance[$i][$zone_id]['maid_name'] = $rpt->maid_name;
                    $maid_attendance[$i][$zone_id]['attendance_status'] = $rpt->attendance_status;
                    
            }
            ++$i;
        }
       
        //echo '<pre>';
        //print_r($maid_attendance);
        //exit;
        
        $data = array();
        $data['maids'] = $this->maids_model->get_maids();
        $data['zones'] = $zones;
        $data['reports'] = $maid_attendance;
        $data['search_date'] = $search_date;
        
        $layout_data['content_body'] = $this->load->view('maid_vehicle_report', $data, TRUE);
        $layout_data['page_title'] = 'Vehicle Attendance Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    function vehicleattendance_report_toexcel($search_date) 
    {
    	$reports = $this->maids_model->get_maid_vehicle_report($search_date);


    	$zones = $this->zones_model->get_all_zones();
        
        $maid_attendance = array();
        $i = 0;
        $zone_id = 0;
        foreach ($reports as $rpt)
        {
            foreach($zones as $zone)
            {
                
                    if($zone_id != $rpt->zone_id)
                    {
                        $zone_id = $rpt->zone_id;
                        $i = 0;
                    }
                    
                    $maid_attendance[$i][$zone_id]['maid_name'] = $rpt->maid_name;
                    $maid_attendance[$i][$zone_id]['attendance_status'] = $rpt->attendance_status;
                    
            }
            ++$i;
        }

        $data = array();
        $data['maids'] = $this->maids_model->get_maids();
        $data['zones'] = $zones;
        $data['reports'] = $maid_attendance;
        $data['search_date'] = $search_date;
        
        $this->load->view('vehicleattendance_report_toexcel_spreadsheetview', $data);
    }


    function useractivity() {
         // if(!user_permission(user_authenticate(), 20))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        if ($this->input->post('activity_date')) {
            $activity_date = $this->input->post('activity_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d');
            $activity_date = date('d/m/Y');
        }
        if ($this->input->post('activity_date_to')) {
            $activity_date_to = $this->input->post('activity_date_to');
            $s_date = explode("/", $activity_date_to);
            $date_to = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date_to = NULL;
            $activity_date_to = date('d/m/Y');
        }

        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;
        
        $data['user_activity_report'] = $this->users_model->get_user_activity_by_date($date, $date_to);
        
        $layout_data['content_body'] = $this->load->view('user_activity_report', $data, TRUE);

        $layout_data['page_title'] = 'User Activity Report';
        $layout_data['meta_description'] = 'User Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    function user_activity_report_to_excel($date,$date_to) {
    	//echo $date.$date_to;
    	 $data['user_activity_report'] = $this->users_model->get_user_activity_by_date($date, $date_to);
        $this->load->view('user_activity_report_to_excel_spreadsheetview', $data);
    }
    
    function maidhours() {
         if(!user_permission(user_authenticate(), 20))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('from_date')) {
            $activity_date = $this->input->post('from_date');
            $s_date = explode("/", $activity_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date = date('Y-m-d',strtotime('-7 days'));
            $activity_date = date('d/m/Y',strtotime('-5 days'));
        }
        if ($this->input->post('to_date')) {
            $activity_date_to = $this->input->post('to_date');
            $s_date = explode("/", $activity_date_to);
            $date_to = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $date_to = date('Y-m-d');
            $activity_date_to = date('d/m/Y');
        }

        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;
        
        $data['maid_hours_report'] = $this->maids_model->get_maid_hours($date, $date_to);
        
        $layout_data['content_body'] = $this->load->view('maid_hours_report', $data, TRUE);

        $layout_data['page_title'] = 'Maid Hours Report';
        $layout_data['meta_description'] = 'Maid Hours Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    function zone_activity_summary() {
         if(!user_permission(user_authenticate(), 21))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('activity_report')) {
            $data['from_date'] = $this->input->post('from_date');
            $data['to_date'] = $this->input->post('to_date');
            list($day, $month, $year) = explode('/', $this->input->post('from_date'));
            $from_date = "$year-$month-$day";
            list($day, $month, $year) = explode('/', $this->input->post('to_date'));
            $to_date = "$year-$month-$day";
            $reports = $this->reports_model->get_zone_activity_summary_report($from_date, $to_date);
        } else {
            $to_date = date('Y-m-d');
            $from_date = date('Y-m-d', strtotime('-30 days', strtotime($to_date)));
            $reports = $this->reports_model->get_zone_activity_summary_report($from_date, $to_date);
        }
        if (!empty($reports)) {
            $data_to_check = "";
            $final_report = array();
            foreach ($reports as $report) {
                if ($report->zone_id == $data_to_check) {
                    $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                    $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                    $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                    $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                    $final_report[$report->zone_id]['hours'] += $report->Total_hours;
                    $final_report[$report->zone_id]['payment'] += $report->Total_payment;
                } else {
                    $activity_report[$report->zone_id] = array();
                    $final_report[$report->zone_id]['zone'] = $report->zone_name;
                    $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                    $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                    $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                    $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                    $final_report[$report->zone_id]['hours'] = $report->Total_hours;
                    $final_report[$report->zone_id]['payment'] = $report->Total_payment;
                }
                $data_to_check = $report->zone_id;
            }
        }
        $data['reports'] = $final_report;
        $data['from_date'] = date('d/m/Y', strtotime($from_date));
        $data['to_date'] = date('d/m/Y', strtotime($to_date));
        $layout_data['content_body'] = $this->load->view('zone_activity_summary_report', $data, TRUE);
        $layout_data['page_title'] = 'Zone Activity Summary Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    function zonwis_booking_report_spreadsheetview($from_date,$to_date) {
    	$reports = $this->reports_model->get_zone_activity_summary_report($from_date, $to_date);
    	$data_to_check = "";
        $final_report = array();
        foreach ($reports as $report) 
        {
            if ($report->zone_id == $data_to_check) 
            {
                $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                $final_report[$report->zone_id]['hours'] += $report->Total_hours;
                $final_report[$report->zone_id]['payment'] += $report->Total_payment;
            } 
            else 
            {
                $activity_report[$report->zone_id] = array();
                $final_report[$report->zone_id]['zone'] = $report->zone_name;
                $final_report[$report->zone_id]['OD'] = $report->booking_type == 'OD' ? $report->hour_count : (isset($final_report[$report->zone_id]['OD']) ? $final_report[$report->zone_id]['OD'] : 0);
                $final_report[$report->zone_id]['WE'] = $report->booking_type == 'WE' ? $report->hour_count : (isset($final_report[$report->zone_id]['WE']) ? $final_report[$report->zone_id]['WE'] : 0);
                $final_report[$report->zone_id]['OD_hrs'] = $report->booking_type == 'OD' ? $report->Total_hours : (isset($final_report[$report->zone_id]['OD_hrs']) ? $final_report[$report->zone_id]['OD_hrs'] : 0);
                $final_report[$report->zone_id]['WE_hrs'] = $report->booking_type == 'WE' ? $report->Total_hours : (isset($final_report[$report->zone_id]['WE_hrs']) ? $final_report[$report->zone_id]['WE_hrs'] : 0);
                $final_report[$report->zone_id]['hours'] = $report->Total_hours;
                $final_report[$report->zone_id]['payment'] = $report->Total_payment;
            }
            $data_to_check = $report->zone_id;
        }

        $data['reports'] = $final_report;
        $data['from_date'] = date('d/m/Y', strtotime($from_date));
        $data['to_date'] = date('d/m/Y', strtotime($to_date));
        $this->load->view('zonwis_booking_report_spreadsheetview_excel', $data);
    }
    function booking_cancel() {
         // if(!user_permission(user_authenticate(), 16))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        if ($this->input->post('ondeday_report')) {
            $data['search_date'] = $this->input->post('search_date');
            list($day, $month, $year) = explode('/', $this->input->post('search_date'));
            $search_date = "$year-$month-$day";
            $data['reports'] = $this->reports_model->get_bookingremarks_report($search_date);
        } else {
            $data['reports'] = $this->reports_model->get_bookingremarks_report();
        }
        
        $layout_data['content_body'] = $this->load->view('booking_cancel_report', $data, TRUE);
        $layout_data['page_title'] = 'Booking Cancel Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    function booking_cancel_spreadsheetview($search_date) {
         // if(!user_permission(user_authenticate(), 16))
        // {
            // show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        // }
        $data=array();
        $data['reports'] = $this->reports_model->get_bookingremarks_report($search_date);
        $this->load->view('booking_cancel_report_spreadsheetview', $data);
    }
    
    
    function maid_leave_report()
    {

          $param=array();
          $data['leaveresults'] = "";
          if ($this->input->post('search')) 
          {
              
          $data['maid_id']     =    $this->input->post('maid_id');
         
          $start_date = str_replace('/', '-', $this->input->post('start_date'));
          $end_date   = str_replace('/', '-', $this->input->post('end_date'));
          
          $data['startdate']   =    $this->input->post('start_date');
          $data['enddate']     =    $this->input->post('end_date');
          
          $data['start_date']  =    date('Y-m-d', strtotime($start_date));
              
          $data['end_date']    =   date('Y-m-d', strtotime($end_date));
          
          $data['leave_type']  =  $this->input->post('leave_type'); 
          
          $data['results']=$this->maids_model->get_maid_leave_report($data);
          $data['leaveresults']=$this->maids_model->get_maid_leave_reportresults($data);
          
          }
          
           if ($this->input->post('action')=="add_leave") {
          
             $maid_id     =    $this->input->post('maid_id');
             
             $start_date = str_replace('/', '-', $this->input->post('start_date'));
             $end_date   = str_replace('/', '-', $this->input->post('end_date'));
          
             $start_date  =    date('Y-m-d', strtotime($start_date));
             $end_date    =   date('Y-m-d', strtotime($end_date)); 
             
             $begin = new DateTime($start_date);
             $end   = new DateTime($end_date);
             $leave_done = array();
             $leave_days=array();
             for($i = $begin; $begin <= $end; $i->modify('+1 day'))
             {
	                //echo $i->format("Y-m-d");
	                $maid_fields = array();
	                $maid_fields['maid_id'] = $maid_id;
	                $maid_fields['leave_date'] = $i->format("Y-m-d");
	                $maid_fields['leave_status'] = 1;  
	                $maid_fields['added_by'] = user_authenticate();
	                $maid_fields['leave_type'] = $this->input->post('leave_type');
	                $maid_fields['typeleaves'] = $this->input->post('leave_type_new');
	                
	               if (!in_array($i->format("Y-m-d"), $leave_days)) 
	                {                                            
	                     
	                     $leave_id = $this->maids_model->add_maid_leave($maid_fields); 
	                     array_push($leave_days, $i->format("Y-m-d"));
	                     array_push($leave_done, $leave_id);
	                      
	                }
	                
	                

	         }
             if(!empty($leave_done))
                {
                    echo "success";
                    exit();
                }
             
             
             
           }
           
          
          $data['maids']=$this->maids_model->get_maids();
          $data['type']=array("1"=>"Full Day","2"=>"Half Day",);
          $layout_data['content_body'] = $this->load->view('maid_leave_report', $data, TRUE);
          $layout_data['page_title'] = 'Maid Leave Reports';
          $layout_data['meta_description'] = 'reports';
          $layout_data['css_files'] = array('jquery.fancybox.css','datepicker.css', 'demo.css');
          $layout_data['external_js_files'] = array();
           $layout_data['reports_active'] = '1'; 
          $layout_data['js_files'] = array('jquery.fancybox.pack.js','bootstrap-datepicker.js','jquery.dataTables.min.js','maid_leave_report.js','leave_report.js');
        
          $this->load->view('layouts/default', $layout_data);   
        
    }
    
    function maid_leave_report_to_excel($maidid,$s_date,$e_date)
    {
    	$data['maid_id']     =    $maidid;
        $data['start_date']  =    $s_date;              
        $data['end_date']    =   $e_date;          
        $data['leave_type']  =  0; 
          
        $data['results']=$this->maids_model->get_maid_leave_report($data);
        $data['leaveresults']=$this->maids_model->get_maid_leave_reportresults($data);

        $data['maids']=$this->maids_model->get_maids();
        $data['type']=array("1"=>"Full Day","2"=>"Half Day",);

        $this->load->view('maid_leave_report_spreadsheetview', $data);

    }
    function remove_leave() {
        
        $leave_id = $this->input->post('leave_id');
        $status = $this->input->post('leave_status');
        $status = $status == 1 ? 0 : 1;
        $this->maids_model->delete_leavereport($leave_id, $status);
        echo $status;
        exit;
    }
    
    function online_payment_report()
    {
//         if(!user_permission(user_authenticate(), 15))
//        {
//            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
//        }
        

        $data['online_payment_reports'] = $this->reports_model->get_online_payments();
        
        $layout_data['content_body'] = $this->load->view('online_payment_reports', $data, TRUE);
        $layout_data['page_title'] = 'Online Payment Reports';
        $layout_data['meta_description'] = 'Online Payment Reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
//        $layout_data['reports_active'] = '1';
        $layout_data['accounts_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function rate_review()
    {
            $data['rate_report'] = array();
            $data['search'] = array();
            $date = "";$maid_id = "";
            $day = strftime("%A", strtotime(date('Y-m-d')));
            $search = array('search_date' => $date, 'search_day' => $day, 'maid_id' => $maid_id);
            $data['search'] = $search;
            $data['maidid'] = $maid_id;
            if ($this->input->post('rate_report')) 
                {
                    //$date = $this->input->post('vehicle_date');
                    $from_datee = $this->input->post('search_date_from');
                    $to_datee = $this->input->post('search_date_to');
                    $maid_id = $this->input->post('maid_id');
                    
                    if ($from_datee != ""&&$to_datee != "") 
                        {
                            list($day, $month, $year) = explode("/", $from_datee);
                            $from_date = "$year-$month-$day";
                            $from_day = strftime("%A", strtotime($from_date));
                            
                            list($day, $month, $year) = explode("/", $to_datee);
                            $to_date = "$year-$month-$day";
                            $to_day = strftime("%A", strtotime($to_date));
                        } 
                    else 
                        {
                            $from_date = "";
                            $to_date = "";
                            $from_day = "";
                            $to_day= "";
                        }

                    $rate_result = $this->reports_model->get_rate_reports_new($from_date,$to_date,$maid_id);
                    $data['rate_report'] = $rate_result;



                    $search = array('search_date_from' => $from_datee,'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day, 'maid_id' => $maid_id);
                    $data['maidid'] = $maid_id;
                    $data['search'] = $search;
                } 
            else 
                {

                    $from_datee = date('d/m/Y', strtotime('-7 days'));
                    $to_datee = date('d/m/Y');

                    if ($from_datee != ""&&$to_datee != "") 
                        {
                            list($day, $month, $year) = explode("/", $from_datee);
                            $from_date = "$year-$month-$day";
                            $from_day = strftime("%A", strtotime($from_date));
                            
                            list($day, $month, $year) = explode("/", $to_datee);
                            $to_date = "$year-$month-$day";
                            $to_day = strftime("%A", strtotime($to_date));
                        } 
                    else 
                        {
                            $from_date = "";
                            $to_date = "";
                            $from_day = "";
                            $to_day= "";
                        }

                    $rate_result = $this->reports_model->get_rate_reports($from_date,$to_date);
                    $data['rate_report'] = $rate_result;



                    $search = array('search_date_from' => $from_datee,'search_date_to' => $to_datee, 'search_day_from' => $from_day, 'search_day_to' => $to_day, 'maid_id' => $maid_id);
                    $data['maidid'] = $maid_id;
                    $data['search'] = $search;
                }
                    // echo '<pre>';
                    // print_r($data);
                    // exit();
                
            //$data['zones'] = $this->settings_model->get_zones();
            $data['maids']=$this->maids_model->get_maids();
            $layout_data['content_body'] = $this->load->view('rate_review', $data, TRUE);
            $layout_data['page_title'] = 'Rate & Review';
            $layout_data['meta_description'] = 'Rate Review';
            $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
            $this->load->view('layouts/default', $layout_data);
    }

    function ratereviewreporttoExcel()
    {
    	
    	$s_date = $this->uri->segment(3);
		$e_date = $this->uri->segment(4);
		$data=array();
		$rate_result = $this->reports_model->get_rate_reports($s_date,$e_date);
        $data['rate_report'] = $rate_result;
		$this->load->view('ratereviewreport_spreadsheetview', $data);
    }


    function bookingreporttoExcel()
    {
    	
    	$s_date = $this->uri->segment(3);
		$e_date = $this->uri->segment(4);
		$zone_id = $this->uri->segment(5);
		$maid_id = $this->uri->segment(6);
		$status = $_GET['status'];
		// echo "s_date=".$s_date.", e_date=".$e_date.", zone_id=".$zone_id.", maid_id=".$maid_id.", status :".$status;

		$per_report = array();$performance_report= array();
                
        $loop_start_date=$s_date;
        $loop_end_date=$e_date;
        $getmaids = $this->maids_model->get_all_maidss($status);
        while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {
        		if(is_numeric($maid_id)) // case in which maid filter is selected
        		{
        			$per_report = $this->bookings_model->get_maid_performance($maid_id, $loop_start_date,$zone_id);
                    array_push($performance_report, $per_report);
        		}
        		else
        		{ // all maids
        			foreach ($getmaids as $getmaid)
                    {
                            $maid_ids = $getmaid->maid_id;
                            $per_report = $this->bookings_model->get_maid_performance($maid_ids, $loop_start_date,$zone_id);
                            array_push($performance_report, $per_report);
                    }
        		}                

                $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));

        }

		$data=array();
		$data['performance_report'] = $performance_report;
		// $rate_result = $this->reports_model->get_rate_reports($s_date,$e_date);
  //       $data['rate_report'] = $rate_result;
		$this->load->view('bookingreport_spreadsheetview', $data);
    }
	
	function call_report() 
	{
        if($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0)
        {
            $payment_date = trim($this->input->post('search_date'));
        }
        else
        {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
		
		if($this->input->post('call_type') && strlen($this->input->post('call_type')) > 0)
        {
            $call_type = trim($this->input->post('call_type'));
        }
        else
        {
            $call_type = 0;
        }
		
		$reports = $this->reports_model->get_call_report_new($date, $customer_id = NULL, $call_type);
		
		$data = array();
        $data['reports'] = $reports;
        $data['payment_date'] = $payment_date;
		$data['call_type'] = $call_type;
		
        $layout_data['content_body'] = $this->load->view('call_report', $data, TRUE);
        $layout_data['page_title'] = 'Call Reports';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function revenue_report()
	{
		$reports = array();
        $search_conditions = array();
		if ($this->input->post('revenuesubmit')) {
            if ($this->input->post('month') != "" && $this->input->post('year') != "") {
				$month = $this->input->post('month');
				$year = $this->input->post('year');
				$maids = $this->reports_model->get_all_maids();
				foreach($maids as $maidval)
				{
					$maid_id = $maidval['maid_id'];
					$list=array();
					//$month = 11;
					//$year = 2020;

					for($d=1; $d<=31; $d++)
					{
						$time=mktime(12, 0, 0, $month, $d, $year);          
						if (date('m', $time)==$month)       
							$list[]=date('Y-m-d', $time);
					}
					$tot_hrs = 0;
					$totamt = 0;
					foreach($list as $dateval)
					{
						$results = $this->reports_model->get_maid_revenue_report($maid_id,$dateval);
						$tot_hrs += $results->hours;
						$totamt += $results->net_amount;
					}
					$maidarray = array();
					$maidarray['maidname'] =  $maidval['maid_name'];
					$maidarray['mobile'] =  $maidval['maid_mobile_1'];
					$maidarray['hrs'] =  $tot_hrs;
					$maidarray['amount'] =  $totamt;
					array_push($reports,$maidarray);
				}
                $search_conditions = array(
                    'month' => $this->input->post('month'),
                    'year' => $this->input->post('year')
                );
			}
		}
		
		$data = array();
        $data['reports'] = $reports;
        $data['search_condition'] = $search_conditions;
		
        $layout_data['content_body'] = $this->load->view('revenue_report', $data, TRUE);
        $layout_data['page_title'] = 'Revenue Report';
        $layout_data['meta_description'] = 'revenue-reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
         $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function revenuetoExcel()
	{
		$reports = array();
		$month = $this->uri->segment(3);
		$year = $this->uri->segment(4);
		$maids = $this->reports_model->get_all_maids();
		foreach($maids as $maidval)
		{
			$maid_id = $maidval['maid_id'];
			$list=array();
			//$month = 11;
			//$year = 2020;

			for($d=1; $d<=31; $d++)
			{
				$time=mktime(12, 0, 0, $month, $d, $year);          
				if (date('m', $time)==$month)       
					$list[]=date('Y-m-d', $time);
			}
			$tot_hrs = 0;
			$totamt = 0;
			foreach($list as $dateval)
			{
				$results = $this->reports_model->get_maid_revenue_report($maid_id,$dateval);
				$tot_hrs += $results->hours;
				$totamt += $results->net_amount;
			}
			$maidarray = array();
			$maidarray['maidname'] =  $maidval['maid_name'];
			$maidarray['mobile'] =  $maidval['maid_mobile_1'];
			$maidarray['hrs'] =  $tot_hrs;
			$maidarray['amount'] =  $totamt;
			array_push($reports,$maidarray);
		}
		$data = array();
        $data['reports'] = $reports;
		$this->load->view('revenuereport_spreadsheetview', $data);
	}
	function invoicereporttoExcel(){
        $from=$this->uri->segment(3)=='nil'?null:date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(3))));
        $to=$this->uri->segment(4)=='nil'?null:date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(4))));
        $c_id=$this->uri->segment(5)=='nil'?null:$this->uri->segment(5);
        $status=$this->uri->segment(6)=='nil'?null:$this->uri->segment(6);
        $company=$this->uri->segment(7)=='nil'?null:str_replace('%20',' ',$this->uri->segment(7));
        $source=$this->uri->segment(8)=='nil'?null:str_replace('%20',' ',$this->uri->segment(8));
        $this->load->model('invoice_model');
        $data['invoice_report']=$this->invoice_model->get_customer_invoices($from,$to,$c_id,$status,$company,$source);
        $this->load->view('invoicereporttoExcel',$data);
    }
    function customerpaymentreporttoExcel(){
        $from=$this->uri->segment(3)=='nil'?null:date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(3))));
        $to=$this->uri->segment(4)=='nil'?null:date('Y-m-d', strtotime(str_replace("/", "-", $this->uri->segment(4))));
        $c_id=$this->uri->segment(5)=='nil'?null:$this->uri->segment(5);
        $company=$this->uri->segment(6)=='nil'?null:str_replace('%20',' ',$this->uri->segment(6));
        $type=$this->uri->segment(7)=='nil'?null:$this->uri->segment(7);
        $source=$this->uri->segment(8)=='nil'?null:str_replace('%20',' ',$this->uri->segment(8));
        $this->load->model('customers_model');
        $data['payment_report']=$this->customers_model->get_customer_payments($from,$to,$c_id,$company,$type,$source);
        $this->load->view('customerpaymentreporttoExcel',$data);
    }

   
       
}