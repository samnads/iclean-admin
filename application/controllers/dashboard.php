<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if(!is_user_loggedin())
		{
			redirect('logout');
			
		}
                if(!user_permission(user_authenticate(), 1))
                { 
                    show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'reports">Go to Reports</a>', 401, 'Access denied');
                }
		$this->load->helper('google_api_helper');		
        }
	public function index()
	{
                $this->load->model('bookings_model');
                $this->load->model('customers_model');
                $this->load->model('maids_model');
                $this->load->model('users_model');
		$data           = array();
                $service_date   = date('Y-m-d');
                $data['total_booking_count']= $this->bookings_model->total_booking_count();
                $data['schedule_count']     = $this->bookings_model->get_schedule_by_date_counts($service_date);
                
                $total_todays_bookng_hrs    = $this->bookings_model->get_schedule_hrs_by_date($service_date);
                $todaybookinghrs            = 0;
                foreach($total_todays_bookng_hrs as $totalhrs){
					$t_val = ((strtotime($totalhrs->time_to.':00') - strtotime($totalhrs->time_from.':00'))/3600);
                    $todaybookinghrs        = ($todaybookinghrs + $t_val);
                }
                $data['customer_count']     = $this->customers_model->get_total_customers_counts();
                
                // daily booking summary calculation starts
                $data['maid_count']         = $maidcount = $this->bookings_model->get_schedule_by_date_counts($service_date);
                $data['dailySummaryperc']   = ($maidcount > 0) ? ($todaybookinghrs / ($maidcount*8)) * 100 : 0;
                // daily booking summary calculation ends
                 
                // Weekly booking summary calculation starts
                $lastSatday                 = new DateTime('last saturday');
                $lastweekend_service_date   = $lastSatday->format('Y-m-d');
               
                $begin      = new DateTime($lastweekend_service_date);
                //$today      =  new DateTime(date('Y-m-d')+1);
                $today      =  new DateTime(date('Y-m-d'));
                $daterange  = new DatePeriod($begin, new DateInterval('P1D'), $today);
                $weeklyTotalBookingHRs = $weeklymaidcount = 0;
                $weekdaysCount = 1 ;
                foreach($daterange as $date){
                    $serviceDateLoop            = $date->format("Y-m-d") ;
                    $weeklymaidcount            = $weeklymaidcount + $this->bookings_model->get_schedule_by_date_counts($serviceDateLoop);
                    $total_weekly_bookng_hrs    = $this->bookings_model->get_schedule_hrs_by_date($serviceDateLoop);
                    foreach($total_weekly_bookng_hrs as $totalhrs){
						$t_val_2 = ((strtotime($totalhrs->time_to.':00') - strtotime($totalhrs->time_from.':00'))/3600);
                        $weeklyTotalBookingHRs  = $weeklyTotalBookingHRs + ($t_val_2);
                    }
                    $weekdaysCount ++ ;
                }
                $data['weeklySummaryperc']  = ($weeklymaidcount >0) ? ($weeklyTotalBookingHRs / ($weeklymaidcount*8 * $weekdaysCount)) * 100 : 0;
                // Weekly booking summary calculation ends
                
                
                
//                Monthly booking summary calculation starts
                
                $beginMonth      = new DateTime(date('Y-m-01'));
                //$today      =  new DateTime(date('Y-m-d')+1);
                $today      =  new DateTime(date('Y-m-d'));
                $mnthdaterange  = new DatePeriod($beginMonth, new DateInterval('P1D'), $today);
                $monthlyTotalBookingHRs = $mnthlymaidcount = 0;
                $mnthdaysCount = 1 ;
                foreach($mnthdaterange as $date){
                    $serviceDateLoop            = $date->format("Y-m-d") ;
                    $mnthlymaidcount            = $mnthlymaidcount + $this->bookings_model->get_schedule_by_date_counts($serviceDateLoop);
                    $total_weekly_bookng_hrs    = $this->bookings_model->get_schedule_hrs_by_date($serviceDateLoop);
                    foreach($total_weekly_bookng_hrs as $totalhrs){
						$t_val_3 = ((strtotime($totalhrs->time_to.':00') - strtotime($totalhrs->time_from.':00'))/3600);
                        $monthlyTotalBookingHRs  = $monthlyTotalBookingHRs + ($t_val_3);
                    }
                    $mnthdaysCount ++ ;
                }
                $data['mnthlySummaryperc']  =  ($monthlyTotalBookingHRs / ($mnthlymaidcount*8 * $mnthdaysCount)) * 100;
                
                
//                Monthly booking summary calculation ends
                
//                 Complaints count starts
                    $data['total_daily_complaint_count']    = $this->bookings_model->get_complaints_count();
                    $data['total_weekly_complaint_count']   = $this->bookings_model->get_complaints_count($lastweekend_service_date);
                    $data['total_monthly_complaint_count']  = $this->bookings_model->get_complaints_count(date('Y-m-01'));
 
//                 Complaints count ends
//                Job in transit count
                    $data['total_job_in_transit_count']    = $this->bookings_model->get_transit_jobs_count();
//                     Daily billed invoices starts
                     $data['billed_daily_invoice_count']   = $this->bookings_model->get_invoice_count('0');
                     $data['billed_weekly_invoice_count']  = $this->bookings_model->get_invoice_count('0',$lastweekend_service_date);
                     $data['billed_monthly_invoice_count'] = $this->bookings_model->get_invoice_count('0',date('Y-m-01'));
//                     Daily billed invoices ends
//                     Daily collected invoices starts
                     $data['collected_daily_invoice_count']     = $this->bookings_model->get_invoice_count('1');
                     $data['collected_weekly_invoice_count']    = $this->bookings_model->get_invoice_count('1',$lastweekend_service_date);
                     $data['collected_monthly_invoice_count']   = $this->bookings_model->get_invoice_count('1',date('Y-m-01'));
//                     Daily collected invoices ends
                //Get week dates
                $saturday = strtotime("last saturday");
                $saturday = date('w', $saturday)==date('w') ? $saturday+6*86400 : $saturday;

                $friday = strtotime(date("Y-m-d",$saturday)." +5 days");

                $this_week_sd = date("Y-m-d",$saturday);
                $this_week_ed = date("Y-m-d",$friday);
                
                $days = ((strtotime($this_week_ed) - strtotime($this_week_sd)) / (60*60*24));
                for ($i = 0; $i <= $days; ++$i) {
                    $date = date('Y-m-d', strtotime("$this_week_sd +$i day"));
                    $year = date('Y',strtotime($date));
                    $month = date('n',strtotime($date)) - 1;
                    $day = date('d',strtotime($date));
                    $count = $this->bookings_model->get_schedule_by_date_counts($date);
                    //$jsarray = array();
                    $element = "new Date($year,$month,$day),$count,'$count bookings'";
                    $jsarray[$i] = $element;
                    //array_push($jsarray, $element);
                }

                //echo "Current week range from $this_week_sd to $this_week_ed ";
                //exit();
                //Ends
//                echo '<pre>';
//                print_r($jsarray);  
//                echo '</pre>';
                $data['grapharray'] = $jsarray;
                $data['total_invoices']     = $this->bookings_model->total_invoices_count();
                $data['recent_activity']    = $this->users_model->get_user_activity();
                
		$layout_data['content_body']= $this->load->view('dashboard', $data, TRUE);
		$layout_data['page_title']  = 'Dashboard';
		$layout_data['dashboard_active'] = '1';
		$layout_data['meta_description'] = 'Dashboard';
		$layout_data['css_files']   = array('daterangepicker.css');
		$layout_data['js_files']    = array('daterangepicker.min.js','dashboard.js');
		$layout_data['external_js_files'] = array();
		
		$this->load->view('layouts/default_dashboard', $layout_data);
	}
    
    public function get_dashboard_details()
    {
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);
        $this->load->model('maids_model');
        $this->load->model('bookings_model');
        $this->load->model('customers_model');
        $this->load->model('service_types_model');

        $start_date=$this->input->post('start_date');
        $end_date=$this->input->post('end_date');
        $zone_id='';
        //$start_date='2021-03-14';
        //$end_date='2021-03-15';

        $getmaids = $this->maids_model->get_maids();
        
        $per_report = array();$performance_report=array();
                
        $loop_start_date=$start_date;
        $loop_end_date=$end_date;$revenue_by_day=0;$totalhrs=0;$new_customers=0;
        $service_types=$this->service_types_model->get_service_types();
        $revenue_by_st=array();$booking_ids=array();

        foreach ($service_types as $sts)
            {
                $revenue_by_st[$sts->service_type_id]['revenue']=0;
                $revenue_by_st[$sts->service_type_id]['name']=$sts->service_type_name;
            }

        while (strtotime($loop_start_date) <= strtotime($loop_end_date)) 
        {
            foreach ($getmaids as $getmaid)
            {
                    $maid_ids = $getmaid->maid_id;
                    $per_report = $this->bookings_model->get_maid_performance($maid_ids, $loop_start_date,$zone_id);
                    array_push($performance_report, $per_report);
            }
            $new_customers+=$this->customers_model->get_new_customers_count($loop_start_date);
            $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
        }

        foreach($performance_report as $performance)
        {
            foreach ($performance as $key)
                    {
                        $totamt = explode(',', $key->totamt);$hrs = explode(',', $key->time_diffs);
                        $stids = explode(',', $key->stids);$bukids = explode(',', $key->bukids);
                        foreach ($stids as $key => $st) {
                            $revenue_by_st[$st]['revenue']+=$totamt[$key];
                            array_push($booking_ids,$bukids[$key]);
                        }

                        $totalhrs+=array_sum($hrs);
                        $total=0;$hrs =0;
                        $total=array_sum($totamt);
                        $revenue_by_day+=$total;
                    }
        }

        $loop_start_date=$start_date;
        $loop_end_date=$end_date;
        $rating_report=array();$per_rating_report=array();

        while (strtotime($loop_start_date) <= strtotime($loop_end_date)) 
        {
            $per_rating_report = $this->bookings_model->get_rating_report_dashboard($booking_ids, $loop_start_date);
            array_push($rating_report, $per_rating_report);
            $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));
        }

        $rating_count=0;$rating_sum=0;
        foreach($rating_report as $rating)
        {
            foreach ($rating as $key)
                    {
                        $rated_value=$key->rating;
                        if($rated_value > 0)
                        {
                            $rating_sum+=$rated_value;
                            $rating_count++;
                        }
                    }
        }
        $average_rating=0;
        if($rating_count>0){$average_rating=round(($rating_sum/$rating_count),2);}
        $rating_details=array();
        $rating_details['rating_count']=$rating_count;
        $rating_details['average_rating']=$average_rating;

        $data=array();
        $data['total_revenue']=$revenue_by_day;
        $data['total_hours']=$totalhrs;
        $data['new_customers']=$new_customers;
        $data['rev_by_st']=$revenue_by_st;
        $data['rating_details']=$rating_details;
        print_r(json_encode($data));
    }  
        public function changepassword()
        {
            $this->load->helper('form');
            if (!is_user_loggedin()) {
                redirect('logout');
            }
            $this->load->model('users_model');
            $data = array();
            $userid = $this->session->userdata('user_logged_in')['user_id'];
            if($this->input->post('change_password'))
            {
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

                $this->form_validation->set_rules('old_password', 'Old Password', 'required|callback_numeric_a');
                $this->form_validation->set_message('required', 'Please enter password', 'old_password');				

                $this->form_validation->set_rules('new_password', 'New Password', 'required|callback_numeric_b');
                $this->form_validation->set_message('required', 'Please enter password', 'new_password');

                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|callback_numeric_c');
                $this->form_validation->set_message('required', 'Please enter password', 'confirm_password');

                if($this->form_validation->run())
                {
                    $update_fields  = array();
                    $old_password   =  trim($this->input->post('old_password'));
                    $new_password   = trim($this->input->post('new_password'));
                    $confirm_password           =  trim($this->input->post('confirm_password'));
                    $update_fields['password']  =  md5($new_password);
                    $chk_password   = $this->users_model->check_passwords($userid,md5($old_password));
                    if($chk_password)
                    {
                        if($confirm_password == $new_password)
                        {
                            $update_settings = $this->users_model->update_users($userid, $update_fields);

                            $this->session->set_flashdata('success_msg', 'Password changed successfully.');
                            redirect(base_url() . 'dashboard/changepassword');
                            exit();
                        }
                        else
                        {
                            $this->session->set_flashdata('failure_msg', 'Password mismatch.');
                            redirect(base_url() . 'dashboard/changepassword');
                            exit();
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('failure_msg', 'Invalid old password.');
                        redirect(base_url() . 'dashboard/changepassword');
                        exit();

                    }
                }
            }
            
            $layout_data['content_body']        = $this->load->view('change_password', $data, TRUE);
            $layout_data['page_title']          = 'Change Password';
            $layout_data['meta_description']    = 'Change Password';
            $layout_data['css_files']           = array();
            $layout_data['external_js_files']   = array();
            $layout_data['js_files']            = array();

            $this->load->view('layouts/default', $layout_data);
               
		
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */