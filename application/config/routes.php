<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']    = "login";
$route['404_override']          = '';

$route['logout'] = 'login/logout';
$route['booking/(:num)-(:num)-(:num)']          = 'booking';
$route['booking/(:num)-(:num)-(:num)/(:num)']   = 'booking';
$route['booking/(:num)-(:num)-(:num)/(:num)/(:num)'] = 'booking';
$route['bookingnew/(:num)-(:num)-(:num)']          = 'bookingnew';
$route['bookingnew/(:num)-(:num)-(:num)/(:num)']   = 'bookingnew';
$route['bookingnew/(:num)-(:num)-(:num)/(:num)/(:num)'] = 'bookingnew';

$route['customer/add']              = 'customer';
$route['customer/validatemobilenumber'] = 'customer/validatemobilenumber';
$route['customer/add/(:any)'] = 'customer/index/$1';
$route['customers']                 = 'customer/customer_list';
$route['customers/(:num)/(:num)']   = 'customer/customer_list/$1/$2';
$route['customers/(:num)/(:num)/(:num)'] = 'customer/customer_list/$1/$2/$3';
$route['customers/(:num)']          = 'customer/customer_list/$1';
$route['customer/edit/(:num)']      = "customer/edit_customer/$1";
$route['customer/view/(:num)']      = "customer/customer_view/$1";
$route['customer/view/(:num)/(:num)']       = "customer/customer_view/$1/$2";
$route['customer/disable/(:num)/(:num)']    = "customer/disable_customer/$1/$2";

$route['maid/add']  = 'maid';
$route['maids']     = 'maid/maid_list';
$route['maids/(:num)'] = 'maid/maid_list/$1';
$route['maid/edit/(:num)'] = "maid/edit_maid/$1";
$route['maid/view/(:num)'] = "maid/maid_view/$1";

$route['zones'] = 'settings';
$route['areas'] = 'settings/area';
$route['flats'] = 'settings/flats';
$route['tablets']   = 'settings/tablets';
$route['tablets/(:num)']  = 'settings/tablets/$1';
$route['services']  = 'settings/services';
$route['souqmaid-price']  = 'settings/souqmaid_price';
$route['teams']     = 'settings/team';
$route['aggregators']     = 'settings/aggregators';
$route['edit-aggregator/(:num)']  = 'settings/edit_aggregator/$1';
$route['backpayment']       = 'settings/backpayment';
$route['backpayment/add']   = 'settings/add_backpayment';
$route['payment-settings']  = 'settings/payment_settings';
$route['sms-settings']      = 'settings/sms_settings';
$route['email-settings']      = 'settings/email_settings';
$route['tax-settings']      = 'settings/tax_settings';
$route['coupons']      = 'settings/coupons';
$route['coupons/add']         = 'settings/add_coupons';
$route['coupons/edit/(:num)']  = 'settings/edit_coupon/$1';

$route['reports/zone']      = 'reports';
$route['reports/schedule']  = 'reports/schedule_reports';
$route['reports/vehicle']   = 'reports/vehicle_report';
$route['reports/payment']   = 'reports/payment_report';
$route['reports/oneday']    = 'reports/one_day_cancel';
$route['reports/incentives']    = 'settings/incentive_reports';
$route['reports/call-report']    = 'reports/call_report';
$route['reports/booking/cancel'] = 'reports/booking_cancel';
$route['reports/work']      = 'reports/employee_work';
$route['reports/work/all']  = 'reports/employee_work_all';
$route['reports/activity']  = 'reports/activity_summary';
$route['reports/zone-wise-booking-report']  = 'reports/zone_activity_summary';
$route['reports/maid-leave-report']         = 'reports/maid_leave_report';
//$route['invoices']           = 'reports/invoice';

$route['users/add']         = 'users/add_user';
$route['users/add/(:num)']  = 'users/add_user/$1';

$route['invoice/(:num)-(:num)-(:num)/(:num)/(:num)']   = 'invoice';
$route['invoices']           = 'invoice/view_invoices';
$route['monthly-invoices']           = 'invoice/monthly_invoices';

$route['customer_statement']           = 'customer/customer_statement';
$route['rating-review']           = 'reports/rate_review';
$route['bulk-sms']           = 'customer/bulk_sms';
$route['sms-list-upload']           = 'customer/sms_list_upload';
$route['email-list-upload']           = 'customer/email_list_upload';
//$route['receivable_payments']           = 'customer/customer_receivables';

/* End of file routes.php */
/* Location: ./application/config/routes.php */