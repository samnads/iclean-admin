<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



function curl_api_service($curl_post,$url)
{
	$curl_header = array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($curl_post)
	);
	$ch2 = curl_init();
	curl_setopt($ch2, CURLOPT_URL, $url);
	curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch2, CURLOPT_POST, 1);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
	curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
	$user_datas = curl_exec($ch2);
	
	if (curl_errno($ch2)) {
		echo $error_msg = curl_error($ch2);
		exit();
	}
	
	return $user_datas;
}
