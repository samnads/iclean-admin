<?php
if(! defined('BASEPATH')) exit('No direct script access allowed');

function android_push($reg_ids = array(), $post_fields = array())
{
	$CI =& get_instance();
	
	$url = 'https://android.googleapis.com/gcm/send';
	
        $post_fields['price'] = $post_fields['message'];
        unset($post_fields['message']);
        
	$fields = array(
            'registration_ids' => $reg_ids,
            'data' => $post_fields,
        );
	
	$headers = array(
		'Authorization: key=' . $CI->config->item('google_api_key'),
		'Content-Type: application/json'
	);
	
	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Disabling SSL Certificate support temporarly
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	// Execute post
	$result = curl_exec($ch);
	curl_close($ch);

	return  $result;
}

function android_customer_push($fields) {
    $CI =& get_instance();
    // $key = "AAAAWuC9FKc:APA91bGHtMi8WUF2lzI-9bKlfGK46RVa65BDfws5M0HeGTysPTnNXQQtQJdQZBUGSp_m9pjsGpAnDNb7op8Gl7oPSsA3p-AWST4cNOHwveZu5R7QTa4R0Y-TZBknIbgt-_gi-eBp-u0A";
    //$key = "AAAAPe9OVT8:APA91bGqnR4IPTWql9bkD3WTq5O3uVwwRtnIdqONvaHPciyDOLB90dzzUgJrSES4JmYAR4bjudrPD3juYPJzzjuCoycWMijXE60mH62qBsU4h7IiN0Tuzuj-zqcC7E2EQfI6E4rAE4Pr";
    $key = "AAAAQHmlp4w:APA91bF_dCkbaLATJ4cS878Ho3BeJ8gx_QMSqQFYDnOX09-ROpoHJj3yZ5CRpeeO0epiaVQvapw_xow9FtaLn4fBcvJH009min7MhYORLotu--mlnmuVNVGEm_G7baii4zTLSczzhmFr";
	// Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }

    // Close connection
    curl_close($ch);

    return $result;
}

function android_customer_app_push($fields) {
    $CI =& get_instance();
    // $key = "AAAAWuC9FKc:APA91bGHtMi8WUF2lzI-9bKlfGK46RVa65BDfws5M0HeGTysPTnNXQQtQJdQZBUGSp_m9pjsGpAnDNb7op8Gl7oPSsA3p-AWST4cNOHwveZu5R7QTa4R0Y-TZBknIbgt-_gi-eBp-u0A";
    //$key = "AAAAPe9OVT8:APA91bGqnR4IPTWql9bkD3WTq5O3uVwwRtnIdqONvaHPciyDOLB90dzzUgJrSES4JmYAR4bjudrPD3juYPJzzjuCoycWMijXE60mH62qBsU4h7IiN0Tuzuj-zqcC7E2EQfI6E4rAE4Pr";
	$key = "AAAAQHmlp4w:APA91bF_dCkbaLATJ4cS878Ho3BeJ8gx_QMSqQFYDnOX09-ROpoHJj3yZ5CRpeeO0epiaVQvapw_xow9FtaLn4fBcvJH009min7MhYORLotu--mlnmuVNVGEm_G7baii4zTLSczzhmFr";
//
// Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }

    // Close connection
    curl_close($ch);

    return $result;
}


function get_pending_booking_count()
{
	$CI =& get_instance();
	
	$CI->load->model('bookings_model');
	
	$filter = 'Pending';
	
	$pending_bookings = $CI->bookings_model->get_all_booking_approval_list($filter);
	
	return count($pending_bookings);
	
}
function get_unread_quotation_count()
{
    $CI = &get_instance();
    $CI->load->model('quotation_model');
    $unread_quotation_count = $CI->quotation_model->get_unread_quotation_count();
    return $unread_quotation_count;
}


function get_ser_stat_from_bid_and_date($bookingid,$servdate)
    {
        $CI =& get_instance();
        
        $CI->load->model('reports_model');
        
        $service_status = $CI->reports_model->get_ser_stat_from_bid_and_date($bookingid,$servdate);
        
        return $service_status;
    }
    
function android_customer_offer_push($fields) {
    $CI =& get_instance();
    $key = "AAAAPv7ED6o:APA91bGwloLl2_9c1ZSV6xr-QwwhVhlOd90QGX9bwpHk-ZfnU5cfFHYpoFP2uC8f5L3OorEi4IIQoIAxeZsXBPYur5DKXJUSp9moWsH6eNQ5pZD9Ictzf41w1_THcP8DOY9CFgIwx_EZ";
//
// Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }

    // Close connection
    curl_close($ch);

    return $result;
}
function android_customer_notification_push($fields) {
    $CI =& get_instance();
    // $key = "AAAAWuC9FKc:APA91bGHtMi8WUF2lzI-9bKlfGK46RVa65BDfws5M0HeGTysPTnNXQQtQJdQZBUGSp_m9pjsGpAnDNb7op8Gl7oPSsA3p-AWST4cNOHwveZu5R7QTa4R0Y-TZBknIbgt-_gi-eBp-u0A";
    //$key = "AAAAPe9OVT8:APA91bGqnR4IPTWql9bkD3WTq5O3uVwwRtnIdqONvaHPciyDOLB90dzzUgJrSES4JmYAR4bjudrPD3juYPJzzjuCoycWMijXE60mH62qBsU4h7IiN0Tuzuj-zqcC7E2EQfI6E4rAE4Pr";
	$key = "AAAAfQeVLR8:APA91bEjwf0kDBvJuXKlEadd6H_QMWADXuk_QGF2sSzzz_UrlkDjMd2QNzT8Ml1DhPHCYIWxscJZDYjwfJ-m5zUe8xnIJUDGD9uG3RCHEDUWnG-j69iKMvHouUh1mCd_Qn6tqJ9pQ90Q";
//
// Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }

    // Close connection
    curl_close($ch);

    return $result;
}
