<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table border="1">
    <thead>
        <tr>
            <td>
                <center>Sl No</center>
            </td>
            <td>
                <center><b>Customer Name</b></center>
            </td>
            <td>
                <center><b>Customer Mobile</b></center>
            </td>
            <td>
                <center><b>Maid Name</b></center>
            </td>
            <td>
                <center><b>Shift</b></center>
            </td>
            <td>
                <center><b>Canceled User</b></center>
            </td>
            <td>
                <center><b>Remarks</b></center>
            </td>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($reports)) {
            $i = 0;
            foreach ($reports as $report) {
                //Payment Type
                if ($report->payment_type == "D") {
                    $paytype = "(D)";
                } else if ($report->payment_type == "W") {
                    $paytype = "(W)";
                } else if ($report->payment_type == "M") {
                    $paytype = "(M)";
                } else {
                    $paytype = "";
                }
                $i++;
        ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $report->customer_name; ?> <?php echo $paytype; ?></td>
                    <td><?php echo $report->mobile_number_1; ?></td>
                    <td><?php echo $report->maid_name; ?></td>
                    <td><?php echo $report->shift; ?></td>
                    <td><?php echo $report->user_fullname; ?></td>
                    <td><?php echo $report->remarks; ?></td>
                </tr>
        <?php
            }
        }
        ?>

    </tbody>
</table>