<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo isset($page_title) ? html_escape($page_title) : '-: Emaid :-'; ?></title>

        <!--<link rel="icon" type="image/png" sizes="16x16" href="<?php// echo base_url(); ?>img/fav/favicon-16x16.png">-->
        <link rel="manifest" href="img/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet">
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">-->
        <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/pages/dashboard.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/hm.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <?php
        if (isset($css_files) && is_array($css_files)) {
            foreach ($css_files as $css_file) {
                echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css_file . '" />';
            }
        }
        $extra_navbar_inner_style = '';
        $extra_subnavbar_inner_style = '';
        $body_background = '';
        if (strtolower($page_title) == 'bookings' || strtolower($page_title) == 'schedule') {
            $extra_navbar_inner_style = 'style="width: 104%;margin-left: -20px;"';
            $extra_subnavbar_inner_style = 'style="margin-top:54px;"';
            $body_background = 'style="background-color:#FFF;"';
        }
        ?>


    </head>
    <body <?php echo $body_background; ?>>
        <?php
        //if (is_user_loggedin()) {
            ?>
           
            <div class="main" style="min-height: 572px;">
                <div class="main-inner">
                    <div class="container" style="width: 95%;">

    <?php
    echo $content_body;
    ?>
                    </div>

                </div>

            </div>
        <!-- Le javascript
        ================================================== --> 
        <!-- Placed at the end of the document so the pages load faster --> 
        <script language="javascript">
            var _base_url = '<?php echo base_url(); ?>';
            var _page_url = '<?php echo current_url(); ?>';
        </script>
        <script src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script> 
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
        <!--<script src="<?php// echo base_url(); ?>js/jquery-ui-1.10.4.custom.min.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>           
        <script type="text/javascript" src="<?php echo base_url(); ?>js/hm.js"></script>
        <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<?php
if (isset($external_js_files) && is_array($external_js_files)) {
    foreach ($external_js_files as $external_js_file) {
        echo '<script src="' . $external_js_file . '"></script>' . "\n";
    }
}
if (isset($js_files) && is_array($js_files)) {
    foreach ($js_files as $js_file) {
        echo '<script type="text/javascript" src="' . base_url() . 'js/' . $js_file . '"></script>' . "\n";
    }
}
?>
    </body>
</html>
