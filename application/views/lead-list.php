<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;border-bottom: 0px;}
    .widget-content{background: linear-gradient(135deg, rgb(58,44,112) 0%,rgb(106,0,91) 100%)!important;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
				<form name="zone-wise-search" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>lead" id="edit-profile">
                    <i class="icon-th-list"></i>
                    <h3>Lead Management</h3> 
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" readonly="true" name="schedule_date_from" id="vehicle_date" value="<?php echo $search_date_from;?>" /> 
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" readonly="true" name="schedule_date_to" id="vehicle_date_to" value="<?php echo $search_date_to;?>" /> 
                   <?php 
                   $sdf_ymd=str_replace('/', '-', $search_date_from);$sdf_ymd=date('Y-m-d', strtotime($sdf_ymd));
                   $sdt_ymd=str_replace('/', '-', $search_date_to);$sdt_ymd=date('Y-m-d', strtotime($sdt_ymd));
                   ?>
                    <span style="margin-left:23px; color: #fff;">Lead Type :</span>

                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="leadtype" name="leadtype">
						<option value="">-- Select Lead Type --</option>
						<option value="Hot" <?php if($leadtype == 'Hot'){ echo 'selected'; }; ?>>Hot</option>
						<option value="Warm" <?php if($leadtype == 'Warm'){ echo 'selected'; }; ?>>Warm</option>
						<option value="Cold" <?php if($leadtype == 'Cold'){ echo 'selected'; }; ?>>Cold</option>
					</select>
					
					<span style="margin-left:23px; color: #fff;">Timeline :</span>

                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="timelines" name="timelines">
						<option value="">-- Select Timeline --</option>
						<option value="Within a week" <?php if($timelines == 'Within a week'){ echo 'selected'; }; ?>>Within a week</option>
						<option value="1 Month" <?php if($timelines == '1 Month'){ echo 'selected'; }; ?>>1 Month</option>
						<option value="3 Months" <?php if($timelines == '3 Months'){ echo 'selected'; }; ?>>3 Months</option>
					</select>

                    <span style="margin-left:15px;"></span>
                    
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
					<!--<div class="topiconnew"><a href="javascript:void(0);"><img src="<?php// echo base_url(); ?>images/fax-icon.png" id="printBtnRpt" title="Print"/></a></div>
					<div class="topiconnew"><a href="<?php// echo base_url(); ?>booking/booking_reports_excel/<?php// echo $st_date; ?>/<?php// echo $fr_date; ?>/<?php// echo $zone_id; ?>"><img src="<?php// echo base_url(); ?>images/excel-icon.png" title="Download to Excel"/></a></div>-->
					<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>lead/add"><img src="<?php echo base_url(); ?>img/add.png" title="Add Lead"/></a>
					<?php
					$ldtyp=(strlen($leadtype)>0)?$leadtype:'nil';
					$timelinee=(strlen($timelines)>0)?$timelines:'nil';
					$sourcee=(strlen($source)>0)?$source:'nil';
					$leadstatuss=(strlen($leadstatus)>0)?$leadstatus:'nil';
					$servicee=(strlen($service)>0)?$service:'nil';
					?>
					<div class="topiconnew"><a href="<?php echo base_url().'reports/leadsreporttoExcel/'.$sdf_ymd.'/'.$sdt_ymd.'/'.$ldtyp.'/'.$timelinee.'/'.$sourcee.'/'.$servicee.'/'.$leadstatuss;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;float:right;margin-right: 12%;" id="service" name="service">
						<option value="">-- Select Service --</option>
						<?php
						$serv=$service;
                        if(count($services)>0)
                        {
                           foreach ($services as $service)
                           {
                           ?>
                                   <option value="<?php echo $service->service_type_id;?>" <?php if($service->service_type_id==$serv){echo 'selected';}?>><?php echo $service->service_type_name; ?></option>
                           <?php
                           }
                        }
                        ?>
					</select>
					<span style="margin-left:23px; color: #fff;float:right;">Service : &nbsp;</span>


					<select style="margin-top: 6px; width:160px; margin-bottom: 9px;float:right;" id="source" name="source">
						<option value="">-- Select Source --</option>
						<option value="Facebook" <?php if($source=='Facebook'){echo 'selected';}?>>Facebook</option>
						<option value="Google" <?php if($source=='Google'){echo 'selected';}?>>Google</option>
						<option value="Direct Call" <?php if($source=='Direct Call'){echo 'selected';}?>>Direct Call</option>
						<option value="Flyers" <?php if($source=='Flyers'){echo 'selected';}?>>Flyers</option>
						<option value="Referral" <?php if($source=='Referral'){echo 'selected';}?>>Referral</option>
						<option value="Maid" <?php if($source=='Maid'){echo 'selected';}?>>Maid</option>
						<option value="Schedule visit" <?php if($source=='Schedule visit'){echo 'selected';}?>>Schedule visit</option>
						<option value="Referred by staff" <?php if($source=='Referred by staff'){echo 'selected';}?>>Referred by staff</option>
						<option value="Website" <?php if($source=='Website'){echo 'selected';}?>>Website</option>
						<option value="Justmop" <?php if($source=='Justmop'){echo 'selected';}?>>Justmop</option>
					</select>
					<span style="margin-left:23px; color: #fff;float:right;">Source : &nbsp;</span>
					
					<select style="margin-top: 6px; width:160px; margin-bottom: 9px;float:right;" id="leadstatus" name="leadstatus">
						<option value="">-- Select Status --</option>
						<option value="1" <?php if($leadstatuss=='1'){echo 'selected';}?>>Active</option>
						<option value="0" <?php if($leadstatuss=='0'){echo 'selected';}?>>Inactive</option>
						<option value="2" <?php if($leadstatuss=='2'){echo 'selected';}?>>Already converted</option>
					</select>
					<span style="margin-left:23px; color: #fff;float:right;">Lead Status : &nbsp;</span>


                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Email</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Mobile</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Address </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Quote</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Comments </th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;">Lead Type </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Timeline </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
                        if (count($leads) > 0) 
						{
                            $j = 1;
                            foreach ($leads as $lead) 
							{
								if($lead->status == 1)
								{
									$status = "Active";
								} else {
									$status = "Inactive";
								}
						?> 
							<tr>
								<td style="line-height: 18px; width: 20px; text-align: center;"><?php echo $j; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->customer_name; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->customer_email; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->customer_phone; ?></td>
								<!--<td style="line-height: 18px; text-align: center;"><?php// echo $discounttype; ?></td>-->
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->customer_address; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->service_type_name; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->quote; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->comments; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->lead_type ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $lead->timeline; ?></td>
								<td style="line-height: 18px; text-align: center;"><?php echo $status; ?></td>
								<td style="line-height: 18px; text-align: center;">
									<a class="btn btn-small btn-warning" title="Edit" href="<?php echo base_url() . 'lead/edit/' . $lead->lead_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>
									<a class="btn btn-small btn-info" title="View Lead" href="<?php echo base_url() . 'lead/lead_view/' . $lead->lead_id ?>"><i class="btn-icon-only fa fa-eye" style="font-size: 13px;"> </i></a>
									<a class="btn btn-small btn-danger" title="Delete" href="<?php echo base_url() . 'lead/delete/' . $lead->lead_id ?>"><i class="btn-icon-only fa fa-trash" style="font-size: 13px;" onclick="return confirm('Are you sure.You want to Delete?')"> </i></a>
									<?php
									if($lead->status == 0)
									{
									?>
									<label class="btn btn-small btn-danger" style="width: 23px; height: 23px; pointer-events: none;"></label>
									<?php
									} else if($lead->status == 1 && $lead->lead_status == 0)
									{
									?>
									<label class="btn btn-small btn-warning" style="width: 23px; height: 23px; pointer-events: none;"></label>
									<?php
									} else if($lead->lead_status == 1)
									{
									?>
									<label class="btn btn-small btn-success" style="width: 23px; height: 23px; pointer-events: none;"></label>
									<?php
									} else if($lead->followup_date == "")
									{	
									?>
									<label class="btn btn-small btn-danger" style="width: 23px; height: 23px; pointer-events: none;"></label>
									<?php
									} else if($lead->followup_date != "")
									{
									?>
									<label class="btn btn-small btn-warning" style="width: 23px; height: 23px; pointer-events: none;"></label>
									<?php } ?>
									<?php
									if($lead->lead_status == 0)
									{
									?>
									<a class="btn mm-btn" href="<?php echo base_url(); ?>customer/add?lead=<?php echo $lead->lead_id ?>" style="margin: 5px 15px 5px 0px;">Convert</a>
									<?php
									} else {
									?>
									<a class="btn mm-btn" href="javascript:void(0)" style="margin: 5px 15px 5px 0px;">Already Converted</a>
									<?php
									}
									?>
								</td>
							</tr>
							<?php
							$j++;
							}
						}
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script type="text/javascript">
$('#edit-profile').submit(function(e) {
        var from = document.getElementById("vehicle_date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("vehicle_date_to").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
</script>