<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    <i class="icon-th-list"></i>
                    <h3>One Day Cancel Report</h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
                    <input type="submit" class="btn" value="Go" name="ondeday_report" style="margin-bottom: 4px;">
                    <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="OneDayPrint" title="Print"/></a></div>
                    <div class="topiconnew"><a href="<?php echo base_url();?>reports/one_day_cancel_to_excel/<?php echo isset($search_date) ? date('Y-m-d',strtotime(str_replace('/', '-', $search_date))) : date('Y-m-d'); ?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div>
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php // echo base_url(); ?>img/printer.png" id="OneDayPrint" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Customer Name</th>
                            <th style="line-height: 18px">Customer Mobile</th>
                            <th style="line-height: 18px">Maid Name</th>
                            <th style="line-height: 18px">Shift</th>
                            <th style="line-height: 18px">Added</th>
                            <th style="line-height: 18px">Canceled User</th>
                            <th style="line-height: 18px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $report) {
                                //Payment Type
                                if($report->payment_type == "D")
                                {
                                    $paytype = "(D)";
                                } else if($report->payment_type == "W")
                                {
                                    $paytype = "(W)";
                                } else if($report->payment_type == "M")
                                {
                                    $paytype = "(M)";
                                } else
                                {
                                    $paytype = "";
                                }
                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->mobile_number_1; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->maid_name; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->shift; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->added ? date('d/m/Y H:i:s', strtotime($report->added)) : date('d/m/Y H:i:s', strtotime($report->service_date)); ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $report->user_fullname; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;font-size: 18px;">
                                        <a class="btn btn-small btn-danger" onclick="deleteOnedayCancel(this,<?php echo $report->booking_delete_id; ?>)" href="javascript:void(0)"><i class="btn-icon-only icon-remove"> </i></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="OneDayReportPrint"> 
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Customer Name</th>
                <th style="line-height: 18px">Customer Mobile</th>
                <th style="line-height: 18px">Maid Name</th>
                <th style="line-height: 18px">Shift</th>
                <th style="line-height: 18px">Canceled User</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($reports)) {
                $i = 0;
                foreach ($reports as $report) {
                    //Payment Type
                    if($report->payment_type == "D")
                    {
                        $paytype = "(D)";
                    } else if($report->payment_type == "W")
                    {
                        $paytype = "(W)";
                    } else if($report->payment_type == "M")
                    {
                        $paytype = "(M)";
                    } else
                    {
                        $paytype = "";
                    }
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->mobile_number_1; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->maid_name; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->shift; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->user_fullname; ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>

