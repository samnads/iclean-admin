<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    <i class="icon-th-list"></i>
                    <h3>Maid Vehicle Report</h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
                    
                </form>   
                <?php 
                $s_date = str_replace('/', '-', $search_date);
                $s_date = date('Y-m-d', strtotime($s_date));
                ?>
                <div class="topiconnew" style="position: absolute;right: 0px;z-index: 1;top: 0;">
                    <a href="<?php echo base_url();?>reports/vehicleattendance_report_toexcel/<?php echo $s_date;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a>
                </div>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead class="new-table-boxes">
                        <tr>
                                                    
                            
                            <?php
                            
                            foreach($zones as $zone)
                            {
                                echo '<th style="line-height: 18px">' . $zone->zone_name . '</th>';
                            }
                            ?>
                        </tr>
                    </thead>
                    <?php
                    //foreach($maids as $maid)
                    //{
                        
                        //echo '<td style="line-height: 18px">' . $maid->maid_name . '</td>';
                        $i = 0;
                        foreach ($reports as $rpt)
                        {
                            echo '<tr>';
                            foreach($zones as $zone)
                            {
                                echo '<td style="line-height: 18px"> ';
                                if(isset($rpt[$zone->zone_id]['maid_name']))
                                {
                                    $color = $rpt[$zone->zone_id]['attendance_status'] == 'IN' ? 'green' : 'red';
                                    
                                    echo $rpt[$zone->zone_id]['maid_name'] 
                                            . ' (<span style="color:'. $color . '">' . 
                                            $rpt[$zone->zone_id]['attendance_status']  
                                            . '</span>)';
                                    
                                }
                                
                                
                                echo '</td>';
                            }
                            echo '</tr>';
                            ++$i;
                          
                        }
                       
                    //}
                    ?>
                    <tbody>
                        
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
