<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    <i class="icon-th-list"></i>
                    <h3>Incentives Report</h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="search_date_to" name="search_date_to" value="<?php echo isset($search_date_to) ? $search_date_to : date('d/m/Y'); ?>">
                    <select style="margin-top: 5px; width:180px; margin-bottom: 9px;" id="slct_incentive_for" name="slct_incentive_for" >
                        <option value="" <?php if($incentive_for==''||$incentive_for==NULL){echo 'selected';}?>>---Select Incentive For---</option>
                        <option value="0" <?php if($incentive_for=='0'){echo 'selected';}?>>Crew</option>
                        <option value="1" <?php if($incentive_for=='1'){echo 'selected';}?>>Aggregator</option>
                    </select>
                    <input type="submit" class="btn" value="Go" name="incentives_report" style="margin-bottom: 4px;">
                    <!-- <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="OneDayPrint" title="Print"/></a></div> -->
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php // echo base_url(); ?>img/printer.png" id="OneDayPrint" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Customer Name</th>
                            <th style="line-height: 18px">Mobile</th>
                            <th style="line-height: 18px">Customer Address</th>
                            <th style="line-height: 18px">Incentive Amount</th>
                            <th style="line-height: 18px">Incentive Type</th>
                            <th style="line-height: 18px">Incentive For</th>
                            <th style="line-height: 18px">Incentive User</th>
                            <th style="line-height: 18px">Added Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($incentives)) {
                            $i = 0;
                            foreach ($incentives as $inc) {
                                //Payment Type
                                if($inc->incentive_for == "0")
                                {
                                    $incentive_for = "Crew";
                                } else 
                                {
                                    $incentive_for = "Aggregator";
                                } 


                                if($inc->incentive_type == "F")
                                {
                                    $incentive_type = "Flat";
                                } else 
                                {
                                    $incentive_type = "Percentage";
                                } 


                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $inc->incentive_cust_name.$inc->incentive_id; ?> 
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $inc->incentive_mobile_number_1; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $inc->incentive_cust_address; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $inc->incentive_amount; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $incentive_type; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $incentive_for; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo $inc->incentive_user_name; ?>
                                    </td>
                                    <td style="line-height: 18px">
                                        <?php echo date("d/m/Y",strtotime($inc->incentive_added_datetime))."<br/>".date("g:i a",strtotime($inc->incentive_added_datetime)); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="OneDayReportPrint"> 
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Customer Name</th>
                <th style="line-height: 18px">Maid Name</th>
                <th style="line-height: 18px">Shift</th>
                <th style="line-height: 18px">Canceled User</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($reports)) {
                $i = 0;
                foreach ($reports as $report) {
                    //Payment Type
                    if($report->payment_type == "D")
                    {
                        $paytype = "(D)";
                    } else if($report->payment_type == "W")
                    {
                        $paytype = "(W)";
                    } else if($report->payment_type == "M")
                    {
                        $paytype = "(M)";
                    } else
                    {
                        $paytype = "";
                    }
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->customer_name; ?> <?php echo $paytype; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->maid_name; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->shift; ?>
                        </td>
                        <td style="line-height: 18px">
                            <?php echo $report->user_fullname; ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">

$('#edit-profile').submit(function(e) {
        var from = document.getElementById("OneDayDate").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("search_date_to").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
            // return false;
        }
    else{
   //alert("Valid date Range");
    }
    
     });

// $('#search_date_to').on('change', function(){
//              var startDate = $('#OneDayDate').val();
//              var endDate = $('#search_date_to').val();
           
//              if (endDate < startDate){
//                  alert("End date should be greater than Start date.");
//                $('#search_date_to').val('');
//              }
//          });

// $("#OneDayDate").datepicker({
//             dateFormat: 'dd/mm/yy',
//             onSelect: function(date) {
//                 $("#search_date_to").datepicker('option', 'minDate', date);
//             }
//         });
// $("#search_date_to").datepicker({ dateFormat: 'dd/mm/yy' });
    //   $('#edit-profile').submit(function(e) {
    //     var from =  $("#OneDayDate").val();
    //     var too = $("#search_date_to").val();
    //     alert(Date.parse(from ));
    //          alert(Date.parse(too));
    //     if(Date.parse(from) > Date.parse(to)){
    //      alert("Invalid Date Range");
    //     }
    // else{
   //alert("Valid date Range");
    // }
   
    
    //  });

    //  $('#search_date_too').on('change', function(){
    //          var startDate = $('#OneDayDate').val();
    //          var endDate = $('#search_date_to').val();
    //          if (Date.parse(endDate) < Date.parse(startDate)){
    //              alert("End date should be greater than Start date.");
    //            // $('#end_date').val('');
    //          }
    //      });

        
     </script>

