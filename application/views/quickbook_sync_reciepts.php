<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data" style="display:inline;">
                    <i class="icon-th-list"></i>
                    <h3>Quick Sync Customer Reciept </h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" placeholder="From Date" value="<?php if($search_date_from != NULL){ echo $search_date_from; } ?>">
					
					
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
                </form> 
                
                <form method="POST" action="<?php echo base_url();?>quickbook/quickbook_sync_reciept_process" style="display:inline;float: right;">
                    <?php $invoiceids=array();
                        foreach ($invoice_report as $inv)
                        {
                            $invoiceids[]=$inv->invoice_id;
                        }
                    ?>
                    <input type="hidden" name="invoice_ids" value="<?php echo implode(",",$invoiceids); ?>">
                    <button style="background: transparent;border:0px;" type="submit">
                    <img style="height: 40px; width:250px; margin: 0 auto;" src="<?php echo base_url();?>images/C2QB_white_btn.png">
                    </button>
                </form>
                    
                    
                    <!-- Statement ends -->
                
            </div>

            <div class="widget-content" style="margin-bottom:30px">
            	<?php 
                        $qb_sync_msg=$qb_sync_msg->tmp_msg_content;
                        //print_r($qb_sync_msg);
                        if(strlen($qb_sync_msg)>5){
                            $qb_sync_msg=json_decode($qb_sync_msg);
                ?>
                <div class="alert alert-success text-center">
                  <strong><?php echo $qb_sync_msg;?></strong>
                </div>
                <?php } ?>
                <div class="text-center">
                	<h2><?php echo $invoice_count;?> invoice(s) remaining for sync.</h2>
                </div>
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Number</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Date</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Amount</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount Due</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$grand_paid = 0;
					$grand_due = 0;
					$i = 1;
                    foreach ($invoice_report as $inv){
						$sub_t += round($inv->invoice_net_amount,2);
						$grand_paid += round($inv->received_amount,2);
						$grand_due += round($inv->balance_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_num; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->customer_name; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_due_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_net_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->received_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->balance_amount; ?></td>
							<!--<td style="line-height: 18px;"></td>-->
							<td style="line-height: 18px">
								<?php
								if($inv->invoice_paid_status == 2)
								{
									$paystat = " (Partially Paid)";
								} else {
									$paystat = "";
								}
								if($inv->invoice_status == '0')
								{
									$invstatus = "Draft";
								} else if($inv->invoice_status == '1')
								{
									$invstatus = "Open";
								} else  if($inv->invoice_status == '2'){
									$invstatus = "Cancelled";
								} else  if($inv->invoice_status == '3'){
									$invstatus = "Paid";
								}
								echo $invstatus.$paystat;
								?>
							</td>
							<td style="line-height: 18px">
								<a class="btn btn-small btn-info" href="<?php echo base_url(); ?>invoice/view_invoice/<?php echo $inv->invoice_id; ?>" title="View Invoice">
									<i class="btn-icon-only fa fa-eye "> </i>
								</a>
								<a class="btn btn-small btn-info" href="<?php echo base_url(); ?>invoice/generateinvoice/<?php echo $inv->invoice_id; ?>" target="_blank" title="Download Invoice">
									<i class="btn-icon-only fa fa-download "> </i>
								</a>
								<div class="btn btn-small btn-info send_inv_mail" title="Download Invoice" data-invoiceid="<?php echo $inv->invoice_id; ?>" data-invoiceemail="<?php echo $inv->email_address; ?>">
									<i class="btn-icon-only fa fa-envelope" ></i>
									<i class="fa fa-spinner fa-spin" style="display:none;"></i>
								</div>
							</td>
						</tr>
					<?php
					$i++;	
					}
					?>
                    </tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td><?php echo $grand_paid; ?></td>
							<td><?php echo $grand_due; ?></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<button type="button" class="btn btn-info btn-lg openmodal hidden" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body popmsg">
        <div class="alert alert-success">
		  <strong>Success!</strong> Indicates a successful or positive action.
		</div>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>



<button type="button" class="btn btn-info btn-lg openemailmodal hidden" data-toggle="modal" data-target="#emailModal">Open Modal</button>

<!-- Modal -->
<div id="emailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
      	  <div id="emailpopmsg"></div>
          <div class="form-group">
		    <label for="email_content">Email Content:</label>
		    <textarea class="form-control" id="email_content"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="email">Email:</label>
		    <input type="email" class="form-control" id="email">
		    <input type="hidden" class="form-control" id="hidinvid">
		  </div>
		  
		  <button type="submit" class="btn btn-default emailsubbtn">Submit</button>
		  <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>



<script type="text/javascript">
$( document ).ready(function() {


	$(".send_inv_mail").click(function() {
		var invoiceid=$(this).data('invoiceid');
		var emailid=$(this).data('invoiceemail');
		$("#email").val(emailid);$("#hidinvid").val(invoiceid);
		$("#emailpopmsg").html('');
		$("#emailpopmsg").html('');
		$(".openemailmodal").click();
	});	


    $(".emailsubbtn").click(function() {
    	$("#emailpopmsg").html('');
	    var invoiceid=$("#hidinvid").val();
	    var invoicecontent=$("#email_content").val();
	    var invoiceemail=$("#email").val();
	    //console.log(invoiceid);
	    if(invoicecontent=='')
	    {
	    	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please enter email content!</strong></div>');
	    	return false;
	    }validateEmail

	    if(!validateEmail(invoiceemail))
	    {
	    	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check the email entered!</strong></div>');
	    	return false;
	    }



	    $(this).hide();
	    $(".emailloadbtn").show();
	    $.ajax({
                   url: "<?php echo base_url();?>invoice/send_invoice_email",
                   type: "post",
                   data: {invoiceid:invoiceid,invoicecontent:invoicecontent,invoiceemail:invoiceemail} ,
                   success: function (response) {
                        $(".emailsubbtn").show();
	    				$(".emailloadbtn").hide();
                        if(response=='success')
                        {
                        	$("#emailpopmsg").html('<div class="alert alert-success text-center"><strong>Invoice email sent successfully!</strong></div>');
                        	$("#email_content,#email").val('')
                        	//$(".openmodal").click(); 
                        }
                    	else if(response=='email_error')
                        {
                        	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check customer email!</strong></div>');
                        	//$(".openmodal").click();
                        }
                    	else
                    	{
                    		$("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                    		//$(".openmodal").click();
                    	}

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".send_inv_mail[data-invoiceid='"+invoiceid+"'] .fa-envelope").show();
                        $(".send_inv_mail[data-invoiceid='"+invoiceid+"'] .fa-spin").hide();
                        $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                    	//$(".openmodal").click();

                   }
               });
	});
});	

function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
</script>
