<div class="row">
        
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Services</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_services();"><img src="<?php echo base_url();?>img/add.png" title="Add Services"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Services </th>
                            <th style="line-height: 18px"> Rate</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($services) > 0) {
                            $i = 1;
                            foreach ($services as $services_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $services_val['service_type_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $services_val['service_rate'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_services(<?php echo $services_val['service_type_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if(user_authenticate() == 1) {?>
								<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_services(<?php echo $services_val['service_type_id']  ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php } ?>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
    <div class="span6" id="add_services" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Add Service Types</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_services();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Service Type</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="service_type_name" name="service_type_name" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="service_rate" name="service_rate" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="services_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    <div class="span6" id="edit_services" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit Services</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_services();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Service Type</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_type_name" name="edit_service_type_name" required="required">
                                                <input type="hidden" class="span3" id="edit_service_type_id" name="edit_service_type_id">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_rate" name="edit_service_rate" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="services_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>