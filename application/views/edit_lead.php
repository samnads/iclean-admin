<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Edit Lead</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" title="Lead List" href="<?php echo base_url(); ?>lead"><i class="icon-th-list"></i></a>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Lead Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                        
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                <?php echo $errors; ?>   
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Lead Updated Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                
                                
                                <fieldset>            
                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="customername">Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
															<input type="text" class="span3" id="customername" name="customername" value="<?php echo $lead_details->customer_name; ?>" required>
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

													<div class="control-group">											
														<label class="control-label" for="customeremail">Email&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="customeremail" name="customeremail" value="<?php echo $lead_details->customer_email; ?>" required>
                                                            <?php echo form_error('customeremail'); ?>
                                                        </div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
														<label class="control-label" for="customermobile">Phone&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="customermobile" name="customermobile" value="<?php echo $lead_details->customer_phone; ?>" required>
                                                            <?php echo form_error('customermobile'); ?>
                                                        </div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
														<label class="control-label" for="contactdate">Date of contact&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="contactdate" name="contactdate" readonly="readonly" value="<?php echo date('d/m/Y',strtotime($lead_details->contacted_date)); ?>" required>
														</div> <!-- /controls -->				
													</div> <!-- /control-group -->
													<?php
													if($lead_details->followup_date != "")
													{
														$followupdate =  date('d/m/Y',strtotime($lead_details->followup_date));
													} else {
														$followupdate = "";
													}
													
													if($lead_details->next_follow_update != "")
													{
														$nextfollowupdate =  date('d/m/Y',strtotime($lead_details->next_follow_update));
													} else {
														$nextfollowupdate = "";
													}
													?>
													<div class="control-group">											
														<label class="control-label" for="followupdate">Followup Date&nbsp;</label>
														<div class="controls">
															<input type="text" class="span3" id="followupdate" name="followupdate" readonly="readonly" value="<?php echo $followupdate; ?>">
														</div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
                                                        <label class="control-label" for="followupcomment">Followup Comment&nbsp;</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="3" id="followupcomment" name="followupcomment"><?php echo $lead_details->followup_comment; ?></textarea>	
                                                        </div> <!-- /controls -->				
                                                    </div>
													
													<div class="control-group">											
														<label class="control-label" for="nextfollowupdate">Next Followup&nbsp;</label>
														<div class="controls">
															<input type="text" class="span3" id="nextfollowupdate" name="nextfollowupdate" readonly="readonly" value="<?php echo $nextfollowupdate; ?>">
														</div> <!-- /controls -->				
													</div>
													
													<div class="control-group">											
                                                        <label class="control-label" for="customeraddress">Address&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="3" id="customeraddress" name="customeraddress" required><?php echo $lead_details->customer_address; ?></textarea>	
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->




                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
												<div class="control-group">
                                                    <label class="control-label" for="servicetype">Service Type&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="servicetype" id="servicetype" class="span3" required >
															<option value="">Select Service</option>
                                                            <?php
                                                            if(count($services)>0)
                                                            {
                                                               foreach ($services as $service)
                                                               {
																   if($service->service_type_id == $lead_details->service_type)
																	{
																		$selected = 'selected="selected"';
																	} else {
																		$selected = '';
																	}
                                                               ?>
                                                                       <option value="<?php echo $service->service_type_id;?>" <?php echo $selected; ?>><?php echo $service->service_type_name; ?></option>
                                                               <?php
                                                               }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
												<div class="control-group">
                                                    <label class="control-label" for="customersource">Source&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="customersource" id="customersource" class="span3" required >
                                                            <option value="">Select Option</option>
															<option value="Facebook" <?php if($lead_details->source == 'Facebook'){ echo 'selected'; }; ?>>Facebook</option>
															<option value="Google" <?php if($lead_details->source == 'Google'){ echo 'selected'; }; ?>>Google</option>
															<option value="Direct Call" <?php if($lead_details->source == 'Direct Call'){ echo 'selected'; }; ?>>Direct Call</option>
															<option value="Flyers" <?php if($lead_details->source == 'Flyers'){ echo 'selected'; }; ?>>Flyers</option>
															<option value="Referral" <?php if($lead_details->source == 'Referral'){ echo 'selected'; }; ?>>Referral</option>
															<option value="Maid" <?php if($lead_details->source == 'Maid'){ echo 'selected'; }; ?>>Maid</option>
															<option value="Schedule visit" <?php if($lead_details->source == 'Schedule visit'){ echo 'selected'; }; ?>>Schedule visit</option>
															<option value="Referred by staff" <?php if($lead_details->source == 'Referred by staff'){ echo 'selected'; }; ?>>Referred by staff</option>
															<option value="Website" <?php if($lead_details->source == 'Website'){ echo 'selected'; }; ?>>Website</option>
															<option value="Justmop" <?php if($lead_details->source == 'Justmop'){ echo 'selected'; }; ?>>Justmop</option>
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="control-group">
                                                    <label class="control-label" for="leadtypes">Classification of lead&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="leadtypes" id="leadtypes" class="span3" required >
                                                            <option value="">Select type</option>
															<option value="Hot" <?php if($lead_details->lead_type == 'Hot'){ echo 'selected'; }; ?>>Hot</option>
															<option value="Warm" <?php if($lead_details->lead_type == 'Warm'){ echo 'selected'; }; ?>>Warm</option>
															<option value="Cold" <?php if($lead_details->lead_type == 'Cold'){ echo 'selected'; }; ?>>Cold</option>
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="control-group">
                                                    <label class="control-label" for="servicetimeline">Timeline of service&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="servicetimeline" id="servicetimeline" class="span3" required >
                                                            <option value="">Select timeline</option>
															<option value="Within a week" <?php if($lead_details->timeline == 'Within a week'){ echo 'selected'; }; ?>>Within a week</option>
															<option value="1 Month" <?php if($lead_details->timeline == '1 Month'){ echo 'selected'; }; ?>>1 Month</option>
															<option value="3 Months" <?php if($lead_details->timeline == '3 Months'){ echo 'selected'; }; ?>>3 Months</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">                                         
                                                        <label class="control-label" for="quote">Quote&nbsp;</label>
                                                        <div class="controls">
                                                            <input type="number" class="span3" id="quote" name="quote" value="<?php echo $lead_details->quote;?>">  
                                                        </div> <!-- /controls -->               
                                                </div> <!-- /control-group -->
												<div class="control-group">                                         
                                                        <label class="control-label" for="customeraddress">Comments&nbsp;</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="3" id="comments" name="comments"><?php echo $lead_details->comments;?></textarea>   
                                                        </div> <!-- /controls -->               
                                                </div> <!-- /control-group -->
												<div class="control-group">											
													<label class="control-label">Status&nbsp;<font style="color: #C00">*</font></label>
													<div class="controls">
														<label class="radio inline">
															<input type="radio" name="status" value="1" <?php echo isset($lead_details->status) ? ($lead_details->status == "1" ? 'checked="checked"' : '') : '' ?>> Active
														</label>
														<label class="radio inline">
															<input type="radio" name="status" value="0" <?php echo isset($lead_details->status) ? ($lead_details->status == "0" ? 'checked="checked"' : '') : '' ?>> Inactive
														</label>
													</div>	<!-- /controls -->			
												</div> <!-- /control-group -->
												
												<div class="control-group">											
													<label class="control-label">Followup Status&nbsp;<font style="color: #C00">*</font></label>
													<div class="controls">
														<?php
														if($lead_details->status == 0)
														{
														?>
														<label class="btn btn-small btn-danger" style="width: 20px; height: 20px; pointer-events: none;"></label>
														<?php
														} else if($lead_details->status == 1 && $lead_details->lead_status == 0)
														{
														?>
														<label class="btn btn-small btn-warning" style="width: 23px; height: 23px; pointer-events: none;"></label>
														<?php
														} else if($lead_details->lead_status == 1)
														{
														?>
														<label class="btn btn-small btn-success" style="width: 20px; height: 20px; pointer-events: none;"></label>
														<?php
														} else if($lead_details->followup_date == "")
														{	
														?>
														<label class="btn btn-small btn-danger" style="width: 20px; height: 20px; pointer-events: none;"></label>
														<?php
														} else if($lead_details->followup_date != "")
														{
														?>
														<label class="btn btn-small btn-warning" style="width: 20px; height: 20px; pointer-events: none;"></label>
														<?php } ?>
														
													</div>	<!-- /controls -->			
												</div>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>   
                                    <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="lead_sub">
                                    
                                    
                                </div> 
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->