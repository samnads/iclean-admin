<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>Customer List </h3>                   
				<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="customer_date" name="customer_date" placeholder="From Date" value="">
				<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="customer_date_to" name="customer_date_to" placeholder="To Date" value="" onchange="myFunction()">
				<div class="mm-drop">
					<select style="margin-left : 10px;width:80px;" id="all-customers">
						<option value="">All</option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</div>
				<div class="mm-drop">
					<select style="width: 120px; margin-right: 10px;" id="payment_type">
						<option value="" selected="selected">All Pay Type</option>
						<option value="D">Daily</option>
						<option value="W">Weekly</option>
						<option value="M">Monthly</option>
					</select>
				</div>
				<div class="mm-drop">    
					<select style="width: 90px;" id="sort_source">
						<option value="">-Select Source-</option>          
						<option value="Facebook">Facebook</option>
						<option value="Google">Google</option>
						<option value="Yahoo">Yahoo</option>
						<option value="Bing">Bing</option>
						<option value="Direct Call">Direct Call</option>
						<option value="CrystalBlu">CrystalBlu</option>
						<option value="Flyers">Flyers</option>
						<option value="Referral">Referral</option>
						<option value="Watchman/Security Guard">Watchman/Security Guard</option>
						<option value="Maid">Maid</option>
						<option value="Driver">Driver</option>
						<option value="By email">By email</option>
						<option value="Schedule visit">Schedule visit</option>
						<option value="Website">Website</option>
						<option value="Referred by staff">Referred by staff</option>
						<option value="Referred by customer">Referred by customer</option>
						<option value="Justmop">Justmop</option>
						<option value="Matic">Matic</option>
						<option value="ServiceMarket">ServiceMarket</option>
						<option value="MYHOME">MYHOME</option>
					</select>
					<select style="width: 90px;" id="sort_cust_type">
						<option value="">-Select-</option>          
						<option value="1">Regular</option>
						<option value="0">Non-Regular</option>
					</select>
					<select style="width: 90px;" id="servicetypes">
						<option value="">-Select Service-</option>
						<?php
						foreach($services as $servval)
						{
						?>
						<option value="<?php echo $servval->service_type_id; ?>"><?php echo $servval->service_type_name; ?></option>
						<?php
						}
						?>
					</select>
				</div>
				<input type="text" style="margin-left :8px;width:115px;" id="keyword-search" placeholder="Search">
				<div class="topiconnew"><a href="<?php echo base_url(); ?>customer/add"><img src="<?php echo base_url(); ?>images/user-plus-icon.png" title="Add Customer"/></a></div>
				<div class="topiconnew"><a href="<?php echo base_url(); ?>customer/toExcel/<?php echo $active; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Export Customer"/></a></div>
			</div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="newCustomerList" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Mobile</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Area</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Address</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Source</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Last Job</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Added Time</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
					
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script type="text/javascript">
 //$(document).ready(function(){
//  $('#customer_date_to').on('change', function(){
// 	$('#customer_date_to').change(function(){
// 	alert('God is Love');
//              var startDate = $('#customer_date').val();
//              var endDate = $('#customer_date_to').val();
//              if (endDate < startDate){
//                 // alert("End date should be greater than Start date.");
//                  alert("Invalid Date Range");
//                $('#customer_date_to').val('');
//              }
//          });
//          });

// function myFunction() {
// 	var x = document.getElementById("customer_date");
// 	alert(x);
// }

</script>