<style>
    .finished {
        width: 50px;
        height: auto;
        background: url(<?php echo base_url(); ?>images/complete.png) no-repeat left top / cover;
        display: block;
        margin: 0 auto;
        height: 30px;
        width: 30px;
    }

    .not-started {
        width: 50px;
        height: auto;
        background: url(<?php echo base_url(); ?>images/not-start.png) no-repeat left top / cover;
        display: block;
        margin: 0 auto;
        height: 30px;
        width: 30px;
    }

    .on-going {
        width: 50px;
        height: auto;
        background: url(<?php echo base_url(); ?>images/on-going.png) no-repeat left top / cover;
        display: block;
        margin: 0 auto;
        height: 30px;
        width: 30px;
    }

    .cancelled {
        width: 50px;
        height: auto;
        background: url(<?php echo base_url(); ?>images/cancel.png) no-repeat left top / cover;
        display: block;
        margin: 0 auto;
        height: 30px;
        width: 30px;
    }

    .booking-fl-box {
        width: 96% !important;
    }

    .verify-payment {
        background: #68025c none repeat scroll 0 0;
        border-radius: 5px;
        color: #fff;
        cursor: pointer;
        padding: 5px 5px;
        margin-top: 5px;
        text-decoration: none;
    }
</style>

<div class="row">

    <div class="span12" style="width: 96% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>Activity</h3>

                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">
                    <input type="hidden" id="formatted-date" value="<?php echo $formatted_date ?>" />
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">&nbsp;&nbsp;&nbsp;
                    <input type="text" id="planInput" placeholder="Search">
                    <!-- <a id="synch-to-odoo-new" href="#" style="cursor: pointer;" class="btn">Synchronize</a> -->
                    <a href="<?php echo base_url(); ?>activity/payment_details/<?= $formatted_date ?>" style="cursor: pointer;" class="btn">Export Payment Details</a>
                    <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url() . 'reports/activity_summary_view/' . $formatted_date; ?>" target="_blank"><img src="<?php echo base_url(); ?>img/printer.png" /></a>
                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 10px;"> No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Mobile Number</th>
                            <th style="line-height: 18px;"> Working Hours</th>
                            <th style="line-height: 18px;"> M.O.P</th>
                            <th style="line-height: 18px;"> Billed Amount</th>
                            <th style="line-height: 18px;"> Collected Amount</th>
                            <th style="line-height: 18px;"> Company<br />Collected Amount</th>
                            <th style="line-height: 18px;"> Balance Amount</th>
                            <th style="line-height: 18px;"> Receipt No</th>

                            <th style="line-height: 18px;"> Status</th>
                            <th style="line-height: 18px; width: 123px;"> Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($dayservices)) {
                            $i = 0;
                            $zone_id = 0;
                            $total_fee = 0;
                            $material_fee = 0;
                            $collected_total_fee = 0;
                            $total_wrk_hrs = 0;
                            $collected_amount = 0;
                            $c_collected_amount = 0;
                            $ztotal_wrk_hrs = 0;
                            foreach ($dayservices as $service) {
                                $check_buk_exist = $this->bookings_model->get_booking_exist($service->booking_id, $formatted_date);
                                if (!empty($check_buk_exist)) {
                                    $mop = $check_buk_exist->mop;
                                    //$ref = $check_buk_exist->just_mop_ref;
                                } else {
                                    if ($service->pay_by != "") {
                                        $mop = $service->pay_by;
                                    } else {
                                        $mop = $service->payment_mode;
                                    }
                                    //$ref = "";
                                }
                                //Payment Type
                                if ($service->payment_type == "D") {
                                    $paytype = "(D)";
                                } else if ($service->payment_type == "W") {
                                    $paytype = "(W)";
                                } else if ($service->payment_type == "M") {
                                    $paytype = "(M)";
                                } else {
                                    $paytype = "";
                                }


                                //                                $service_status = $service->service_status == 1 ? 'ON GOING' : ($service->service_status == 2 ? 'FINISHED' : ($service->service_status == 3 ? 'CANCELLED' : 'NOT STARTED'));                                
                                $service_status = $service->service_status == 1 ? '' : ($service->service_status == 2 ? '' : ($service->service_status == 3 ? '' : ''));
                                $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
                                $title = $service->service_status == 1 ? 'On-Going' : ($service->service_status == 2 ? 'Finished' : ($service->service_status == 3 ? 'Cancelled' : 'Not-Started'));



                                //$service_status .=  $service->payment_status == 1 ? (' ' . $service->total_fee) : ($service->payment_status == 0  && $service->service_status == 2 ? ' NP': '');

                                $activity_status = 0;
                                //$activity_status = $service->service_status ? $service->service_status : 0;                                                                
                                $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));
                                $activity_status =  $service->payment_status == 1 ? 5 : ($service->payment_status == 0  && $service->service_status == 2 ? 6 : ($service->service_status == 1 && $service->payment_status != NULL  ? 1 : ($service->service_status == 3 ? 3 : 0)));
                                $service->total_fee = $service->service_status == 2 ? $service->total_fee : '';
                                $service->material_fee = $service->service_status == 2 ? $service->material_fee : '';
                                // if($service->t_zone != "")
                                // {
                                // $service->zone_id = $service->t_zone;
                                // } else {
                                // $service->zone_id = $service->zone_id;
                                // }
                                $transfer_text = (strlen($service->transferred_zone) > 0) ? "<br><b> - Transferred Zone - " . $service->transferred_zone . "</b>" : "";
                                if ($zone_id != $service->zone_id) {
                                    if ($ztotal_wrk_hrs != 0) {

                                        echo '<tr>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '</tr>';
                                        $ztotal_wrk_hrs = 0;
                                    }

                                    $zone_id = $service->zone_id;
                                    echo '<tr>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->zone_name . '</b></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '</tr>';
                                }
                                if ($service->total_fee == "") {
                                    $tot_hrs = ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);

                                    if ($service->cleaning_material == 'Y') {
                                        $materialfee = ($tot_hrs * 5);
                                        $materialview = '(' . $materialfee . ')';
                                    } else {
                                        $materialview = "";
                                    }
                                    //$servicetotalfee = (($service->price_hourly)*((strtotime($service->end_time) - strtotime($service->start_time))/ 3600));total_amount
                                    $servicetotalfee = $service->total_amount;
                                } else {
                                    $servicetotalfee = $service->total_fee;

                                    if ($service->cleaning_material == 'Y') {
                                        $materialfee = $service->material_fee;;
                                        $materialview = '(' . $materialfee . ')';
                                    } else {
                                        $materialview = "";
                                    }
                                }
                                //$total_fee += $service->total_fee;
                                $total_fee += $servicetotalfee;
                                $material_fee += $service->material_fee;
                                $collected_total_fee += $service->collected_amount;
                                $collected_amount += ($service->payment_status == 1 ? $service->total_fee : 0);
                                $total_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);
                                $ztotal_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);
                                //$total_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                //$ztotal_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                $verify_text = '';
                                $company_collected_amount = '';
                                if ($service->verified_status == '0') {
                                    $verify_text = '<br/><div class="verify-payment text-center" data-paymntid="' . $service->payment_id . '"  data-mpid="' . $service->mp_id . '" data-bukid="' . $service->booking_id . '" data-vamt="' . $service->collected_amount . '" title="Verify Payment" id="verifpay' . $service->mp_id . '">Verify Payment</div>';
                                } else if ($service->verified_status == '1') {
                                    //$verify_text='<br/><div class="text-center text-success">Verified</div>';
                                }

                                if ($service->verified_status == '1') {
                                    $c_collected_amount += $service->collected_amount;
                                    $company_collected_amount = $service->collected_amount;
                                }

                                echo '<tr class="dayserveview">'
                                    . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . ++$i . '</td>'
                                    . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . $service->maid_name . '</td>'
                                    . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . $service->customer_name . '(' . $service->odoo_customer_id . ') <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) ' . $paytype . $transfer_text . '</td>'
                                    . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . $service->mobile_number_1 .'</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600) . ']' . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $mop . '</td>'
                                    //. '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->total_fee . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $servicetotalfee . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . ' clct_amt_' . $service->booking_id . '">' . $service->collected_amount . $verify_text . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . ' c_clct_amt_' . $service->booking_id . '">' . $company_collected_amount . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $service->balance . $service->signed . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $service->ps_no . '</td>';
                                if ($service->ps_no != "") {
                                    $pinkslip_no = $service->ps_no;
                                } else {
                                    $pinkslip_no = "0";
                                }
                                if ($service->collected_amount != "") {

                                    echo '<td style="line-height: 18px;"><a title=' . $title . ' href="javascript:void(0)" data-bind="' . $service->collected_amount . '" style="text-decoration:none;" onclick="get_activity_new(' . $activity_status . ', ' . $service->booking_id . ', this, ' . $service->collected_amount . ', ' . $service->service_status . ',' . $service->payment_status . ',' . $pinkslip_no . ');"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
                                } else {
                                    //  $today= date('d/m/Y');
                                    //  $check=$service_date;

                                    //  if ( $today >=  $check ) {
                                    echo '<td style="line-height: 18px;"><a  title=' . $title . '  href="javascript:void(0)" data-bind="' . $service->collected_amount . '" style="text-decoration:none;" onclick="get_activity(' . $activity_status . ', ' . $service->booking_id . ', this);"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
                                    //    }
                                    //   else if($today > $check){
                                    //     print_r($today ); 
                                    //   echo $check; die;
                                    //   echo '<td style="line-height: 18px;"><a  title='.$title.'  href="javascript:void(0)" data-bind="'.$service->collected_amount.'" style="text-decoration:none;" onclick="get_activity(' . $activity_status . ', ' . $service->booking_id . ', this);"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
                                    //   }


                                    //   else{
                                    //     //echo 'wrong';die;
                                    //     echo '<td style="line-height: 18px;"></td>';
                                    //   }
                                }
                                echo '<td style="line-height: 18px;" class="">';
                                if ($service->service_status == 2 && $service->invoice_status == 0 && $service->payment_type != "M") {
                                    echo '<div class="invoicebtshow">'
                                        . '<div class="invoice-create" id="invbtnshide-' . $service->booking_id . '" data-id="' . $service->service_id . '" data-paytype="' . $service->payment_type . '" title="Create Invoice">Create Invoice</div>'
                                        . '<div class="" style="display: none; color: #68025c; font-weight: bold; text-decoration: none;" id="invbtns-' . $service->booking_id . '">Invoiced</div>'
                                        . '</div>';
                                } else if ($service->service_status == 2 && $service->invoice_status == 1) {
                                    echo '<div class="invoicebtshow">'
                                        . '<div class="" style="color: #68025c; font-weight: bold; text-decoration: none;">Invoiced</div>'
                                        . '</div>';
                                } else {
                                    echo '<div class="invoicebtshow">'
                                        . '<div class="invoice-create" style="display: none;" id="invbtnshide-' . $service->booking_id . '" data-id="' . $service->service_id . '" data-paytype="' . $service->payment_type . '" title="Create Invoice">Create Invoice</div>'
                                        . '<div class="" style="display: none; color: #68025c; font-weight: bold; text-decoration: none;" id="invbtns-' . $service->booking_id . '">Invoiced</div>'
                                        . '</div>';
                                }
                                echo '<div>'
                                    . '<div class="invoiced-' . $service->booking_id . '" style="color: #68025c; font-weight: bold; text-decoration: none;display: none;">Invoiced</div>'
                                    . '</div>';
                                /*if($service->service_status == 2)
								  {
                                  $dayserid=(is_numeric($service->service_id))?$service->service_id:'0';
									  echo '<div class="complaintbtnshow">'
									  . '<div class="complaint-create" id="compbtnshide-'.$service->booking_id.'" data-id="'.$service->service_id.'" title="Create Complaint" onclick="showcomplaintpopup('.$service->booking_id.','.$dayserid.',\''.$formatted_date.'\','.$service->customer_id.')">Add Complaint</div>'
									  . '</div>';
								  }*/
                                echo '</td></tr>';
                            }
                            echo '<tr style="font-weight:bold;">'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px;">TOTALS</td>'
                                . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px; text-align : right;">' . number_format($total_fee, 2) . '(' . number_format($material_fee, 2) . ')</td>'
                                . '<td style="line-height: 18px; text-align : right;">' . number_format($collected_total_fee, 2) . '</td>'
                                . '<td style="line-height: 18px; text-align : right;">' . number_format($c_collected_amount, 2) . '</td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '<td style="line-height: 18px; text-align : right;">' . /*number_format($collected_amount, 2) . */ '</td>'
                                . '<td style="line-height: 18px;"></td>'
                                . '</tr>';
                        } else {
                        ?>
                            <tr>
                                <td colspan="10" style="line-height: 18px; text-align:justify;">No records found</td>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>


<div id="activity-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 style="color:white; padding-bottom: 0px !important;">Activity</h3>
    </div>
    <div class="modal-body">
        <p>
            <a class="btn btn-medium btn-success" id="start" title="Start" href="javascript:void(0)"><i class="icon-play"> </i>START</a>
            <a class="btn btn-medium btn-primary" style="background-color: #7D7D7D !important;" id="restart" title="Restart" href="javascript:void(0)"><i class="icon-play"> </i>RESTART</a>
            <a class="btn btn-medium btn-warning" title="Stop" href="javascript:void(0)"><i class="icon-stop"> </i>STOP</a>
            <a class="btn btn-medium btn-info" title="Transfer" href="javascript:void(0)"><i class="icon-share"> </i>TRANSFER</a>
            <!--<a class="btn btn-medium btn-danger" title="Cancel" href="javascript:void" ><i class="icon-ban-circle"> </i>CANCEL</a>-->
        </p>

        <div class="controls" id="payment-details">
            <label class="radio inline">
                <input id="paymnt" type="radio" name="payment_type" value="1"> Payment
            </label>

            <label class="radio inline">
                <input id="no-paymnt" type="radio" name="payment_type" value="0"> Payment Not Received
            </label>
            <label class="radio inline">
                <input id="no-service" type="radio" name="payment_type" value="3"> Service Not Done
            </label>
        </div>
        <div style="padding: 15px 0;" id="pinkid">
            <label class="inline" style="display: inline-block;">Amount
                <input class="inline" type="text" id="paid-amount" style="width: 50px;" />
            </label>
            <label class="inline" style="display: inline-block; padding-left: 15px;">Receipt No
                <input type="text" id="ps-no" style="width: 100px;" />
            </label>
        </div>

        <p id="frm-transfer">
            <!--<form id="frm-transfer">-->
            <select id="transfer-zone-id">
                <option value="">Select Driver</option>
                <?php
                foreach ($tablets as $val) {
                    echo '<option value="' . $val->tablet_id . '">' . $val->tablet_driver_name . '</option>';
                }

                ?>
            </select>
            <!--</form>-->
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a class="save-but" style="text-decoration: none;" href="javascript:void(0)" onclick="add_activity();">Submit</a>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg opencomplaintmodal hidden" data-toggle="modal" data-target="#complaintModal">Open Modal</button>

<!-- Modal -->
<div id="complaintModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div id="emailpopmsg"></div>

                <div class="form-group">
                    <label for="email">Complaint Category:</label>
                    <select class="form-control" id="complaint_category">
                        <option value="">---Select Category---</option>
                        <?php foreach ($complaint_categories as $cat) { ?>
                            <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name']; ?></option>
                        <?php  } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="email_content">Complaint Description:</label>
                    <textarea class="form-control" id="complaint_description"></textarea>
                </div>

                <input type="hidden" name="complaint_service_date" id="complaint_service_date">
                <input type="hidden" name="complaint_day_service_id" id="complaint_day_service_id">
                <input type="hidden" name="complaint_booking_id" id="complaint_booking_id">
                <input type="hidden" name="complaint_customer_id" id="complaint_customer_id">
                <button type="submit" class="btn btn-default emailsubbtn" id="submit_complaint">Submit</button>
                <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin"></i></button>
            </div>
            <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
        </div>

    </div>
</div>