<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Customer Payments </h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" placeholder="From Date" autocomplete="off" value="<?php if($search_date_from != NULL){ echo $search_date_from; } ?>">
					<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" placeholder="To Date" autocomplete="off" value="<?php if($search_date_to != NULL){ echo $search_date_to; } ?>">
					
                    <select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="0">-- Select Customer --</option>
						
						<?php
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
                    </select>
					<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="company_vh_rep_new" name="company_vh_rep">
						<option value="">-- Select Company --</option>
						<?php
						$company_list=['Justmop','Matic','ServiceMarket','Helpling','Rizek','Ifsg','Urban Clap','MYHOME'];
						foreach($company_list as $c_val)
						{
							if($c_val == $company_vh_rep)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val; ?>" <?php echo $selected; ?>><?php echo $c_val ?></option>
						<?php
						}
						?>  
                    </select>
					<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="source" name="source">
						<option value="">-- Select Source --</option>
						<?php
						$company_list=[
							"Justmop",
							"Matic",
							"ServiceMarket",
							"Helpling",
							"Rizek",
							"Ifsg",
							"Urban",
							"MYHOME",
							"Facebook",
							"Google",
							"Yahoo",
							"Bing",
							"Direct Call",
							"Flyers",
							"Referral",
							"Watchman/Security Guard",
							"Maid",
							"Driver",
							"By email",
							"Schedule visit",
							"Website",
							"Referred by staff",
							"Referred by customer",
						];
						foreach($company_list as $c_val)
						{
							if($c_val == $source)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val; ?>" <?php echo $selected; ?>><?php echo $c_val ?></option>
						<?php
						}

						$source=$source==''?'nil':$source;
						$source=str_replace(' ','%20',$source);
						?>  
                    </select>
					<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="type_vh_rep" name="type_vh_rep">
						<option value="">-- Select Type --</option>
						<?php
						$type=[['id'=>'22','label'=>'Cash'],['id'=>'1','label'=>'Card'],['id'=>'2','label'=>'Cheque'],['id'=>'3','label'=>'Bank']];
						foreach($type as $c_val)
						{
							if($c_val['id'] === $type_vh_rep)
							{
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val['id']; ?>" <?php echo $selected; ?>><?php echo $c_val['label']; ?></option>
						<?php
						}
						?>  
                    </select>
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
					<a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php echo base_url(); ?>customer/add_payment">
						<img src="<?php echo base_url(); ?>img/add.png" title="Add Payment">
					</a>
					<div class="topiconnew">
					<a href="<?php echo base_url(); ?>reports/customerpaymentreporttoExcel/<?=$search_date_from!=''?implode('-',explode('/',$search_date_from)):'nil'?>/<?=$search_date_to!=''?implode('-',explode('/',$search_date_to)):'nil'?>/<?=$customer_id!=''?$customer_id:'nil'?>/<?=$company_vh_rep!=''?str_replace(' ','%20',$company_vh_rep):'nil'?>/<?=$type_vh_rep!=''?$type_vh_rep:'nil'?>/<?=$source?>">
						<img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel">
					</a>
				</div>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Mobile</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Memo</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Balance Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$i = 1;
                    foreach ($payment_report as $pay){
						$sub_t += round($pay->paid_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($pay->paid_datetime)); ?> </td>
							<td style="line-height: 18px;"><?php echo $pay->customer_name; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->mobile_number_1; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft Payment";
								} else {
									echo $pay->payment_reference;
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->ps_no; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_method == 0)
								{
									echo "Cash";
								} else if($pay->payment_method == 1){
									echo "Card";
								}
								else if($pay->payment_method == 2){
									echo "Cheque";
								}
								else if($pay->payment_method == 3){
									echo "Bank";
								}
								?>
							</td>
							<td style="line-height: 18px;"><?php echo $pay->paid_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->allocated_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $pay->balance_amount; ?></td>
							<td style="line-height: 18px;">
								<?php
								if($pay->payment_status == 0)
								{
									echo "Draft";
								} else {
									echo "Posted";
								}
								?>
							</td>
							<td style="line-height: 18px">
								<?php if($pay->verified_status == 1){?>
								<a class="btn btn-small btn-info" href="<?php echo base_url(); ?>customer/view_payment/<?php echo $pay->payment_id; ?>" title="View Payment">
									<i class="btn-icon-only fa fa-eye "> </i>
								</a>
								<?php }else{?>
									Payment not<br/>verified
								<?php }?>
							</td>
						</tr>
					<?php
					$i++;	
					}
					?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script type="text/javascript">
$('#edit-profile').submit(function(e) {
        var from = document.getElementById("vehicle_date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("vehicle_date_to").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
			 window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });

</script>
