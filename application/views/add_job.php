<script type="text/javascript">
$('.flexdatalist-json').flexdatalist({
     searchContain: false,
     textProperty: '{capital}, {name}, {continent}',
     valueProperty: 'iso2',
     minLength: 1,
     focusFirstResult: true,
     selectionRequired: true,
     groupBy: 'continent',
     visibleProperties: ["name","continent","capital","capital_timezone"],
     searchIn: ["name","continent","capital"],
     data: 'countries.json'
});
</script>
<style>
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #eae8e8;
    border-radius: 3px;
    cursor: pointer;
    font-size: 14px;
    height: 36px;
    line-height: 30px;
   padding: 3px 10px 3px 25px;

    text-indent: 0.01px;
	}
        
        .text-field-main div.time-box .select2-container a.select2-choice { background: transparent; overflow: hidden;} 
        
        .text-field-details { padding: 10px;}
        .text-field-details p { line-height:  20px; padding-bottom: 5px;}
        
        
        
        
        .job-customer{width: 100% !important;}
        #job-b-error{
            color: #f00;
            font-weight: bold;
        }
        
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                <div class="widget-header"> 
                    <!--<form class="form-horizontal" method="POST" action="<?php// echo base_url(); ?>activity/jobs">-->
                        <i class="icon-th-list"></i>
                        <h3>New Job</h3> 
<!--                                <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php// echo $service_date ?>">                       
                            <input type="hidden" id="formatted-date-job" value="<?php// echo $formatted_date ?>"/>
                        <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;"> -->
                        <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                        <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                        <!--<a class="newjobbutton" id="newjobbutton" href="<?php// echo base_url(); ?>activity/addjob"><i class="fa fa-plus"></i> New Job</a>-->
                    <!--</form>-->

                </div>
            </div>
    </div>
    <div class="row job-field-wrapper no-left-right-margin">
      <div class="col-md-12 col-sm-12 job-field-main box-center">
      
          <form name="job_add_form" id="job_add_form">
     
     <h2 class="text-center">Job Details</h2>
     
     
     
                 
     <div class="col-md-6 col-sm-12  newjobbox" style="height:auto;">
                <div class="text-field-main">
                    <p> Customer :</p>
                    <select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="">
                        <option></option>
                        <?php                           
                        // foreach($customers as $customer)
                        // {  
                            // if($customer->payment_type == "D")
                            // {
                                // $paytype = "(Daily)";
                            // } else if($customer->payment_type == "W")
                            // {
                                // $paytype = "(Weekly)";
                            // } else if($customer->payment_type == "M")
                            // {
                                // $paytype = "(Monthly)";
                            // } else
                            // {
                                // $paytype = "";
                            // } 
                            // $p_number = array();
                            // if($customer->phone_number != NULL)
                                // array_push ($p_number, $customer->phone_number);
                            // if($customer->mobile_number_1 != NULL)
                                // array_push ($p_number, $customer->mobile_number_1);
                            // if($customer->mobile_number_2 != NULL)
                                // array_push ($p_number, $customer->mobile_number_2);
                            // if(@$customer->mobile_number_3 != NULL)
                                // array_push ($p_number, @$customer->mobile_number_3);

                            // $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';

                            // echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
                        // }
                        ?>
                    </select>
                    <div class="clear"></div>
                </div>
                  
                  <div class="text-field-details" id="selected-address" style="display:none;">
                      <p><b>Selected Address</b><br>
                          <i class="fa fa-home"></i> &nbsp; <span id="job-picked-address">uiiujioujo joijoioojdsfsdfdsa  sdfs</span></p>
                  <div class="clear"></div>
                  </div>
                  <div id="job-customer-address-panel">
                            <div class="head">Pick One Address <span class="close">Close</span></div>

                            <div class="inner">
                                    Loading<span class="dots_loader"></span>
                            </div>			
                    </div>
              </div><!--booking-form-field end-->
              
              
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Select Maid:</p>                                    
                                    
                               
                    <input type='text'
           placeholder='Select Maids'
           class='flexdatalist form-control'
           data-min-length='1'
           data-searchContain='true'
           multiple='multiple'
           list='skills'
           data-value-property='value'
           name='skill' id='jobmaidval'>
           
                    <datalist id="skills">
                        <?php
                        foreach($maids as $maid)
                        {
                            if(!in_array($maid->maid_id, $leave_maid_ids)) // Excluding maids on leave
                              {
                        ?>
                            <option value="<?php echo $maid->maid_id; ?>"><?php echo $maid->maid_name; ?></option>

                        <?php } 
                        } ?>
    </datalist>           
                               
                           
                 <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->   
              
              
              
              
              
                                                
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Service Type :</p>
                    <select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="job-customer">
                        <option></option>
                        <?php
                        foreach($service_types as $service_type)
                        {
                            $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                            echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
                        }
                        ?>
                    </select>
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->                                 
                                        
                                        
              
              

              
              
              
              
               <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding">
                   <div class="text-field-main">
                               <p>Cleaning Materials :</p>
                            
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                     <input type="radio" value="Y" id="b-cleaning-materials-1" name="cleaning_materials" class="">
                                    <label for="b-cleaning-materials"> <span></span> Yes</label>
                                </div><!--week-date end-->
                                
                                
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                    <input type="radio" value="N" id="b-cleaning-materials-2" name="cleaning_materials" class="">
      <label for="b-cleaning-materials-2"> <span></span> No</label>
                                </div><!--week-date end-->
                                
                               
                            <div class="clear"></div><!--clear end-->
                            </div>
                            </div><!--week-type-box end-->
                            
                            
                            
              
                                                
              <div class="col-md-6 col-sm-12  newjobbox">
                
                <div class="text-field-main">
                              <p>Time:</p>
                    
                              
                              <div class="time-box">
                        
                        	<div class="col-md-12 col-sm-12 no-left-right-padding">
                            	<div class="col-md-6 col-sm-12 center-opt no-left-right-padding">
                                    <select name="time_from" id="b-from-time" data-placeholder="From" class="job-customer fromtime">
                                        <option></option>
                                        <?php
                                        foreach($times as $time_index=>$time)
                                        {
                                                echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
<!--                                        <option value="">09:00am</option> 
                                        <option value="">09:30am</option> 
                                        <option value="">10:00am</option> 
                                        <option value="">10:30am</option> 
                                        <option value="">11:00am</option> -->
                                    </select>
                                </div><!--time-box end-->
                                <div class="col-md-6 col-sm-12 center-opt no-right-padding">
                                    <!--<select id="abc2"  name="bc2" class="totime">-->
                                    <select name="time_to" id="b-to-time" data-placeholder="To" class="totime job-customer">
                                        <option></option>
                                        <?php
                                        foreach($times as $time_index=>$time)
                                        {
                                            if($time_index == 't-0')
                                            {                                                                                
                                                    continue;
                                            }

                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
<!--                                        <option value="">09:00am</option> 
                                        <option value="">09:30am</option> 
                                        <option value="">10:00am</option> 
                                        <option value="">10:30am</option> 
                                        <option value="">11:00am</option> -->
                                    </select>
                                </div><!--time-box end-->
                                <div class="clear"></div>
                            </div><!--time-box end-->
                        
                        </div><!--time-box end-->
                          </div>
                
              </div><!--booking-form-field end--> 
              
              
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Repeats :</p>
<!--                    <select  name="bc">
                        <option value="1">One Day</option> 
                        <option value="2">Weekly</option> 
                    </select>-->
                    <select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="job-customer">
                            <option></option>
                            <option value="OD">Never Repeat - One Day Only</option>
                            <option value="WE">Every Week</option>
                            <!--
                            <option value="BW">Every Other Week</option>
                            -->
                    </select>
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->
              
              
              
              
              
              
              
              <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding" id="jobweekday" style="display:none;">
              <div class="text-field-main">
               <p>Repeat On :</p>
                            
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="0" name="w_day[]" id="repeat-on-0" <?php if($day_number == 0) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-0"> <span></span> Sun</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="1" name="w_day[]" id="repeat-on-1" <?php if($day_number == 1) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-1"> <span></span> Mon</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="2" name="w_day[]" id="repeat-on-2" <?php if($day_number == 2) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-2"> <span></span> Tue</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="3" name="w_day[]" id="repeat-on-3" <?php if($day_number == 3) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-3"> <span></span> Wed</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="4" name="w_day[]" id="repeat-on-4" <?php if($day_number == 4) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-4"> <span></span> Thu</label>
              </div><!--week-date end-->
              
<!--              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="5" name="w_day[]" id="repeat-on-5" class="" type="checkbox">
                  <label for="repeat-on-5"> <span></span> Fri</label>
              </div>week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="6" name="w_day[]" id="repeat-on-6" <?php if($day_number == 6) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-6"> <span></span> Sat</label>
              </div><!--week-date end-->
              
                                          
          <div class="clear"></div><!--clear end-->
              </div>
          </div><!--week-type-box end-->
          
          <div class="col-md-6 col-sm-12  newjobbox" id="jobneverweekday" style="display:none;">
                <div class="col-md-6 text-field-main week-type-box no-left-right-padding">
                    <p>End :</p>
                    <div class="col-md-6 no-left-right-padding">
                        <input value="never" id="job-repeat-end-never" name="repeat_end" class="" type="radio" checked="checked">
                        <label for="job-repeat-end-never"> <span></span> Never</label>
                    </div>
                    <div class="col-md-6 no-left-right-padding">
                        <span class="pull-right">
                            <input value="ondate" id="job-repeat-end-ondate" name="repeat_end" class="" type="radio">
                        <label for="job-repeat-end-ondate"> <span></span> On</label>
                        </span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="col-md-6 text-field-main no-right-padding">
                    <p>&nbsp;</p>
                    <input class="text-field end_datepicker" id="job-repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" type="text">
                    <div class="clear"></div>
                </div>
            </div>
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Contact Number :</p>
                    <input name="jobcontact" class="text-field" type="text" placeholder="Contact Number" id="job-contact" style="pointer-events: none;">
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->
                                                

              
              
              <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding">
              <div class="text-field-main">
                               <p>Lock Booking :</p>
                            
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                     <input type="radio" value="1" id="lock-booking-1" name="lock_booking" class="">
                                     <label for="lock-booking-1"> <span></span> Yes</label>
                                </div><!--week-date end-->
                                
                                
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                    <input type="radio" value="0" id="lock-booking-2" name="lock_booking" class="">
                                    <label for="lock-booking-2"> <span></span> No</label>
                                </div><!--week-date end-->
                                
                               
                            <div class="clear"></div><!--clear end-->
              </div>
                            </div><!--week-type-box end-->
                            
                            
                            
                            
                                            
              
              
              
            
                          <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding">
              <div class="text-field-main">
                               <p>Notification</p>
                            
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                    <input value="" name="notification_email_booking" id="job-notification-email-booking" class="" type="checkbox">
                                    <label for="job-notification-email-booking"> <span></span> Email</label>
                                </div><!--week-date end-->
                                
                                
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                    <input value="" name="notification_sms_booking" id="job-notification-sms-booking" class="" type="checkbox">
                                    <label for="job-notification-sms-booking"> <span></span> SMS</label>
                                </div><!--week-date end-->
                                
                               
                            <div class="clear"></div><!--clear end-->
              </div>
                            </div><!--week-type-box end-->
                            
                            
                            
                            
                            
                            
                            
                             
              
              
              
              
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Email ID:</p>
                    <input name="jobemail" id="jobemail" class="text-field" type="text" style="pointer-events: none;">
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Total Amount:</p>
                    <input name="n_totamt" id="n_totamt" class="text-field" type="text">
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->
              
              
              

              
                      
                           
                <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main pop-main-button">
                    <p>&nbsp;</p>                                    
                                    
                    <input type="hidden" id="job-customer-address-id" name="job-customer-address-id" />             
                  <input class="save-but" id="save-booking" value="Save" type="button"><span id="job-b-error"></span>
                               
                           
                 <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->              
                           
                           
                           
                           
                           
                           
                                    
     <br>
<br>
<br>

</form>
     
      </div><!--job-field-main end--> 
  </div><!--row job-field-wrapper end--> 
</section>
