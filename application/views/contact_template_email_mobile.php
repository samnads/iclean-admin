<html>
	<head>
	</head>
	<body>
		<div class="main-wrapper" style="width: 800px; margin:0 auto;">
			<div style="width: 800px;"><img src="<?php echo base_url(); ?>images/iclean-banner.jpg" width="800" height="232" alt="" /></div>
			<div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc; width: 798px;">
				<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
					<div style="width: 600px; height:auto; padding: 0px 0px 00px 0px; margin-bottom: 10px;">
						<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear Admin,</p>
						<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">New Enquiry Received...</p>
						
					</div>
					<div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
					  <table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
						<tr style="background: #FFF;">
						  <td width="298"><p style="border-top:1px solid #e5e2e2;font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Customer Name</p></td>
						  <td width="4">:</td>
						  <td width="298"><p style="border-top:1px solid #e5e2e2;font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $name; ?></p></td>
					  
						</tr>
						
						<tr style="  ">
						  <td><p style="border-top:1px solid #e5e2e2;font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Mobile</p></td>
						  <td style="border-top:1px solid #e5e2e2;">:</td>
						  <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $mobile; ?></p></td>
						</tr>
						
						
						<tr style="background: #FFF;">
						  <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Email</p></td>
						  <td style="border-top:1px solid #e5e2e2;">:</td>
						  <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $email; ?></p></td>
						</tr>
						
						
						<tr>
						  <td colspan="3">
						  <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px;">Message</p>
						  
						  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px;"><?php echo $message; ?></p></td>
						</tr>
					  </table>

				 </div>
					<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
						  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
						  <strong>iClean Cleaning Services</strong><br />
						  Road 31, Bldg. 1007/A. Block 635,<br />
							Ma'ameer,<br />
							Sitra, Kingdom of Bahrain<br />
							For Bookings : +973 1770 0211-135 / +973 6639 6076<br />
							Email : info@iclean.bh</p>
					</div>
     
     

     
     </div>
     
<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> iClean All Rights Reserved.</p>
     </div>

</div>
	 
	 </div>
	</body>
</html>