<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
				<form name="zone-wise-search" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>booking/booking_reports" id="edit-profile">
                    <i class="icon-th-list"></i>
                    <h3>Maid Performance Report</h3> 
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" readonly="true" name="schedule_date_from" id="vehicle_date" value="<?php echo $schedule_date_from?>" /> 
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" readonly="true" name="schedule_date_to" id="vehicle_date_to" value="<?php echo $schedule_date_to?>" /> 
                   
					<span style="margin-left:15px;color:#FFF;">Zone :</span>

                    <select style="margin-top: 6px; width:140px; margin-bottom: 9px;" id="zones" name="zones">
						<option value="">-- Select Zone --</option>
						<?php
						if (count($zones) > 0) {
							foreach ($zones as $zones_val) {
								?>
							<option value="<?php echo $zones_val->zone_id; ?>" <?php if($zone_id == $zones_val->zone_id){ echo 'selected="selected"'; } else { echo ''; } ?> ><?php echo $zones_val->zone_name; ?></option>
								<?php
							}
						}
						?>
					</select>

                    <span style="margin-left:15px;"></span>
                    <select style="margin-top: 6px; width:140px; margin-bottom: 9px;" id="maids" name="maids">
						<option value="">-- Select Maid --</option>
						<?php
						if (count($maids) > 0) {
							foreach ($maids as $maid) {
								?>
							<option value="<?php echo $maid->maid_id; ?>" <?php if($maid_id == $maid->maid_id){ echo 'selected="selected"'; } else { echo ''; } ?> ><?php echo $maid->maid_name; ?></option>
								<?php
							}
						}
						?>
					</select>
					<select style="margin-top: 6px; width:140px; margin-bottom: 9px;" id="maids_status" name="maids_status">
						<option value="">--Maid Status --</option>
						<?php
						$status=(object) [['id'=>'1','label'=>'Active'],['id'=>'0','label'=>'InActive']];
						if (count($status) > 0) {
							foreach ($status as $stat) {

								$stat = (object) $stat
								?>
							<option value="<?php echo $stat->id; ?>" <?php if($statuss === $stat->id){ echo 'selected="selected"'; } else { echo ''; } ?> ><?php echo $stat->label; ?></option>
								<?php
							}
						}
						?>
					</select>
					<span style="margin-left:15px;"></span>
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">


					<div class="topiconnew" sty><a href="<?php echo base_url();?>reports/bookingreporttoExcel/<?php echo date("Y-m-d",strtotime(str_replace('/', '-',$schedule_date_from)));?>/<?php echo date("Y-m-d",strtotime(str_replace('/', '-',$schedule_date_to)));?>/<?php echo $zone_id;?>/<?php echo $maid_id;?>?status=<?php echo $statuss;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div>
                </form>   

            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Cleaner</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Cleaner Status</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total Timings</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Client Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Phone </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Payment<br/>Mode </th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Booking Type</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Client Location</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Source</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Timings</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total <br>Hrs</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Zone</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Driver</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Payment</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Pay Mode</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
				$sln = 1;$subtotal=0;
				foreach($performance_report as $performance)
					{
                                            foreach ($performance as $key)
                                                    {
                                                    	
							?>
							<tr>
								<td style="line-height: 18px;"><?php echo $sln++; ?></td>
								<td style="line-height: 18px;">
                                                                    <?php //echo html_escape($key->maid_name); ?>
                                                                    <?php
									$maid_names = explode(',', $key->maid_names);
									foreach ($maid_names as $maid_name) {
										// output each hobby and decorate/separate them however you'd like
										echo $maid_name . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
                                </td>
								<td style="line-height: 18px;">
								<?php //echo html_escape($key->maid_name); ?>
                                                                    <?php
									$maid_status = explode(',', $key->maid_status);
									foreach ($maid_status as $status) {
										// output each hobby and decorate/separate them however you'd like
										echo $status==1?'Active':'Inactive';
										echo'<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
                                </td>
								<td style="line-height: 18px;">
									<?php
									$customers = explode(',', $key->customer_names);
									foreach ($customers as $customer) {
										// output each hobby and decorate/separate them however you'd like
										echo $customer . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
                                <td style="line-height: 18px;">
									<?php
                                                                        
                                    $customers = explode(',', $key->customer_names);
									foreach ($customers as $customer) {
										// output each hobby and decorate/separate them however you'd like
										echo $key->service_start_date . '<br />';
									}
                                                                        
									
									//echo html_escape($performance->customer_name);
									?>
								</td>
                                <td style="line-height: 18px;">
									<?php
									$mobile_numbers = explode(',', $key->mobile_number);
									foreach ($mobile_numbers as $mobile_number) {
										// output each hobby and decorate/separate them however you'd like
										echo $mobile_number . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
								<td style="line-height: 18px;">
									<?php
									$payment_modes = explode(',', $key->payment_modes);
									foreach ($payment_modes as $payment_mode) {
										// output each hobby and decorate/separate them however you'd like
										echo $payment_mode . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
                                <td style="line-height: 18px;">
									<?php
									$booking_typ = explode(',', $key->booking_type);
									foreach ($booking_typ as $b_type) {
										// output each hobby and decorate/separate them however you'd like
                                                                                if($b_type == "OD")
                                                                                    {
                                                                                        $booktype = "One Time";
                                                                                    }else if($b_type == "WE")
                                                                                    {
                                                                                        $booktype = "Weekly";
                                                                                    }else if($b_type == "BW")
                                                                                    {
                                                                                        $booktype = "Bi-Weekly";
                                                                                    }
                                                                                    else {
                                                                                        $booktype = "";
                                                                                    }
										echo $booktype . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
								<td style="line-height: 18px;">
									<?php
									$sources = explode(',', $key->sources);
									foreach ($sources as $source) {
										// output each hobby and decorate/separate them however you'd like
										echo $source . '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
								<td style="line-height: 18px;">
									<?php
									$shifts = explode(',', $key->time_from);
									$shifts2 = explode(',', $key->time_to);
									$i = 0;$time_diff=0;
									foreach ($shifts as $shift) {
										// output each hobby and decorate/separate them however you'd like
										echo $shift . ' - ' . $shifts2[$i]. '<br />';
										$time_diff+=strtotime($shifts2[$i]) - strtotime($shift);
										$i++;
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
								<td style="line-height: 18px;text-align: center;">
									<?php
									$time_diffs = explode(',', $key->time_diffs);
									echo array_sum($time_diffs);
									//echo html_escape($performance->customer_name);
									?>
								</td>
								<td style="line-height: 18px;">
									<?php
									$drivers = explode(',', $key->drivers);
									foreach ($drivers as $driver) {
										// output each hobby and decorate/separate them however you'd like
										echo $driver. '<br />';
									}
									//echo html_escape($performance->customer_name);
									?>
								</td>
								
								<td style="line-height: 18px;">
									<?php
									$totamt = explode(',', $key->totamt);
                                    $total=0;
                                    $total=array_sum($totamt);
                                    $subtotal+=$total;
									foreach ($totamt as $totam) {
										// output each hobby and decorate/separate them however you'd like
										echo $totam. '<br />';
									}
									//echo html_escape($performance->customer_name);
                                                                        echo "<br /><span style='font-style: oblique;'>Total - ".$total."</span>";
									?>
								</td>
							<?php
								
								
							}
								
						
						}
                       
                        ?>



                    </tbody>
                    <tfoot>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
                    	<td></td>
						<td></td>
                    	<td></td>
                    	<td style="text-align: right;">SubTotal</td>
                    	<td><?php echo $subtotal;?></td>
                    </tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script type="text/javascript">

$('#edit-profile').submit(function(e) {
        var from = document.getElementById("vehicle_date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("vehicle_date_to").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
			 window.location.reload();
            // return false;
        }
    else{
   //alert("Valid date Range");
    }
    
     });
</script>