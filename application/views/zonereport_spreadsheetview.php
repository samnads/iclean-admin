<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Employee</th>
            <th> Booking 1</th>
            <th> Booking 2</th>
            <th> Booking 3</th>
            <th> Booking 4</th>
            <th> Booking 5</th>
            <th> Total [Hrs]</th>
        </tr>
    </thead>
    <tbody>
    <?php
                        //echo "<pre>";
                        //print_r($zone_report);

                        $tot = 0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {
                                $displayed_bookids=array();
                                $slno=1;
                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
                                    ?>
                                    <tr>
                                        <td><center><?php echo $slno;?></center></td>
                                        <td><?php echo $maids_val['maid_name'] ?> </td>
                                        <td>
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
                                                    if(!in_array($bookingid, $displayed_bookids))
                                                    {
                                                        $total_hrs_served+=$time_diff;
                                                        $time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                        
                                                        array_push($displayed_bookids,$bookingid);
                                                        break;
                                                    }
                                                
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td>

                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
                                                    if(!in_array($bookingid, $displayed_bookids))
                                                    {
                                                        $total_hrs_served+=$time_diff;
                                                        $time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                        
                                                        array_push($displayed_bookids,$bookingid);
                                                        break;
                                                    }
                                                
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>

                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
                                                    if(!in_array($bookingid, $displayed_bookids))
                                                    {
                                                        $total_hrs_served+=$time_diff;
                                                        $time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                        
                                                        array_push($displayed_bookids,$bookingid);
                                                        break;
                                                    }
                                                
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                                
                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
                                                    if(!in_array($bookingid, $displayed_bookids))
                                                    {
                                                        $total_hrs_served+=$time_diff;
                                                        $time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                        
                                                        array_push($displayed_bookids,$bookingid);
                                                        break;
                                                    }
                                                
                                                }
                                            }
                                            ?>
                                            
                                        </td>


                                        <td>
                                            
                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
                                                    if(!in_array($bookingid, $displayed_bookids))
                                                    {
                                                        $total_hrs_served+=$time_diff;
                                                        $time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                        
                                                        array_push($displayed_bookids,$bookingid);
                                                        break;
                                                    }
                                                
                                                }
                                            }
                                            ?>

                                        </td>                                     
                                        

                                        <td><center>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
                                                
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?></center>
                                        </td>
                                    </tr>

                                            <?php
                                        $slno++;
                                        }
                                    }
                                } else {

                                    if (count($maids) != 0) {
                                        foreach ($maids as $maids_val) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 18px; width: 200px">&nbsp;</td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"><span style="line-height: 18px; width: 200px"><?php echo $maids_val['maid_name'] ?></span></td>
                                        
                                    </tr>

            <?php
        }
    }
}
?>
    </tbody>
    <tfoot>
      <tr>
           <td></td>
           <td>&nbsp;</td>
           <td></td>
           <td></td>
           <td></td>
           <td></td>
           <td><center><strong>Total</strong></center></td>
           <td align="center"><center><strong><?php echo $tot; ?></strong></center></td>
       </tr>
    </tfoot>
</table>