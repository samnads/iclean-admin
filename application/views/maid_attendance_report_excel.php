<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No.</th>
            <th> Maid</th>
            <th> Zone</th>
            <th> Maid In</th>
            <th> Maid Out</th>
            <th> Attendance</th>
        </tr>
    </thead>
    <tbody>
        <?php
                        if(!empty($attendance_report))
                        {
                            $i = 0;
                            foreach ($attendance_report as $attendance)
                            {
                                if($attendance->attandence_status == 1)
                                {
                                    $stat = "Maid In";
                                } else if($attendance->attandence_status == 2)
                                {
                                    $stat = "Maid Out";
                                } else {
                                    $stat ="";
                                }
                                $maid_in_time = date('h:i:s a', strtotime($attendance->maid_in_time));
                                if($attendance->maid_out_time != "00:00:00")
                                {
                                    $maid_out_time = date('h:i:s a', strtotime($attendance->maid_out_time));
                                } else {
                                    $maid_out_time = "";
                                }
                                
                                echo '<tr>'
                                        . '<td>' . ++$i . '</td>'
                                        . '<td>' . $attendance->maid_name . '</td>'
                                        . '<td>' . $attendance->zone_name . '</td>'
                                        . '<td>' . $maid_in_time . '</td>'
                                        . '<td>' . $maid_out_time . '</td>'
                                        . '<td>' . $stat . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            echo '<tr><td colspan="6">No Results!</td></tr>';
                        }
                        ?>
    </tbody>
</table>
