<style>
#divToPrint p {
	margin:0px;
	padding:0px;	
}
.ptable td{
	padding:0px;
	margin:0px;	
}
.widget .widget-header{margin-bottom: 0px;}
#da-ex-datatable-numberpaging_wrapper table.dataTable.no-footer{border-bottom: none !important; }
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Invoice</h3>                   
                        <?php if($post_invoice_date!=""){ ?>
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="invoice_date" name="invoice_date" value="<?php echo $post_invoice_date ?>">
                        <?php } else {?> 
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="invoice_date" name="invoice_date" value="<?php echo date('d/m/Y') ?>">
                        <?php } ?>

                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day" value="">
                    
                    <input type="submit" class="btn" value="Go" name="invoice_report" style="margin-bottom: 4px;">
                    <div class="topiconnew"><a style="cursor:pointer;"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="printinvoice" title="Print"/></a></div>
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php// echo base_url(); ?>img/printer.png" id="printinvoice" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:0px">
                <div style="width: 100%; /*width:1180px;*/">
<!--                <table id="da-datatable" class="table da-table" cellspacing="0" width="100%">-->
<!--                <div class="da-panel collapsible scrollable">-->
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    
                    
                    
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> <center>SI No</center></th>
                            <th style="line-height: 18px"> <center>Maid</center></th>
                            <th style="line-height: 18px"> <center>Time</center></th>
                            <th style="line-height: 18px"> <center>Customer</center></th>
                            <th style="line-height: 18px"> <center>Address</center></th>
                            <th style="line-height: 18px"> <center>Service date</center></th>
                            <th style="line-height: 18px"> <center>Amount</center></th>
                            <th style="line-height: 18px"> <center>Paid Amount</center></th>
                            <th style="line-height: 30px"> <center>Balance</center></th>
                        </tr>
                    </thead>
                    <tbody>
                   
                    <?php 
                    if($invoice_report){
                  
                    $i=1;
                    $total_amount_paid = 0;
                    
                    foreach($invoice_report as $report) { 
                        $total_amount_paid+= $report->received_amount;
                        ?>
                         <tr>
                          <td style="text-align:center"><?php echo $i ?></td>    
                          <td style="text-align:center"><?php echo $report->maid_name; ?></td>
                          <td style="text-align:center"><?php echo $report->time_from." - ".$report->time_to; ?></td>
                          <td style="text-align:center"><?php echo $report->customer_name; ?></td>
                          <td style="text-align:center"><?php echo $report->customer_address; ?></td>
                          <td style="text-align:center"><?php echo $report->service_date; ?></td>
                          <td style="text-align:center"><?php echo $report->billed_amount; ?></td>
                          <td style="text-align:center"><?php echo $report->received_amount; ?></td>
                          <td style="text-align:center"><?php echo $report->balance_amount; ?></td>
<!--                          <td style="text-align:center"><a href="javascript:void(0)" id="invoice_view">View</td>-->
                         </tr>

                    <?php 
                    $i++;
                    }
                    ?>
                         <tr>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td style="text-align:center"><b>Total</b></td>
                             <td style="text-align:center"><?php echo $total_amount_paid;  ?></td>
                             <td>&nbsp;</td>
                         </tr>     
                   <?php
                    }
                    ?>

                    </tbody>

                    
                </table>
    </div><!-- /widget-content --> 
  </div><!-- /widget --> 
</div><!-- /span12 --> 
</div> 
</div>    
 <div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                  
                    <thead>
                        <tr>
                            <th> Sl No.</th>
                            <th> Maid</th>
                            <th> Time</th>
                            <th> Customer</th>
                            <th> Address</th>
                            <th> Service date</th>
                            <th> Amount</th>
                            <th> Paid Amount</th>
                            <th> Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($invoice_report){
                  
                    $i=1;
                    $total_amount_paid = 0;
                    
                    foreach($invoice_report as $report) { 
                        $total_amount_paid+= $report->received_amount;
                        ?>
                         <tr>
                          <td style="text-align:center"><?php echo $i ?></td>    
                          <td style="text-align:center"><?php echo $report->maid_name ?></td>
                          <td style="text-align:center"><?php echo $report->time_from." - ".$report->time_to; ?></td>
                          <td style="text-align:center"><?php echo $report->customer_name ?></td>
                          <td style="text-align:center"><?php echo $report->customer_address ?></td>
                          <td style="text-align:center"><?php echo $report->service_date; ?></td>
                          <td style="text-align:center"><?php echo $report->billed_amount; ?></td>
                          <td style="text-align:center"><?php echo $report->received_amount; ?></td>
                          <td style="text-align:center"><?php echo $report->balance_amount; ?></td>
                         </tr>

                    <?php 
                    $i++;
                    }
                     ?>
                         <tr>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td style="text-align:center"><b>Total</b></td>
                             <td style="text-align:center"><?php echo $total_amount_paid;  ?></td>
                             <td>&nbsp;</td>
                         </tr>     
                   <?php
                    }
                    ?>

                       



                    </tbody>
                    
                   
                    
                </table>
    </div><!-- /widget-content --> 
</div>   
    