<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>View Leads</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url(); ?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>lead/edit/<?php echo $link_lead_id; ?>"><img src="<?php echo base_url(); ?>images/customer-edit.png" title="Edit Lead"/></a></div>
                <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>lead"><i class="icon-th-list"></i></a>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post">      
                        <div class="tab-content">
                                <div class="tab-pane active" id="personal">
                                    <fieldset>            
                                        <div class="span11">
                                            <div class="widget">                                            
                                                <div class="widget widget-table action-table">
    <!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                        <h3>Personal Details</h3> 
                                                    </div> /widget-header -->
                                                    <div class="widget-content">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Name</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->customer_name; ?></td>  
                                                                    <td style="line-height: 20px; width: 200px"><b>Email</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->customer_email; ?></td> 
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Phone</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->customer_phone; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Date of Contact</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->contacted_date; ?></td> 

                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Service</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->service_type_name; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Source</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->source; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Classification of Lead</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->lead_type; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Timeline of Service</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->timeline; ?></td> 
                                                                </tr>
																<tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Followup Date</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->followup_date; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Followup Comments</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->followup_comment; ?></td> 
                                                                </tr>
																<tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Next Followup Date</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->next_follow_update; ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Address</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $lead_details->customer_address; ?></td> 
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Status</b></td>
                                                                    <td style="line-height: 20px; width: 300px">
																	<?php
																	if($lead_details->status == 0)
																	{
																		echo "Inactive";
																	} else {
																		echo "Active";
																	}
																	?>
																	</td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Followup Status</b></td>
                                                                    <td style="line-height: 20px; width: 300px">
																	<?php
																	if($lead_details->status == 0)
																	{
																	?>
																	<label class="btn btn-small btn-danger" style="width: 20px; height: 20px; pointer-events: none;"></label>
																	<?php
																	} else if($lead_details->status == 1 && $lead_details->lead_status == 0)
																	{
																	?>
																	<label class="btn btn-small btn-warning" style="width: 23px; height: 23px; pointer-events: none;"></label>
																	<?php
																	} else if($lead_details->lead_status == 1)
																	{
																	?>
																	<label class="btn btn-small btn-success" style="width: 20px; height: 20px; pointer-events: none;"></label>
																	<?php
																	} else if($lead_details->followup_date == "")
																	{	
																	?>
																	<label class="btn btn-small btn-danger" style="width: 20px; height: 20px; pointer-events: none;"></label>
																	<?php
																	} else if($lead_details->followup_date != "")
																	{
																	?>
																	<label class="btn btn-small btn-warning" style="width: 20px; height: 20px; pointer-events: none;"></label>
																	<?php } ?>
																	</td>
                                                                </tr>

                                                                

                                                            </tbody>
                                                        </table>
                                                    </div><!-- /widget-content -->    
                                                </div> 
                                            </div> <!-- /widget -->
                                        </div> <!-- /span6 -->
                                    </fieldset>  
                                </div>
                              


                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->						
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>-->
