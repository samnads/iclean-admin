<div class="row">
        
    <div class="span6">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Complaint Category</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_comp_category();"><img src="<?php echo base_url();?>img/add.png" title="Add Category"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
               
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Category </th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($categories) > 0) {
                            $i = 1;
                            foreach ($categories as $cat_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $cat_val['category_name'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_comp_category(<?php echo $cat_val['category_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_comp_category(<?php echo $cat_val['category_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
                 
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
    <div class="span6" id="add_comp_category" style="display: none; float:right;  margin-left: 0px;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Add Category</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_category();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="zone" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="categoryname">Category Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="categoryname" name="categoryname" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="cat_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span6" id="edit_catsub" style="display: none; float:right;  margin-left: 0px;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Edit Category Name</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_cat();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="zone" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="edit_catname">Category Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_catname" name="edit_catname" required="required">
                                                <input type="hidden" class="span6" style="width: 300px;" id="edit_catid" name="edit_catid">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="cat_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>

     