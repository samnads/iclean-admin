<style>
    #da-ex-datatable-numberpaging th, #da-ex-datatable-numberpaging td{
        line-height: 20px !important;
    }
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>Employee Work Report</h3>                   

                    <span style="margin-left:23px;">Select Month :</span>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;"  name="month" id="month">
                        <option >-- Select Month --</option>
                        <option value="01" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '01') ? 'selected="selected"' : '';
                        }
                        ?>>January</option>
                        <option value="02" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '02') ? 'selected="selected"' : '';
                        }
                        ?>>February</option>
                        <option value="03" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '03') ? 'selected="selected"' : '';
                        }
                        ?>>March</option>
                        <option value="04" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '04') ? 'selected="selected"' : '';
                        }
                        ?>>April</option>
                        <option value="05" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '05') ? 'selected="selected"' : '';
                        }
                        ?>>May</option>
                        <option value="06" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '06') ? 'selected="selected"' : '';
                        }
                        ?>>June</option>
                        <option value="07" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '07') ? 'selected="selected"' : '';
                        }
                        ?>>July</option>
                        <option value="08" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '08') ? 'selected="selected"' : '';
                        }
                        ?>>August</option>
                        <option value="09" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '09') ? 'selected="selected"' : '';
                        }
                        ?>>September</option>
                        <option value="10" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '10') ? 'selected="selected"' : '';
                        }
                        ?>>October</option>
                        <option value="11" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '11') ? 'selected="selected"' : '';
                        }
                        ?>>November</option>
                        <option value="12" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '12') ? 'selected="selected"' : '';
                        }
                        ?>>December</option>

                    </select>
                    <span style="margin-left:23px;">Select Year :</span>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="year" name="year">
                        <option value="">-- Select Year --</option>
                        <?php
                        for ($i = 2014; $i <= 2025; $i++) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php
                            if (!empty($search_condition)) {
                                echo ($search_condition['year'] == $i) ? 'selected="selected"' : '';
                            }
                            ?>><?php echo $i; ?></option>

                            <?php
                        }
                        ?>
                    </select>
                    <span style="margin-left:15px;"></span>
                    <input type="submit" class="btn" value="Go" name="activity_report" style="margin-bottom: 4px;">
                    <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="EmpWorkPrint" title="Print"/></a></div>
                    <div class="topiconnew"><a href="<?php echo base_url(); ?>reports/empworkreporttoExcel/<?php echo $month;?>/<?php echo $year;?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel"></a></div>
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php// echo base_url(); ?>img/printer.png" id="EmpWorkPrint" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div style="width: 100%; /*width:1180px;*/">
                    <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%" style="line-height:20px !important">
                        <thead>
                            <tr>
                                <th style="width: 10%" >Sl No.</th>
                                <th > Maid</th> 
                                <th > After 6 [Hrs]</th>
                                <th > Holiday [Hrs]</th> 
                                <th > Normal [Hrs]</th>
                                <th > Total [Hrs]</th>
                                <th > Customer Reference</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($reports)) {
                                $i = 0;
                                $extra_work_hrs=0;
                                foreach ($reports as $wrk) {
                                    $work_total_hrs=0;
                                    $extra_work_hrs=0;
                                    $extra_work_hrs_holiday=0;
                                    
                                    $extra_work_hrs=$this->reports_model->get_employee_extra_work($month, $year,$wrk->maid_id);
                                    $extra_work_hrs_holiday=$this->reports_model->get_employee_extra_work_holiday($month, $year,$wrk->maid_id);
                                    $normal_work_hrs=$this->reports_model->get_employee_work_normal($month, $year,$wrk->maid_id);
                                    $work_total_hrs= $normal_work_hrs + $extra_work_hrs_holiday + $extra_work_hrs;
                                    
                                    echo '<tr>'
                                    . '<td>' . ++$i . '</td>'
                                    . '<td>' . $wrk->maid_name . '</td>'
                                    . '<td>'.$extra_work_hrs.'</td>' 
                                    . '<td>'.$extra_work_hrs_holiday.'</td>'    
                                    . '<td>' . $normal_work_hrs . '</td>'
                                    . '<td>' . $work_total_hrs . '</td>'        
                                    . '<td>' . ((!empty($maid_references) && array_key_exists($wrk->maid_id, $maid_references)) ? $maid_references[$wrk->maid_id]: 0 ). '</td>'
                                    . '</tr>';
                                }
                            } else {
                                //echo '<tr><td colspan="4">No Results!</td></tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="employeeWorkPrint">
    <table border="1" width="100%" cellspacing="0" cellpadding="10">
        <thead>
            <tr>
                <th style="width: 10%" >Sl No.</th>
                <th > Maid</th> 
                 <th > After 6 [Hrs]</th>
                 <th > Holiday [Hrs]</th> 
                 <th > Normal [Hrs]</th>
                 <th > Total [Hrs]</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($reports)) {
            $i = 0;
            foreach ($reports as $wrk) {
                
                $work_total_hrs=0;
                $extra_work_hrs=0;
                $extra_work_hrs_holiday=0;
                                    
                $extra_work_hrs=$this->reports_model->get_employee_extra_work($month, $year,$wrk->maid_id);
                $extra_work_hrs_holiday=$this->reports_model->get_employee_extra_work_holiday($month, $year,$wrk->maid_id);
                $normal_work_hrs=$this->reports_model->get_employee_work_normal($month, $year,$wrk->maid_id);
                $work_total_hrs= $normal_work_hrs + $extra_work_hrs_holiday + $extra_work_hrs;
                echo '<tr>'
                . '<td>' . ++$i . '</td>'
                . '<td>' . $wrk->maid_name . '</td>'
                . '<td>'.$extra_work_hrs.'</td>' 
                . '<td>'.$extra_work_hrs_holiday.'</td>'    
                . '<td>' . $normal_work_hrs . '</td>'
                . '<td>' . $work_total_hrs . '</td>'      
                . '</tr>';
            }
        } else {
            echo '<tr><td colspan="3">No Results!</td></tr>';
        }
        ?>
        </tbody>
    </table>
</div>