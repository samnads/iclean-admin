<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>


<table id="" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <td><center>Sl.No</center></td>
            <td><center><b>Name</b></center></td>
            <td><center><b>Email</b></center></td>
            <td><center><b>Mobile</b></center></td>
            <td><center><b>Address</b></center></td>
            <td><center><b>Service</b></center></td>
            <td><center><b>Quote</b></center></td>
            <td><center><b>Comments</b></center></td>
            <td><center><b>Lead Type</b></center></td>
            <td><center><b>Timeline</b></center></td>
            <td><center><b>Date Of Contact</b></center></td>
            <td><center><b>Source</b></center></td>
            <td><center><b>Followup Date</b></center></td>
            <td><center><b>Followup Comments</b></center></td>
            <td><center><b>Next Followup</b></center></td>
            <td><center><b>Status</b></center></td>
            <td><center><b>Converted</b></center></td>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($leads) > 0) 
		{
            $j = 1;
            foreach ($leads as $lead) 
			{
				if($lead->status == 1)
				{
					$status = "Active";
				} else {
					$status = "Inactive";
				}
		?> 
        <tr>
            <td><?php echo $j; ?></td>
            <td><?php echo $lead->customer_name; ?></td>
            <td><?php echo $lead->customer_email; ?></td>
            <td><?php echo $lead->customer_phone; ?></td>
            <td><?php echo $lead->customer_address; ?></td>
            <td><?php echo $lead->service_type_name; ?></td>
            <td><?php echo $lead->quote; ?></td>
            <td><?php echo $lead->comments; ?></td>
            <td><?php echo $lead->lead_type; ?></td>
            <td><?php echo $lead->timeline; ?></td>
            <td>
				<?php
				if($lead->contacted_date != "")
				{
					echo date('d/m/Y', strtotime($lead->contacted_date));
				}
				?>
			</td>
            <td><?php echo $lead->source; ?></td>
            <td>
				<?php
				if($lead->followup_date != "")
				{
					echo date('d/m/Y', strtotime($lead->followup_date));
				}
				?>
			</td>
            <td><?php echo $lead->followup_comment; ?></td>
			<td>
				<?php
				if($lead->next_follow_update != "")
				{
					echo date('d/m/Y', strtotime($lead->next_follow_update));
				}
				?>
			</td>
            <td><?php echo $status; ?></td>
            <td><?php 
                if($lead->lead_status == 0){echo 'No';}else{echo 'Yes';}
             ?></td>
        </tr>
        <?php
			 $j++;
			}
		}
      ?>

    </tbody>
</table>