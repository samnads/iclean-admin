<script type="text/javascript">
$('.flexdatalist-json').flexdatalist({
     searchContain: false,
     textProperty: '{capital}, {name}, {continent}',
     valueProperty: 'iso2',
     minLength: 1,
     focusFirstResult: true,
     selectionRequired: true,
     groupBy: 'continent',
     visibleProperties: ["name","continent","capital","capital_timezone"],
     searchIn: ["name","continent","capital"],
     data: 'countries.json'
});
</script>
<style>
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #eae8e8;
    border-radius: 3px;
    cursor: pointer;
    font-size: 14px;
    height: 36px;
    line-height: 30px;
   padding: 3px 10px 3px 25px;

    text-indent: 0.01px;
	}
        
        .text-field-main div.time-box .select2-container a.select2-choice { background: transparent; overflow: hidden;} 
        
        .text-field-details { padding: 10px;}
        .text-field-details p { line-height:  20px; padding-bottom: 5px;}
        
        
        
        
        .job-customer{width: 100% !important;}
        #job-b-error{
            color: #f00;
            font-weight: bold;
        }
        
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                <div class="widget-header"> 
                    <!--<form class="form-horizontal" method="POST" action="<?php// echo base_url(); ?>activity/jobs">-->
                        <i class="icon-th-list"></i>
                        <h3>Quotation</h3> 
<!--                                <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php// echo $service_date ?>">                       
                            <input type="hidden" id="formatted-date-job" value="<?php// echo $formatted_date ?>"/>
                        <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;"> -->
                        <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                        <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                        <!--<a class="newjobbutton" id="newjobbutton" href="<?php// echo base_url(); ?>activity/addjob"><i class="fa fa-plus"></i> New Job</a>-->
                    <!--</form>-->

                </div>
            </div>
    </div>
    <div class="row job-field-wrapper no-left-right-margin">
      <div class="col-md-12 col-sm-12 job-field-main box-center">
      
          <form name="quotation_add_form" id="quotation_add_form">
     
     <h2 class="text-center">Quotation Details</h2>
     
     
     
                 
     <div class="col-md-6 col-sm-12  newjobbox" style="height:auto;">
                <div class="text-field-main">
                    <p> Customer :</p>
                    <select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="">
                        <option></option>
                        
                    </select>
                    <div class="clear"></div>
                </div>
                  
                  <div class="text-field-details" id="selected-address" style="display:none;">
                      <p><b>Selected Address</b><br>
                          <i class="fa fa-home"></i> &nbsp; <span id="job-picked-address">uiiujioujo joijoioojdsfsdfdsa  sdfs</span></p>
                  <div class="clear"></div>
                  </div>
                  <div id="job-customer-address-panel">
                            <div class="head">Pick One Address <span class="close">Close</span></div>

                            <div class="inner">
                                    Loading<span class="dots_loader"></span>
                            </div>			
                    </div>
              </div><!--booking-form-field end-->
              
              
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                        <p>Time:</p>
                        <div class="time-box">
                        
                          <div class="col-md-12 col-sm-12 no-left-right-padding">
                              <div class="col-md-6 col-sm-12 center-opt no-left-right-padding">
                                    <!-- <select name="time_from" id="b-from-time" data-placeholder="From" class="job-customer fromtime">
                                        <option></option>
                                        <?php
                                        foreach($times as $time_index=>$time)
                                        {
                                                echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
                                    </select> -->
                                </div><!--time-box end-->
                                <div class="col-md-6 col-sm-12 center-opt no-right-padding">
                                    <!--<select id="abc2"  name="bc2" class="totime">-->
                                    <!-- <select name="time_to" id="b-to-time" data-placeholder="To" class="totime job-customer">
                                        <option></option>
                                        <?php
                                        foreach($times as $time_index=>$time)
                                        {
                                            if($time_index == 't-0')
                                            {                                                                                
                                                    continue;
                                            }

                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
                                    </select> -->
                                </div>

                                <div class="col-md-12 col-sm-12 center-opt no-left-right-padding">
                                  <input name="quotation_time" class="text-field end_datepicker" type="text" placeholder="Quotation Date" id="quotation_time" autocomplete="off">
                                </div><!--time-box end-->
                                <div class="clear"></div>
                            </div><!--time-box end-->
                        
                        </div><!--time-box end-->
                  </div>
              </div><!--booking-form-field end-->   
              
              
              
              
              
                                                
              <div class="col-md-6 col-sm-12  newjobbox">
                  <div class="text-field-main">
                               <p>Cleaning Materials :</p>
                            
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                     <input type="radio" value="Y" id="b-cleaning-materials-1" name="cleaning_materials" class="">
                                    <label for="b-cleaning-materials-1"> <span></span> Yes</label>
                                </div><!--week-date end-->
                                
                                
                                <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                                    <input type="radio" value="N" id="b-cleaning-materials-2" name="cleaning_materials" class="" checked>
                    <label for="b-cleaning-materials-2"> <span></span> No</label>
                                </div><!--week-date end-->
                                
                               
                            <div class="clear"></div><!--clear end-->
                   </div>
              </div><!--booking-form-field end-->                                 
                                        
                                        
              
              

              
              
              
              
               <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding">
                   


                  <div class="text-field-main">
                    <!-- <p>Service Type :</p>
                    <select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="job-customer">
                        <option></option>
                        <?php
                        foreach($service_types as $service_type)
                        {
                            $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                            echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
                        }
                        ?>
                    </select> -->
                    <div class="text-field-main">
                    <p style="padding-bottom: 0px;">Email ID:</p>
                    <input name="jobemail" id="jobemail" class="text-field" type="text" style="pointer-events: none;" placeholder="Email id">
                    <div class="clear"></div>
                </div>
                    <div class="clear"></div>
                 </div>          


               </div><!--week-type-box end-->
              
              
              
              <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding" id="jobweekday" style="display:none;">
              <div class="text-field-main">
               <p>Repeat On :</p>
                            
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="0" name="w_day[]" id="repeat-on-0" <?php if($day_number == 0) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-0"> <span></span> Sun</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="1" name="w_day[]" id="repeat-on-1" <?php if($day_number == 1) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-1"> <span></span> Mon</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="2" name="w_day[]" id="repeat-on-2" <?php if($day_number == 2) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-2"> <span></span> Tue</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="3" name="w_day[]" id="repeat-on-3" <?php if($day_number == 3) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-3"> <span></span> Wed</label>
              </div><!--week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="4" name="w_day[]" id="repeat-on-4" <?php if($day_number == 4) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-4"> <span></span> Thu</label>
              </div><!--week-date end-->
              
<!--              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="5" name="w_day[]" id="repeat-on-5" class="" type="checkbox">
                  <label for="repeat-on-5"> <span></span> Fri</label>
              </div>week-date end-->
              
              
              <div class="col-md-2 col-sm-2 col-xs-3 week-date no-left-right-padding">
                  <input value="6" name="w_day[]" id="repeat-on-6" <?php if($day_number == 6) { echo 'checked="checked"'; } ?> class="" type="checkbox">
                  <label for="repeat-on-6"> <span></span> Sat</label>
              </div><!--week-date end-->
              
                                          
          <div class="clear"></div><!--clear end-->
              </div>
          </div><!--week-type-box end-->
          
          <div class="col-md-6 col-sm-12  newjobbox" id="jobneverweekday" style="display:none;">
                <div class="col-md-6 text-field-main week-type-box no-left-right-padding">
                    <p>End :</p>
                    <div class="col-md-6 no-left-right-padding">
                        <input value="never" id="job-repeat-end-never" name="repeat_end" class="" type="radio" checked="checked">
                        <label for="job-repeat-end-never"> <span></span> Never</label>
                    </div>
                    <div class="col-md-6 no-left-right-padding">
                        <span class="pull-right">
                            <input value="ondate" id="job-repeat-end-ondate" name="repeat_end" class="" type="radio">
                        <label for="job-repeat-end-ondate"> <span></span> On</label>
                        </span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="col-md-6 text-field-main no-right-padding">
                    <p>&nbsp;</p>
                    <input class="text-field end_datepicker" id="job-repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" type="text">
                    <div class="clear"></div>
                </div>
            </div>
              
              
              <div class="col-md-6 col-sm-12  newjobbox">
                <div class="text-field-main">
                    <p>Contact Number :</p>
                    <input name="jobcontact" class="text-field" type="text" placeholder="Contact Number" id="job-contact" style="pointer-events: none;">
                    <div class="clear"></div>
                </div>
              </div><!--booking-form-field end-->
                                                

              
              
              <div class="col-md-6 col-sm-12 week-type-box newjobbox no-right-padding">
                  <!-- <div class="text-field-main" style="padding-top: 0px;">
                      <p>Total Amount:</p>
                      <input name="n_totamt" id="n_totamt" class="text-field" type="text">
                      <div class="clear"></div>
                  </div> -->
              </div><!--week-type-box end-->
                            
                            
                            
                            
                                            
              
              
              
            
                          
              
              <div class="col-md-6 col-sm-12  newjobbox">
                
              </div><!--booking-form-field end-->
              
              <?php 
                    $serhtml='<option></option>'; 
                    foreach($service_types as $service_type)
                    {
                        
                        $serhtml.='<option value="' . $service_type->service_type_id . '">' . html_escape($service_type->service_type_name) . '</option>';
                    }
              ?>
              <input type="hidden" name="hid_serv_optns" value='<?php echo $serhtml;?>' id="hid_serv_optns">
              <input type="hidden" name="hid_serv_count" value='1' id="hid_serv_count">

              <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 70px"> Sl. No. </th>
                            <th style="line-height: 18px"> Service </th>
                            <th style="line-height: 18px"> Description </th>
                            <th style="line-height: 18px"> Rate</th>
                            <th style="line-height: 18px"> Qty</th>
                            <th style="line-height: 18px"> VAT (10%)</th>
                            <th style="line-height: 18px">Total
                            </th>
                            <th style="line-height: 18px;text-align:center;">
                              <div style="cursor:pointer;" onclick="addServiceRow()"><img src="<?php echo base_url();?>img/add.png" title="Add Service"></div>
                            </th>
                      </tr>
                    </thead>
                    <tbody id="service_rows">
                        <tr>
                            <td style="line-height: 18px; width: 20px">1</td>
                            <td style="line-height: 18px">
                              <select name="service_type[]" id="service_type_1" data-placeholder="Select service type">
                                  <option></option>
                                  <?php
                                  foreach($service_types as $service_type)
                                  {
                                      $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                                      echo '<option value="' . $service_type->service_type_id . '" >' . html_escape($service_type->service_type_name) . '</option>';
                                  }
                                  ?>
                              </select>
                            </td>
                            <td style="line-height: 18px">
                                <textarea name="description[]" id="description_1" rows="1"></textarea>
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_rate[]" id="service_rate_1" oninput="changeRate(this)">
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_qty[]" id="service_qty_1" oninput="changeQty(this)">
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_vat[]" id="service_vat_1" readonly>
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_total[]" id="service_total_1" readonly>
                            </td>
                            <td style="line-height: 18px;text-align:center;">
                               <button type="button" class="text-danger" onclick="removeServiceRow(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-field-main text-center">          
                  <input id="btn" class="save-but" type="submit" value="Save" style="float: none;">
                </div>
            </div>









            <!--<div class="widget-content">
                <div id="serv_table_msg"></div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Service </th>
                            <th style="line-height: 18px"> Description </th>
                            <th style="line-height: 18px"> Rate</th>
                            <th style="line-height: 18px"> Qty</th>
                            <th style="line-height: 18px"> <span style="margin-top: 15px;position: absolute;">Price </span> 
                              <div style="float:right ; margin-right:20px; cursor:pointer;" id="addser"><img src="<?php echo base_url();?>img/add.png" title="Add Service"></div>
                            </th>
                      </tr>
                    </thead>
                    <tbody id="st_body">
                        <tr>
                            <td style="line-height: 18px; width: 20px">1</td>
                            <td style="line-height: 18px">
                              <select name="service_type_1" id="b-service-type-1" data-placeholder="Select service type" class="job-customer">
                                  <option></option>
                                  <?php
                                  foreach($service_types as $service_type)
                                  {
                                      $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                                      echo '<option value="' . $service_type->service_type_id . '" >' . html_escape($service_type->service_type_name) . '</option>';
                                  }
                                  ?>
                              </select>
                            </td>
                            <td style="line-height: 18px">
                                <textarea name="description_1" id="description_1" ></textarea>
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_rate_1" id="service_rate_1">
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_qty_1" id="service_qty_1">
                            </td>
                            <td style="line-height: 18px">
                                <input type="number" name="service_price_1" id="service_price_1">
                            </td>
                        </tr> 
                         
                        
                    </tbody>
                </table>
            </div>
              
                      
                           
                <div class="col-md-12 col-sm-12  newjobbox text-center">
                <div class="text-field-main text-center">
                    <p>&nbsp;</p>                                    
                                    
                    <input type="hidden" id="job-customer-address-id" name="job-customer-address-id" />             
                  <input class="save-but" id="save-booking" value="Save" type="button" style="float: none;"><span id="job-b-error"></span>
                               
                           
                 <div class="clear"></div>
                </div>
              </div>
                                -->
              <!--booking-form-field end-->              
                           
                           
                           
                           
                           
                           
                                    
     <br>
<br>
<br>

</form>
     
      </div><!--job-field-main end--> 
  </div><!--row job-field-wrapper end--> 
</section>