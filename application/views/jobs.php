<style>
    #exTab2 ul li { margin-left: 0px !important; }
    #exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}
.add-complaint-job
{
    display: none;
}
.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }
    /*.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: auto;}*/
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                    <div class="widget-header"> 
                        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>activity/jobs">
                              <div class="book-nav-top">

                                <ul>
                                    <li style="padding-top:14px;">
                            <i class="icon-th-list"></i>
                            <h3>Jobs</h3>
                                    </li>
                                    <li style="padding-top:5px;">
                                <input type="text" style="float: left; margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">                       
                                <input type="hidden" id="formatted-date-job" value="<?php echo $formatted_date ?>"/>
                                <input type="submit" id="gohide" class="btn" value="Go" name="vehicle_report" style="float: left; margin-bottom: 4px; margin-top: 6px; margin-left: 10px;"> 
                                 <div class="clear"></div>
                                    </li>
                                
                                <!-- Job start date -->
                                <li style="padding-top:9px;">
                                <input type="text" id="b-date-from-job" style="width: 160px; margin-right: 10px; display: none; padding: 5px;" data-date="<?php echo $search_date_from_job; ?>" readonly value="<?php echo $search_date_from_job; ?>" data-date-format="dd/mm/yyyy"/> 
                                </li>
                                <li style="padding-top:9px;">
                                <input type="text" id="b-date-to-job" style="width: 160px; padding: 5px; margin-right: 10px; display: none;" data-date='<?php echo $search_date_to_job ?>' readonly value='<?php echo $search_date_to_job ?>' data-date-format="dd/mm/yyyy"/>
                                </li>
                                <li style="padding-top:9px;">
                                    <input type="button" id="job-search" value="Search" class="" style="color: #fff !important; background: #8f137d;" />
                                </li>
                                <!--<li class="pull-right no-right-margin">
                                    <a class="newjobbutton" id="newjobbutton" href="<?php// echo base_url(); ?>activity/addjob"><i class="fa fa-plus"></i> New Job</a>
                                </li>-->
                                <div class="clear"></div>
                                <!-- Job ends -->
                                </ul>
                              </div>
                                
                            
                            <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                            <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                            
                        </form>
                        
                    </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
                <div id="exTab2" class="">	
                    <ul class="nav nav-tabs">
			<li class="active">
                            <a data-target="#1" data-toggle="tab" id="tab1">New(Unscheduled)</a>
			</li>
                        <li>
                            <a data-target="#2" data-toggle="tab" id="tab2">Scheduled</a>
			</li>
			<li>
                            <a data-target="#3" data-toggle="tab" id="tab3">In Progress</a>
			</li>
                        <li>
                            <a data-target="#4" data-toggle="tab" id="tab4">Finished</a>
			</li>
                        <li>
                            <a data-target="#5" data-toggle="tab" id="tab5">Recurring</a>
			</li>
                        <li>
                            <a data-target="#9" data-toggle="tab" id="tab9">Delayed</a>
			</li>
                        <li>
                            <a data-target="#6" data-toggle="tab" id="tab6">Missed</a>
			</li>
                        <li>
                            <a data-target="#7" data-toggle="tab" id="tab7">Cancelled</a>
			</li>
                        <li>
                            <a data-target="#8" data-toggle="tab" id="tab8">All</a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="1">
                            <table id="job-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <!-- <th class="no-right-border">Compliant</th> -->
                                    </tr>
				</thead>
                                <tbody id="tabtbody1">
                                    <?php
                                    $i=1;
                                    //$date = date('Y-m-d');
                                    foreach($approval_list as $ap_list)
                                    {
                                        if($ap_list->booking_type == "WE")
                                        {
                                            $booking_type = "Weekly";
                                        } else if($ap_list->booking_type == "OD"){
                                            $booking_type = "One Day";
                                        }
										
										if($ap_list->customer_address == "")
										{
											$a_address = 'Building - '.$ap_list->building.', '.$ap_list->unit_no.''.$ap_list->street;
										} else {
											$a_address = $ap_list->customer_address;
										}
						
                                        $checkcomplaint = $this->bookings_model->getjobcomplaintbyid($ap_list->booking_id,$formatted_date);
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td><a href="<?php echo base_url(); ?>activity/job_view/<?php echo $formatted_date; ?>/<?php echo $ap_list->booking_id; ?>"><?php echo $i; ?></a></td>
                                        <td><?php echo $ap_list->customer_name; ?> [<a href="<?php echo base_url(); ?>customer/view/<?php echo $ap_list->customer_id; ?>">Details</a>] </td>
                                        <td><?php echo $a_address; ?></td>
                                        <td><?php echo $ap_list->shift; ?></td>
                                        <td><?php echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php echo $ap_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php echo $ap_list->email_address; ?>
                                        </td>
                                        <!--<td class="<?php echo $ap_list->booking_id; ?>-<?php echo strtotime($formatted_date); ?>complaintclass">
                                            <?php
                                            if(empty($checkcomplaint))
                                            {
                                            ?>
                                            <span class="btn add-complaint-job" onclick="addcomplaint(<?php echo $ap_list->booking_id; ?>,<?php echo strtotime($formatted_date); ?>)"><i class="fa fa-plus"></i></span>
                                            <?php
                                            } else {
                                            ?>
                                            <span class="btn edit-complaint-job" onclick="editcomplaint(<?php echo $checkcomplaint->cmp_id; ?>)"><i class="fa fa-pencil"></i></span>
                                            <span class="btn view-complaint-job"  onclick="viewcomplaint(<?php echo $checkcomplaint->cmp_id; ?>)"><i class="fa fa-eye"></i></span>
                                            <?php } ?>
                                        </td>-->
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="2">
                            <table id="job-table2" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <th class="no-right-border">Compliant</th>
                                    </tr>
				</thead>
                                <tbody id="tabtbody2">
                                    <?php
                                    //$i=1;
                                    //foreach($booking_list as $b_list)
                                    //{
                                    //    if($b_list->booking_type == "WE")
                                    //    {
                                    //        $booking_type = "Weekly";
                                    //    } else if($b_list->booking_type == "OD"){
                                           // $booking_type = "One Day";
                                    //    }
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $b_list->customer_name; ?></td>
                                        <td><?php// echo $b_list->customer_address; ?></td>
                                        <td><?php// echo $b_list->shift; ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $b_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $b_list->email_address; ?>
                                        </td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn2">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="3">
                            <table id="job-table3" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <th class="no-right-border">Started</th>
                                    </tr>
				</thead>
                                <tbody id="tabtbody3">
                                    <?php
                                    //$i=1;
                                    //foreach($progress_list as $p_list)
                                    //{
                                    //    if($p_list->booking_type == "WE")
                                    //    {
                                    //        $booking_type = "Weekly";
                                    //    } else if($p_list->booking_type == "OD"){
                                    //        $booking_type = "One Day";
                                    //    }
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $p_list->customer_name; ?></td>
                                        <td><?php// echo $p_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($p_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($p_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $p_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $p_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($p_list->start_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn3">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="4">
                            <table id="job-table4" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <th class="no-right-border">Actual Time</th>
                                    </tr>
				</thead>
				<tbody id="tabtbody4">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn4">Submit</button>
                            </p>-->
                        </div>
                        <div class="tab-pane" id="5">
                            <table id="job-table5" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th class="no-right-border">Contacts</th>
                                        <!--<th>Actual Time</th>-->
                                    </tr>
				</thead>
				<tbody id="tabtbody5">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn5">Submit</button>
                            </p>-->
                        </div>
                        <div class="tab-pane" id="9">
                            <table id="job-table9" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <!-- <th class="no-right-border">Complaint</th> -->
                                        <!--<th>Actual Time</th>-->
                                    </tr>
				</thead>
				<tbody id="tabtbody9">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn5">Submit</button>
                            </p>-->
                        </div>
                        <div class="tab-pane" id="6">
                            <table id="job-table6" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <!-- <th class="no-right-border">Complaint</th> -->
                                    </tr>
				</thead>
				<tbody id="tabtbody6">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn6">Submit</button>
                            </p>-->
                        </div>
                        <div class="tab-pane" id="7">
                            <table id="job-table7" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th class="no-right-border">Contacts</th>
                                        <!--<th>Actual Time</th>-->
                                    </tr>
				</thead>
				<tbody id="tabtbody7">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn7">Submit</button>
                            </p>-->
                        </div>
                        <div class="tab-pane" id="8">
                            <table id="job-table8" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-left-border"></th>
                                        <th>Job#</th>
                                        <th>Customer</th>
                                        <th>Location</th>
                                        <th>Service Date</th>
                                        <th>Shift</th>
                                        <th>Type</th>
                                        <th>Contacts</th>
                                        <th>Status</th>
                                        <!-- <th class="no-right-border">Compliant</th> -->
                                    </tr>
				</thead>
				<tbody id="tabtbody8">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn8">Submit</button>
                            </p>-->
                        </div>
                    </div>
                <!--</div>-->
                    <!--</div>-->
                </div>
            </div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->


<button type="button" class="btn btn-info btn-lg opencomplaintmodal hidden" data-toggle="modal" data-target="#complaintModal">Open Modal</button>

<!-- Modal -->
<div id="complaintModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
          <div id="emailpopmsg"></div>

          <div class="form-group">
            <label for="email">Complaint Category:</label>
            <select class="form-control" id="complaint_category">
                <option value="">---Select Category---</option>
                <?php  foreach ($complaint_categories as $cat) { ?>
                <option value="<?php echo $cat['category_id'];?>"><?php echo $cat['category_name'];?></option>
                <?php  }?>
            </select>
          </div>
          <div class="form-group">
            <label for="email_content">Complaint Description:</label>
            <textarea class="form-control" id="complaint_description"></textarea>
          </div>
          
          <input type="hidden" name="complaint_service_date" id="complaint_service_date">
          <input type="hidden" name="complaint_day_service_id" id="complaint_day_service_id">
          <input type="hidden" name="complaint_booking_id" id="complaint_booking_id">
          <input type="hidden" name="complaint_customer_id" id="complaint_customer_id">
          <button type="submit" class="btn btn-default emailsubbtn" id="submit_complaint">Submit</button>
          <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>