<html>
    <head>
    </head>
	<body>
		<div class="main-wrapper" style="width: 800px; margin:0 auto;">
			<div style="width: 800px;"><img src="<?php echo base_url(); ?>images/iclean-banner.jpg" width="800" height="232" alt="" /></div>
			<div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc; width: 798px;">
			<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
				<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear <?php echo $f_name; ?>,</p>
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">We received a request to change your password on iClean Cleaning Services.</p>
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
						<span style="font-size:15px; background: #00bff3; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Your new password - <?php echo $f_password; ?></span>
					</p>
				</div>
				<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
						<strong>iClean Cleaning Services</strong><br />
						Road 31, Bldg. 1007/A. Block 635,<br />
						Ma'ameer,<br />
						Sitra, Kingdom of Bahrain<br />
						For Bookings : +973 1770 0211-135 / +973 6639 6076<br />
						Email : info@iclean.bh
					</p>
				</div>
			</div>
 
			<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> iClean All Rights Reserved.</p>
			</div>
			</div>
		</div>
	</body>
</html>