<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1">
    <thead>
        <tr>
            <th>Sl No.</th>
            <?php if(!@$maid_id){ ?>
            <th>Maid</th>
            <?php }?>
            <th>Date</th>
            <th>Type</th>
            <th>Leave Type</th>                              
        </tr>
    </thead>
    <tbody>
     <?php
                        
                        if(!empty($results))
                        {
                            $i = 0;
                            foreach ($results as $lists)
                            {
                                if($lists->typeleaves == "leave")
                                {
                                    $leave = "Leave";
                                } else if($lists->typeleaves == "absent")
                                {
                                    $leave = "Absent";
                                } else if($lists->typeleaves == "dayoff")
                                {
                                    $leave = "Dayoff";
                                } else {
                                    $leave = "";
                                }
                                $leavedate = @$lists->leave_date;
                                //$originalDate = "2010-03-21";
                                $newDate = date("d/m/Y", strtotime($leavedate));
                                
                               
                                echo '<tr>'
                                        .'<td>' . ++$i . '</td>';
                                          if(!@$maid_id)
                                          {
                                        echo '<td>' . $lists->maid_name . '</td>';
                                          }       
                                        echo '<td>' . $newDate . '</td>'
                                        . '<td>' . @$type[$lists->leave_type] . '</td>'
                                         . '<td>' . $leave . '</td>' ;
                                        ?>                   
                    
                                <?php echo '</tr>';
                            }
                        }
                        else
                        {
                          // echo '<tr><td>No Results!</td></tr>';
                        }
                        ?>
    </tbody>    
</table>