<div class="row">
    <!-- /span6 --> 
    <div class="span6" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    <div class="widget-header" style="margin-bottom: 0;">
	      	<i class="icon-globe"></i>
                    <h3>Edit Coupon</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php echo base_url(); ?>coupons"><img src="<?php echo base_url();?>img/male-list.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="useraadd" action="<?php echo base_url(); ?>coupons/edit/<?php echo $coupon_id; ?>" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></div>
                                    </div>
                                    <div class="control-group">											
					<label class="control-label" for="couponname">Coupon Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="couponname" name="couponname" value="<?php echo $coupon_info->coupon_name; ?>">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="expirydate">Expiry Date</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="expirydate" readonly="readonly" name="expirydate" value="<?php echo $coupon_info->expiry_date; ?>">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
									<div class="control-group">                                            
                                            <label class="control-label" for="coupontype">Service</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="service_type" id="service_type" style="width: 300px;">
                                                      <option value="">Select Option</option>
                                                      <?php foreach ($services as $service){ ?>
                                                      <option value="<?php echo $service['service_type_id'];?>" <?php if($service['service_type_id']==$coupon_info->service_id){echo 'selected';}?>><?php echo $service['service_type_name'];?></option>
                                                      <?php } ?>
                                                </select>
                                            </div> <!-- /controls -->               
                                    </div> <!-- /control-group -->
									<div class="control-group" id="">
                                            <?php  
                                            if($coupon_info->offer_type == "F")
                                            {
                                                if($coupon->discount_type == 0)
                                                {
                                                    $offertype = "Flat Percentage";
                                                }
                                                else
                                                {
                                                    $offertype = "Flat Rate";
                                                }
                                                
                                            } else {
                                                $offertype = "Rate Per Hour";
                                            }
                                            ?>											
											<label class="control-label" for="discountprice">Discount <?php echo $offertype;?></label>
											<div class="controls">
												<input type="text" class="span6" style="width: 300px;" id="discountprice" name="discountprice" value="<?php echo $coupon_info->percentage; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
												<label class="control-label" for="minhour">Minimum Hour</label>
												<div class="controls">
													<select class="form-control" name="minhour" id="" style="width: 300px;">
														  <option value="">Select Hour</option>
														  <option value="2" <?php if($coupon_info->min_hrs == 2) { echo 'selected="selected"'; } ?>>2</option>
														  <option value="3" <?php if($coupon_info->min_hrs == 3) { echo 'selected="selected"'; } ?>>3</option>
														  <option value="4" <?php if($coupon_info->min_hrs == 4) { echo 'selected="selected"'; } ?>>4</option>
														  <option value="5" <?php if($coupon_info->min_hrs == 5) { echo 'selected="selected"'; } ?>>5</option>
														  <option value="6" <?php if($coupon_info->min_hrs == 6) { echo 'selected="selected"'; } ?>>6</option>
														  <option value="7" <?php if($coupon_info->min_hrs == 7) { echo 'selected="selected"'; } ?>>7</option>
													</select>
												</div>			
										</div>
										
										
									<?php
									$weeekdays = $coupon_info->valid_week_day;
									$arr = explode(",",$weeekdays);
									?>
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Week Days</label>
                                            <div class="controls">
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="0" <?php echo (in_array('0', $arr)) ? "checked='checked'" : ''; ?> /> SUN</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="1" <?php echo (in_array('1', $arr)) ? "checked='checked'" : ''; ?> /> MON</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="2" <?php echo (in_array('2', $arr)) ? "checked='checked'" : ''; ?> /> TUE</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="3" <?php echo (in_array('3', $arr)) ? "checked='checked'" : ''; ?> /> WED</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="4" <?php echo (in_array('4', $arr)) ? "checked='checked'" : ''; ?> /> THU</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="5" <?php echo (in_array('5', $arr)) ? "checked='checked'" : ''; ?> /> FRI</label>
                                                <label class="checkbox inline"><input type="checkbox" name="w_day[]" value="6" <?php echo (in_array('6', $arr)) ? "checked='checked'" : ''; ?> /> SAT</label>
                                            </div>		
                                    </div>
									
									<div class="control-group">											
                                        <label class="control-label">Status</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio" name="status" value="1" <?php if($coupon_info->status == 1) { echo 'checked="checked"'; } ?>> Active</label>
                                                <label class="radio inline"><input type="radio" name="status" value="0" <?php if($coupon_info->status == 0) { echo 'checked="checked"'; } ?>> Inactive</label>
                                            </div>	<!-- /controls -->			
                                    </div>
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary pull-right" value="Submit" name="coupon_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>