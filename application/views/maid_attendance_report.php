<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>Maid Attendance Reports</h3>                   
                   
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="attendance_date" value="<?php echo $attendance_date ?>">                       
                    
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">    
                    <?php
                    
                    $url_date = str_replace('/', '-', $attendance_date);
                    $url_date = date('Y-m-d', strtotime($url_date));
                    ?>
                    <div class="topiconnew"><a href="<?php echo base_url(); ?>reports/maidattendance_rptto_xcl/<?php echo $url_date;?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel"></a></div>                
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                            <th style="line-height: 18px; width: 70px;"> Maid</th>
                            <th style="line-height: 18px; width: 50px;"> Zone</th>
                            <th style="line-height: 18px; width: 50px;"> Maid In</th>
							<th style="line-height: 18px; width: 50px;"> Maid Out</th>
                            <th style="line-height: 18px; width: 50px;"> Attendance</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($attendance_report))
                        {
                            $i = 0;
                            foreach ($attendance_report as $attendance)
                            {
								if($attendance->attandence_status == 1)
								{
									$stat = "Maid In";
								} else if($attendance->attandence_status == 2)
								{
									$stat = "Maid Out";
								} else {
									$stat ="";
								}
								$maid_in_time = date('h:i:s a', strtotime($attendance->maid_in_time));
								if($attendance->maid_out_time != "00:00:00")
								{
									$maid_out_time = date('h:i:s a', strtotime($attendance->maid_out_time));
								} else {
									$maid_out_time = "";
								}
								
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $attendance->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $attendance->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_in_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_out_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $stat . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="4">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
