<div class="content-wrapper" style="width: 1285px; margin-left: -58px;">
	
	<div id="schedule-top">	
            <form method="post" action="<?php echo base_url() . 'maid/schedule'?>">
		<div class="row">			
			<div class="cell1">
				
			</div>
			<div class="cell2">
                            <select style="margin-top: 10px; margin-right: 5px; " name="maid_id" id="search-maid-id" class="sel2">
                                <option value="0">Select Maid</option>
                                    <?php
                                    foreach($maids as $maid)
                                    {       
                                            $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                                            echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                                    }
                                    ?>
                            </select>
				<input type="text" name="start_date" id="start-date" class="span3" style="height: 30px; margin-top: 10px; margin-right: 5px; width: 90px;" value="<?php echo $start_date ?>">
                                <input type="text" name="end_date" id="end-date" class="span3" style="height: 30px; margin-top: 10px; margin-right: 45px; width: 90px;" value="<?php echo $end_date ?>">
                                <input type="submit" class="save-but" name="search_maid_schedule" style="float: none; margin-top: 10px; margin-right: 5px;" value="Search" />	
<!--                                <input type="button" class="save-but" name="mark_maid_leave" id="mark_maid_leave" style="float: none; margin-top: 10px; margin-right: 5px;" value="Leave" />-->
                        </div>
			<div class="cell3">
                            			
			</div>
		</div>
            </form>
	</div>
	
	<div id="schedule-wrapper"><!-- style="min-height: <?php //echo ((count($maids) * 80) + 70); ?>px;" -->
		<div id="schedule">
			<div class="head">Maid Name</div>
			<div class="prev"><input type="submit" value="" id="tb-slide-left" /></div>
			<div class="time_line">			
				<div class="time_slider">
					<div style="width:4800px;">
						<?php
						foreach($times as $time_index=>$time)
						{
							echo '<div>' . $time->display . '</div>';
						}
						?>
					</div>
				</div>

			</div>
			<div class="next"><input type="submit" value="" id="tb-slide-right" /></div>
			<div class="clear"></div>

			<div class="maids">
				<?php
				foreach($maid_schedule as $maid)
				{
					?>
                            <div class="maid"><?php echo html_escape(date("d/m/Y", strtotime($maid->date))); ?></div>
					<?php
				}
				?>
			</div>

			<div class="time_grid" style="height: <?php echo (count($maid_schedule) * 79) + count($maid_schedule) + 1; ?>px;">	
				<div class="grids"><div id="schedule-grid-rows"><?php echo $schedule_grid; ?></div></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<input type="hidden" id="all-maids" value='<?php echo $all_maids; ?>' />
<input type="hidden" id="time-slots" value='<?php echo json_encode($times); ?>' />
<input type="hidden" id="repeate-end-start" value="<?php echo html_escape($repeate_end_start_c); ?>" />

<div id="booking-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="green-popup-head"><span id="b-maid-name"></span> <span id="b-time-slot"></span> <span class="pop_close"><img src="<?php echo base_url() ?>images/pup_close.png" alt="" /></span> </div>
		<div class="white-content-box">
			<div id="b-error"></div>
			<div class="booking_form" id="customer-info">
				<div class="row">
					<div class="cell1"><span></span> Customer</div>
					<div class="cell2">:</div>
					<div class="cell3" id="b-customer-id-cell">
						<select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="sel2">
							<option></option>
							<?php
							foreach($customers as $customer)
							{
                                                                $p_number = array();
                                                                if($customer->phone_number != NULL)
                                                                    array_push ($p_number, $customer->phone_number);
                                                                if($customer->mobile_number_1 != NULL)
                                                                    array_push ($p_number, $customer->mobile_number_1);
                                                                if($customer->mobile_number_2 != NULL)
                                                                    array_push ($p_number, $customer->mobile_number_2);
                                                                if($customer->mobile_numebr_3 != NULL)
                                                                    array_push ($p_number, $customer->mobile_number_3);
                                                                
                                                                $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
                                                                
								echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name) . $phone_number . '</option>';
							}
							?>
						</select>
						<div id="customer-picked-address"></div>
					</div>				
				</div>
			</div>
			<div id="customer-address-panel">
				<div class="head">Pick One Address <span class="close">Close</span></div>
				
				<div class="inner">
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        <!-- Edited by Geethu -->
                       <div id="maids-panel"> <!--style=" min-height:  338px; box-shadow: 0px 0px 0px;" -->
				<div class="head">Pick Maids <span class="close">Close</span></div>
				<div class="controls">
                                    <label class="radio inline">
                                        
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="0" checked="checked"> All
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="1"> Same Zone
                                    </label>
                                    
                                    
                                </div>
				<div class="inner"> <!-- height:260px; overflow: scroll; overflow-x:hidden; padding-top:  0px;  -->
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        <!-- End Edited by Geethu -->
			<div class="booking_form">
				<div class="row">
					<div class="cell1"><span class="icon_stype"></span> Service Type</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="sel2">
							<option></option>
							<?php
							foreach($service_types as $service_type)
							{
                                                                $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
								echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
							}
							?>
						</select>
					</div>				
				</div>
                                <!--<div class="row">

                                    <div class="cell1"><span class="icon_clean"></span> Cleaning Type</div>
                                    <div class="cell2">:</div>
                                    <div class="cell3">
                                        <label>   <input type="checkbox" name="vaccum_cleaning" id="b-vaccum-cleaning" value="1">Vaccum Cleaning <br/>
                                            <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="1">Cleaning Materials <br/>
                                            <input type="checkbox" name="cleaning_chemicals" id="b-cleaning-chemicals" value="1">Cleaning Chemicals
                                        </label>
                                    </div>
                                </div>-->
				<div class="row">
					<div class="cell1"><span class="icon_time"></span> Time</div>
					<div class="cell2">:</div>
					<div class="cell3">
						From: <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel2 small mr15">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
						To: <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel2 small">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									if($time_index == 't-0')
									{                                                                                
										continue;
									}
									
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
                                                        
					</div>				
				</div>
				<div class="row">
					<div class="cell1"><span class="icon_btype"></span> Repeats</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="sel2">
							<option></option>
							<option value="OD">Never Repeat - One Day Only</option>
							<option value="WE">Every Week</option>
							<!--
							<option value="BW">Every Other Week</option>
							-->
						</select>	
					</div>				
				</div>
				<div class="row" id="repeat-days">
					<div class="cell1"><span class="icon_btype"></span> Repeat On</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input type="checkbox" name="w_day[]" id="repeat-on-0" value="0" <?php if($day_number == 0) { echo 'checked="checked"'; } ?> /> Sun</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-1" value="1" <?php if($day_number == 1) { echo 'checked="checked"'; } ?> /> Mon</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-2" value="2" <?php if($day_number == 2) { echo 'checked="checked"'; } ?> /> Tue</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-3" value="3" <?php if($day_number == 3) { echo 'checked="checked"'; } ?> /> Wed</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-4" value="4" <?php if($day_number == 4) { echo 'checked="checked"'; } ?> /> Thu</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-5" value="5" <?php if($day_number == 5) { echo 'checked="checked"'; } ?> /> Fri</label>
						<label><input type="checkbox" name="w_day[]" id="repeat-on-6" value="6" <?php if($day_number == 6) { echo 'checked="checked"'; } ?> /> Sat</label>
					</div>				
				</div>
				<div class="row" id="repeat-ends">
					<div class="cell1"><span class="icon_btype"></span> Ends</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label class="mr15"><input type="radio" name="repeat_end" id="repeat-end-never" value="never" checked="checked" /> Never</label>
						<label><input type="radio" name="repeat_end" id="repeat-end-ondate" value="ondate" /> On</label> 
						<input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
					</div>				
				</div>
                                <div class="row">
					<div class="cell1"><span class="icon_lock"></span> Lock Booking</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input name="lock_booking" id="lock-booking" type="checkbox" /> Lock Booking</label>
					</div>				
				</div>
                            <div class="row">
					<div class="cell1"><span class="icon_disc"></span> Pending Amount</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input name="pending_amount" id="b-pending-amount" type="text" class="popup-disc-fld" style="width: 100px;"/></label>
					</div>				
				</div>
                            <div class="row">
					<div class="cell1"><span class="icon_disc"></span> Discount</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input name="discount" id="b-discount" type="text" class="popup-disc-fld" style="width: 100px;" /></label>
					</div>				
				</div>
				
			</div>			
			<div style="clear:both; padding: 5px;"></div>
			<div class="pop-main-cont-fld-box"><textarea name="booking_note" id="booking-note" class="popup-note-fld" placeholder="Note to Driver"></textarea></div>
			
			<div class="pop-main-button">
				<input type="hidden" id="booking-id" />
				<input type="hidden" id="customer-address-id" />
				<input type="hidden" id="maid-id" />
                                <input type="hidden" id="service-date" />
				<input type="button" class="save-but" id="save-booking" value="Save" />
                                <!--<input type="button" class="copy-but" id="copy-booking" value="Copy" />-->
				<input type="button" class="delete-but" id="delete-booking" value="Delete" />
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<div id="schedule-report" style="display: none !important;"></div>