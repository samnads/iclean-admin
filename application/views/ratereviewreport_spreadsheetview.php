<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1" align="center">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Customer</th>
            <th> Customer Mobile</th>
            <th> Maid</th>
            <th> Service date</th>
            <th> Rating</th>
            <th> Comments</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($rate_report != NULL) {
            $i = 1;
            foreach ($rate_report as $veh) {
        ?>
                <tr>
                    <td><?php echo $i; ?> </td>
                    <td><?php echo $veh->customer_name; ?> </td>
                    <td><?php echo $veh->mobile_number_1; ?> </td>
                    <td><?php echo $veh->maid_name; ?> </td>
                    <td><?php echo date('d/m/Y', strtotime($veh->service_date)); ?> </td>
                    <td><?php echo (int)$veh->rating; ?> stars</td>


                    <td>
                        <?php if ($veh->comments == "") {
                            echo "";
                        } else {
                            echo $veh->comments;
                        } ?>
                    </td>
                </tr>
        <?php
                $i++;
            }
        } ?>
    </tbody>
</table>