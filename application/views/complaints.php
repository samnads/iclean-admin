<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Complaint Reports </h3>                   
                    <?php
                    if ($search['search_date'] == "") {
                        ?>
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" value="<?php echo date('d/m/Y', strtotime('-7 day')); ?>">

                        <?Php
                    } else {
                        ?>
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" value="<?php echo $search['search_date'] ?>">
                        <?php
                    }
                    $s_date2 = explode("/", $search['search_date']);
                    $t_dates = $s_date2[2] . '-' . $s_date2[1] . '-' . $s_date2[0];
                    
                    $s_date4 = explode("/", $search['search_date_to']);
                    $t_dates2 = $s_date4[2] . '-' . $s_date4[1] . '-' . $s_date4[0];
                    $custtid=$search['customer'];
                    if ($search['search_date_to'] == "") {?>
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" value="<?php echo date('d/m/Y'); ?>">
                    <?Php
                    } else {
                        ?>
                     <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" value="<?php echo $search['search_date_to'] ?>">
                        <?php
                    }
                    ?>

                    <span style="margin-left:23px;color:#FFF;">Ticket Status :</span>

                        <?php 
                        if ($search['status'] == "") {$stat='';}
                        else{$stat=$search['status'];}?>
                        ?>

                        <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="complaint_status" name="complaint_status">
                            <option value="">-- Select Status --</option>
                            <option value="PENDING" <?php if($stat=='PENDING'){echo 'selected';}?>>PENDING</option>
                            <option value="ACTIVE" <?php if($stat=='ACTIVE'){echo 'selected';}?>>ACTIVE</option>
                            <option value="CLOSED" <?php if($stat=='CLOSED'){echo 'selected';}?>>CLOSED</option>
                            
                        </select>

                        
                    <span style="margin-left:23px;color:#FFF;">Customer :</span>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="customers_vh_rep" name="customers_vh_rep">
                            <option value="0">-- Select Customer --</option>
                            
                    </select>
                     
                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day" value="<?php echo $search['search_day'] ?>">
                    <input type="hidden" name="zone_name" id="zone_name" value="<?php echo $search['search_zone_name'] ?>">
                    
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
                    
                    
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php// echo base_url(); ?>img/printer.png" id="printBtn" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service Time</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Ticket Ref</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Ticket Category</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Ticket Description</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Ticket Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Added Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Added By</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						
                        if ($complaint_report != NULL) {
                            $i = 1;
                            foreach ($complaint_report as $com){
                           ?>
                                <tr>
                                    <td style="line-height: 18px;"><?php echo $i; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->customer_name; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->time_from." - ".$com->time_to; ?> </td>
                                    <td style="line-height: 18px;">
                                        <?php echo $com->service_status; 
                                                if($com->service_status==''){echo "Not Started";}
                                                else if($com->service_status=='1'){echo "Service Started";}
                                                else if($com->service_status=='2'){echo "Service Finished";}
                                                else if($com->service_status=='3'){echo "Service Not Done";}
                                        ?> 
                                    </td>
                                    <td style="line-height: 18px;"><?php echo $com->cmp_ticket_ref; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->category_name; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->cmp_ticket_description; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->cmp_ticket_status; ?> </td>
                                    <td style="line-height: 18px;"><?php echo date("d/m/Y g:i a",strtotime($com->cmp_created_datetime)); ?> </td>
                                    <td style="line-height: 18px;"><?php echo $com->user_fullname; ?> </td>
                                    <td style="line-height: 18px;">
                                        <div class="widget-header text-center" style="background: #FFF !important;border-color: #FFF;">
                                            <a href="<?php echo base_url().'settings/view_complaint/'.$com->cmp_id;?>"><button class="btn"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        </div> 
                                    </td>
                                </tr>
                            <?php
                            $i++;
                            }
							?>
							
							<?php
                                } ?>



                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

