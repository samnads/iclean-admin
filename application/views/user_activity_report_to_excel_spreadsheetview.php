<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No</th>
            <th> Type</th>
            <th> Booking Type</th>
            <th> Shift</th>
            <th> Action</th>
            <th> Added By</th>
            <th> Time</th>
        </tr>
    </thead>
    <tbody>
    <?php
                        if(!empty($user_activity_report))
                        {
                            $i = 0;
                            
                            foreach ($user_activity_report as $activity)
                            {
                                $action_type = $activity->action_type;
                                switch ($activity->action_type)
                                {
                                    case 'Add' : $action_type = 'New Booking';
                                        break;
                                    case 'Update' : $action_type = 'Booking Update';
                                        break;
                                    case 'Delete' : $action_type = 'Booking Delete';
                                        break;
                                }
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . str_replace("_",' ',$action_type) . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->booking_type . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->shift . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->user_fullname . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->added . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            echo '<tr><td colspan="7">No Results!</td></tr>';
                        }
                        ?>
    </tbody>
    
</table>