<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Service Master</title>
</head>
<style>
  @page {
    margin: 0px 0px 0px 0px !important;
    padding: 0px 0px 0px 0px !important;
  }
</style>

<body style="padding: 0px; margin: 0px;">

  <div class="main" style="width:793px; height:auto; padding: 0px; margin: 0px auto;">

    <header style="height: 94px; overflow: hidden; position: fixed; left:0; top:0; z-index: 999;">


      <div class="header" style="width:100%; height:auto; padding: 5px 0px 10px 0px; margin: 0px; background: #f4f4f4;">


        <table width="100%" border="0">
          <tr>
            <td width="15%" align="right"><img src="https://iclean.bh/images/logo.png" width="70" height="70" /></td>
            <td width="50%">
              <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 10px; margin: 0px;">
                <strong style="font-size: 13px;">I CLEAN SERVICES W.L.L</strong><br />
                Road 31, Bldg. 1007/A. Block 635,<br />
                Ma'ameer,<br />
                Sitra, Kingdom of Bahrain
              </p>
            </td>
            <td width="35%" valign="bottom">
              <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                <tr>
                  <td width="35%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                  <td width="5%">:</td>
                  <td width="60%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>+973 1770 0211-135</strong></td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 0px">Email </td>
                  <td>:</td>
                  <td valign="middle" style="padding: 0px 25px 0px 0px">info@iclean.bh</td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 0px">TRN</td>
                  <td>:</td>
                  <td valign="middle" style="padding: 0px 25px 0px 0px"></td>
                </tr>

              </table>
            </td>
          </tr>
        </table>


      </div>


    </header>

    <section style="width:100%; height: 700px;  padding: 170px 30px 0px 30px;">
      <div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
        <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">

          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
            Recipient :
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
            <b><?php echo $quotation_detail->quo_customer_name; ?></b>,
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
            <?php echo $quotation_detail->quo_bill_address; ?>
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 50px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
            Mobile :
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
            <b><?php echo $quotation_detail->mobile_number_1; ?></b>,
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 50px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
            Service Address :
          </p>

          <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
            <?php echo $quotation_detail->quo_bill_address; ?>
          </p>

        </div>

        <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
          <div style="width: 100%; height:auto; margin: 0px; padding: 0px; background: #62A6C9;">
            <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 10px 20px; margin: 0px;">
              Tax Invoice : <?php echo $quotation_detail->quo_reference; ?>
            </p>
          </div>


          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 25px; color: #333; padding: 5px 0px 0px; margin: 0px; background: #eee;">
            <?php $tot_hrs = ((strtotime($quotation_detail->quo_time_to) - strtotime($quotation_detail->quo_time_from)) / 3600); ?>
            <tr>
              <td width="50%" style="padding-left: 20px;">Hours</td>
              <td width="50%" align="right" style="padding-right: 20px;"><?php echo $tot_hrs; ?></td>
            </tr>

            <tr>
              <td width="50%" style="padding-left: 20px;">Issued</td>
              <td width="50%" align="right" style="padding-right: 20px;"><?php echo date('d F - Y', strtotime($quotation_detail->quo_date)) ?></td>
            </tr>
            <!-- <tr>
                            <td style="padding-left: 20px;">Due</td>
                            <td align="right" style="padding-right: 20px;"><?php echo date('d F - Y', strtotime($invoice_detail[0]->invoice_due_date)) ?></td>
                          </tr> -->
            <tr>
              <td style="padding-left: 20px;padding-bottom: 10px;">Attn</td>
              <td align="right" style="padding-right: 20px; line-height:15px; padding-bottom: 5px;">Finance and Accounts Department</td>
            </tr>

            <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
              <td style="padding: 15px 20px; background: #6098b7; ">Total</td>
              <td align="right" style="padding: 15px 20px; background: #6098b7; "><span style="font-size: 14px;">BHD</span> <?php echo number_format($quotation_detail->quo_net_amount, 2); ?></td>
            </tr>
          </table>
        </div>
        <div style="clear:both;"></div>
      </div>




      <div style="width: 100%; height:auto; margin: 0px; padding: 50px 0px 10px 0px;">
        <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
            <!-- For Services Rendered : -->
          </p>
        </div>
        <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
          <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; text-align: right; padding: 0px; margin: 0px;">
            <!-- <span style="color: #777;">For the Month of</span> July 2020 -->
          </p>
        </div>
        <div style="clear:both;"></div>
      </div>




      <div style="width: 100%; height:auto; margin: 0px; padding: 0px 0px 0px 0px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
          <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; line-height: 20px; color: #FFF; background: #62A6C9; ">
            <td width="26%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">SERVICE</td>
            <td width="45%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">RATE</td>
            <td width="5%" align="center" style="border-right: 1px solid; border-color: #FFF;">QTY</td>
            <!-- <td width="12%" align="center" style="border-right: 1px solid; border-color: #FFF;">Unit Cost</td> -->
            <td width="12%" align="center">TOTAL</td>
          </tr>





          <?php
          $i = 1;
          $totprice = 0;
          foreach ($quotation_line_items as $line_item) {
          ?>
            <tr>
              <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-left: 1px solid; border-color: #CCC;">
                <!-- <span style="font-size: 13px; font-weight: bold;"><?php echo date('M d,Y', strtotime($jobs->service_date)); ?></span><br /> -->
                <span><?php echo $line_item->service_type_name; ?>
                </span>
              </td>
              <?php
              $nameOfDay = date('l', strtotime($quotation_detail->quo_date));
              $day = date('d', strtotime($quotation_detail->quo_date));
              $month = date('F', strtotime($quotation_detail->quo_date));
              $year = date('Y', strtotime($quotation_detail->quo_date));
              ?>
              <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><!-- Work to be carried out at:  --><?php echo $line_item->qli_rate; ?> <!-- On: <?php echo $nameOfDay; ?> <?php echo $day; ?> <?php echo $month; ?> <?php echo $year; ?> --></td>
              <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo /*$tot_hrs*/ $line_item->qli_quantity; ?></td>
              <!-- <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">BHD</span> 25:00</td> -->
              <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><!-- <span style="font-size: 11px;">BHD</span> --> <?php echo number_format($line_item->qli_quantity * $line_item->qli_rate, 2); ?></td>
            </tr>
          <?php
            $i++;
            $totprice += $line_item->qli_price;
          }
          ?>

          <?php
          $subtotal = $totprice / 1.05;
          $vatprice = $totprice - $subtotal;
          ?>
        </table>





        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px; color: #333; font-weight: bold; padding: 0px; margin: 0px;">
          <tr style="">
            <td width="30%">&nbsp;</td>
            <td width="35%" style="padding:7px 15px; background: #eee; border-left: 1px solid; border-color: #CCC;"">Subtotal</td>
                      <td width=" 35%" align="right" style="padding: 7px 15px; background: #eee; border-right: 1px solid; border-color: #CCC;""><span style=" font-size:11px;">BHD</span> <?php echo number_format($quotation_detail->quo_total_amount, 2); ?></td>
          </tr>


          <tr>
            <td>&nbsp;</td>
            <?php
            $div = $quotation_detail->quo_tax_amount / $quotation_detail->quo_total_amount;
            $div = is_nan($div) ? 0 : $div;
            $service_vat_percentage = number_format(round($div * 100), 2);
            ?>
            <td style="padding: 7px 15px; border-left: 1px solid; border-color: #CCC;">VAT(<?= $service_vat_percentage; ?>%) </td>
            <td align="right" style="padding: 7px 15px; border-right: 1px solid; border-color: #CCC;"><span style="font-size:11px;">BHD</span> <?php echo number_format($quotation_detail->quo_tax_amount, 2); ?></td>
          </tr>


          <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
            <td>&nbsp;</td>
            <td style="padding: 15px 15px; background: #6098b7; ">Total</td>
            <td align="right" style="padding: 15px 15px; background: #6098b7; "><span style="font-size: 14px;">BHD</span> <?php echo number_format($quotation_detail->quo_net_amount, 2); ?></td>
          </tr>
        </table>



      </div>










    </section>

    <footer style=" height: 190px; position: fixed; left:0; bottom:0; z-index: 999;">


      <!--<div class="bank-details" style="width: 50%; padding-bottom: 30px; padding-left: 30px;">
             <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 5px 0px; margin: 0px;">Bank Account for  Payment Settlement</p>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #555; line-height: 16px; padding: 0px; margin: 0px;">
              <tr>
                <td width="35%" style="padding: 0px 0px 3px 0px;">Payment Tearms</td>
                <td width="4%">:</td>
                <td width="61%">Cash / Cheque</td>
              </tr>
              
              <tr>
                <td style="padding: 0px 0px 3px 0px;">Beneficiary</td>
                <td>:</td>
                <td>Duo Star Technical Services LLC</td>
              </tr>
              
              <tr>
                <td style="padding: 0px 0px 3px 0px;">Bank Name</td>
                <td>:</td>
                <td>RAKBANK</td>
              </tr>
              
              <tr>
                <td style="padding: 0px 0px 3px 0px;">Bank Account No</td>
                <td>:</td>
                <td>0035-306301061</td>
              </tr>
              
              <tr>
                <td style="padding: 0px 0px 3px 0px;">IBAN No</td>
                <td>:</td>
                <td>AE860400000035306301061</td>
              </tr>
              
              <tr style=" padding: 0px 0px 3px 0px;">
                <td>Swift Code</td>
                <td>:</td>
                <td>NRAKAEAK</td>
              </tr>
              
            </table>
       </div>
       
       
    
    <div style="width:100%; height:auto;padding: 5px 0px; background: #eee; margin: 0px; text-align:center;">
         <a href="https://www.duo-star.com" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color: #555; line-height: 16px; text-decoration: none; padding: 0px 0px 0px 0px; margin: 0px;">www.duo-star.com</a>
    </div>-->

    </footer>

  </div>

</body>

</html>