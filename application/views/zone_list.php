<div class="row">
        
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Zone</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_zone();"><img src="<?php echo base_url();?>img/add.png" title="Add Zone"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
               
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Zone Name </th>
                            <th style="line-height: 18px"> Driver Name</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($zones) > 0) {
                            $i = 1;
                            foreach ($zones as $zones_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $zones_val['zone_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $zones_val['driver_name'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_zone(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if(user_authenticate() == 1) {?>
								<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_zone(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php } ?>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
                 
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
    <div class="span6" id="add_zone" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Add Zone</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_zone();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="zone" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="zonename">Zone Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="zonename" name="zonename" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="drivername">Driver Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="drivername" name="drivername" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Is Spare Zone</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="spare" checked="checked" value="Y"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="spare" value="N"> No</label>
                                            </div>	<!-- /controls -->			
                                    </div> <!-- /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="zone_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span6" id="edit_zone" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Edit Zone</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_zone();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="zone" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="zonename">Zone Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_zonename" name="edit_zonename" required="required">
                                                <input type="hidden" class="span6" style="width: 300px;" id="edit_zoneid" name="edit_zoneid">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="drivername">Driver Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_drivername" name="edit_drivername" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Is Spare Zone</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="edit_spare" value="Y"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="edit_spare" value="N"> No</label>
                                            </div>	<!-- /controls -->			
                                    </div> <!-- /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="zone_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>

     