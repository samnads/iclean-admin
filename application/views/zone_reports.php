<style>
#divToPrint p {
	margin:0px;
	padding:0px;	
}
.ptable td{
	padding:0px;
	margin:0px;	
}
</style>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Zone Report from Booking</h3>                   
                    <?php
                    //print_r($search);
                    //echo "DATE ". $search['search_date'] . "<br />";
                    if ($search['search_date'] == "") {
                        ?>
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="zone_date" name="zone_date" value="<?php echo date('d/m/Y') ?>">

                        <?Php
                    } else {
                        ?>
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="zone_date" name="zone_date" value="<?php echo $search['search_date'] ?>">
                        <?Php
                    }
                    ?>

                    <span style="margin-left:23px;">Zone :</span>

                    <?php
                    if ($search['search_zone'] == "") {
                        ?>

                        <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <?Php
                    } else {
                        ?>
                        <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($search['search_zone']) ? ($search['search_zone'] == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?Php
                    }
                    ?>

                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day" value="<?php echo $search['search_day'] ?>">
                    <input type="hidden" name="zone_name" id="zone_name" value="<?php echo $search['search_zone_name'] ?>">
                    <input type="submit" class="btn" value="Go" name="zone_report" style="margin-bottom: 4px;">
                    <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="printButton" title="Print"/></a></div>
                    <?php ?>
                    <div class="topiconnew"><a href="<?php echo base_url(); ?>reports/zonereporttoExcel/<?php echo str_replace('/','-',$search['search_date']);?>/<?php  if(strlen($search['search_zone'])==0){echo "All";}else{echo $search['search_zone'];} ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel"/></a></div>
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php// echo base_url(); ?>img/printer.png" id="printButton" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div style="width: 100%; /*width:1180px;*/">
<!--                <table id="da-datatable" class="table da-table" cellspacing="0" width="100%">-->
<!--                <div class="da-panel collapsible scrollable">-->
                <!--<table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    
                    
                    
                    <thead>
                        <tr>
                            <th style="line-height: 30px;"> <center>Employee</center></th>
                            <th style="line-height: 30px"> <center>08am-10am</center></th>
                            <th style="line-height: 30px"> <center>10am-12pm</center></th>
                            <th style="line-height: 30px"> <center>12pm-02pm</center></th>
                            <th style="line-height: 30px"> <center>02pm-04pm</center></th>
                            <th style="line-height: 30px"> <center>04pm-06pm</center></th>
                            <th style="line-height: 30px"> <center>Total [Hrs]</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);

						$tot = 0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {

                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
                                    ?>
                                    <tr>
                                        <td><?php echo $maids_val['maid_name'] ?> </td>
                                        <td>
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("10:00:00" > $time_from && "10:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                
                                                                if("08:00:00" < $time_from)
                                                                {      
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("08:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("08:00:00" < $time_to && '10:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("08:00:00" < $time_to && '10:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                                
                                                            }                                                            
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                            
                                                        }
                                                        else if("08:00:00" < $time_from && '10:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("08:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("10:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "08:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td>

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("10:00:00" >= $time_from && "10:00:00" < $time_to) || ("12:00:00" > $time_from && "12:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("12:00:00" > $time_from && "12:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("10:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("10:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("10:00:00" < $time_to && '12:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("10:00:00" < $time_to && '12:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("10:00:00" < $time_from && '12:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("10:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("12:00:00" >= $time_to)
                                                        {   
                                                            if($time_from > "10:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>


                                        </td>

                                        <td>


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("12:00:00" >= $time_from && "12:00:00" < $time_to) || ("14:00:00" > $time_from && "14:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("14:00:00" > $time_from && "14:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("12:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("12:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("12:00:00" < $time_to && '14:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("12:00:00" < $time_to && '14:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }         
                                                        else if("12:00:00" < $time_from && '14:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("12:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("14:00:00" >= $time_to)
                                                        {          
                                                            if($time_from > "12:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>




                                        </td>

                                        <td>


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("14:00:00" >= $time_from && "14:00:00" < $time_to) || ("16:00:00" > $time_from && "16:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("16:00:00" > $time_from && "16:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("14:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("14:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("14:00:00" < $time_to && '16:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("14:00:00" < $time_to && '16:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                                
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("14:00:00" < $time_from && '16:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("14:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("16:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "14:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>


                                        </td>


                                        <td>

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("16:00:00" >= $time_from && "16:00:00" < $time_to) || ("18:00:00" > $time_from && "18:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("18:00:00" > $time_from && "18:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("16:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("16:00:00" < $time_to && '18:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("16:00:00" < $time_to && '18:00:00' < $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("16:00:00" < $time_from && '18:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("16:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("18:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "16:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                         
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '</br>&nbsp; (' . $area . ')<br />[ ' . $time_display .' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>



                                        </td>                                     
                                        

                                        <td>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?>
                                        </td>
                                    </tr>

                                            <?php
                                        }
                                    }
                                } else {

                                    if (count($maids) != 0) {
                                        foreach ($maids as $maids_val) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 18px; width: 200px">&nbsp;</td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"><span style="line-height: 18px; width: 200px"><?php echo $maids_val['maid_name'] ?></span></td>
                                        
                                    </tr>

            <?php
        }
    }
}
?>
					<tr>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td align="center"><strong><?php echo $tot; ?></strong></td>
                    </tr>


                    </tbody>
                    
                   
                    
                </table>-->
				
				
				
				
				<table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    
                    
                    
                    <thead>
                        <tr>
							<th style="line-height: 30px;"> <center>Sl No</center></th>
                            <th style="line-height: 30px;"> <center>Employee</center></th>
                            <th style="line-height: 30px"> <center>Booking 1</center></th>
                            <th style="line-height: 30px"> <center>Booking 2</center></th>
                            <th style="line-height: 30px"> <center>Booking 3</center></th>
                            <th style="line-height: 30px"> <center>Booking 4</center></th>
                            <th style="line-height: 30px"> <center>Booking 5</center></th>
                            <th style="line-height: 30px"> <center>Total [Hrs]</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);

						$tot = 0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {
								$displayed_bookids=array();
								$slno=1;
                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
                                    ?>
                                    <tr>
										<td><center><?php echo $slno;?></center></td>
                                        <td><?php echo $maids_val['maid_name'] ?> </td>
                                        <td>
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td>

                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>

											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
												
											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
											
                                        </td>


                                        <td>
											
											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>                                     
                                        

                                        <td><center>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?></center>
                                        </td>
                                    </tr>

                                            <?php
                                        $slno++;
										}
                                    }
                                } else {

                                    if (count($maids) != 0) {
                                        foreach ($maids as $maids_val) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 18px; width: 200px">&nbsp;</td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"><span style="line-height: 18px; width: 200px"><?php echo $maids_val['maid_name'] ?></span></td>
                                        
                                    </tr>

            <?php
        }
    }
}
?>
					


                    </tbody>
                    
                   <tfoot>
				   <tr>
						<td></td>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><center><strong>Total</strong></center></td>
                        <td align="center"><center><strong><?php echo $tot; ?></strong></center></td>
                    </tr>
				   </tfoot>
                    
                </table>
				
				
				
				
                </div>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                  
                    <thead>
                        <tr>
                        	<th> Sl No.</th>
                            <th> Employee</th>
                            <th> Booking 1</th>
                            <th> Booking 2</th>
                            <th> Booking 3</th>
                            <th> Booking 4</th>
                            <th> Booking 5</th>
                            <th> Total [Hrs]</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);
						$tot = 0;
						$p=0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {
                                $displayed_bookids=array();
                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
									$p++;
                                    ?>
                                    <tr>
                                    	<td><?php echo $p; ?></td>
                                        <td style="width: 200px"><?php echo $maids_val['maid_name'] ?> </td>
                                        <td style="padding:3px;">
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td style="padding:3px;">

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>


                                        </td>

                                        <td style="padding:3px;">


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>




                                        </td>

                                        <td style="padding:3px;">


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>


                                        </td>


                                        <td style="padding:3px;">

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="color: #ff7223; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="color: #9e6ab8; margin:0px; padding:0px;">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>&nbsp; - ' . $zone . '&nbsp; (' . $area . ')<br />'.$time_det.' [ ' . $time_display . ' ]' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>



                                        </td>
                                        
                                        <td style="padding:3px;"><center>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?></center>
                                        </td>


                                    </tr>

                                            <?php
                                        }
                                    }
                        }

//                                else {
//
//                                    if (count($maids) != 0) {
//                                        foreach ($maids as $maids_val) {
//                                            ?>
<!--                                    <tr>
                                        <td style="line-height: 18px; width: 200px"><?php //echo $maids_val['maid_name'] ?> </td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>                                    
                                        <td style="line-height: 18px"></td>
                                    </tr>-->

            <?php
//        }
//    }
//}
?>



                    </tbody>
                    
                   <tr>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td align="center"><strong><?php echo $tot; ?></strong></td>
                    </tr>
                    
                </table>
    </div><!-- /widget-content --> 
</div>