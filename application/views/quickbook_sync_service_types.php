<style>
    .book-nav-top li{
        margin: 0 10px 0 0 !important;
    }
    .select2-arrow{visibility : hidden;}
    .select2-container .select2-choice{
	-moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;
        text-indent: 0.01px;
    }
    .select2-results li{margin-left: 0px !important;}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
/*notification*/

.navbar--nav .nav-item, .navbar--search, .sidebar--nav li {
    position: relative;
    z-index: 0;
}


.no-outlines .btn-link, .no-outlines .btn-link:active, .no-outlines .btn-link:focus, .no-outlines .btn-link:hover, .no-outlines .btn-link:link, .no-outlines .btn-link:visited, .no-outlines a, .no-outlines a:active, .no-outlines a:focus, .no-outlines a:hover, .no-outlines a:link, .no-outlines a:visited, .no-outlines button:focus {
    outline: 0 none;
}
.navbar--nav .nav-link {
    padding-left: 30px;
    padding-right: 30px;
}
.navbar--nav .nav-link {
    font-size: 18px;
    line-height: 50px;
    padding: 20px 15px;
}
.nav li a {
    color: inherit;
}

.navbar--nav .nav-link .badge {
    left: 20px;
}
.navbar--nav .nav-link .badge {
    left: 5px;
    position: absolute;
    top: 23px;
}
.badge {
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    font-size: 14px;
    line-height: 17px;
	position: absolute;
    
    
	 right: -10px;
    top: -10px;
}
.badge, .label {
    font-weight: 400;
}
.bg-blue {
    background-color: #2bb3c0;
}
.text-white {
    color: #fff;
}
.badge {
    border-radius: 50%;
    display: inline-block;
    font-size: 75%;
    font-weight: 700;
    padding: 2px 0 0 0 !important;
	width:23px;
	height:23px;
    text-align: center;
    vertical-align: baseline;
    white-space: nowrap;
}
.nav-item {

    padding-right: 18px;
    padding-top: 20px;
	float: right !important;
}
.nav-link {

    position: relative;
}
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin" style="padding-top: 20px;">
        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header"> 
                
                    <div class="book-nav-top">
                        <ul>
                            <li style="padding-top:14px; padding-bottom:14px;">
                                <i class="icon-th-list"></i>
                                <h3>Quickbook Sync Service Types</h3>
                            </li>
                            <!-- statement start date -->
                            
							<li class="nav-item" style="margin-top: 0px !important;padding-top:4px !important;"> 
                                <form method="POST" action="<?php echo base_url();?>quickbook/quickbook_sync_service_type_process">
                                    <?php $customerids=array();
                                        foreach($service_types as $service_type)
                                        {
                                            $stids[]=$service_type->service_type_id;
                                        }
                                    ?>
                                    <input type="hidden" name="service_type_ids" value="<?php echo implode(",",$stids); ?>">
                                    <button style="background: transparent;border:0px;" type="submit">
                                    <img style="height: 40px; width:250px; margin: 0 auto;" src="<?php echo base_url();?>images/C2QB_white_btn.png">
                                    </button>
                                </form>
                            </li>

                            <div class="clear"></div>
                            <!-- Statement ends -->
                        </ul>
                    </div>
                              
                
            </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
            <div id="statement_content" class="text-center">
                <?php 
                        $qb_sync_msg=$qb_sync_msg->tmp_msg_content;
                        //print_r($qb_sync_msg);
                        if(strlen($qb_sync_msg)>5){
                            $qb_sync_msg=json_decode($qb_sync_msg);
                ?>
                <div class="alert alert-success">
                  <strong><?php echo $qb_sync_msg;?></strong>
                </div>
                <?php } ?>
                

                <h2>Total Customer Count - <?php echo $service_types_count;?></h2>	
                <table id="statement-content-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!--<th class="no-left-border"></th>-->
                            <th>Sl</th>
                            <th>Service Type </th>
                        </tr>
                    </thead>
                    <tbody id="invoice-tabtbody1">
                        <?php
                        if(!empty($service_types))
                        {

                        $sln = 1;
                        foreach($service_types as $service_type)
                        {
                            
                        ?>
                        <tr >
                            <td ><?php echo $sln++; ?></td>
                            <td ><?php echo $service_type->service_type_name; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        
                        <?php
                        }
                        ?>
                    </tbody>
                 </table> 
            </div>
        </div><!--welcome-text-main end-->
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->

<script type="text/javascript">
setTimeout(function(){
   window.location.reload(1);
}, 60000);    
</script>