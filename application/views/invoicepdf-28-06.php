<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Master</title>
</head>
<style>
@page {
	margin: 0px 0px 0px 0px !important;
	padding: 0px 0px 0px 0px !important;
}
.header { position: fixed; top: 0px; left: 0px; right: 0px; }
<!--.footer { position: fixed; bottom: 0px; left: 0px; right: 0px;}-->
<!--.main-content { page-break-after: always; }-->
</style>
<body style="padding: 0px; margin: 0px;">


<div class="main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
     <div class="header" style="width:100%; height:auto; padding: 10px 0px 20px 0px; margin: 0px; background: #eee;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15%" rowspan="2" align="right"><img src="<?php echo base_url(); ?>images/invoice-logo-icon.png" width="90" height="94" /></td>
                  <td style=" padding: 15px 0px 0px 0px;"><img src="<?php echo base_url(); ?>images/invoice-logo-name.png" width="150" height="16" /></td>
                  <td width="35%" rowspan="2" valign="bottom">
                  
                  
                  
                         <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                                      <tr>
                                        <td width="50%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                                        <td width="5%">:</td>
                                        <td width="45%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>054 459 0300</strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Email </td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px">info@servicemaster.ae</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px">100243560800003</td>
                                      </tr>
                                      
                             </table>
                  
                  
                  
                  </td>
                </tr>
                <tr>
                  <td width="50%"><p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px 0px; margin: 0px;">
                            <b>Service Master Cleaning Services L.L.C.</b><br />
                            ACICO Business Park, Office no. 603-A Rm. 265<br />
                            Al Hilal Al Arabi Business Centre Port Saeed Deira, Dubai, UAE.</p>
                  </td>
                </tr>
              </table>

     </div>
     
     
     <div class="main-content" style="width: 90%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
          <div class="tax-head" style="width:100%; height:auto; padding: 50px 0px 20px 0px; margin: 0px;">
                 <p style="font-family: Roboto, sans-serif; font-size: 30px; color: #555; line-height: 30px; padding: 0px; margin: 0px; text-align: center;">TAX INVOICE</p>
          </div>
             
       <div class="to-job" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px 0px 20px 0px;">
                  
                  <div class="to-address" style="width:100%; height:auto; background: #e5f6fc; padding: 20px 0px 25px; margin: 0px;">
                  
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="middle">
                                <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px; margin: 0px;"><strong>To</strong></p>
                            
                                <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px 45px; margin: 0px;">
                            <strong> <?php echo $invoice_detail[0]->customer_name; ?></strong><br />
                            <?php echo $invoice_detail[0]->bill_address; ?><br />
                            <!--Apt No: 408, Dubai Internet City, Building 12 4th Floor.<br />-->
                       </p>
                       
                       
                        <!--<p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 10px 25px 0px 45px; margin: 0px; font-weight: bold;">Customer TRN &nbsp;&nbsp;&nbsp; :&nbsp; 0087267213283283</p>-->
                        
                            </td>
                            
                            <td align="right" valign="middle"> 
                              
                                
                                
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px;">
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px"><strong>Invoice Number</strong></td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?php echo $invoice_detail[0]->invoice_num; ?></strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Invoice Date</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_date)) ?></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Due Date</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_due_date)) ?></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Prepared By</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo $invoice_detail[0]->user_fullname; ?></td>
                                      </tr>
                                    </table>

                                
                                
                                
                                
                            </td>
                          </tr>
                        </table>

                  
                  
                       
                       
                       
         </div>
                  
                  
                   
       </div>
             
          <div class="table-main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px; border:1px solid #ffe0e0;">
          
          
            <table width="100%" border="1" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px; margin: 0px;">
                    <tr class="table-head" bgcolor="#fe6565" style=" font-size: 12px; line-height: 30px; font-weight: bold; color: #FFF;">
                          <td width="8%" align="center" valign="middle" style="padding: 10px 10px;">Sl.No</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px;">Date & Time</td>
                          <td width="31%" valign="middle" style="padding: 10px 10px;">Description</td>
                          <td width="7%" align="center" valign="middle" style="padding: 10px 10px;">Hours</td>
                          <td width="10%" align="center" valign="middle" style="padding: 10px 10px;">Amount</td>
                          <td width="10%" align="center" valign="middle" style="padding: 10px 10px;">VAT-5%</td>
                          <td width="14%" align="center" valign="middle" style="padding: 10px 10px;">Net Amount</td>
                    </tr>
					<?php
					$i = 1;
					foreach ($invoice_detail as $jobs)
					{
						$tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
					?>
                    <tr bgcolor="<?php echo ($i%2 ? "#FFF" : "#e5f6fc"); ?>">
                            <td align="center" style="padding: 10px 10px;"><?php echo $i; ?></td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 12px;"><?php echo date('d/m/Y', strtotime($jobs->service_date)); ?></strong><br />
                            <?php echo date('H:i A', strtotime($jobs->service_from_time)); ?>  - <?php echo date('H:i A', strtotime($jobs->service_to_time)); ?></td> 
                             
                            <td style="padding: 10px 10px;"><?php echo $jobs->description; ?></td>
                            <td align="center" style="padding: 10px 10px;"><?php echo $tot_hrs; ?></td>
                            <td align="center" style="padding: 10px 10px;"><?php echo $jobs->line_amount; ?></td>
                            <td align="center" style="padding: 10px 10px;"><?php echo $jobs->line_vat_amount; ?></td>
                            <td align="center" style="padding: 10px 10px;"><?php echo $jobs->line_net_amount; ?></td>
                    </tr>
                    <?php
					$i++; } ?> 
       </table>
       
      

       </div>
       
       
       
       <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">
       
       
               <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffefef" style="font-family: Roboto, sans-serif; font-size:18px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">
               
               
               <tr style="font-size: 14px; color: #333;">
                  <td  valign="middle" style="padding: 5px 0px 0px 20px;">Net Amount</td>
                  <td  align="right" valign="middle" style="padding: 5px 20px 0px 0px; font-size: 17px;"> <?php echo $invoice_detail[0]->invoice_total_amount; ?>  <span style="font-size:11px; ">AED</span></td>
                </tr> 
                
                
                <tr style="font-size: 14px; color: #333;">
                  <td  valign="middle" style="padding: 0px 0px 10px 20px;">Vat 5%</td>
                  <td  align="right" valign="middle" style="padding: 0px 20px 10px 0px; font-size: 17px;"> <?php echo $invoice_detail[0]->invoice_tax_amount; ?> <span style="font-size:11px; ">AED</span></td>
                </tr> 
              
                
              </table>
       
       
       </div>
       
       
       
       <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">
       
       
               <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#00a8de" style="font-family: Roboto, sans-serif; font-size:14px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">
               
               
 
                <tr>
                  <td  valign="middle" style="padding: 10px 0px 0px 20px;">Total Amount</td>
                  <td  align="right" valign="middle" style="font-size: 20px; padding: 10px 20px 0px 0px;"> <?php echo $invoice_detail[0]->invoice_net_amount; ?> <span style="font-size:11px; ">AED</span></td>
                </tr> 
                
                
                <tr>
                  <td colspan="2" align="right"  valign="middle" style="padding: 0px 20px 10px 20px; font-size: 13px;">AED <strong><?php echo numberTowords($invoice_detail[0]->invoice_net_amount); ?></strong> Only</td>
                 </tr> 
              </table>
       
       
       
       
       </div>
       
       
             
       <div class="bot-text" style="width:100%; height:auto; padding: 200px 0px 30px 0px; margin: 0px;">
               
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">Accountant Signature :</p>
                    </td>
                    <td align="right" valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">
                            Customer Signature
                        </p>
                    </td>
                  </tr>
                </table>

          </div>
             
          
  
  
  </div>
      
      <div class="footer" style="width:100%; height:auto; padding: 5px 0px; background: #ffe0e0; margin: 0px; text-align:center;">
               <a href="https://servicemaster.ae/" target="_blank" style="font-family: Roboto, sans-serif; font-size:12px; color: #555; line-height: 16px; text-decoration: none; padding: 0px 0px 0px 0px; margin: 0px;">www.servicemaster.ae</a>
          </div>
  
</div>

</body>
</html>
<?php
function numberTowords($num)
{
	$ones = array(
		0 =>"Zero",
		1 => "One",
		2 => "Two",
		3 => "Three",
		4 => "Four",
		5 => "Five",
		6 => "Six",
		7 => "Seven",
		8 => "Eight",
		9 => "Nine",
		10 => "Ten",
		11 => "Eleven",
		12 => "Twelve",
		13 => "Thirteen",
		14 => "Fourteen",
		15 => "Fifteen",
		16 => "Sixteen",
		17 => "Seventeen",
		18 => "Eighteen",
		19 => "Nineteen",
		"014" => "Fourteen"
	);
	$tens = array( 
		0 => "Zero",
		1 => "Ten",
		2 => "Twenty",
		3 => "Thirty", 
		4 => "Forty", 
		5 => "Fifty", 
		6 => "Sixty", 
		7 => "Seventy", 
		8 => "Eighty", 
		9 => "Ninety" 
	); 
	$hundreds = array( 
		"Hundred", 
		"Thousand", 
		"Million", 
		"Billion", 
		"Trillion", 
		"Quardrillion" 
	); /*limit t quadrillion */
	$num = number_format($num,2,".",","); 
	$num_arr = explode(".",$num); 
	$wholenum = $num_arr[0]; 
	$decnum = $num_arr[1]; 
	$whole_arr = array_reverse(explode(",",$wholenum)); 
	krsort($whole_arr,1); 
	$rettxt = ""; 
	foreach($whole_arr as $key => $i)
	{
		while(substr($i,0,1)=="0")
			$i=substr($i,1,5);
		if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
		}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
		} 
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		}
	} 
if($decnum > 0){
$rettxt .= " and ";
if($decnum < 20){
$rettxt .= $ones[$decnum];
$rettxt .= " Phills";
}elseif($decnum < 100){
$rettxt .= $tens[substr($decnum,0,1)];
if(substr($decnum,1,1) > 0)
{
$rettxt .= " ".$ones[substr($decnum,1,1)];
}
$rettxt .= " Phills";
}
}
return $rettxt;
}
?>