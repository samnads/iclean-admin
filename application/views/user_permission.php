<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-globe"></i>
                <h3>Edit User - <?php echo $user->user_fullname?></h3> 
                <a href="<?php echo base_url() . 'users/'; ?>" style="float:right ; margin-right:15px; cursor:pointer;"><i class="icon-tasks"></i><span></span> </a>
            </div>

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">   
                        <form class="permission-list" method="post" action="<?php echo base_url() . 'users/permissions/' . $user_id?>">
                            <fieldset>
                                <ul>
                                         <?php if($this->session->flashdata('success_msg') || $this->session->flashdata('error_msg')) {?>
                                    <li>
                                <div class="control-group">                                                            
                                    <div class="controls">
                                        <div class="alert <?php echo $this->session->flashdata('success_msg') ? 'alert-success' : ''; ?>">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <?php echo $this->session->flashdata('success_msg') ? $this->session->flashdata('success_msg')  : $this->session->flashdata('error_msg') ; ?>
                                        </div>
                                    </div>
                                </div>
                                    </li>
                                <?php } ?>
                                    <li>
                                        <div class="control-group no-bottom-margin">
                                    <div class="controls">
                                        <label class="radio inline permission-select-all"><input type="checkbox" id="select-all" name="selectall"> Select All</label>
                                    </div> 			
                                </div> 
                                    </li>
                                    
                                          <?php 
                                foreach ($modules as $module) {
                                    //print_r($e_user);exit;
                                    echo '<li><div class="control-group" >';
                                    echo '<div class="controls">';
                                    echo '<label class="radio inline ">';
                                    echo form_checkbox(array('name' => 'permissions[]', 'value' => $module['module_id'], 'id' => 'parent-' . $module['module_id'], 'tabindex' => 3, 'class' => 'parent', 'checked' => set_checkbox('permissions', 1, !empty($user_modules) ? (in_array($module['module_id'], $user_modules) ? 1 : 0) : 0)));
                                    echo '  '.(ucwords($module['module_name']));
                                    echo '</label></div> 			
                                 </div> </li>';
                                   
                                   
                                   if (isset($module['sub_modules'])) {
                                       foreach ($module['sub_modules'] as $sub_module) {
                                           
                                            echo '<li><div class="control-group">';
                                            echo '<div class="controls">';
                                            echo '<label class="radio inline">';
                                            echo form_checkbox(array('name' => 'permissions[]', 'value' => $sub_module['module_id'], 'id' => 'child-' . $sub_module['module_id'], 'tabindex' => 3, 'class' => 'child-' . $sub_module['parent_id'], 'onclick' => 'checkChildModules(this)', 'checked' => set_checkbox('permissions', 1, !empty($user_modules) ? (in_array($sub_module['module_id'], $user_modules) ? 1 : 0) : 0)));
                                            echo '  '.(ucwords($sub_module['sub_module']));
                                            echo '</label></div> </div></li> ';
                                       }
                                   }
                                   
                               }
                               ?>
                                        
                                    <div class="clear"></div>

                                </ul>
                               
                                
                               
                              
                                <div class="form-actions">
                                    <input type="submit" class="btn btn-primary pull-right" value="Submit" name="update_user">

                                </div> 
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</div>