<div class="row">

    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Admin Users</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>users/add"><img src="<?php echo base_url(); ?>img/add.png" title="Add User"/></a>
            </div>            
            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl.No. </th>
                            <th style="line-height: 18px; text-align: center;">Name </th>
                            <th style="line-height: 18px; text-align: center;"> Is Admin?</th>
                            <th style="line-height: 18px; text-align: center;">User Name </th>
                            <th style="line-height: 18px; text-align: center;"> Last Logged in</th>
                            <th style="line-height: 18px; text-align: center;"> Last Logged in IP </th>
                            <th style="line-height: 18px; text-align: center; width: 30px;"> Status</th>
                            <th style="line-height: 18px; text-align: center;" class="td-actions">Permission</th>
                            <th style="line-height: 18px; text-align: center;" class="td-actions2">Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($users) > 0) {
                            $i = 1;
                            foreach ($users as $user) {
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px; text-align: center;"><?php echo $i; ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->user_fullname ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->is_admin ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->username ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->last_loggedin_datetime ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->last_loggedin_ip ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->user_status == 1 ? '<sapn style="color:rgb(35, 169, 35); border-radius:1px; font-size:18px; cursor: pointer;"><i class="btn-icon-only icon-ok-sign"> </i></span>' : '<sapn style="color:rgb(244, 55, 55); border-radius:1px; font-size:18px; cursor: pointer;"><i class="btn-icon-only icon-remove-sign"> </i></span>'; ?></td>
                                    <td style="line-height: 18px; width: 110px;" class="td-actions ">
                                    <center>                                        
                                        <!--<a class="btn btn-small btn-warning" title="Edit" href="<?php// echo base_url() . 'users/edit/' . $user->user_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>-->
                                        <a class="btn btn-small btn-info" title="Permissions" href="<?php echo base_url() . 'users/permissions/' . $user->user_id ?>"><i class="btn-icon-only icon-key" style="font-size: 13px;"> </i></a>
                                    </center>

                                    </td>
                                    <td style="line-height: 18px; text-align: center;">
                                        <a class="btn btn-small btn-warning" title="Edit" href="<?php echo base_url() . 'users/add/' . $user->user_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>
                                        <a class="btn btn-small btn-info" title="Login Info" href="<?php echo base_url() . 'users/userlogininfo/' . $user->user_id ?>"><i class="btn-icon-only icon-search" style="font-size: 13px;"> </i></a>
                                        <a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_active_user(<?php echo $user->user_id; ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                    </td>
                                </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>   

                    </tbody>
                </table>                 
            </div>            
        </div>        
    </div>

</div>

