<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" id="edit-profile">
                    <i class="icon-th-list"></i>
                    <h3>Maid Hours & Revenue Report</h3>                   
                   
                    <input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="from_date" value="<?php echo $activity_date ?>">                       
                    <input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="schedule_date" name="to_date" value="<?php echo $activity_date_to ?>">                       
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">                    
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Hours</th> 
                            <th style="line-height: 18px;"> Revenue</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($maid_hours_report))
                        {
                            $i = 0;
                            $total_hrs=0;
                            foreach ($maid_hours_report as $report)
                            {
                                $maid_time=explode(":",$report->hours);
                                $maid_time=$maid_time[0]+($maid_time[1]/60);
                                $total_hrs+=$maid_time;
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                                        . '<td style="line-height: 18px;"> AED ' . $report->revenue . '</td>'
                                    .'</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script  type="text/javascript">
     $('#edit-profile').submit(function(e) {
        var from = document.getElementById("vehicle_date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("schedule_date").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
     </script>
