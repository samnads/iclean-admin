<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/revenue_report' ?>">
                    <i class="icon-th-list"></i>
                    <h3>Revenue Report</h3> 
					<span style="margin-left:23px; color: #fff;">Select Month :</span>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;"  name="month" id="month">
                        <option >-- Select Month --</option>
                        <option value="01" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '01') ? 'selected="selected"' : '';
                        }
                        ?>>January</option>
                        <option value="02" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '02') ? 'selected="selected"' : '';
                        }
                        ?>>February</option>
                        <option value="03" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '03') ? 'selected="selected"' : '';
                        }
                        ?>>March</option>
                        <option value="04" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '04') ? 'selected="selected"' : '';
                        }
                        ?>>April</option>
                        <option value="05" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '05') ? 'selected="selected"' : '';
                        }
                        ?>>May</option>
                        <option value="06" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '06') ? 'selected="selected"' : '';
                        }
                        ?>>June</option>
                        <option value="07" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '07') ? 'selected="selected"' : '';
                        }
                        ?>>July</option>
                        <option value="08" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '08') ? 'selected="selected"' : '';
                        }
                        ?>>August</option>
                        <option value="09" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '09') ? 'selected="selected"' : '';
                        }
                        ?>>September</option>
                        <option value="10" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '10') ? 'selected="selected"' : '';
                        }
                        ?>>October</option>
                        <option value="11" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '11') ? 'selected="selected"' : '';
                        }
                        ?>>November</option>
                        <option value="12" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '12') ? 'selected="selected"' : '';
                        }
                        ?>>December</option>

                    </select>
                    <span style="margin-left:23px; color: #fff;">Select Year :</span>
                    <select style="margin-top: 6px; width:160px; margin-bottom: 9px;" id="year" name="year">
                        <option value="">-- Select Year --</option>
                        <?php
                        for ($i = 2014; $i <= 2025; $i++) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php
                            if (!empty($search_condition)) {
                                echo ($search_condition['year'] == $i) ? 'selected="selected"' : '';
                            }
                            ?>><?php echo $i; ?></option>

                            <?php
                        }
                        ?>
                    </select>
                    <input type="submit" class="btn" value="Go" name="revenuesubmit" style="margin-bottom: 4px;">
					<div class="topiconnew"><a href="<?php echo base_url(); ?>reports/revenuetoExcel/<?php echo $search_condition['month']; ?>/<?php echo $search_condition['year']; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Export revenue"/></a></div>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Cleaner</th>
                            <th style="line-height: 18px">Mobile</th>
                            <th style="line-height: 18px">Total Hours</th>
                            <th style="line-height: 18px">Total Revenue</th>
                            <th style="line-height: 18px">Avg Price/Hr</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
						if(!empty($reports))
						{
							$i = 1;
							foreach($reports as $val)
							{
								$avg = number_format(($val['amount']/$val['hrs']),2);
						?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?></td>
							<td style="line-height: 18px;"><?php echo $val['maidname']; ?></td>
							<td style="line-height: 18px;"><?php echo $val['mobile']; ?></td>
							<td style="line-height: 18px;"><?php echo $val['hrs']; ?></td>
							<td style="line-height: 18px;"><?php echo $val['amount']; ?></td>
							<td style="line-height: 18px;"><?php echo $avg; ?></td>
						</tr>
						<?php
							$i++;
							}
						}
						?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>