<html>
    <head></head>
    <body>
        <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:800px!important">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;border:0">
                    <tr>
                        <td colspan="2"><img align="left" alt="" src="<?php echo base_url(); ?>images/elitemaidemaidbanner.jpg" width="794" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; text-align: center;">
                            <br />
                            <strong>Dear Admin,</strong><br /><br />
                            A Payment of <?php echo $amount; ?> has been received via online payment...<br />
                            <span style=" font-size: 18px; color:#FFF; background: #78c056; padding: 7px 30px; border-radius: 25px; margin: 20px 0px 40px 0px; display: inline-block;">Reference ID - <?php echo $bookingdetails[0]->reference_id;?></span> 
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td valign="top" style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Address Summary</b><br></span>
                            Name - <i> <?php echo $name; ?> </i><br />
                            Address - <i>Building : <?php echo $bookingdetails[0]->building; ?></i><br>
                            <i>Unit : <?php echo $bookingdetails[0]->unit_no; ?></i><br>
                            <i>Street : <?php echo $bookingdetails[0]->street; ?></i><br>
                            Area - <i> <?php if($bookingdetails[0]->area_name == 'Other') { echo $bookingdetails[0]->other_area; } else { echo $bookingdetails[0]->area_name; } ?><!--<?php// echo $bookingdetails[0]->area_name; ?>--> </i><br />
                            Email ID - <i> <?php echo $bookingdetails[0]->email_address; ?> </i><br />
                            Mobile - <i> <?php echo $bookingdetails[0]->mobile_number_1; ?> </i><br />
                        </td>
					</tr>
					<tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Booking Summary</b><br></span>
                            
                            No of Maids-<i><?php echo $bookingdetails[0]->no_of_maids ?></i><br />
							Instructions - <i><?php echo $bookingdetails[0]->booking_note; ?></i><br />
							Crew In - <i><?php echo $bookingdetails[0]->crew_in; ?></i><br /><br />
							<span style="line-height:30px;"><b>Service Date & Time</b><br></span>
							<?php
							$clean_fee = 0;
							$service_charge = 0;
							$vat_charge = 0;
							$tot_service = 0;
							foreach ($bookingdetails as $details)
							{
								//$clean_fee += $details->cleaning_material_fee;
								$service_charge += $details->service_charge;
								$vat_charge += $details->vat_charge;
								$tot_service += $details->total_amount;
							?>
							<b><?php echo $details->service_start_date; ?> (One Day) <?php echo "- ".date('l', strtotime($details->service_start_date));  ?></b><br /><br />
							<b><?php echo date('h:i a', strtotime($details->time_from)) . ' - ' . date('h:i a', strtotime($details->time_to)) ?></b><br /><br />
							<?php
							}
							?>
							
							
                            <!--<div style="width: 100%; padding: 0px 0px 0px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Service Date & Time</strong></div>
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;">
                                    <?php
                                    // $clean_fee = 0;
                                    // $service_charge = 0;
                                    // $vat_charge = 0;
                                    // $tot_service = 0;
                                    // foreach ($bookingdetails as $details)
                                    // {
                                        //$clean_fee += $details->cleaning_material_fee;
                                        // $service_charge += $details->service_charge;
                                        // $vat_charge += $details->vat_charge;
                                        // $tot_service += $details->total_amount;
                                    ?>
                                    <div style="width: 100%; padding: 0px 0px 20px 0px;">
                                        <div style="width: 100%;"><strong><?php// echo $details->service_start_date; ?> (One Day)</strong></div>
                                        <div style="width: 100%; font-size:12px; padding: 0px 0px 0px 0px;"><?php// echo $details->start_time . ' - ' . $details->end_time ?></div>
                                    </div>
                                    <?php
                                    //}
                                    ?>
                                </div>
                                <div style="clear:both"></div>
                            </div>-->
                        </td>
                    </tr>
                    <?php
                    if($bookingdetails[0]->cleaning_material=="Y"): ?>
                    <tr>
                        <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #78c056; text-align: center; padding: 20px 0px 0px 0px;"><strong>Customer has requested for cleaning materials.</strong></td>
                    </tr>
                    <?php endif; ?>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 25px;">
                        <?php
                        if($bookingdetails[0]->interior_window_clean == 1)
                        {
                            $intrior = "Yes";
                        } else {
                            $intrior = "No";
                        }
                        if($bookingdetails[0]->fridge_cleaning  == 1)
                        {
                            $fridge = "Yes";
                        } else {
                            $fridge = "No";
                        }
                        if($bookingdetails[0]->ironing_services  == 1)
                        {
                            $ironing = "Yes";
                        } else {
                            $ironing = "No";
                        }
                        if($bookingdetails[0]->oven_cleaning  == 1)
                        {
                            $oven = "Yes";
                        } else {
                            $oven = "No";
                        }
                        ?>
                        <?php
						if($bookingdetails[0]->interior_window_clean == 1 || $bookingdetails[0]->fridge_cleaning == 1 || $bookingdetails[0]->ironing_services == 1 || $bookingdetails[0]->oven_cleaning == 1)
						{
						?>
                        <td style="padding: 0px 0px 20px 20px;">
                            <b>Extra Services : </b><br>
							<?php
							if($bookingdetails[0]->interior_window_clean == 1)
							{
							?>
                            Interior Windows - <b><?php echo $intrior; ?></b><br>
							<?php } ?>
							<?php
							if($bookingdetails[0]->fridge_cleaning == 1)
							{
							?>
                            Fridge Cleaning - <b><?php echo $fridge; ?></b>&nbsp;<br>
							<?php
							}
							?>
							<?php
							if($bookingdetails[0]->ironing_services  == 1)
							{
							?>
                            Ironing - <b><?php echo $ironing; ?></b>&nbsp;<br>
							<?php } ?>
							<?php
							if($bookingdetails[0]->oven_cleaning  == 1)
							{
							?>
                            Oven Cleaning - <b><?php echo $oven; ?></b><br />
							<?php
							}
							?>
                        </td>
						<?php
						}
						?>
					</tr>
					<tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td style="padding: 20px 0px 20px 20px;">
                            <b>Price Summary</b><br>
                            No of Maids - <b><?php echo $bookingdetails[0]->no_of_maids; ?></b><br>
                            Total Hours - <b><?php echo $bookingdetails[0]->no_of_hrs ?></b>&nbsp;<br>
							Payment Method - <b><?php if($bookingdetails[0]->pay_by == 'card') { echo 'Card'; } else { echo 'Cash'; } ?></b>&nbsp;<br /><br /><br />
<!--                            
                                <?php
                            //if($bookingdetails[0]->cleaning_material=="Y")
                            //{
                            ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Cleaning Material Cost</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php// echo $clean_fee; ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <?php// } ?>
                            -->
                            <?php
                            //$service_charges = ($service_charge + $coupon_fee);
                            //$service_charges = ($service_charge);
                            if($coupon_fee > 0)
                            {
                            ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Service Cost</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($service_charge, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Discount(Coupon used)</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($coupon_fee, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <?php    
                            }
                            if($coupon_fee > 0)
                            {
								$fe = ($service_charge - $coupon_fee);
                            ?>
							<div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Total Cost</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($fe, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
							<?php
							} else {
							?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Total Cost</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($service_charge, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
							<?php } ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>VAT Cost</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($vat_charge, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Net Payable</strong></div>
                                <div style="width: 70%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">BHD <strong><?php echo number_format($tot_service, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                        </td>
					</tr>
					<tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
						<td style="padding: 0px 0px 20px 20px;">
                            <b>Payment Received : </b><br>
                            Amount - <b><?php echo $amount; ?></b><br>
                            Transaction No - <b><?php echo $transaction_id; ?></b>&nbsp;<br>
                            Payment date - <b><?php echo $payment_datetime; ?></b>&nbsp;<br>
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 18px;">
                        <td style="padding: 20px 0px 20px 20px;">
                            <span style="line-height:30px;"><b>Elite Maids Cleaning Services</b><br></span>
                            Office 201B, Prime Business Center,<br />
						  Jumeirah Village Circle, Dubai,<br />
						  United Arab Emirates.<br />
						  For Bookings : +971 800 258 / +971 58 286 4783<br />
						  Email : info@elitemaids.ae<br>                                          
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #555; line-height: 18px; text-align:center; ">
                        <td colspan="2" style="padding: 20px 0px 20px 0px">© <?php echo date('Y'); ?> Elite Maids All Rights Reserved.</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </center>
        </div>
    </body>
</html>