<style>
.tabbable .nav-tabs > li > a{ padding: 7px 12px; }
#delmul{border: 1px solid #FF313E;background: #FF313E;}
</style>
<div class="row">
    <div class="span12">      		
      <div class="widget">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Customer</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>customers"><img src="<?php echo base_url(); ?>img/male-list.png" title="Customer List"/></a>                
                <a style="float:right ; margin-right:5px; cursor:pointer;" href="<?php echo base_url() . 'customer/edit/' . $customer_id; ?>"><img src="<?php echo base_url(); ?>images/customer-edit.png" title="Edit Customer"  width="28" height="32"/> </i></a>
                <?php
                if(user_authenticate() == 1)
                {
                ?>
<a style="float:right ; margin-right:5px; cursor:pointer;height: 23px;width: 23px;padding: 0px;margin-top: 7px;" href="<?php echo base_url() . 'customer/disable/' . $customer_id . '/' . $customer_status; ?>" class="<?php echo $customer_status == 1 ? 'btn btn-danger btn-small' : 'btn btn-success btn-small'; ?>" title="<?php echo $customer_status == 1 ? 'Delete' : 'Recover'; ?>" ><?php echo $customer_status == 1 ? '<i class="btn-icon-only icon-trash" style="margin: 2px;color:#fff"> </i>' : '<i style="margin: 2px;" class="btn-icon-only icon-ok"> </i>' ?></a>
                <?php
                }
                ?>
            </div> <!-- /widget-header -->
            <div class="widget-content no-top-padding">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="personal-tab active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                        <li><a href="#account" data-toggle="tab">Account and Other Details</a></li>
                        <li><a href="#maid_history" data-toggle="tab">Maid history</a></li>
                        <li><a href="#payment_history" data-toggle="tab">Payment History</a></li>
						<!--<li><a href="#call_history" data-toggle="tab">Call History</a></li>-->
                        <li><a href="#current-service" data-toggle="tab">Current Services</a></li>
                        <li><a href="#pause-booking" data-toggle="tab">Pause Booking</a></li>
                        <li><a href="#booking-history" data-toggle="tab">Booking History</a></li>
                        <li class="booking-tab"><a href="#booking" data-toggle="tab">Booking</a></li>
                        <li><a href="#sent-mail" data-toggle="tab">Send Mail</a></li>
                        <li><a href="#pay-history" data-toggle="tab" id="paylinksec">Payment Link History</a></li>
                        <li><a href="#customer-statement" data-toggle="tab">Statement</a></li>
                    </ul>
                    
                    <form id="edit-profile" class="form-horizontal" method="post">      
                        <div class="tab-content">

                            <?php
                            //if (count($customer_details) > 0) {
                            foreach ($customer_details as $customer_val) {
                                ?>                            

                                <div class="tab-pane active" id="personal">
                                    <fieldset>            
                                        <div class="span12" style="width: 100%;margin-left: 0px;">
                                            <div class="widget" style="border: 0px;"> 
    <!--                                            <a style="float:right ; margin-right:20px; cursor:pointer;"><img id="customer_photo" src="<?php //echo base_url();   ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> </a>-->                                          
                                                <div class="widget widget-table action-table">
                                                    <div class="widget-header"> <i class="icon-th-list"></i>
                                                        <h3>Personal Details</h3> 
                                                    </div>
                                                    <!-- /widget-header -->


                                                    <div class="widget-content">
                                                        <?php
                                                        if ($customer_val['customer_photo_file'] == "") {
                                                            $image = base_url() . "img/no_image.jpg";
                                                        } else {
                                                            $image = base_url() . "customer_img/" . $customer_val['customer_photo_file'];
                                                        }
                                                        if ($customer_val['customer_type'] == "HO") {
                                                            $cust_type = "Home";
                                                        } else if ($customer_val['customer_type'] == "OF") {
                                                            $cust_type = "Office";
                                                        } else if ($customer_val['customer_type'] == "WH") {
                                                            $cust_type = "Warehouse";
                                                        } else if ($customer_val['customer_type'] == "") {
                                                            $cust_type = "";
                                                        }
                                                        if($customer_val['customer_booktype'] == 0) {
                                                            $customerbooktype = "Non Regular";
                                                        } else if($customer_val['customer_booktype'] == 1) {
                                                            $customerbooktype = "Regular";
                                                        }
                                                        if ($customer_val['payment_type'] == "D") {
                                                            $pay_type = "Daily Paying";
                                                        } else if ($customer_val['payment_type'] == "W") {
                                                            $pay_type = "Weekly Paying";
                                                        } else if ($customer_val['payment_type'] == "M") {
                                                            $pay_type = "Monthly Paying";
                                                        } else if ($customer_val['payment_type'] == "") {
                                                            $pay_type = "";
                                                        }
                                                        
                                                        if($customer_val['is_company'] == "Y") {
                                                            $iscompany = "YES";
                                                        } else if($customer_val['is_company'] == "N") {
                                                            $iscompany = "NO";
                                                        }
														
														if($customer_val['is_flag'] == "Y") {
                                                            $isflag = "YES";
                                                        } else if($customer_val['is_flag'] == "N") {
                                                            $isflag = "NO";
                                                        }
                                                        ?>
														<input type="hidden" value="<?php echo $customer_val['customer_name'] ?>" id="hiddencustomername" />
														<input type="hidden" value="<?php echo $customer_val['mobile_number_1'] ?>" id="hiddenmobile" />
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="line-height: 18px; width: 200px"><b>Customer Name</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['customer_name'] ?></td>  
                                                                    <td style="line-height: 18px; width: 200px"><b>Customer Nick Name</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['customer_nick_name'] ?></td> 
                                                                    <td style="line-height: 18px; width: 200px" rowspan="7"><center><img src="<?php echo $image ?>" style="height: 150px; width: 150px"/></center></td> 
                                                            </tr>
															<tr>
                                                                    <td style="line-height: 18px; width: 200px"><b>Flag Status</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $isflag ?></td>  
                                                                    <td style="line-height: 18px; width: 200px"><b>Flag Reason</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['flag_reason'] ?></td>
                                                            </tr>
                                                            <tr>

                                                                <td style="line-height: 18px; width: 200px"><b>Contact Person</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['contact_person'] ?></td> 
                                                                <td style="line-height: 18px; width: 200px"><b>Phone</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['phone_number'] ?></td>

                                                            </tr>
                                                            <tr>

                                                                <td style="line-height: 18px; width: 200px"><b>Mobile Number 1</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['mobile_number_1'] ?></td>
                                                                <td style="line-height: 18px; width: 200px"><b>Mobile Number 2</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['mobile_number_2'] ?></td>
                                                            </tr>
                                                            <tr>


                                                                <td style="line-height: 18px; width: 200px"><b>Mobile Number 3</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['mobile_number_3'] ?></td>
                                                                <td style="line-height: 18px; width: 200px"><b>Fax</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['fax_number'] ?></td>  
                                                            </tr>
                                                            
                                                            <tr>


                                                                <td style="line-height: 18px; width: 200px"><b>Is Company?</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $iscompany ?></td>
                                                                <td style="line-height: 18px; width: 200px"><b>Company Name</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['company_name'] ?></td>  
                                                            </tr>

                                                            <tr>

                                                                <td style="line-height: 18px; width: 200px"><b>User Name</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['customer_username'] ?></td>
                                                                <td style="line-height: 18px; width: 200px"><b>Password</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['customer_password'] ?></td>
                                                            </tr>                                                            
                                                            <tr>
                                                                <td style="line-height: 18px; width: 200px"><b>Customer ID</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['customer_id'] ?></td>   
                                                                <td style="line-height: 18px; width: 200px"><b>Email</b></td>
                                                                <td style="line-height: 18px; width: 300px"><?php echo $customer_val['email_address'] ?></td>
                                                            </tr>                                                           
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /widget-content --> 

                                                    <div class="widget-header"> <i class="icon-th-list"></i>
                                                        <h3>Customer Address</h3> 
                                                    </div>
                                                    <div class="widget-content">

                                                        <table class="table table-striped table-bordered">

                                                            <tbody>
                                                                <tr>
                                                                    <td style="line-height: 18px; width: 50px"><b>Sl No.</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><b>Area</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><b>Zone</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><b>Address</b></td>
                                                                </tr>
                                                                <?php
                                                                if (count($customer_address) > 0) {
                                                                    $j = 1;
                                                                    foreach ($customer_address as $customer_address_val) {
																		if($customer_address_val['customer_address'] == "")
																		{
																			$cust_address = "Building: ".$customer_address_val['building'].", Unit No: ".$customer_address_val['unit_no'].", Street : ".$customer_address_val['street'];
                                                                        } else {
																			$cust_address = $customer_address_val['customer_address'];
																		}
																		?> 
                                                                        <tr>
                                                                            <td style="line-height: 18px; width: 50px"><b><?php echo $j++; ?></b></td>

                                                                            <td style="line-height: 18px; width: 300px"><?php echo $customer_address_val['area_name'] ?></td> 

                                                                            <td style="line-height: 18px; width: 300px"><?php echo $customer_address_val['zone_name'] ?></td> 

                                                                            <td style="line-height: 18px; width: 300px"><?php echo $cust_address; ?></td>
                                                                        </tr>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </tbody>
                                                        </table>



                                                    </div><!-- /widget-content --> 


                                                </div> 
                                            </div> <!-- /widget -->
                                        </div> <!-- /span6 -->

                                    </fieldset>  

                                </div>
                                <div class="tab-pane" id="account">
                                    <fieldset>            
                                        <div class="span12" style="width: 100%;margin-left: 0px;">
                                           <div class="widget" style="border: 0px;">
                                                <div class="widget widget-table action-table">
    <!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                        <h3>Account and Other Details</h3> 
                                                    </div>-->
                                                    <!-- /widget-header -->
                                                    <div class="widget-content">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>

                                                                    <td style="line-height: 18px; width: 200px"><b>Payment Type</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $pay_type ?></td>
                                                                    <td style="line-height: 18px; width: 200px"><b>Customer Book Type</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customerbooktype ?></td>

                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 18px; width: 200px"><b>Customer Type</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $cust_type ?></td>
                                                                    <td style="line-height: 18px;"><b>Payment Mode</b></td>
                                                                    <td style="line-height: 18px;"><?php echo $customer_val['payment_mode'] ?></td>



                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 18px; width: 200px"><b>Latitude</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['latitude'] ?></td>
                                                                    <td style="line-height: 18px; width: 200px"><b>Longitude</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['longitude'] ?></td>
                                                                    


                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 18px;"><b>Hourly</b></td>
                                                                    <td style="line-height: 18px;"><?php echo $customer_val['price_hourly'] ?></td>
                                                                    <td style="line-height: 18px;"><b>Weekly</b></td>
                                                                    <td style="line-height: 18px;"><?php echo $customer_val['price_weekend'] ?></td>


                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 18px; width: 200px"><b>Key Given</b></td>
                                                                    <td style="line-height: 18px; width: 300px"><?php echo $customer_val['key_given'] == 'Y' ? 'Yes' : 'No' ?></td>
                                                                    <td style="line-height: 18px;"><b>Extras</b></td>
                                                                    <td style="line-height: 18px;"><?php echo $customer_val['price_extra'] ?></td>
                                                                    
                                                                    <!--<td style="line-height: 18px;" colspan="2"><b><center>Notes</center></b></td>-->

                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 18px;"><b>Pending Amount</b></td>
                                                                    <td style="line-height: 18px; color:<?php if($customer_val['signed'] == 'Cr') echo '#081775'; else echo '#980407'; ?>; font-weight:bold;"><?php echo $customer_val['balance'].$customer_val['signed'] ?></td>
                                                                    <td style="line-height: 18px;"><b>Notes</b></td>
                                                                    <td style="line-height: 18px;"><?php echo $customer_val['customer_notes'] ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /widget-content --> 
                                                </div> 
                                            </div> <!-- /widget -->
                                        </div> <!-- /span6 -->

                                    </fieldset>                   
                                </div>
                                <?php
                            }
                            //}
                            ?>
                            <div class="tab-pane" id="maid_history">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                      <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">
<!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Maid History</h3> 
                                                </div>-->
                                                <!-- /widget-header -->
                                                <div class="widget-content">
<!--                                                    <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">-->
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                                                        <th style="line-height: 18px"> <center>Maid Name </center></th>
                                                        <th style="line-height: 18px"> <center>Date</center></th>
                                                        <th style="line-height: 18px"> <center>Shift</center></th>
                                                        <th style="line-height: 18px"> <center>Status</center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($maid_history)) {
                                                                $i = 0;
                                                                foreach ($maid_history as $m_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->service_date) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->start_from_time) . '-' . html_escape($m_history->end_to_time) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($m_history->service_status) . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>

                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>            
                            </div>
                            <div class="tab-pane" id="payment_history">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                       <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">
<!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Payment History</h3> 
                                                </div>-->
                                                <!-- /widget-header -->
                                                <div class="widget-content">
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                                                        <th style="line-height: 18px"> <center>Date </center></th>
                                                        <th style="line-height: 18px"> <center>Shift</center></th>
                                                        <th style="line-height: 18px"> <center>Maid Name</center></th>
                                                        <th style="line-height: 18px"> <center>Extra Hours</center></th>
                                                        <th style="line-height: 18px"> <center>Payment</center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php
                                                            if (!empty($payment_history)) {
                                                                $i = 0;
                                                                foreach ($payment_history as $p_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>

                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->service_date) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->start_from_time) . '-' . html_escape($p_history->end_to_time) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($p_history->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : right;">0</td>    
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : right;">' . html_escape($p_history->paid_amount) . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="6"><center>No Records!</center></td>';
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>             
                            </div>
							
							<div class="tab-pane" id="call_history">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">
<!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Maid History</h3> 
                                                </div>-->
                                                <!-- /widget-header -->
												<!--<div style="width: 300px; height: 30px; padding-bottom: 8px; margin-left: 764px;">
                                                    <input type="text" id="call-date-from" style="width: 150px;" data-date="<?php// echo $search_call_date_from; ?>" readonly value="<?php// echo $search_call_date_from; ?>" data-date-format="dd/mm/yyyy"/> 
                                                    <input type="button" class="save-but" id="btn-call-search" value="Search" style="float: right;" />
                                                </div>-->
                                                <div class="widget-content">
<!--                                                    <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">-->
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                                                        <th style="line-height: 18px"> <center>Customer Name </center></th>
                                                        <th style="line-height: 18px"> <center>Mobile</center></th>
                                                        <th style="line-height: 18px"> <center>Date</center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($call_history)) {
                                                                $i = 0;
                                                                foreach ($call_history as $c_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->customer_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->mobile_no) . '</td>
																			<td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->added_date_time) . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>

                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>             
                            </div>
							
                            <div class="tab-pane" id="current-service">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">
<!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Payment History</h3> 
                                                </div>-->
                                                <!-- /widget-header -->
                                                <div class="widget-content">
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                                                        <th style="line-height: 18px"> <center>Maid Name</center></th>
                                                        <th style="line-height: 18px"> <center>Photo</center></th>
                                                        <th style="line-height: 18px"> <center>Country</center></th>
                                                        <th style="line-height: 18px"> <center>Shift 
                                                            <input type="checkbox" class="del_checkall" style="display: inline;float:right;margin-top: 6px;">
                                                            <div style="float:right;cursor:pointer;padding: 0px;margin-right: 7px;display:none;" class="btn btn-danger btn-small " title="Delete" id="curbukdel_btn"><i class="btn-icon-only icon-trash" style="margin: 2px;color:#fff;"> </i></div>
                                                            </center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($current_service)) {
                                                                $i = 0;
                                                                foreach ($current_service as $service) {
                                                                    $path = './maidimg/thumb/'.$service->maid_photo_file;
                                                                    $path2 = './maidimg/'.$service->maid_photo_file;
                                                                    if (file_exists($path)) {
                                                                        $imgpath = base_url() . 'maidimg/thumb/' . html_escape($service->maid_photo_file);
                                                                    } elseif (file_exists($path2)){
                                                                        $imgpath = base_url() . 'maidimg/' . html_escape($service->maid_photo_file);
                                                                    } else {
                                                                        $imgpath = base_url() . 'img/no_image.jpg';
                                                                    }
                                                                    //$filename = $service->maid_photo_file;
                                                                    //$color = substr($service->shifts, 0,2) == 'OD' ? '#43ACB0' :'#ff7223';
                                                                    $color = $service->booking_type == 'OD' ? '#ff7223' : '#9e6ab8';
                                                                    //$service->shifts = str_replace(substr($service->shifts, 0,2) .'_', "", $service->shifts);
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>

                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($service->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><img style="width:50px; height:50px;" src="' . $imgpath . '" /></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($service->maid_nationality) . '</td>

                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center; color:#ffffff;">' . ($service->shifts) . '</td>
                                                                                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>             
                            </div>




                            <style type="text/css">

                                .new-form-main { width: 100%; height: auto; padding: 0px 0px 20px 0px;}
                                .new-form-sub-box { width: 100%; height: auto; float: left; padding: 15px 0px 0px 15px;}
                                .div_left{ width: 50%; height: auto; float: left; padding: 0px 0px 0px 0px;}
                                .div_left-cont { width: 100%; height: auto; float: left; padding: 0px 0px 15px 0px;}
                                .div_right { width: 50%; height: auto; float: right; padding: 0px 0px 0px 0px;}

                                .new-form-sub-cont { width: 30%; height: auto; float: left; padding: 0px 0px 0px 0px;}
                                .new-form-sub-field { width: 30%; height: auto; float: left; padding: 0px 0px 0px 0px;}

                                .new-radio-sub-box { width: 25%; height: auto; float: left; padding: 0px 0px 0px 0px;}



                                /*
                                * Date - 12-09-16 : Date picker Calender style
                                */

                                .active.day { background: #00a642; color: #fff; }
                                .old.disabled.day { color: #CCC;}
                                .new.day { color: #CCC;}
                                .datepicker { padding: 5px 15px;}
                                .disabled.day {  color :#8f8f8f;}

                                #repeat-days-search,#repeat-ends-search { display: none;}

                                .cell3 > label { float: left; padding-right: 16px;}

                                input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] { background-color: #fff;}
                            </style>


                            <!--Booking -->

                            <div class="tab-pane" id="booking">




                                <!--New starts-->
                                <fieldset>     

                                    <input type="hidden" id="cust_id" name="cust_id"  value="<?php echo $customer_id; ?>">
                                    <input type="hidden" id="cust_zone_id" name="cust_zone_id"  value="<?php echo $customer_zone_area_province->zone_id; ?>">
                                    <input type="hidden" id="cust_area_id" name="cust_area_id"  value="<?php echo $customer_zone_area_province->area_id; ?>">
                         <input type="hidden" id="cust_prov_id" name="cust_prov_id"  value="<?php echo $customer_zone_area_province->province_id; ?>">
                         <input type="hidden" id="booking_ID" name="booking_ID"  value="<?php if($book_ID != ""){ echo $book_ID; } ?>">
                         <?php if(@$justmop_areaid == "") { ?>
                         <input type="hidden" id="area_justmop" name="area_justmop" value="<?php echo $customer_zone_area_province->area_id; ?>">
                         <?php } else { ?> 
                         <input type="hidden" id="area_justmop" name="area_justmop" value="<?php echo $justmop_areaid; ?>">
                         <?php } ?>
                                    <input type="hidden" id="cust_add_id" name="cust_add_id"  value="<?php echo $customer_zone_area_province->customer_address_id; ?>">


                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">

                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Filter Maid Details</h3> 
                                                </div>
                                                <!-- /widget-header -->


                                                <div class="widget-content">


                                                    <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Select Date</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><input type="text"  name="booking_date" id="booking_date" value="<?php echo $start_service_date ?>" readonly/></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->
                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Time From</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="time_from" id="book-from-time" data-placeholder="Select">
                                                                                <!--<option value="">Select</option>-->
                                                                                <?php
                                                                                foreach ($times as $time_index => $time) {
                                                                                    $selected = $time->stamp == @$start_time_from ? 'selected="selected"' : '';
                                                                                    echo '<option value="' . $time->stamp . '" ' . $selected . ' >' . $time->display . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Repeat</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="booking_type" id="book-booking-type" data-placeholder="Select repeat type">
                                                                                <option value="">Select</option>
                                                                                <option value="OD" <?php if ($booking_type == "OD") { ?> selected="selected" <?php } ?>>Never Repeat - One Day Only</option>
                                                                                <option value="WE" <?php if ($booking_type == "WE") { ?> selected="selected" <?php } ?>>Every Week</option>
                                                                                <option value="BW" <?php if ($booking_type == "BW") { ?> selected="selected" <?php } ?>>Every 2 Week</option>
                                                                            </select>
                                                                        </div><!--new-form-sub-field end-->

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->


                                                                    <div class="new-form-sub-box" id="repeat-ends-search" <?php if ($booking_type == "WE" || $booking_type == "BW") { ?> style="display:inline;" <?php } ?>>
                                                                        <div class="cell1" style="float: left;width: 20%;"><span class="icon_btype"></span> Ends</div>
                                                                        <div class="cell2" style="float: left;width: 2%;">:</div>
                                        <div class="cell3" style="float: left; width: 78%;">
                                                <label class="mr15"><input type="radio" name="repeat_end" id="repeat-end-never" value="never" <?php if(@$service_end==0) { ?> checked="checked" <?php } ?> /> Never</label>
						<label><input type="radio" name="repeat_end" id="repeat-end-ondate" value="ondate" <?php if(@$service_end==1) { ?> checked="checked" <?php } ?> /> On</label> 
						<input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly disabled="disabled" value="<?php echo @$service_actual_end_date  ?>" />
                                        <div class="clear"></div><!--clear end-->
					</div>	

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div>   






                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Time To</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><select name="time_to" id="book-to-time" data-placeholder="Select">
                                                                                <!--<option value="">Select</option>-->
                                                                                <?php
                                                                                foreach ($times as $time_index => $time) {
                                                                                    $selected = $time->stamp == @$start_time_to ? 'selected="selected"' : '';
                                                                                    if ($time_index == 't-0') {
                                                                                        continue;
                                                                                    }
                                                                                    echo '<option value="' . $time->stamp . '" ' . $selected . '>' . $time->display . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
                                                            <tr>
																<td style="line-height: 18px; width: 500px">
																<div class="new-form-sub-box">
                                                                            <div class="new-form-sub-cont">Total Amount</div><!--new-form-sub-cont end-->
                                                                            <div class="new-form-sub-field">
																			<?php
																			if($total_amount != "")
																			{
																				$tot_amt = $total_amount;
																			} else {
																				$tot_amt = "";
																			}
																			?>
																				<input type="text" id="c_total_amt" name="c_total_amt"  value="<?php echo $tot_amt; ?>">
                                                                            </div><!--new-form-sub-field end-->

                                                                            <div class="clear"></div><!--clear end-->
                                                                        </div><!--new-form-sub-main end-->
																</td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <?php
                                                                    if($justmop_areaid !="")
                                                                    {
                                                                    if ($justmop_areaid == 0): ?>

                                                                        <div class="new-form-sub-box">
                                                                            <div class="new-form-sub-cont">Select Area</div><!--new-form-sub-cont end-->
                                                                            <div class="new-form-sub-field">
                                                                                <select name="just_address_area" id="just_address_area">
                                                                                    <option value="0">Select Area</option>
                                                                                    <?php
                                                                                    foreach ($areas as $area) :
                                                                                        ?>
                                                                                        <option value="<?php echo $area['area_id']; ?>"><?php echo $area['area_name']; ?></option>

                                                                                        <?php
                                                                                    endforeach;
                                                                                    ?>                      

                                                                                </select>
                                                                            </div><!--new-form-sub-field end-->

                                                                            <div class="clear"></div><!--clear end-->
                                                                        </div><!--new-form-sub-main end-->
                                                                    <?php endif; ?>
                                                                    <?php } ?>
                                                                </td>
															</tr>
															<tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Cleaning Material</div><!--new-form-sub-cont end-->
																		<?php
																			if($cleaning_material == "Y")
																			{
																				$selected = 'checked="checked"';
																			} else {
																				$selected = "";
																			}
																			?>
                                                                        <div class="new-form-sub-field"><input id="c-cleaning-materials" name="c_cleaning_materials" value="Y" <?php echo $selected; ?> type="checkbox"> Yes</div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->
                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Notes</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
																			<?php
																			if($booking_note != "")
																			{
																				$booking_note = $booking_note;
																			} else {
																				$booking_note = "";
																			}
																			?>
																			<textarea id="c-booking-note" class="popup-note-fld" style="width:225px;" name="c_booking_note" placeholder="Note to Driver"><?php echo $booking_note; ?></textarea>
																		</div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
															<tr>
																<td style="line-height: 18px; width: 500px">
																</td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">&nbsp;</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="button" class="save-but" id="btn-search-maid" value="Search" />
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>

                                                </div><!-- /widget-content --> 





                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset> 
                                <!--New endss-->

                                <fieldset>
                                    <div style="clear:both"></div><br>

                                    <div id="LoadingImage" style="text-align:center;display:none;width:50px;height:50px;"><img src="<?php echo base_url() ?>img/loader.gif"></div>
                                    <!--<div class="widget-content" id="maid_search">-->
                                    <div id="maid_search" style="border: medium none !important;"></div>
                                </fieldset>   


                            </div>




                            <!-- Pause Booking -->
                            <div class="tab-pane" id="pause-booking">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                       <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">   
                                                <div style="width: 300px; height: 30px; padding-bottom: 8px; margin-left: 764px;">
                                                    <input type="text" id="b-date-from" style="width: 80px; float: left; margin-right: 10px; padding: 5px; text-align: center;" data-date="<?php echo $search_date_from; ?>" readonly value="<?php echo $search_date_from; ?>" data-date-format="dd/mm/yyyy"/> 
                                                    <input type="text" id="b-date-to" style="width: 80px; padding: 5px; text-align: center; float: left; margin-right: 10px;" data-date='<?php echo $search_date_to ?>' readonly value='<?php echo $search_date_to ?>' data-date-format="dd/mm/yyyy"/>
                                                    <!--<a href="javascript:;" class="btn btn-medium btn-success" id='btn-search-booking'>Search</a>-->
                                                    <input type="button" class="save-but" id="btn-search-booking" value="Search" style="float: right; margin-right: 10px; margin-top: 0px;" />
                                                </div>
                                                <div class="widget-content">
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px;"><center> Sl.No.</center> </th>
                                                                <th style="line-height: 18px"> <center>Service Satrt Date</center></th>
                                                                <th style="line-height: 18px"> <center>Day</center></th>
                                                                <th style="line-height: 18px"> <center>Shift</center></th>
                                                                <th style="line-height: 18px"> <center>Booking Type</center></th>
                                                                <th style="line-height: 18px"> <center>Maid</center></th>
                                                                <th style="line-height: 18px"> <center>Action</center></th>
                                                                <!-- <th style="line-height: 18px"> <center><input type="checkbox" /></center></th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($bookings)) {
                                                                $i = 0;
                                                                foreach ($bookings as $subcat){
                                                                    foreach ($subcat as $booking)
                                                                    {
                                                                        $newDate = date("d/m/Y", strtotime($booking->scheduledates));
                                                                        //$onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                        if($booking->booking_type == "WE"){
                                                                            $classes = "weekblue";
                                                                        } else if($booking->booking_type == "OD") {
                                                                            $classes = "oneorange";
                                                                        } else {
                                                                            $classes = "oneblack";
                                                                        }
                                                                        $ndate = date("Y/m/d", strtotime($booking->scheduledates));
                                                                        $day = date('l', strtotime($ndate));
                                                                        //added by vishnu
                                                                        if($booking->booking_type == "OD")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_oneday($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause Booking';
                                                                                    $btnclass = "btn-danger";
                                                                                } else {
                                                                                    $pausetext = "Paused";
                                                                                    $onclick = "start_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $btnclass = "btn-success";
                                                                                }
                                                                            } 
                                                                        else if($booking->booking_type == "WE")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause Booking';
                                                                                    $btnclass = "btn-danger";
                                                                                } else {
                                                                                    $onclick = "start_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = "Paused";
                                                                                    $btnclass = "btn-success";
                                                                                }
                                                                            }
                                                                        else if($booking->booking_type == "BW")
                                                                            {
                                                                                $check_booking = $this->bookings_model->checkbooking_in_delete_weekly($booking->booking_id,$booking->scheduledates);
                                                                                if($check_booking == 0){
                                                                                    $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = '<i class="btn-icon-only icon-pause"> </i>Pause Booking';
                                                                                    $btnclass = "btn-danger";
                                                                                } else {
                                                                                    $onclick = "start_booking(" . html_escape($booking->booking_id) . ", '" . html_escape($booking->scheduledates) . "', '" . html_escape($booking->booking_type) . "')";
                                                                                    $pausetext = "Paused";
                                                                                    $btnclass = "btn-success";
                                                                                }
                                                                            }
                                                                        
                                                                        //ends
                                                                        
																		
                                                                        echo '<tr>
                                                                            <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($newDate) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $day . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><span class="'.$classes.'">' . html_escape($booking->time_from) . '-' . html_escape($booking->time_to) . '</span></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->booking_type) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><a class="btn '.$btnclass.' btn-small" style="width:100px;" href="javascript:void" onclick="' . $onclick . '">'.$pausetext.'</a></td>
                                                                            
                                                                        </tr>';
                                                                    }
                                                                    
                                                                }
                                                                // <td style="line-height: 18px; cursor: pointer; text-align : center;"><input type="checkbox" /></td>
//                                                                foreach ($bookings as $booking) {
//                                                                    $onclick = "pause_booking(" . html_escape($booking->booking_id) . ", '" . ($booking->service_date) . "', '" . html_escape($booking->booking_type) . "')";
//                                                                    echo '<tr>
//                                                                            <td style="line-height: 18px;"><center>' . ++$i . '</center></td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->service_date) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->shift_day) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->time_from) . '-' . html_escape($booking->time_to) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->booking_type) . '</td>
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($booking->maid_name) . '</td>    
//                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><a class="btn btn-danger btn-small" href="javascript:void" onclick="' . $onclick . '"><i class="btn-icon-only icon-pause"> </i>Pause Booking</a></td>
//                                                                        </tr>';
//                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="7"><center>No Records!</center></td></tr>';
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>             
                            </div>
                            
                            
                            
                            <div class="tab-pane" id="customer-statement">
                                <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">

                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Customer Statement</h3> 
                                                </div>
                                                <!-- /widget-header -->


                                                <div class="widget-content">


                                                    <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Statement Type</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
																			<select id="statementtype" name="statementtype">
																				<option value="">Select a type</option>
																				<option value="1">All</option>
																				<!--<option value="2">Pick a Start date</option>-->
																			</select>
																			<input type="hidden" id="cust-stat-id" name="cust-stat-id" value="<?php echo $customer_id;?>"/>
																		</div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                    
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Start Date</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="cust_state_frmdt" name="cust_state_frmdt" value="<?php echo date('d/m/Y');?>"/>
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->


                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">End Date</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="cust_state_todt" name="cust_state_todt" value="<?php echo date('d/m/Y');?>"/>
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>



                                                            <tr id="dateview" style="display:none;">
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Start Date</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="text" id="startdate" name="startdate" value=""/>
                                                                        </div><!--new-form-sub-field end-->

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->


                                                                     

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">&nbsp;</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <input type="button" class="save-but" id="view-statement" value="View" />
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div>
                                                            </td>
                                                            <td style="line-height: 18px; width: 500px"></td>   
                                                            </tr>
                                                            

                                                        </tbody>
                                                    </table>

                                                </div><!-- /widget-content --> 





                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> 
                            </div>
			    
			    <div class="tab-pane" id="booking-history">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                       <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">   
                                                <div style="width: 300px; height: 30px; padding-bottom: 8px; margin-left: 764px;">
                                                    <input type="text" id="bh-date-from" style="width: 80px; float: left; margin-right: 10px; padding: 5px; text-align: center;" data-date="<?php echo $search_date_from_bh; ?>" readonly value="<?php echo $search_date_from_bh; ?>" data-date-format="dd/mm/yyyy"/> 
                                                    <input type="text" id="bh-date-to" style="width: 80px; padding: 5px; text-align: center; float: left; margin-right: 10px;" data-date='<?php echo $search_date_to_bh ?>' readonly value='<?php echo $search_date_to_bh; ?>' data-date-format="dd/mm/yyyy"/>
                                                    <!--<a href="javascript:;" class="btn btn-medium btn-success" id='btn-search-booking'>Search</a>-->
                                                    <input type="button" class="save-but" id="btn-search-booking-history" value="Search" style="float: right; margin-right: 10px; margin-top: 0px;" />
                                                </div>
                                                <div class="widget-content">
                                                     <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                                                        <th style="line-height: 18px"> <center>Maid Name </center></th>
                                                        <th style="line-height: 18px"> <center>Date</center></th>
                                                        <th style="line-height: 18px"> <center>Shift</center></th>
                                                        <th style="line-height: 18px"> <center>Status</center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($booking_history)) {
                                                                $i = 0;
                                                                $display_ids=array();
                                                                foreach ($booking_history as $history) {
                                                                    foreach ($history as $b_history) {
                                                                        $serv_stat=get_ser_stat_from_bid_and_date($b_history->booking_id,$b_history->bh_date);
                                                                        if(in_array($b_history->booking_id, $display_ids))
                                                                        {
                                                                            continue;
                                                                        }
                                                                        else
                                                                        {
                                                                            $display_ids[]=$b_history->booking_id;
                                                                        }
                                                                        $bh_stat='';
                                                                        if($serv_stat == 1)
                                                                        {
                                                                            $bh_stat="In Progress";
                                                                        } else if($serv_stat == 2){
                                                                            $bh_stat="Finished";
                                                                        } else if($serv_stat == 3){
                                                                            $bh_stat="Cancelled";
                                                                        } else {
                                                                            //echo "Not Started";
                                                                            if($b_history->deleted_status=='1')
                                                                            {
                                                                                $bh_stat="Deleted";
                                                                            }
                                                                            else if($b_history->booking_status=='2')
                                                                            {
                                                                                $bh_stat="Deleted";
                                                                            }
                                                                            else
                                                                            {
                                                                                $bh_stat="Not Started";
                                                                            }
                                                                        }
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($b_history->maid_name) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($b_history->bh_date) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($b_history->time_from) . '-' . html_escape($b_history->time_to) . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($bh_stat) . '</td>
                                                                        </tr>';
                                                                    }
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
							
							<div class="tab-pane" id="sent-mail">
                                <div class="span12" style="width: 100%;margin-left: 0px;">
                                        <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">

                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Send Mail</h3> 
                                                </div>
                                                <!-- /widget-header -->


                                                <div class="widget-content">


                                                    <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box ">
                                                                        <div class="new-form-sub-cont">Amount</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field"><input type="text"  name="send_amount" id="send_amount" value="" /></div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->
                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">Description</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field">
                                                                            <textarea name="send_description" id="send_description"></textarea>
                                                                            <input type="hidden" id="custtid" name="custtid" value="<?php echo $customer_id;?>"/>
                                                                        </div><!--new-form-sub-field end-->

                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->


                                                                     

                                                                </td>
                                                                <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div><!--new-form-sub-main end-->

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="line-height: 18px; width: 500px">
                                                                    <div class="new-form-sub-box">
                                                                        <div class="new-form-sub-cont">&nbsp;</div><!--new-form-sub-cont end-->
                                                                        <div class="new-form-sub-field" style="width: auto !important;">
                                                                            <input type="button" class="save-but" id="btn-snd-mail" value="Send" />
																			<input type="button" class="save-but" id="btn-snd-mail-whatsapp" value="Whatsapp" onclick="open_link()" />
                                                                            <input type="button" class="save-but" id="btn-snd-mail-copy" value="Copy Link" onclick="copy_link()" />
																			<a href='#' target='_blank' id='open_link'></a>
                                                                        </div><!--new-form-sub-field end-->
                                                                        <div class="clear"></div><!--clear end-->
                                                                    </div>
                                                            </td>
                                                            <td style="line-height: 18px; width: 500px"></td>   
                                                            </tr>
                                                            

                                                        </tbody>
                                                    </table>

                                                </div><!-- /widget-content --> 





                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> 
                            </div>
							
							<div class="tab-pane" id="pay-history">
                                <fieldset>            
                                    <div class="span12" style="width: 100%;margin-left: 0px;">
                                      <div class="widget" style="border: 0px;">
                                            <div class="widget widget-table action-table">
												<!--<div class="widget-header"> <i class="icon-th-list"></i>
                                                    <h3>Payment Link History</h3> 
                                                </div>-->
                                                <div class="widget-content">
                                                    <table class="table table-striped table-bordered" id="paylinktable">
                                                        <thead>
                                                            <tr>
                                                                <th style="line-height: 18px; width: 20px"><center> Sl No</center> </th>
                                                                <th style="line-height: 18px;"><center> Amount</center> </th>
																<th style="line-height: 18px"> <center>Description </center></th>
																<th style="line-height: 18px"> <center>Payment Link</center></th>
																<th style="line-height: 18px"> <center>Sent Via</center></th>
															</tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($link_history)) {
                                                                $i = 0;
                                                                foreach ($link_history as $l_history) {
                                                                    echo '<tr>
                                                                            <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $l_history->amount . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $l_history->description . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $l_history->payment_link . '</td>
                                                                            <td style="line-height: 18px; cursor: pointer; text-align : center;">' . $l_history->type . '</td>
                                                                        </tr>';
                                                                }
                                                            } else {
                                                                echo '<tr><td style="line-height: 18px;" colspan="5"><center>No Records!</center></td>';
                                                            }
                                                            ?>

                                                        </tbody>
                                                    </table>
                                                </div><!-- /widget-content --> 
                                            </div> 
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>            
                            </div>
							
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->						
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>