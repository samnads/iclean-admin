<div class="row">      
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Maids</h3> 
                <span style="margin-left:815px;">Date:</span>
                <input type="text" style="width: 125px;" id="leave_date" value="<?php echo date('d/m/Y');?>"/>
                
            </div>
            <div class="widget-content" style="margin-bottom:30px">

                <table class="table table-striped table-bordered" id="maid-leave-list">
                    <thead>
                        <tr>
                            <th style="line-height: 28px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 28px"> Maid Name </th>
                            <th style="line-height: 28px"> Mobile</th>
                            <th style="line-height: 28px"> Leave Date</th>
                            <th style="line-height: 28px"> Added By</th>
                            <th style="line-height: 28px" class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($maid_list)) {
                            $i = 1;
                            foreach ($maid_list as $maids_val) {
                                
                                $l_date = explode("-", $maids_val->leave_date);
                                $leave_date = $l_date[0] . '/' . $l_date[1] . '/' . $l_date[2]; 
                                
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px"><?php echo $i++; ?></td>
                                    <td style="line-height: 18px; cursor: spointer"><?php echo $maids_val->maid_name ?></td>
                                    <td style="line-height: 18px; cursor: pointer"><?php echo $maids_val->maid_mobile_1 ?></td>
                                    <td style="line-height: 18px; cursor: pointer"><?php echo $leave_date ?></td>
                                    <td style="line-height: 18px; cursor: pointer"><?php echo $maids_val->user_fullname ?></td>   
                                    <td style="line-height: 18px; width: 110px" class="td-actions"><center>
                                <?php //$btn_class = $maids_val->leave_status == 1 ? 'btn btn-success btn-small' : 'btn btn-danger btn-small'; ?>
<!--                                        <a href="javascript:;" class="<?php //echo $btn_class ?>" onclick="undo_leave(<?php //echo $maids_val->leave_id ?>);" title="Cancel"><?php //echo $maids_val->leave_status == 1 ? '<i class="btn-icon-only icon-ok"> </i>' : '<i class="btn-icon-only icon-remove"> </i>' ?></a>-->
                                        <a href="javascript:void" style="text-decoration:none;" onclick="undo_leave(<?php echo $maids_val->leave_id ?>);"><span class="not-started">Cancel</span></a>
                                    </center></td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo '<tr><td colspan="6" style="line-height: 18px; text-align:center;">No Records!</td></tr>';
                    }
                    ?>   

                    </tbody>
                </table>

            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
</div>