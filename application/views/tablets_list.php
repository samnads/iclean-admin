<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">       
    <div class="span12">
    <?php if($error!='') { ?>
        <div class="alert alert-danger" role="alert"> 
            <?php
        echo $error; ?>  </div>
        <?php } ?>
   
        <div class="widget widget-table action-table">
     
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Tablets</h3>
                <div class="error"></div>

                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_tablet();"><img src="<?php echo base_url();?>img/add.png" title="Add Tablet"/></a>                    
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Tablet Imei</th>
                             <th style="line-height: 18px"> Access Code</th>
                            <th style="line-height: 18px"> Zone </th>
                            <th style="line-height: 18px"> Driver </th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($tablets) > 0) {
                            $i = 1;
                            foreach ($tablets as $tablets_val) {
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['imei'] ?></td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['access_code'] ?></td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['zone_name'] ?></td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['tablet_driver_name'] ?></td>
                                    <td style="line-height: 18px" class="td-actions">
                                        <a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_tablet(<?php echo $tablets_val['tablet_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                                        
                                        <?php if ($tablets_val['tablet_status'] == 1) 
                                            {                                         
                                            ?>
                                                <a href="javascript:;" class="btn btn-success btn-small" onclick="tablet_status(<?php echo $tablets_val['tablet_id']?>,<?php echo $tablets_val['tablet_status']?>);"><i class="btn-icon-only icon-ok"> </i></a>
                                            <?php  
                                            }
                                            else     
                                            {
                                            ?>
                                                <a href="javascript:;" class="btn btn-danger btn-small" onclick="tablet_status(<?php echo $tablets_val['tablet_id']?>,<?php echo $tablets_val['tablet_status']?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                            <?php
                                            }
                                             ?>
                                        
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>   

                    </tbody>
                </table>
            </div> <!-- /widget-content -->          
        </div><!-- /widget --> 
    </div><!-- /span6 --> 
    <div class="span5" id="add_tablet" style="display: none;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Add Tablet</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_tablet();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
                        <form id="tablet1" class="form-horizontal" method="post">
                            <fieldset>                             
                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="zone_id" id="zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Access Code</label>
                                    <div class="controls">
                                        <input type="text" readonly="true" class="span3" id="access_code" name="access_code" required="required" value="<?php echo 'SM' . rand(1000, 2000) ?>">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
								<div class="control-group">											
                                    <label class="control-label" for="drivernameval">Driver Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="drivernameval" name="drivernameval">
                                    </div> <!-- /controls -->				
                                </div>
                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Tablet IMEI</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="imei" name="imei">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
                                <div class="control-group">											
                                    <label class="control-label" for="username">User Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="username" name="username">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
                                <div class="control-group">											
                                    <label class="control-label" for="password_tab">Password</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="password_tab" name="password_tab" >
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="tablet_sub">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span6 -->
    <div class="span5" id="edit_tablet" style="display: none;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Edit Tablet</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_tablet();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
                        <form id="tablet2" class="form-horizontal" method="post">
                            <fieldset>                             
                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="edit_zone_id" id="edit_zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Access Code</label>
                                    <div class="controls">
                                        <span id="edit_access_code"></span>
                                        <!--<input type="text" class="span3" id="edit_access_code" name="edit_access_code" required="required">-->
                                        <input type="hidden" class="span3" id="edit_tabletid" name="edit_tabletid">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
								
								<div class="control-group">											
                                    <label class="control-label" for="editdrivernameval">Driver Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="editdrivernameval" name="editdrivernameval">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
                                
                                 <div class="control-group">											
                                    <label class="control-label" for="zonename">Tablet imei</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="edit_imei" name="edit_imei">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
                                 <div class="control-group">											
                                    <label class="control-label" for="edit_username_tab">User Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="edit_username_tab" name="edit_username_tab">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->
                                 <div class="control-group">											
                                    <label class="control-label" for="edit_password_tab">Password</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="edit_password_tab" name="edit_password_tab">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="tablet_edit">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>

