<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    .container.booking-fl-box {
        width: 100% !important;
    }
</style>
<div class="row">
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Lead Bookings </h3>


                    <!--<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="0">-- Select Customer --</option>
						<?php
                        // foreach($customerlist as $c_val)
                        // {
                        // if($c_val->customer_id == $customer_id)
                        // {
                        // $selected = 'selected="selected"';
                        // } else {
                        // $selected = '';
                        // }
                        ?>
						<option value="<? php // echo $c_val->customer_id; 
                                        ?>" <? php // echo $selected; 
                                                                                ?>><? php // echo $c_val->customer_name; 
                                                                                                        ?></option>
						<?php
                        //}
                        ?>  
                    </select>-->
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">
                    <!--<a style="float:right ; margin-right:15px; cursor:pointer;" href="<? php // echo base_url(); 
                                                                                            ?>customer/add_payment">
						<img src="<? php // echo base_url(); 
                                    ?>img/add.png" title="Add Payment">
					</a>-->
                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Mobile</th>

                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Reference No</th>

                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Service Name</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service start/end date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service start/end time</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Booking Date and Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $i = 1;
                        foreach ($lead_report as $pay) {
                            $date1 = $pay->service_start_date;

                            $old_date_timestamp1 = strtotime($date1);

                            $dateFormat1 = date('l jS, F', $old_date_timestamp1);
                            $date2 = $pay->service_end_date;

                            $old_date_timestamp2 = strtotime($date2);

                            $dateFormat2 = date('l jS, F', $old_date_timestamp2);
                            $start = date("g:i a", strtotime($pay->time_from));
                            $end = date("g:i a", strtotime($pay->time_to));
                            if ($pay->booking_type == 'OD') {
                                $val = 'One Day';
                            } else if ($pay->booking_type == 'WE') {
                                $val = 'Weekly';
                            } else {
                                $val = 'Bi-weekly';
                            }
                        ?>
                            <tr>
                                <td style="line-height: 18px;"><?php echo $i; ?> </td>
                                <td style="line-height: 18px;"><?php echo $pay->customer_name; ?></td>
                                <td style="line-height: 18px;"><?php echo $pay->mobile_number_1; ?></td>

                                <td style="line-height: 18px;"><?php echo $pay->reference_id; ?></td>

                                <td style="line-height: 18px;"><?php echo $pay->service_type_name; ?></td>


                                <td style="line-height: 18px;"><?php echo $dateFormat1 . ' / ' . $dateFormat2; ?></td>

                                <td style="line-height: 18px;"><?php echo $start . ' - ' . $end; ?></td>
                                <td style="line-height: 18px;"><?php echo $val; ?></td>
                                <td style="line-height: 18px;"><?php echo $pay->total_amount; ?></td>
                                <td style="line-height: 18px;"><?php echo date('d/m/Y h:i A', strtotime($pay->booked_datetime)); ?> </td>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                    </tbody>

                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>