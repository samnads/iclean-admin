<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1" align="center">
      <thead>
                <tr>
                    <th> Sl.NO</th>
                    <th> Maid Name</th>
                    <th> Total Full Day</th>
                    <th>Total Half Day</th>
                    <th>Total Day</th>
                </tr>
            </thead>
            <tbody>
               <?php 
                if ($leaveresults) {
                $i = 0;
                $total = 0;
                    foreach ($leaveresults as $leaveresultsrow){
                        $totalday = ($leaveresultsrow->fullday + ($leaveresultsrow->halfday/2));
                        $total += $totalday;
                   ?>
                
                <tr>
                    <td><?php echo ++$i; ?></td>
                    <td><?php echo $leaveresultsrow->maid_name; ?></td>
                    <td><?php echo $leaveresultsrow->fullday; ?></td>
                    <td><?php echo $leaveresultsrow->halfday; ?></td>
                    <td><?php echo $totalday; ?></td>
                </tr>
                    
                    <?php
                    }
                    ?>
                    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total :</td>
                    <td><?php echo number_format($total,1); ?></td>
                </tr>
                <?php }
                    ?>
                 
            </tbody>
            
</table>