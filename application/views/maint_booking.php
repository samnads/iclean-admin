<style>
    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }
    /*.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: auto;}*/
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                    <div class="widget-header"> 
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                              <div class="book-nav-top">

                                <ul>
                                    <li style="padding-top:14px; padding-bottom:14px;">
                            <i class="icon-th-list"></i>
                            <h3>Bookings</h3>
                                    </li>
                                    <!--<li style="padding-top:5px;">
                                        <?php// if($post_invoice_date!=""){ ?>
                                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="invoice_date" name="invoice_date" value="<?php echo $post_invoice_date ?>">
                                        <?php// } else {?> 
                                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="invoice_date" name="invoice_date" value="<?php echo date('d/m/Y') ?>">
                                        <?php// } ?>
                                        <input type="hidden" name="day" id="day" value="">
                                        <input type="hidden" name="invoicedateformat" id="invoicedateformat" value="<?php echo $invoice_date; ?>" />
                                        <input type="submit" class="btn" value="Go" id="invoicego" name="invoice_report" style="margin-bottom: 4px;">
                                        <div class="clear"></div>
                                    </li>-->
                                
                                <!-- Job start date -->
                                <li style="padding-top:9px;">
                                    <input type="button" id="invoice-search" value="Search" style="width: 65px; padding: 4px; background: #5c9bd1; border: 1px solid #5c9bd1; color: #fff; margin-right: 10px; display: none;" />
                                </li>
<!--                                <li class="pull-right no-right-margin">
                                    <a class="newjobbutton" id="newjobbutton" href="<?php// echo base_url(); ?>activity/addjob"><i class="fa fa-plus"></i> New Job</a>
                                </li>-->
                                <div class="clear"></div>
                                <!-- Job ends -->
                                </ul>
                              </div>
                                
                            
                            <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                            <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                            
                        </form>
                        
                    </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
                <div id="invoice-exTab2" class="">	
                    <table id="invoice-job-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <!--<th class="no-left-border"></th>-->
                                        <th>Sl</th>
                                        <th>Customer</th>
										<th>Location</th>
										<th>Service Type</th>
										<th>Priority Type</th>
                                        <th>Service Date</th>
                                        <th>Time Type</th>
										<th>Notes</th>
										<th>Status</th>
										<th class="no-right-border">Action</th>
                                    </tr>
				</thead>
                                <tbody id="invoice-tabtbody1">
                                    <?php
                                    $i=1;
                                    //$total_amount_paid = 0;
                                    foreach($maint_report as $report)
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $report->customer_name; ?></td>
										<td><?php echo $report->customer_address; ?></td>
                                        <td><?php echo $report->service_type_name; ?></td>
                                        <td><?php echo $report->priority_type; ?></td>
                                        <td><?php echo $report->service_start_date; ?></td>
                                        <td><?php echo $report->time_type; ?></td>
										<td><?php echo $report->booking_note; ?></td>
                                        <td><?php  if($report->booking_status =='2') { echo 'Cancelled'; } else if ($report->booking_status =='1'){ echo 'Active'; } else { echo 'Pending' ; } ?></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
				</tbody>
                             </table>
                <!--</div>-->
                    <!--</div>-->
                </div>
            </div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->