<div class="row">   
  
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <i class="icon-th-list"></i>
                <h3>Change Password</h3> 
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div class="ad-content-hed-main">
                    <div class="ad-content-hed-lft-cor"></div>
                        <div class="ad-content-main-area">

                            <div class="ad-content-area">
                                <div class="ad-cont-box-main-content"> 
                                    <?php if($this->session->flashdata('failure_msg')) { echo '<div class="flash_failure">' . $this->session->flashdata('failure_msg') . '</div>'; } else if($this->session->flashdata('success_msg')) { echo '<div class="flash_success">' . $this->session->flashdata('success_msg') . '</div>'; } ?>
                                    <div class="fild-main-box-big">
                                            <?php echo form_open(base_url() . 'dashboard/changepassword', array('id' => 'update-password-form')); ?>


                                            <div class="fild-main-box">
                                                    <?php echo form_label('Old Password <span>:</span>', 'old_password',  array( 'class' => 'fild-name')); ?>
                                                    <div class="fild-box">
                                                            <?php echo form_input(array('name' => 'old_password', 'type' => 'password', 'id' => 'old-password', 'maxlength' => 15, 'tabindex' => 1, 'class' => 'in-bookingform-field', 'value' => set_value('old_password', ''))); ?>
                                                            <?php echo form_error('old_password'); ?>
                                                    </div>

                                                    <div class="clear"></div>
                                            </div>
                                            <div class="fild-main-box">
                                                    <?php echo form_label('New Password <span>:</span>', 'new_password',  array( 'class' => 'fild-name')); ?>
                                                    <div class="fild-box">
                                                            <?php echo form_input(array('name' => 'new_password', 'type' => 'password', 'id' => 'new-password', 'maxlength' => 15, 'tabindex' => 2, 'class' => 'in-bookingform-field', 'value' => set_value('new_password', ''))); ?>
                                                            <?php echo form_error('new_password'); ?>
                                                    </div>

                                                    <div class="clear"></div>
                                            </div>
                                            <div class="fild-main-box">
                                                    <?php echo form_label('Confirm Password <span>:</span>', 'confirm_password',  array( 'class' => 'fild-name')); ?>
                                                    <div class="fild-box">
                                                            <?php echo form_input(array('name' => 'confirm_password', 'type' => 'password', 'id' => 'confirm-password', 'maxlength' => 15, 'tabindex' => 3, 'class' => 'in-bookingform-field', 'value' => set_value('confirm_password', ''))); ?>
                                                            <?php echo form_error('confirm_password'); ?>
                                                    </div>

                                                    <div class="clear"></div>
                                            </div>
                                            <br />

                                            <div class="fild-main-box">
                                                    <div class="fild-name">&nbsp;</div>
                                                    <div class="fild-box">
                                                            <?php echo form_submit(array('name' => 'change_password', 'id' => 'change-password', 'tabindex' => 4, 'value' => 'Update', 'class' => 'save-but') ); ?>
                                                    </div>
                                                    <div class="clear"></div>
                                            </div>
                                            <?php echo form_close(); ?>
                                    </div>

                                    <div class="fild-main-box-big-rit"></div>

                                    <br />
                                    <br />
                                    <br />

                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="ad-content-hed-rgt-cor"></div>
                        <div class="clear"></div>
                    </div>
                
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>