<style>
	
</style>

<div class="schedule_report" id="newschedulereport">
	<div class="inner">
		<div class="head">Schedule - <?php echo $schedule_day; ?></div>
		<div class="table">
			<div class="row">
				<div class="cell1"><strong>No</strong></div>
				<div class="cell2"><strong>Cleaner</strong></div>
				<div class="cell5"><strong>Total Time</strong></div>
				<div class="cell4"><strong>Client</strong></div>
				<div class="cell5"><strong>Timing</strong></div>
				<div class="cell4"><strong>Mobile</strong></div>
				<div class="cell5"><strong>Location</strong></div>
				<div class="cell5"><strong>Payment</strong></div>
				<div class="cell5"><strong>Pay-Mode</strong></div>
				<div class="cell5"><strong>Driver</strong></div>
				<div class="cell5"><strong>Notes</strong></div>
			</div>
			
			<?php
			$sln = 1;
			$tottime = 0;
			$totprice = 0;
			foreach($all_bookings as $booking)
			{
				?>
				<div class="row">
					<div class="cell1"><?php echo $sln++; ?>.</div>
					<div class="cell2"><?php echo html_escape($booking->maid_name); ?></div>
					<div class="cell5" align="center">
                        <?php
						$from = $booking->time_from;
						$to = $booking->time_to;
						$d1 = strtotime($from);
						$d2 = strtotime($to);
						$diff = (($d2 - $d1)/60)/60;
						$tottime += $diff;
						echo $diff . ' Hrs';
                        ?>
					</div>
					<div class="cell4"><?php echo html_escape($booking->customer_nick_name); ?></div>
					<div class="cell5"><?php echo $booking->time_from; ?> - <?php echo $booking->time_to; ?></div>
					<div class="cell4"><?php echo $booking->customer_mobile; ?></div>
					<div class="cell5"><?php echo $booking->customer_address. '<br> area - ' .$booking->customer_area.'<br> zone - '.$booking->customer_zone;  ?></div>
                    <div class="cell5" align="right"><?php echo $booking->total_amount; $totprice += $booking->total_amount; ?></div>	
                    <div class="cell5"><?php echo $booking->payment_mode; ?></div>    
                    <div class="cell5"><?php echo html_escape($booking->driver_name); ?></div>    
                    <div class="cell5"><?php echo html_escape($booking->booking_note); ?></div> 
				</div>
				<?php
			}
			?>
            <div class="row">
				<div class="cell1"><strong></strong></div>
				<div class="cell2"><strong></strong></div>
				<div class="cell5"><strong><?php echo $tottime. ' Hrs'; ?></strong></div>
				<div class="cell4"><strong></strong></div>
                <div class="cell5" align="center"><strong></strong></div>
                <div class="cell3" align="center"><strong></strong></div>
                <div class="cell3" align="center"><strong></strong></div>
                <div class="cell5" align="right"><strong><?php echo $totprice; ?></strong></div>
                <div class="cell5" align="right"><strong></strong></div>
                <div class="cell5" align="right"><strong></strong></div>
                <div class="cell5" align="right"><strong></strong></div>
			</div>
		</div>
	</div>
</div>
<div id="divSchedulePrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "0">
              <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px;"> No</th>
                            <th style="line-height: 18px; padding: 5px;"> Cleaner</th>
                            <th style="line-height: 18px; padding: 5px;"> Total Time</th>
                            <th style="line-height: 18px; padding: 5px;"> Client</th>
                            <th style="line-height: 18px; padding: 5px;"> Timing</th>
                            <th style="line-height: 18px; padding: 5px;"> Mobile</th>
                            <th style="line-height: 18px; padding: 5px;"> Location</th>
                            <th style="line-height: 18px; padding: 5px;"> Payment</th>
                            <th style="line-height: 18px; padding: 5px;"> Pay-Mode</th>
                            <th style="line-height: 18px; padding: 5px;"> Driver</th>
                            <th style="line-height: 18px; padding: 5px;"> Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       $sln = 1;
						$tottime = 0;
						$totprice = 0;
						foreach($all_bookings as $booking)
						{
                           ?>
                                <tr>
									<td style="line-height: 18px; padding: 5px;"><?php echo $sln; ?> </td>
									<td style="line-height: 18px; padding: 5px;"><?php echo html_escape($booking->maid_name); ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php
						$from = $booking->time_from;
						$to = $booking->time_to;
						$d1 = strtotime($from);
						$d2 = strtotime($to);
						$diff = (($d2 - $d1)/60)/60;
						$tottime += $diff;
						echo $diff . ' Hrs';
                        ?></td>
					<td style="line-height: 18px; padding: 5px;">
                                            <?php echo html_escape($booking->customer_nick_name); ?>
					</td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $booking->time_from; ?> - <?php echo $booking->time_to; ?></td>
					<td>
					<?php echo $booking->customer_mobile; ?>
					</td>
					<td>
					<?php echo $booking->customer_address. '<br> area - ' .$booking->customer_area.'<br> zone - '.$booking->customer_zone; ; ?>
					</td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $booking->total_amount; $totprice += $booking->total_amount; ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $booking->payment_mode; ?></td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo html_escape($booking->driver_name); ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php echo html_escape($booking->booking_note); ?></td>
                                    </tr>
                            <?php
                            $i++;
                            }
							?>
							<tr>
								<td></td>
								<td></td>
								<td style="text-align:center"><strong><?php echo $tottime. ' Hrs'; ?></strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td style="text-align:center"><strong><?php echo $totprice; ?></strong></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>



                    </tbody>     
        </table>
    </div><!-- /widget-content --> 
</div>
<input type="hidden" value ="<?php echo $schedule_day; ?>" name="printdayschedule" id="printdayschedule"/>