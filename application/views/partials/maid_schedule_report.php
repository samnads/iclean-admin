<style>
	
</style>

<div class="schedule_report">
	<div class="inner">
		<div class="head">Schedule - <?php echo $schedule_day; ?></div>
		<div class="table">
			<div class="row">
				<div class="cell1"><strong>Sl. No</strong></div>
				<div class="cell2"><strong>Maid</strong></div>
				<div class="cell3"><strong>Time</strong></div>
				<div class="cell4"><strong>Customer</strong></div>
			</div>
			
			<?php
			$sln = 1;
			foreach($all_bookings as $booking)
			{
				?>
				<div class="row">
					<div class="cell1"><?php echo $sln++; ?>.</div>
					<div class="cell2"><?php echo html_escape($booking->maid_name); ?></div>
					<div class="cell3"><?php echo $booking->time_from; ?> - <?php echo $booking->time_to; ?></div>
					<div class="cell4"><?php echo html_escape($booking->customer_nick_name); ?></div>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</div>