<div class="row">
        
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Flat</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_flat();"><img src="<?php echo base_url();?>img/add.png" title="Add Flat"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Flat </th>
                            <th style="line-height: 18px"> Tablet imei</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($flats) > 0) {
                            $i = 1;
                            foreach ($flats as $flats_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $flats_val['flat_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $flats_val['tablet_imei'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_flat(<?php echo $flats_val['flat_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if(user_authenticate() == 1) {?>
								<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_flat(<?php echo $flats_val['flat_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php } ?>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
    <div class="span6" id="add_flat" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-bar-chart"></i>
                    <h3>Add Flat</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_flat();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Flat Name</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="flatname" name="flatname" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Tablet imei</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="tablet_imei" name="tablet_imei" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="flat_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    <div class="span6" id="edit_flat" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-bar-chart"></i>
                    <h3>Edit Flat</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_flat();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Flat Name</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_flatname" name="edit_flatname" required="required">
                                                <input type="hidden" class="span3" id="edit_flatid" name="edit_flatid">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Tablet imei</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_tablet_imei" name="edit_tablet_imei" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="flat_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>