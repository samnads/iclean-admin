<div class="row">
    <div class="span6" id="add_user" style="float: none; margin: 0 auto;">      		
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-globe"></i>
				<h3>Quickbook Connection</h3>
				<!--<a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php echo base_url(); ?>users"><img src="<?php// echo base_url();?>img/male-list.png" title="Hide"/></a>-->
            </div>
            <div class="widget-content">
				<div class="tabbable">
                    <div class="tab-content">
                        <?php
						if(isset($access_token) && !empty($access_token)){
						?>
						<fieldset>
							<div class="control-group">											
								<p style="color: green; font-weight: bold;">Connection established successfully for 1 hour.</p>			
								<p>Please start synching now.</p>			
							</div> <!-- /control-group -->
							<br />
						</fieldset>
						<?php
						} else {
						?>
						<fieldset>
							<div class="control-group">											
								<p>Please click the below button to connect Emaid to Quickbooks for syncing the data.</p>			
								<p>Please note login in to quickbook account in same browser before you try to connect.</p>			
							</div> <!-- /control-group -->
							<br />
						</fieldset>
						<a class="" align="center" href="<?php echo base_url(); ?>quickbook/connect_progress">
							<img style='height: 40px; width:250px; margin: 0 auto;' src='<?php echo base_url(); ?>images/C2QB_white_btn.png'/>
						</a>
						<?php
						}
						?>
                    </div>
				</div>
            </div> <!-- /widget-content -->
		</div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>