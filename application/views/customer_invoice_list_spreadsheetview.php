<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
         <tr>
            <th> Sl.No</th>
            <th> Number</th>
            <th> Customer</th>
            <th> Invoice Date</th>
            <th> Due Date</th>
            <th> Total</th>
            <th> Paid Amount</th>
            <th> Due Amount</th>
            <th> Status</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $sub_t = 0;
    $grand_paid = 0;
    $grand_due = 0;
    $i = 1;
    foreach ($invoice_report as $inv){
        $sub_t += round($inv->invoice_net_amount,2);
        $grand_paid += round($inv->received_amount,2);
        $grand_due += round($inv->balance_amount,2);
    ?>
        <tr>
            <td><?php echo $i;?> </td>
            <td><?php echo $inv->invoice_num; ?> </td>
            <td><?php echo $inv->customer_name; ?></td>
            <td><?php echo $inv->invoice_date; ?></td>
            <td><?php echo $inv->invoice_due_date; ?></td>
            <td><?php echo $inv->invoice_net_amount; ?></td>
            <td><?php echo $inv->received_amount; ?></td>
            <td><?php echo $inv->balance_amount; ?></td>
            <td>
                <?php
                if($inv->invoice_paid_status == 2)
                {
                    $paystat = " (Partially Paid)";
                } else {
                    $paystat = "";
                }
                if($inv->invoice_status == '0')
                {
                    $invstatus = "Draft";
                } else if($inv->invoice_status == '1')
                {
                    $invstatus = "Open";
                } else  if($inv->invoice_status == '2'){
                    $invstatus = "Cancelled";
                } else  if($inv->invoice_status == '3'){
                    $invstatus = "Paid";
                }
                echo $invstatus.$paystat;
                ?>
            </td>
        </tr>
    <?php
    $i++;   
    }
    ?>
    </tbody>
    
</table>