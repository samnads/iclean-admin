<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table  border="1" align="center">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Cleaner</th>
            <th> Cleaner Status</th>
            <th> Client Name</th>
            <th> Date</th>
            <th> Phone</th>
            <th> Booking Type</th>    
            <th> Source</th>
            <th> Timings</th>
            <th> Total Hrs</th>
            <th> Driver</th>   
            <th> Payment</th>        
        </tr>
    </thead>
    <tbody>
        <?php       
        $sln = 1;
        $sumamt = 0;
            foreach($performance_report as $performance)
                   {
                    foreach ($performance as $key)
                        {

                            $noofmaids=count(explode(',', $key->maid_names));
                            $maid_names = explode(',', $key->maid_names);
                            $maid_status = explode(',', $key->maid_status);
                            $customer_names = explode(',', $key->customer_names);
                            $service_date=$key->service_start_date;
                            $mobile_numbers = explode(',', $key->mobile_number);
                            $booking_type = explode(',', $key->booking_type);
                            $sources = explode(',', $key->sources);
                            $time_from = explode(',', $key->time_from);
                            $time_to = explode(',', $key->time_to);
                            $time_diffs = explode(',', $key->time_diffs);
                            $drivers = explode(',', $key->drivers);
                            $totamt = explode(',', $key->totamt);
                            

                            for($i=0;$i<$noofmaids;$i++){
           ?>
                <tr>
                        <td><?php echo $sln; ?> </td>
                        <td><?php echo $maid_names[$i]; ?> </td>
                        <td><?php echo $maid_status[$i]==1?'Active':'Inactive'; ?> </td>
                        <td><?php echo $customer_names[$i]; ?> </td>
                        <td><?php echo $service_date; ?> </td>
                        <td><?php echo $mobile_numbers[$i]; ?></td>
                        <td><?php $b_type=$booking_type[$i];$booktype = "";
                                  if($b_type == "OD"){$booktype = "One Time";}else if($b_type == "WE"){$booktype = "Weekly";}else if($b_type == "BW"){$booktype = "Bi-Weekly";}
                                  echo $booktype;
                        ?></td>
                        <td><?php echo $sources[$i]; ?></td>
                        <td><?php echo $time_from[$i]." - ".$time_to[$i]; ?></td>
                        <td><?php echo round($time_diffs[$i],2); ?></td>
                        <td><?php echo $drivers[$i]; ?></td>
                        <td><?php echo $totamt[$i]; ?></td>
                        <?php $sumamt += floatval($totamt[$i]); ?>
                    </tr>
            <?php
            $sln++;                                      }
            }
                } ?>
                 <tr>
                 <td colspan="11"> Total </td>
                        <td><?php echo $sumamt ?></td>
                    </tr>
    </tbody>
</table>