
<link href="<?php echo base_url() ?>css/pages/signin.css" rel="stylesheet" type="text/css">

<div class="login-n-wrapper gradient">

<div class="account-container">

<div class="login-logo"><img src="<?php echo base_url(); ?>images/logo-white.png" alt=""></div>

    <!--<div class="e-login-pic"> &nbsp; <br /> &nbsp; </div>-->
    <div class="e-login-cont-main">
    
         
         
        <div class="content clearfix e-login-cont">
            <?php echo form_open(base_url() . 'login', array('id' => 'login-form')); ?> 
            <?php echo isset($error) ? '<div class="error">' . $error . '</div>' : ''; ?>
            <input type="hidden" name="login" value="1" />
            <h1>Login</h1>
            <div class="login-fields">
                <!--<p>Please provide your details</p>-->
                <div class="field">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
                </div> 
                <div class="field">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
                </div> 
            </div>
            
            
            
            
            
    
            <div class="login-actions">
                <span class="login-checkbox">
                    <input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
                    <label class="choice" for="Field">Keep me signed in</label>
                </span>
                <button class="text-field-but">Sign In</button>
    
    
            </div>
            </form>
        </div> 
        
    </div>
</div> 



<div class="footer" style="position: fixed;">
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <div class="span12"> &copy; <?php echo date('Y'); ?> Emaid. All rights reserved.</div>

            </div>

        </div>

    </div>

</div>

</div>
