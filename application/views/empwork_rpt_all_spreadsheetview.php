<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No.</th>
            <th> Maid</th>
            <th> After 6 [Hrs]</th>
            <th> Holiday [Hrs]</th>
            <th> Normal [Hrs]</th>
            <th> Total [Hrs]</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($reports)) {
            $i = 0;
            foreach ($reports as $wrk) {
                
                $work_total_hrs=0;
                $extra_work_hrs=0;
                $extra_work_hrs_holiday=0;
                                    
                $extra_work_hrs=$this->reports_model->get_employee_extra_work($month, $year,$wrk->maid_id);
                $extra_work_hrs_holiday=$this->reports_model->get_employee_extra_work_holiday($month, $year,$wrk->maid_id);
                $normal_work_hrs=$this->reports_model->get_employee_work_normal($month, $year,$wrk->maid_id);
                $work_total_hrs= $normal_work_hrs + $extra_work_hrs_holiday + $extra_work_hrs;
                echo '<tr>'
                . '<td>' . ++$i . '</td>'
                . '<td>' . $wrk->maid_name . '</td>'
                . '<td>'.$extra_work_hrs.'</td>' 
                . '<td>'.$extra_work_hrs_holiday.'</td>'    
                . '<td>' . $normal_work_hrs . '</td>'
                . '<td>' . $work_total_hrs . '</td>'      
                . '</tr>';
            }
        } else {
            echo '<tr><td colspan="6">No Results!</td></tr>';
        }
        ?>
    </tbody>
</table>