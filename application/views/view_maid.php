<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Maid</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url(); ?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>maid/edit/<?php echo $link_maid_id; ?>"><img src="<?php echo base_url(); ?>images/customer-edit.png" title="Edit Maid"/></a></div>
                <div class="topiconnew"><a href="<?php echo base_url(); ?>maids"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Maid List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post">      
                        <div class="tab-content">

                            <?php
                            foreach ($maid_details as $maid_val) {
                                ?> 

                                <div class="tab-pane active" id="personal">
                                    <fieldset>            
                                        <div class="span11">
                                            <div class="widget">                                            
                                                <div class="widget widget-table action-table">
    <!--                                                <div class="widget-header"> <i class="icon-th-list"></i>
                                                        <h3>Personal Details</h3> 
                                                    </div> /widget-header -->
                                                    <div class="widget-content">

                                                        <?php
                                                        if ($maid_val['maid_photo_file'] == "") {
                                                            $image = base_url() . "img/no_image.jpg";
                                                        } else {
                                                            $image = base_url() . "maidimg/" . $maid_val['maid_photo_file'];
                                                        }
                                                        if ($maid_val['maid_gender'] == "F") {
                                                            $gender = "Female";
                                                        } else if ($maid_val['maid_gender'] == "M") {
                                                            $gender = "Male";
                                                        }
                                                        if ($maid_val['maid_passport_expiry_date'] != '' && $maid_val['maid_passport_expiry_date'] != '0000-00-00') {
                                                            list($year, $month, $day) = explode("-", $maid_val['maid_passport_expiry_date']);
                                                            $passport_expiry = "$day/$month/$year";
                                                        } else if ($maid_val['maid_passport_expiry_date'] = '0000-00-00') {
                                                            $passport_expiry = "";
                                                        }
                                                        if ($maid_val['maid_passport_file'] != '') {
                                                            $passport_image = base_url() . "maid_passport/" . $maid_val['maid_passport_file'];
                                                        } else {
                                                            $passport_image = base_url() . "img/no_image.jpg";
                                                        }
                                                        if ($maid_val['maid_visa_expiry_date'] != '' && $maid_val['maid_visa_expiry_date'] != '0000-00-00') {
                                                            list($year, $month, $day) = explode("-", $maid_val['maid_visa_expiry_date']);
                                                            $visa_expiry = "$day/$month/$year";
                                                        } else if ($maid_val['maid_visa_expiry_date'] = '0000-00-00') {
                                                            $visa_expiry = "";
                                                        }
                                                        if ($maid_val['maid_visa_file'] != '') {
                                                            $visa_image = base_url() . "maid_visa/" . $maid_val['maid_visa_file'];
                                                        } else {
                                                            $visa_image = base_url() . "img/no_image.jpg";
                                                        }
                                                        if ($maid_val['maid_labour_card_expiry_date'] != '' && $maid_val['maid_labour_card_expiry_date'] != '0000-00-00') {
                                                            list($year, $month, $day) = explode("-", $maid_val['maid_labour_card_expiry_date']);
                                                            $labour_expiry = "$day/$month/$year";
                                                        } else if ($maid_val['maid_labour_card_expiry_date'] = '0000-00-00') {
                                                            $labour_expiry = "";
                                                        }
                                                        if ($maid_val['maid_labour_card_file'] != '') {
                                                            $labour_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                        } else {
                                                            $labour_image = base_url() . "img/no_image.jpg";
                                                        }
                                                        if ($maid_val['maid_emirates_expiry_date'] != '' && $maid_val['maid_emirates_expiry_date'] != '0000-00-00') {
                                                            list($year, $month, $day) = explode("-", $maid_val['maid_emirates_expiry_date']);
                                                            $emirates_expiry = "$day/$month/$year";
                                                        } else if ($maid_val['maid_emirates_expiry_date'] = '0000-00-00') {
                                                            $emirates_expiry = "";
                                                        }
                                                        if ($maid_val['maid_emirates_file'] != '') {
                                                            $emirates_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                        } else {
                                                            $emirates_image = base_url() . "img/no_image.jpg";
                                                        }
                                                        
                                                        
                                                        if ($maid_val['maid_joining'] != '' && $maid_val['maid_joining'] != '0000-00-00') {
                                                            list($year, $month, $day) = explode("-", $maid_val['maid_joining']);
                                                            $doj = "$day/$month/$year";
                                                        } else if ($maid_val['maid_joining'] = '0000-00-00') {
                                                            $doj = "";
                                                        }
                                                        ?>

                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Maid Name</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_name'] ?></td>  
                                                                    <td style="line-height: 20px; width: 200px"><b>Gender</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $gender ?></td> 
                                                                    <td style="line-height: 20px; width: 200px" rowspan="6">
                                                            <center><img src="<?php echo $image ?>" style="height: 150px; width: 150px"/></center>
                                                        
                                                        </td> 
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Nationality</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_nationality'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Flat</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['flat_name'] ?></td> 

                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Mobile Number 1</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_mobile_1'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Mobile Number 2</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_mobile_2'] ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Present Address</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_present_address'] ?></td>
                                                                    <td style="line-height: 20px; width: 200px"><b>Permanent Address</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $maid_val['maid_permanent_address'] ?></td> 
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 20px; width: 200px"><b>Joining Date</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php echo $doj; ?></td>
                                                                    <!--<td style="line-height: 20px; width: 200px"><b>Team</b></td>
                                                                    <td style="line-height: 20px; width: 300px"><?php// echo $maid_val['team_name'] ?></td>-->
																	<td style="line-height: 20px; width: 200px"></td>
                                                                    <td style="line-height: 20px; width: 300px"></td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="line-height: 20px;" colspan="2"><b><center>Services</center></b></td>
                                                                    <td style="line-height: 20px;" colspan="2"><b><center>Notes</center></b></td>
                                                                </tr>
                                                                <tr>
                                                                     
                                                                
                                                                    <td style="line-height: 20px;" rowspan="2" colspan="2">
                                                                        
                                                                        <?php
                                                                    if (count($maid_services) > 0) {
                                                                        $j =1;          
                                                                        foreach ($maid_services as $maid_services_val) {
                                                                            ?> 
                                                                                    <?php echo $j++; ?> .&nbsp;
                                                                                    <?php echo $maid_services_val['service_type_name'] ?> &nbsp; <br>

                                                                            <?php }
                                                                    }?>    
                                                                    </td>
                                                                    <td style="line-height: 20px;" rowspan="2" colspan="2" ><?php echo $maid_val['maid_notes'] ?></td>
                                                                    <td style="line-height: 20px;" >   
                                                                        <span>
                                                                <?php  
                                                                if (($maid_val['maid_status']!=2) && (user_authenticate() == 1)) {
                                        ?>
                                            
                                                                            <input class="btn btn-primary pull-right red" value="Delete" name="maid_delete" onclick="return change_status(<?php echo $maid_val['maid_id'] ?>,'2');" type="button">
                                           
                                        <?php  
                                         } 
                                         ?>
                                                                
                                                            </span>
                                                                    </td>
                                                                </tr>      

                                                            </tbody>
                                                        </table>
                                                    </div><!-- /widget-content -->    
                                                </div> 
                                            </div> <!-- /widget -->
                                        </div> <!-- /span6 -->
                                    </fieldset>  
                                </div>
                              

<?php }
?>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->						
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>-->
