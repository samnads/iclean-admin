<div class="row">
    <!-- /span6 --> 
    <div class="span6" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Add User</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" href="<?php echo base_url(); ?>users"><img src="<?php echo base_url();?>img/male-list.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="useraadd" action="<?php echo base_url(); ?>users/add/<?php if(!empty($e_user)) { echo $e_user->user_id; } else { echo ''; } ?>" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></div>
                                    </div>
                                    <div class="control-group">											
					<label class="control-label" for="name">Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="usersname" name="name" value="<?php if(!empty($e_user)) { echo $e_user->user_fullname; } else { echo ''; } ?>">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="username">Username</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="user_name" name="username" value="<?php if(!empty($e_user)) { echo $e_user->username; } else { echo ''; } ?>">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    <?php if(empty($e_user)) { ?>
                                    <div class="control-group">											
					<label class="control-label" for="password">Password</label>
                                            <div class="controls">
                                                <input type="password" class="span6" style="width: 300px;" id="password" name="password" value="">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    <?php } ?>
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Is Admin?</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="is_admin" value="Y" <?php if(!empty($e_user)) { if($e_user->is_admin == 'Y') { echo "checked"; } else { echo ""; } } ?> /> Yes</label>
                                                <label class="radio inline"><input type="radio" name="is_admin" value="N" <?php if(!empty($e_user)) { if($e_user->is_admin == 'N') { echo "checked"; } else { echo ""; } } else { echo "checked"; } ?> /> No</label>
                                            </div>	<!-- /controls -->			
                                    </div> <!-- /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary pull-right" value="Submit" name="user_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span6" id="edit_zone" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Edit Zone</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_zone();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="zone" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="zonename">Zone Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_zonename" name="edit_zonename" required="required">
                                                <input type="hidden" class="span6" style="width: 300px;" id="edit_zoneid" name="edit_zoneid">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="drivername">Driver Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_drivername" name="edit_drivername" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                    <div class="control-group">											
                                        <label class="control-label">Is Spare Zone</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="edit_spare" value="Y"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="edit_spare" value="N"> No</label>
                                            </div>	<!-- /controls -->			
                                    </div> <!-- /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary pull-right" value="Submit" name="zone_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>