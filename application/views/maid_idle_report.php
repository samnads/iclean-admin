<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    .excel-icon:hover {
        cursor: pointer;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.5/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Blob.js/1.1.1/Blob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

<div class="row">
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form id="edit-profile" class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>Maid Idle Reports</h3>

                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="idle_date" value="<?php echo $idle_date ?>">

                    <input type="submit" class="btn" value="Go" name="idle_date_sbmt" style="margin-bottom: 4px;">
                    <?php
                    $url_date = str_replace('/', '-', $idle_date);
                    $url_date = date('Y-m-d', strtotime($url_date));
                    ?>
                    <div class="topiconnew border-0 green-btn">
                        <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-file-excel-o excel-icon" style="font-size:22px;color:green"></i></a>
                    </div>
                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" id='idle_table' class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                            <th style="line-height: 18px; width: 70px;"> Service date</th>
                            <th style="line-height: 18px; width: 50px;"> Maid Name</th>
                            <th style="line-height: 18px; width: 50px;"> working Hours</th>
                            <th style="line-height: 18px; width: 50px;"> Idle Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                        foreach ($maids as $maid) {
                            $tota_wh = $this->bookings_model->getbookingsbymaid_id_new($url_date, $maid->maid_id);
                            //    print_r($tota_wh);exit();
                            $tot_working_hrs = 8;
                            if ($tota_wh[0]->total_hrs !== 0.0) {
                                $tothrs = $tota_wh[0]->total_hrs;
                                $tothrs = number_format((float)$tothrs, 1, '.', '');
                                $idle_hours = $tot_working_hrs - $tothrs;
                            } else {
                                $tothrs = 0;
                                $idle_hours = '8';
                            }

                            echo '<tr>'
                                . '<td style="line-height: 18px;">' . $i++ . '</td>'
                                . '<td style="line-height: 18px;">' . date('d-m-Y', strtotime(str_replace('/', '-', $idle_date))) . '</td>'
                                . '<td style="line-height: 18px;">' . html_escape($maid->maid_name) . '</td>'
                                . '<td style="line-height: 18px;">' . $tothrs . '</td>'
                                . '<td style="line-height: 18px;">' . $idle_hours . ' Hours Idle' . '</td>'
                                . '</tr>';
                        } ?>
                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>

<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding="10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead>
                <tr>
                    <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                    <th style="line-height: 18px; width: 70px;"> Service date</th>
                    <th style="line-height: 18px; width: 50px;"> Maid Name</th>
                    <th style="line-height: 18px; width: 50px;"> working Hours</th>
                    <th style="line-height: 18px; width: 50px;"> Idle Hours</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=1;
                foreach ($maids as $maid) {
                    $tota_wh = $this->bookings_model->getbookingsbymaid_id_new($url_date, $maid->maid_id);
                    //    print_r($tota_wh);exit();
                    $tot_working_hrs = 8;
                    if ($tota_wh[0]->total_hrs !== 0.0) {
                        $tothrs = $tota_wh[0]->total_hrs;
                        $tothrs = number_format((float)$tothrs, 1, '.', '');
                        $idle_hours = $tot_working_hrs - $tothrs;
                    } else {
                        $tothrs = 0;
                        $idle_hours = '8';
                    }

                    echo '<tr>'
                        . '<td style="line-height: 18px;">' .$i++. '</td>'
                        . '<td style="line-height: 18px;">' . date('d-m-Y', strtotime(str_replace('/', '-', $idle_date))) . '</td>'
                        . '<td style="line-height: 18px;">' . html_escape($maid->maid_name) . '</td>'
                        . '<td style="line-height: 18px;">' . $tothrs . '</td>'
                        . '<td style="line-height: 18px;">' . $idle_hours . ' Hours Idle' . '</td>'
                        . '</tr>';
                } ?>
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript">
    function formatDate(dateString) {
        if (dateString && typeof dateString === 'string') {
            var parts = dateString.split('/');
            if (parts.length === 3) {
                var formattedDate = parts[0].padStart(2, '0') + '-' + parts[1].padStart(2, '0') + '-' + parts[2];
                return formattedDate;
            }
        }
        return dateString;
    }

    function exportF(elem) {
        var startDate = $("#vehicle_date").val();

        var formattedStartDate = formatDate(startDate);

        var fileName = "Maid_idle_Hours_Report";
        fileName += "_" + formattedStartDate + "_";
        var fileType = "xlsx";
        var table = document.getElementById("divToPrint");
        var wb = XLSX.utils.table_to_book(table, {
            sheet: "Report",
            dateNF: 'dd/mm/yyyy;@',
            cellDates: true,
            raw: true
        });
        const ws = wb.Sheets['Report'];
        var wscols = [{
                wch: 15
            },
            {
                wch: 25
            },
            {
                wch: 30
            },
            {
                wch: 25
            },
            {
                wch: 20
            },
        ];
        ws['!cols'] = wscols;
        return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
    }
</script>