<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>Driver Activity Report</h3>                   
                   
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="activity_date" value="<?php echo $activity_date ?>">                       
                    
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">  
                    <?php 
                            
                            $exceldate = str_replace('/', '-', $activity_date);
                            $exceldate = date('Y-m-d', strtotime($exceldate));
                    ?>
                    <div class="topiconnew">
                        <a href="<?php echo base_url();?>reports/driveractivity_report_to_excel/<?php echo $exceldate;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a>
                    </div>                  
                </form>   
                
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Zone</th>
                            <th style="line-height: 18px;"> Type</th>
                            <th style="line-height: 18px;"> Action</th>                            
                            <th style="line-height: 18px;"> Time</th>     
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($driver_activity_report))
                        {
                            $i = 0;
                            foreach ($driver_activity_report as $activity)
                            {
                                switch ($activity->action_type)
                                {
                                    case 1 : $action_type = 'Vehicle IN';
                                        break;
                                    case 2 : $action_type = 'Vehicle OUT';
                                        break;
                                    case 3 : $action_type = 'Customer IN';
                                        break;
                                    case 4 : $action_type = 'Customer OUT';
                                        break;
                                    case 5 : $action_type = 'Transfer';
                                        break;
                                    case 6 : $action_type = 'Change Maid';
                                        break;
                                    case 7 : $action_type = 'Payment';
                                        break;
                                    case 8 : $action_type = 'Service Cancel';
                                        break;
                                    default : $action_type = 'Invalid';
                                        break;
                                }
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $action_type . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->tracked_time . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
