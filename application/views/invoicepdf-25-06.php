<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service master</title>
</head>
<style>
@page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
</style>
<body style="padding: 0px; margin: 0px;">


<div class="main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
     <div class="header" style="width:100%; height:auto; padding: 10px 0px 20px 0px; margin: 0px; background: #eee;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="10%" rowspan="2" align="right"><img src="<?php echo base_url(); ?>images/invoice-logo-icon.png" width="100" height="104" /></td>
                  <td style=" padding: 15px 0px 5px 0px;"><img src="<?php echo base_url(); ?>images/invoice-logo-name.png" width="199" height="21" /></td>
                  <td width="30%" rowspan="2" valign="bottom">
                  
                  
                  
                         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; margin: 0px;">
                                      <tr>
                                        <td width="50%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                                        <td width="5%">:</td>
                                        <td width="45%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>054 459 0300</strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 5px 0px 0px 0px">Email</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 5px 25px 0px 0px">info@servicemaster.ae</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 6px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 6px 25px 0px 0px">0087267213283283</td>
                                      </tr>
                                      
                             </table>
                  
                  
                  
                  </td>
                </tr>
                <tr>
                  <td width="60%"><p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 22px; padding: 0px 25px 0px 0px; margin: 0px;">
                            <strong>Service Master Cleaning Services L.L.C.</strong><br />
                            ACICO Business Park, Office no. 603-A Rm. 265<br />
                            Al Hilal Al Arabi Business Centre Port Saeed Deira, Dubai, UAE.</p>
                  </td>
                </tr>
              </table>

     </div>
     
     
     <div class="main-content" style="width: 90%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
          <div class="tax-head" style="width:100%; height:auto; padding: 110px 0px 50px 0px; margin: 0px;">
                 <p style="font-family: Roboto, sans-serif; font-size: 50px; color: #555; line-height: 30px; padding: 0px; margin: 0px; text-align: center;">TAX INVOICE</p>
          </div>
             
       <div class="to-job" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px 0px 50px 0px;">
                  
                  <div class="to-address" style="width:100%; height:auto; background: #e5f6fc; padding: 20px 0px 25px; margin: 0px;">
                  
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="middle">
                                <p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; padding: 0px 25px 0px; margin: 0px;"><strong>To</strong></p>
                            
                                <p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 22px; padding: 0px 25px 0px 45px; margin: 0px;">
                            <strong>Mathew Thomas</strong><br />
                            Western Union Financial Services Inc.<br />
                            Apt No: 408, Dubai Internet City, Building 12 4th Floor.<br />
                       </p>
                       
                       
                        <p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; padding: 10px 25px 0px 45px; margin: 0px; font-weight: bold;">Customer TRN &nbsp;&nbsp;&nbsp; :&nbsp; 0087267213283283</p>
                        
                            </td>
                            
                            <td align="right" valign="middle"> 
                              
                                
                                
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; margin: 0px;">
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px"><strong>Invoice Number</strong></td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><strong>SM-652358</strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 10px 0px 0px 0px">Invoice Date</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 10px 25px 0px 0px">26 June - 2020</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 10px 0px 0px 0px">Due Date</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 10px 25px 0px 0px">26 June - 2020</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 10px 0px 0px 0px">Prepared By</td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 10px 25px 0px 0px">Nancy Hendry</td>
                                      </tr>
                                    </table>

                                
                                
                                
                                
                            </td>
                          </tr>
                        </table>

                  
                  
                       
                       
                       
         </div>
                  
                  
                   
       </div>
             
          <div class="table-main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px; border:1px solid #ffe0e0;">
          
          
            <table width="100%" border="1" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 22px; padding: 0px; margin: 0px;">
                    <tr class="table-head" bgcolor="#fe6565" style=" font-size: 16px; line-height: 30px; font-weight: bold; color: #FFF;">
                          <td width="8%" align="center" valign="middle" style="padding: 10px 10px;">Sl.No</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px;">Date & Time</td>
                          <td width="31%" valign="middle" style="padding: 10px 10px;">Description</td>
                          <td width="7%" align="center" valign="middle" style="padding: 10px 10px;">Hours</td>
                          <td width="10%" align="center" valign="middle" style="padding: 10px 10px;">Amount</td>
                          <td width="10%" align="center" valign="middle" style="padding: 10px 10px;">VAT-5%</td>
                          <td width="14%" align="center" valign="middle" style="padding: 10px 10px;">Net Amount</td>
                    </tr>
              
                    <tr bgcolor="#FFF">
                            <td align="center" style="padding: 10px 10px;">01</td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 17px;">24/06/2020</strong><br />
                            09:30 AM  - 12:00 PM</td> 
                             
                            <td style="padding: 10px 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                            <td align="center" style="padding: 10px 10px;">2.5</td>
                            <td align="center" style="padding: 10px 10px;">87.50</td>
                            <td align="center" style="padding: 10px 10px;">4.38</td>
                            <td align="center" style="padding: 10px 10px;">91.88</td>
                    </tr>
                    
                    
                    <tr bgcolor="#e5f6fc">
                            <td align="center" style="padding: 10px 10px;">02</td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 17px;">24/06/2020</strong><br />
                            09:30 AM  - 12:00 PM</td>
                             
                            <td style="padding: 10px 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                            <td align="center" style="padding: 10px 10px;">2.5</td>
                            <td align="center" style="padding: 10px 10px;">87.50</td>
                            <td align="center" style="padding: 10px 10px;">4.38</td>
                            <td align="center" style="padding: 10px 10px;">91.88</td>
                    </tr>
                    
                    
                    <tr bgcolor="#FFF">
                            <td align="center" style="padding: 10px 10px;">03</td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 17px;">24/06/2020</strong><br />
                            09:30 AM  - 12:00 PM</td>
                             
                            <td style="padding: 10px 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                            <td align="center" style="padding: 10px 10px;">2.5</td>
                            <td align="center" style="padding: 10px 10px;">87.50</td>
                            <td align="center" style="padding: 10px 10px;">4.38</td>
                            <td align="center" style="padding: 10px 10px;">91.88</td>
                    </tr>
                    
                    
                    <tr bgcolor="#e5f6fc">
                            <td align="center" style="padding: 10px 10px;">04</td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 17px;">24/06/2020</strong><br />
                            09:30 AM  - 12:00 PM</td>
                             
                            <td style="padding: 10px 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                            <td align="center" style="padding: 10px 10px;">2.5</td>
                            <td align="center" style="padding: 10px 10px;">87.50</td>
                            <td align="center" style="padding: 10px 10px;">4.38</td>
                            <td align="center" style="padding: 10px 10px;">91.88</td>
                    </tr>
                    
                    
                    <tr bgcolor="#FFF">
                            <td align="center" style="padding: 10px 10px;">05</td>
                            <td style="padding: 10px 10px;"><strong style="font-size: 17px;">24/06/2020</strong><br />
                            09:30 AM  - 12:00 PM</td>
                             
                            <td style="padding: 10px 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                            <td align="center" style="padding: 10px 10px;">2.5</td>
                            <td align="center" style="padding: 10px 10px;">87.50</td>
                            <td align="center" style="padding: 10px 10px;">4.38</td>
                            <td align="center" style="padding: 10px 10px;">91.88</td>
                    </tr>
                    
                               
       </table>
       
      

       </div>
       
       
       
       <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">
       
       
               <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffefef" style="font-family: Roboto, sans-serif; font-size:18px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">
               
               
               <tr style="font-size: 18px; color: #333;">
                  <td  valign="middle" style="padding: 20px 0px 0px 20px;">Net Amount</td>
                  <td  align="right" valign="middle" style="padding: 20px 20px 0px 0px; font-size: 22px;">  355.50 <span style="font-size:16px; ">AED</span></td>
                </tr> 
                
                
                <tr style="font-size: 18px; color: #333;">
                  <td  valign="middle" style="padding: 10px 0px 20px 20px;">Vat 5%</td>
                  <td  align="right" valign="middle" style="padding: 10px 20px 20px 0px; font-size: 22px;">  20.50 <span style="font-size:16px; ">AED</span></td>
                </tr> 
              
                
              </table>
       
       
       </div>
       
       
       
       <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">
       
       
               <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#00a8de" style="font-family: Roboto, sans-serif; font-size:18px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">
               
               
 
                <tr>
                  <td  valign="middle" style="padding: 20px 0px 10px 20px;">Total Amount</td>
                  <td  align="right" valign="middle" style="font-size: 32px; padding: 20px 20px 10px 0px;"> 376.00 <span style="font-size:16px; ">AED</span></td>
                </tr> 
                
                
                <tr>
                  <td colspan="2" align="right"  valign="middle" style="padding: 0px 20px 20px 20px;">AED <strong>Three Hundred Seventy Six</strong> Only</td>
                 </tr> 
              </table>
       
       
       
       
       </div>
       
       
             
       <div class="bot-text" style="width:100%; height:auto; padding: 200px 0px 100px 0px; margin: 0px;">
               
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; padding: 10px 0px 0px 0px; margin: 0px;">Accountant Signature :</p>
                    </td>
                    <td align="right" valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:16px; color: #555; line-height: 20px; padding: 10px 0px 0px 0px; margin: 0px;">
                            Customer Signature
                        </p>
                    </td>
                  </tr>
                </table>

          </div>
             
          
  
  
  </div>
      
      <div class="footer" style="width:100%; height:auto; padding: 15px 0px; background: #ffe0e0; margin: 0px; text-align:center;">
               <a href="https://servicemaster.ae/" target="_blank" style="font-family: Roboto, sans-serif; font-size:14px; color: #555; line-height: 20px; text-decoration: none; padding: 10px 0px 0px 0px; margin: 0px;">www.servicemaster.ae</a>
          </div>
  
</div>

</body>
</html>