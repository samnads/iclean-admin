<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                    <i class="icon-th-list"></i>
                    <h3>User Login Info</h3>
					<a href="<?php echo base_url(); ?>users" style="float:right ; margin-right:15px; cursor:pointer;">
					<i class="icon-tasks"></i>
					<span></span>
					</a>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Name</th>
                            <th style="line-height: 18px;"> Login IP</th>
                            <th style="line-height: 18px;"> Agent</th>                            
                            <th style="line-height: 18px;"> Login Date Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($userlogininfo))
                        {
                            $i = 0;
                            
                            foreach ($userlogininfo as $userlogin)
                            {
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->user_fullname . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->login_ip . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->user_agent . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->login_date_time . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
