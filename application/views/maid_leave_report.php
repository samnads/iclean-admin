<style>
    .add-leave {
    background-color: #b2d157;
    background-image: linear-gradient(to bottom, #b2d157, #64a434);
    border: 0 none;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 14px;
    font-weight: bold;
    margin-right: 20px;
    padding: 6px 15px;
    text-decoration: none;
    text-transform: uppercase;
}
</style>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   

    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="maid_leave" class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/maid-leave-report'?>">
                    <i class="icon-th-list"></i>
                    <h3>Maid Leave Reports</h3>  
                    
                        <select name="maid_id" id="search-maid-id" style="margin-bottom: 4px;" class="sel2">
                                <option value="0">All Maids</option>
                                    <?php
									//$maid_id = "";
                                    foreach($maids as $maid)
                                    {       
                                            $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                                            echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                                    }
                                    ?>
                         </select> 
                    <?php 
                    if(isset($startdate)){ $s_date = $startdate; }
                    else { $s_date = date('d/m/Y'); }
                    if(isset($enddate)){ $e_date = $enddate; }
                    else { $e_date = date('d/m/Y'); }
                      ?>
                        <input type="text" name="start_date" id="start-date" class="span3" style="margin-bottom: 4px;width: 90px;" value="<?php echo $s_date ?>" placeholder="Start date" >
                        <input type="text" name="end_date" id="end-date" class="span3" style="margin-bottom: 4px;width: 90px;" value="<?php echo $e_date ?>" placeholder="End date">
                        
                        <input type="submit" class="btn" value="Go" id="searchgo" name="search" style="margin-bottom: 4px;">
                        <input type="button" class="add-leave" value="Add Leave" id="add_leave" name="leave" style="margin-bottom: 4px; display: none;">
                        <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="printButnforleave" title="Print"/></a></div>
                        <?php $maidtext=($maid_id=='0'||!is_numeric($maid_id))?'All':$maid_id;  ?>
                        <div class="topiconnew"><a href="<?php echo base_url(); ?>reports/maidleavereporttoExcel/<?php echo $maidtext;?>/<?php echo date("Y-m-d",strtotime(str_replace('/', '-',$s_date)));?>/<?php echo date("Y-m-d",strtotime(str_replace('/', '-',$e_date)));?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel"></a></div>
                        <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php // echo base_url(); ?>img/printer.png" id="printButnforleave" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                 <div id="LoadingImage" style="text-align:center;display:none;"><img src="<?php echo base_url() ?>img/loader.gif"></div>

                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                            <?php if(!@$maid_id){ ?>
                            <th style="line-height: 18px; width: 30px;"> Maid</th>
                            <?php }?>
                            <th style="line-height: 18px; width: 50px;"> Date</th>
                            <th style="line-height: 18px; width: 50px;"> Type</th>
                            <th style="line-height: 18px; width: 50px;"> Leave Type</th>
                            <th style="line-height: 18px; width: 50px;"> Actions </th>
                                                 
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                        
                        if(!empty($results))
                        {
                            $i = 0;
                            foreach ($results as $lists)
                            {
                                if($lists->typeleaves == "leave")
                                {
                                    $leave = "Leave";
                                } else if($lists->typeleaves == "absent")
                                {
                                    $leave = "Absent";
                                } else if($lists->typeleaves == "dayoff")
                                {
                                    $leave = "Dayoff";
                                } else {
                                    $leave = "";
                                }
                                $leavedate = @$lists->leave_date;
                                //$originalDate = "2010-03-21";
                                $newDate = date("d/m/Y", strtotime($leavedate));
                                
                               
                                echo '<tr>'
                                        .'<td style="line-height: 18px;">' . ++$i . '</td>';
                                          if(!@$maid_id)
                                          {
                                        echo '<td style="line-height: 18px;">' . $lists->maid_name . '</td>';
                                          }       
                                        echo '<td style="line-height: 18px;">' . $newDate . '</td>'
                                        . '<td style="line-height: 18px;">' . @$type[$lists->leave_type] . '</td>'
                                         . '<td style="line-height: 18px;">' . $leave . '</td>' ;
                                        ?>                   
                    <td style="line-height: 18px;"><a href="javascript:void(0);" onclick="delete_maidleaves(this,<?php echo $lists->leave_id ?>, <?php echo $lists->leave_status ?>);" style="text-decoration:none;"><?php echo $lists->leave_status == 1 ? '<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>' : '<i class="btn-icon-only icon-ok" style="font-size:25px;"> </i>' ?></a></td>
                                   <?php echo '</tr>';
                            }
                        }
                        else
                        {
                          // echo '<tr><td>No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<!-- For Print -->
<div id="divForPrintLeave" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="70%" cellspacing="0" cellpadding = "0" align="center">
              <thead>
                        <tr>
                            <th style="line-height: 18px; width: 75px; padding: 4px;"> Sl.NO</th>
                            <th style="line-height: 18px; width: 350px; padding: 4px;"> Maid Name</th>
                            <th style="line-height: 18px; width: 175px; padding: 4px;"> Total Full Day</th>
                            <th style="line-height: 18px; width: 175px; padding: 4px;">Total Half Day</th>
                            <th style="line-height: 18px; width: 175px; padding: 4px;">Total Day</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        if ($leaveresults) {
                        $i = 0;
                        $total = 0;
                            foreach ($leaveresults as $leaveresultsrow){
                                $totalday = ($leaveresultsrow->fullday + ($leaveresultsrow->halfday/2));
                                $total += $totalday;
                           ?>
                        
                        <tr>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo ++$i; ?></td>
                            <td style="line-height: 18px; padding: 4px;"><?php echo $leaveresultsrow->maid_name; ?></td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->fullday; ?></td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->halfday; ?></td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $totalday; ?></td>
                        </tr>
                            
                            <?php
                            }
                            ?>
                            <tr>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
                            <td style="line-height: 18px; padding: 4px;"></td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;">Total :</td>
                            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo number_format($total,1); ?></td>
                        </tr>
                        <?php }
                            ?>
                         
                    </tbody>
                    
        </table>
    </div><!-- /widget-content --> 
</div>

<script  type="text/javascript">
     $('#maid_leave').submit(function(e) {
        var from = document.getElementById("start-date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("end-date").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
     </script>


