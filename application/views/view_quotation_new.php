<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDpDi6GKhcr4mTO180oPmDh3cFQ5ciBP94&amp;libraries=places"></script>-->
<section>
    <div class="row invoice-field-wrapper no-left-right-margin">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>View Quotation</h3>
            <a href="<?php echo base_url(); ?>invoice/list_customer_quotations" class="btn" style="float: right; margin: 5px 15px 5px 0px;">Back</a>
            <a class="btn" style="margin: 5px 15px 5px 5px; float:right;" href="<?php echo base_url(); ?>invoice/generatequotation/<?php echo $quotation_detail->quo_id; ?>" target="_blank" title="Download Quotation">
                <i class="btn-icon-only fa fa-download "> </i>
            </a>
            <?php
            if($invoice_detail[0]->invoice_status == 0)
            {
            ?>
            <!--<a class="btn" id="inv-edit" style="margin: 5px 10px 5px 5px; float:right;">Edit</a>-->
            <!-- <div style="margin: 5px 10px 5px 5px; float:right;" class="btn" id="inv-validate" data-custid="<?php echo $quotation_detail->customer_id; ?>" data-invamt="<?php echo $quotation_detail->invoice_net_amount; ?>" data-invid="<?php echo $quotation_detail->quo_id; ?>">Validate</div> -->
            <?php
            }
            ?>
            <?php
            if($invoice_detail[0]->invoice_status == 0 || $invoice_detail[0]->invoice_status == 1 && $invoice_detail[0]->invoice_paid_status == 0)
            {
            ?>
            <!-- <div style="margin: 5px 10px 5px 5px; float:right;" class="btn" id="inv-cancel" data-custid="<?php echo $quotation_detail->customer_id; ?>" data-invamt="<?php echo $quotation_detail->invoice_net_amount; ?>" data-invid="<?php echo $invoice_detail[0]->invoice_id; ?>" data-invstat="<?php echo $quotation_detail->invoice_status; ?>">Cancel Invoice</div> -->
            <?php
            }
            ?>
            <div style="margin: 5px 10px 5px 5px; float:right;" class="btn send_quo_mail"   data-quotationid="<?php echo $quotation_detail->quo_id; ?>"  data-quotationemail="<?php echo $quotation_detail->email_address; ?>"><span class="btntxt">Send Quotation Mail</span></div>
        </div>
        <div class="col-md-12 col-sm-12 invoice-box-main  box-center no-left-right-padding">
            
            <!--<h2 class="text-center">Invoice Details</h2>-->
            <div class="col-md-12 col-sm-12 invoice-box-right-main no-left-padding">
                <div class="col-md-12 col-sm-12 invoice-box-right">
                    <div class="col-md-9 col-sm-9 invoice-logo-box">
                        <div class="invoice-logo"><img src="https://iclean.bh/images/logo.png"></div>
                    </div>
                    <div class="col-md-3 col-sm-3 invoice-logo-box">
                        <div class="invoice-logo-text" style="padding-top: 12px; font-weight: bold; font-size:20px; color: #3a2c70;">
                            
                            <!-- Status : <span id="invoice-stat-text"><?php echo $invstatus.$paystat; ?></span> -->
                        </div>
                    </div>
                    <form id="quotation-view-form">
                    <!--<form name="invoiceaddform" action="<?php// echo base_url(); ?>invoice/add_invoice" id="invoiceaddform" method="POST">-->
                        <!--<input type="hidden" value="<?php// echo $jobdetail[$bookings_id]->customer_id; ?>" name="jobhiddencustomerid" id="jobhiddencustomerid" />    -->
                        <div class="col-md-12 col-sm-12 invoice-address-box">
                            <div class="col-md-6 col-sm-12 invoice-address no-left-right-padding">
                                <p><strong>I CLEAN SERVICES W.L.L</strong><br>
                                    Road 31, Bldg. 1007/A. Block 635,<br>
                                    Ma'ameer,<br>
                                    Sitra, Kingdom of Bahrain<br><br>
                                    Tel &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    : &nbsp; <strong> +973 1770 0211-135</strong><br>
                                    Email &nbsp;&nbsp;&nbsp;&nbsp; : &nbsp; <strong>info@iclean.bh</strong>
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-12 invoice-date no-left-right-padding">
                                <div class="col-md-3 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Issue date :</p>
                                        <!--<input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text">-->
                                        <p class="issue_date"><?php echo date("d/m/Y",strtotime($quotation_detail->quo_date)); ?> 
                                        <?php if($invoice_detail[0]->invoice_status == '0'){ ?>
                                        <button class="btn btn-small btn-info issuedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>
                                        <?php } ?>
                                        </p>
                                        <p class="issue_date_edit" style="display: none;">
                                            <input type="text" id="invoice_date" value="<?php echo date("d/m/Y",strtotime($invoice_detail[0]->invoice_date));?>" style="width: 92px;">
                                            <button class="btn btn-small btn-info upd_new_iss_dt" data-invoiceid="<?php echo $invoice_detail[0]->invoice_id; ?>">Update</button>
                                            <i class="fa fa-spinner fa-spin iss_dt_loader" style="font-size:20px;display: none;position: relative;top: 3px;"></i>
                                        </p>
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-3 col-sm-12 no-right-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <!-- <p>From :</p>
                                        
                                        <p class=""><?php echo date('h:i A', strtotime($quotation_detail->quo_time_from)); ?>
                                        
                                        </p> -->
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-3 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <!-- <p>To :</p> -->
                                        <!--<input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text">-->
                                        <!-- <p class="issue_date"><?php echo date('h:i A', strtotime($quotation_detail->quo_time_to)); ?>
                                        </p> -->
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-3 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <!-- <p>Hours :</p> -->
                                        <!--<input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text">-->
                                        <!-- <p class="issue_date"><?php echo (strtotime($quotation_detail->quo_time_to) - strtotime($quotation_detail->quo_time_from))/(60*60); ?> 
                                        
                                        </p> -->
                                    </div><!--text-field-main end-->
                                </div>
                                <!--<div class="col-md-12 col-sm-12 no-right-padding">  
                                    <div class="text-field-main">
                                        <p class="text-right">Amount Due : <strong>$0.00</strong></p>
                                    </div><!--text-field-main end-->
                                <!--</div>-->
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-to-address no-left-right-padding">
                                <p><strong>To,</strong><br>
                                <span><strong><?php echo $quotation_detail->quo_customer_name; ?></strong><br>
                                <?php echo $quotation_detail->quo_bill_address; ?><br>
                                </span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-job-det no-left-right-padding">
                                <p><strong>Job</strong><br>
                                <?php
                                $nameOfDay = date('l', strtotime($quotation_detail->quo_date));
                                $day = date('d', strtotime($quotation_detail->quo_date));
                                $month = date('F', strtotime($quotation_detail->quo_date));
                                $year = date('Y', strtotime($quotation_detail->quo_date));
                                ?>
                                <span>For work at: <?php echo $quotation_detail->quo_bill_address; ?>. Date: <?php echo $nameOfDay; ?> <?php echo $day; ?> <?php echo $month; ?> <?php echo $year; ?></span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-table no-left-right-padding">
                                <div class="Table table-top-style-box no-top-border">
                                    <div class="Heading table-head">
                                        <div class="Cell">
                                            <p><strong>Sl. No</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Service</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Description</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Rate</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Quantity</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>VAT</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Net Amount</strong></p>
                                        </div>
                                    </div>
                                    <?php
                                    $i = 1;
                                    
                                       foreach ($quotation_line_items as $line_item) {
                                            
                                    ?>
                                    <div class="Row tr">
                                        <div class="Cell">
                                            <p><?php echo $i; ?></p>
                                        </div>
                                        <div class="Cell">
                                            <p><?php echo $line_item->service_type_name; ?></p>
                                        </div>
                                        <div class="Cell">
                                            <p><?php echo $line_item->qli_description; ?></p>
                                        </div>
                                        <div class="Cell">
                                            <input type="hidden" name="line_id[]" value="<?php echo $line_item->qli_id; ?>"/>
                                            <input type="number" step="any" min="0" name="service_rate[]" value="<?php echo $line_item->qli_rate; ?>" oninput="changedRate(this)"/>
                                        </div>
                                        <div class="Cell">
                                            <input type="number" step="any" min="1" name="service_quantity[]" value="<?php echo $line_item->qli_quantity; ?>" oninput="changedQuantity(this)"/>
                                        </div>
                                        <div class="Cell">
                                            <input type="number" step="any" name="service_vat_charge[]" value="<?php echo number_format($line_item->qli_vat_charge,2); ?>" readonly/>
                                        </div>
                                        <div class="Cell">
                                            <input type="number" step="any" name="service_net_amount[]" value="<?php echo number_format($line_item->qli_price,2); ?>" readonly/>
                                        </div>
                                        
                                    </div>
                                    <?php
                                    $i++; } ?>
                                    <div class="Row">
                                        
                                        <div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-left-border">
                                          <span class="total-text text-right"><b class="pull-right">
                                            <input style="display:none;" type="submit" value="Update" class="save-but" id="update-btn"/>
                                        </b></span>
                                        </div>
                                        <div class="Cell total-box ">
                                           <span class="total-text text-right"><b class="pull-right">Total &nbsp;&nbsp; </b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b class="quotaxamt">BHD <span id="service_vat_charge"><?php echo number_format($quotation_detail->quo_tax_amount,2); ?></span></b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b class="quonetamt">BHD <span id="service_net_amount"><?php echo number_format($quotation_detail->quo_net_amount,2); ?></span></b></span>
                                        </div>
                                    </div>
                                </div><!--Table table-top-style-box end--> 
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-note no-left-right-padding">
                                <p><!--<strong>Note</strong>--><br></p>
                                <!--<span><?php// echo $notes; ?></span>-->
                            </div>
<!--                            <div class="col-md-12 col-sm-12 invoice-button-main no-left-right-padding">
                                <div class="col-md-6 col-sm-12 invoice-button">
                                   &nbsp;
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-left-padding">
                                    <input value="CANCEL" class="text-field-but dark" type="button">
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-right-padding">
                                    <input value="SAVE" name="jobinvoicesave" id="jobinvoicesave" class="text-field-but" type="submit">
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div><!--invoice-box-right end-->
            </div><!--invoice-box-right-main end-->
        </div>
    </div>
</section>

<button type="button" class="btn btn-info btn-lg openemailmodal hidden" data-toggle="modal" data-target="#emailModal">Open Modal</button>

<!-- Modal -->
<div id="emailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
          <div id="emailpopmsg"></div>
          <div class="form-group">
            <label for="email_content">Email Content:</label>
            <textarea class="form-control" id="email_content"></textarea>
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email">
            <input type="hidden" class="form-control" id="hidquoid">
          </div>
          
          <button type="submit" class="btn btn-default emailsubbtn">Submit</button>
          <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

<script type="text/javascript">
$( document ).ready(function() {

    $(".quoedit").click(function() {
        var quotationid=$(this).data("quotationid");
        $(".quoadd").hide();
        $(".quoaddr"+quotationid).hide();        
        $(".quoadd"+quotationid).show();
    });  
    $(".addrupdatebtn").click(function() {
        var quotationid=$(this).data("quotationid");
        $(this).prop('disabled', true);
        var address=$(".newadd_"+quotationid).val();   
                
        $(".addrload"+quotationid).show();
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_quo_addr",
                   type: "post",
                   data: {address:address,quotationid:quotationid} ,
                   success: function (response) {
                        $(".addrload"+quotationid).hide();
                        $(".addrupdatebtn[data-lineid='"+quotationid+"']").prop('disabled', false);
                        $(".quoaddrtext"+quotationid).html(address); 
                        $(".quoaddr"+quotationid).show();        
                        $(".quoadd"+quotationid).hide();

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".addrload"+quotationid).hide();
                        $(".adddrupdatebtn[data-lineid='"+quotationid+"']").prop('disabled', false);
                        alert("Error!Try again later.");

                   }
               });
    });  

    $(".send_quo_mail").click(function() {
        var quotationid=$(this).data('quotationid');
        var emailid=$(this).data('quotationemail');
        $("#email").val(emailid);$("#hidquoid").val(quotationid);
        $("#emailpopmsg").html('');
        $("#emailpopmsg").html('');
        $(".openemailmodal").click();
    });


    $(".emailsubbtn").click(function() {
        $("#emailpopmsg").html('');
        var quoid=$("#hidquoid").val();
        var quotationcontent=$("#email_content").val();
        var quotationemail=$("#email").val();
        //console.log(invoiceid);
        if(quotationcontent=='')
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please enter email content!</strong></div>');
            return false;
        }

        if(!validateEmail(quotationemail))
        {
            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check the email entered!</strong></div>');
            return false;
        }



        $(this).hide();
        $(".emailloadbtn").show();
        $.ajax({
                   url: "<?php echo base_url();?>invoice/send_quotation_email",
                   type: "post",
                   data: {quoid:quoid,quotationcontent:quotationcontent,quotationemail:quotationemail} ,
                   success: function (response) {
                        $(".emailsubbtn").show();
                        $(".emailloadbtn").hide();
                        if(response=='success')
                        {
                            $("#emailpopmsg").html('<div class="alert alert-success text-center"><strong>Quotation email sent successfully!</strong></div>');
                            $("#email_content,#email").val('')
                            //$(".openmodal").click(); 
                        }
                        else if(response=='email_error')
                        {
                            $("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check customer email!</strong></div>');
                            //$(".openmodal").click();
                        }
                        else
                        {
                            $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                            //$(".openmodal").click();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".emailsubbtn").show();
                        $(".emailloadbtn").hide();
                        $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                        //$(".openmodal").click();

                   }
               });
    });



    $('body').on('click',".issuedt_edtbtn",function() {
        $(".issue_date").hide();
        $(".issue_date_edit").show();
    });



    $(".upd_new_iss_dt").click(function() {
        var invoice_id=$(this).data('invoiceid');
        var new_issue_date=$("#invoice_date").val();
        $(".iss_dt_loader").show();
        $(this).prop('disabled', true);
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_invoice_issue_date",
                   type: "post",
                   data: {invoice_id:invoice_id,new_issue_date:new_issue_date} ,
                   success: function (response) {
                        $(".iss_dt_loader").hide();
                        $(".upd_new_iss_dt").prop('disabled', false);
                        if(response=='success')
                        {
                            var date_parts = new_issue_date.split('/');
                            var newdate_ymd=date_parts[2] +'-'+ date_parts[1] +'-'+ date_parts[0];
                            $(".issue_date").html(newdate_ymd+' <button class="btn btn-small btn-info issuedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>');
                            $(".issue_date").show();
                            $(".issue_date_edit").hide();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".iss_dt_loader").hide();
                        $(".upd_new_iss_dt").prop('disabled', false);                       

                   }
               });

    });


    $('body').on('click',".duedt_edtbtn",function() {
        $(".due_date").hide();
        $(".due_date_edit").show();
    });


    $(".upd_new_due_dt").click(function() {
        var invoice_id=$(this).data('invoiceid');
        var new_due_date=$("#search_date_to").val();
        $(".iss_dt_loader").show();
        $(this).prop('disabled', true);
        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_invoice_due_date",
                   type: "post",
                   data: {invoice_id:invoice_id,new_due_date:new_due_date} ,
                   success: function (response) {
                        $(".due_dt_loader").hide();
                        $(".upd_new_due_dt").prop('disabled', false);
                        if(response=='success')
                        {
                            var date_parts = new_due_date.split('/');
                            var newdate_ymd=date_parts[2] +'-'+ date_parts[1] +'-'+ date_parts[0];
                            $(".due_date").html(newdate_ymd+' <button class="btn btn-small btn-info duedt_edtbtn" ><i class="btn-icon-only fa fa-edit"> </i></button>');
                            $(".due_date").show();
                            $(".due_date_edit").hide();
                        }

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".due_dt_loader").hide();
                        $(".upd_new_due_dt").prop('disabled', false);                       

                   }
               });

    });


    $('body').on('click',".quoamtedit",function() {
    //$(".descedit").click(function() {
        var quotationid=$(this).data("quotationid");
        //$(".jobtxa").hide();
        $(".quotamt"+quotationid).hide();        
        $(".amttxt"+quotationid).show();
    });  

    $(".amtupdatebtn").click(function() {
        var quotationid=$(this).data("quotationid");
        var updateamt=$(".amtinp_"+quotationid).val();
        var line_old_amount=$(".quotamttext"+quotationid).html();
        var quo_id='<?php echo $quotation_detail->quo_id; ?>';
        if(updateamt.length==0 || isNaN(updateamt)){alert("Please check the entered amount");return false;}
        $(".amtload"+quotationid).show();
        $(this).prop('disabled', true);

        $.ajax({
                   url: "<?php echo base_url();?>invoice/update_quotation_amt",
                   type: "post",
                   dataType: "json",
                   data: {quotation_amount:updateamt,quotation_id:quotationid,quo_old_amount:line_old_amount} ,
                   success: function (response) {
                        $(".amtload"+quotationid).hide();
                        $(".amtupdatebtn[data-quotationid='"+quotationid+"']").prop('disabled', false);
                        $(".quotamttext"+quotationid).html(updateamt); 
                        $(".quotamt"+quotationid).show();        
                        $(".amttxt"+quotationid).hide();
                        $(".quovatamttext"+quotationid).html(response.quo_tax_amount);
                        $(".quonetamttext"+quotationid).html(response.quo_total_amount);

                        $(".quototamt").html('BHD '+response.quo_net_amount);
                        $(".quotaxamt").html('BHD '+response.quo_tax_amount);
                        $(".quonetamt").html('BHD '+response.quo_total_amount);

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".amtload"+quotationid).hide();
                        $(".amtupdatebtn[data-lineid='"+quotationid+"']").prop('disabled', false);
                        //alert("Error!Try again later.");

                   }
               });
        
    });

});

function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
$(document).ready(function () {
    $("#quotation-view-form").validate({
        ignore: [],
        rules: {
            'service_rate[]': {
                required: true
            },
            'service_quantity[]': {
                required: true
            },
            'service_vat_charge[]': {
                required: true
            },
            'service_net_amount[]': {
                required: true
            },
        },
        errorPlacement: function (err, ele) {
            alert();
            err.insertAfter(ele);
        },
        submitHandler: function (form, element) {
            $('.mm-loader').css('display', 'block');
            fm = new FormData(form);
            fm.append('action','update-quotation');
            $.ajax({
                url: _page_url,
                type: 'post',
                data: fm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        $('.mm-loader').css('display', 'none');
                        _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close_success"></span></div><div class="content padd20">Quotation updated successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close_success" /></div></div>';
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'fade',
                            closeClick: false,
                            openSpeed: 100,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                                }
                            },
                            padding: 0,
                            closeBtn: false,
                            content: _alert_html
                        });
                        $('#update-btn').hide();
                    } else {
                        $('.mm-loader').css('display', 'none');
                    }
                },
                error: function (data) {
                    $('.mm-loader').css('display', 'none');
                }
            })
        }
    })
});
$('body').on('click', '.pop_close_success', function() {
	parent.$.fancybox.close();
});
function changedRate(self){
    var service_rate = self.value;
    var quanity = $(self).closest('.tr').find("input[name='service_quantity[]']").val() || 0;
    var service_without_vat = service_rate * quanity;
    var vat_charge = (10 / 100) * service_without_vat;
    $(self).closest('.tr').find("input[name='service_vat_charge[]']").val(vat_charge.toFixed(2));
    var row_total = service_without_vat + vat_charge;
    $(self).closest('.tr').find("input[name='service_net_amount[]']").val(row_total);
    showHideUpdate();
    calcTotal();
}
function showHideUpdate() {
    var show = true;
    $('input[name^="service_rate"]').each(function(){
        if($(this).val()){
            $('#update-btn').show();
        }
        else{
            $('#update-btn').hide();
            show = false;
            return false;
        }
    });
    if(show){
        $('input[name^="service_quantity"]').each(function(){
            if($(this).val()){
                $('#update-btn').show();
            }
            else{
                $('#update-btn').hide();
                return false;
            }
        });
    }
}
function changedQuantity(self) {
    var quanity = self.value;
    var service_rate = $(self).closest('.tr').find("input[name='service_rate[]']").val() || 0;
    var service_without_vat = service_rate * quanity;
    var vat_charge = (10 / 100) * service_without_vat;
    $(self).closest('.tr').find("input[name='service_vat_charge[]']").val(vat_charge.toFixed(2));
    var row_total = service_without_vat + vat_charge;
    $(self).closest('.tr').find("input[name='service_net_amount[]']").val(row_total);
    showHideUpdate();
    calcTotal();
}
function calcTotal(){
    var service_vat_charge_total = 0;
    var service_net_amount_total = 0;
    $('input[name^="service_vat_charge"]').each(function(){
        service_vat_charge_total += parseFloat($(this).val());
    });
    $('input[name^="service_net_amount"]').each(function(){
        service_net_amount_total += parseFloat($(this).val());
    });
    $('#service_vat_charge').html(service_vat_charge_total.toFixed(2));
    $('#service_net_amount').html(service_net_amount_total.toFixed(2));
}
</script>