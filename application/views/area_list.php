<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">       
    <div class="span7">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Area</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_area();"><img src="<?php echo base_url();?>img/add.png" title="Add Area"/></a>                    
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
<!--                <table class="table table-striped table-bordered">-->
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Area </th>
                            <th style="line-height: 18px"> Zone</th>
                            <th style="line-height: 18px"> Driver</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($areas) > 0) {
                            $i = 1;
                            foreach ($areas as $areas_val) {
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['area_name'] ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['zone_name'] ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['driver_name'] ?></td>
                                    <td style="line-height: 18px" class="td-actions">
										<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_area(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
										<?php if(user_authenticate() == 1) {?>
											<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_area(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
										<?php } ?>
									</td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>   

                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div><!-- /span5 --> 
    <div class="span5" id="add_area" style="display: none; float: right; margin-left: 15px;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Add Area</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_area();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url(); ?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Area Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="areaname" name="areaname" required="required">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="zone_id" id="zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <br />

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="area_sub">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span5 -->
    <div class="span5" id="edit_area" style="display: none; float: right; margin-left: 15px;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Edit Area</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_area();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Area Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="edit_areaname" name="edit_areaname" required="required">
                                        <input type="hidden" class="span3" id="edit_areaid" name="edit_areaid">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="edit_zone_id" id="edit_zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <br />

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="area_edit">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span5 -->
</div>