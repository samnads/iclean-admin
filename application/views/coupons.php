<div class="row">

    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Coupons</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>coupons/add"><img src="<?php echo base_url(); ?>img/add.png" title="Add User"/></a>
            </div>            
            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl.No. </th>
                            <th style="line-height: 18px; text-align: center;">Name </th>
                            <th style="line-height: 18px; text-align: center;"> Discount Price</th>
                            <th style="line-height: 18px; text-align: center;"> Offer Type</th>
                            <!--<th style="line-height: 18px; text-align: center;"> Discount Type</th>-->
							<th style="line-height: 18px; text-align: center;"> Coupon Type</th>
                            <th style="line-height: 18px; text-align: center;"> Week Days</th>
                            <th style="line-height: 18px; text-align: center;">Validity </th>
                            <th style="line-height: 18px; text-align: center;">Min Hrs </th>
                            <th style="line-height: 18px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; text-align: center;"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($coupons) > 0) {
                            $j = 1;
                            foreach ($coupons as $coupon) {
								if($coupon->status == 1)
								{
									$status = "Active";
								} else {
									$status = "Inactive";
								}
								
								if($coupon->coupon_type == "FT")
								{
									$coupon_type = "First Booking";
								} else {
									$coupon_type = "All Bookings";
								}
								
								if($coupon->offer_type == "F")
								{
									if($coupon->discount_type == 0)
									{
										$offertype = "Percentage";
									}
									else
									{
										$offertype = "Flat Rate";
									}
									
								} else {
									$offertype = "Per Hour";
								}
								
								// if($coupon->discount_type == "0")
								// {
									// $discounttype = "Percentage based";
								// } else {
									// $discounttype = "Price based";
								// }
								
								if($coupon->valid_week_day != "")
								{
									$arr = explode(",",$coupon->valid_week_day);
									$weekarrays = array();
									for($i=0; $i<7; $i++)
									{
										if(in_array($i, $arr))
										{
											if($i == 0)
											{
												$dayname = "SUN";
											} else if($i == 1){
												$dayname = "MON";
											} else if($i == 2){
												$dayname = "TUE";
											} else if($i == 3){
												$dayname = "WED";
											} else if($i == 4){
												$dayname = "THU";
											} else if($i == 5){
												$dayname = "FRI";
											} else if($i == 6){
												$dayname = "SAT";
											}
											array_push($weekarrays, $dayname);
										}
									}
									$validweekday = implode(",", $weekarrays);
									
								} else {
									$validweekday = "";
								}
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px; text-align: center;"><?php echo $j; ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->coupon_name ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->percentage ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $offertype; ?></td>
                                    <!--<td style="line-height: 18px; text-align: center;"><?php// echo $discounttype; ?></td>-->
									<td style="line-height: 18px; text-align: center;"><?php echo $coupon_type; ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $validweekday; ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->expiry_date ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->min_hrs; ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $coupon->status == 1 ? '<span style="color:rgb(35, 169, 35); border-radius:1px; font-size:18px; cursor: pointer;"><i class="btn-icon-only icon-ok-sign"> </i></span>' : '<span style="color:rgb(244, 55, 55); border-radius:1px; font-size:18px; cursor: pointer;"><i class="btn-icon-only icon-remove-sign"> </i></span>'; ?></td>
									<td style="line-height: 18px; text-align: center;">
										<a class="btn btn-small btn-warning" title="Edit" href="<?php echo base_url() . 'coupons/edit/' . $coupon->coupon_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>
                                        <!--<a class="btn btn-small btn-info" title="Login Info" href="<?php// echo base_url() . 'users/userlogininfo/' . $coupon->coupon_id ?>"><i class="btn-icon-only icon-search" style="font-size: 13px;"> </i></a>-->
                                        <a href="javascript:;" class="btn btn-danger btn-small" title="Deactivate" onclick="delete_active_coupon(<?php echo $coupon->coupon_id; ?>);"><i class="btn-icon-only icon-remove"> </i></a>
									</td>
                                </tr>
                            <?php
                            $j++;
                        }
                    }
                    ?>   

                    </tbody>
                </table>                 
            </div>            
        </div>        
    </div>

</div>

