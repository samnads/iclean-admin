<style>
.bootstrap-tagsinput {

    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    display: inline-block;
    padding: 4px 6px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
    overflow-y: scroll;
    max-height: 300px;
    width:350px;

}
</style>
<div class="row">
    <!-- /span6 --> 
    <div class="span6" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    <div class="widget-header" style="margin-bottom: 0;">
	      	<i class="icon-globe"></i>
                    <h3>Send Bulk Sms</h3>
                    
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="sms" action="<?php echo base_url(); ?>customer/bulk_sms" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;">
										 <?php if($this->session->flashdata('sms_success')){ ?>
										 <p style="color:green;text-align:center;"><?php echo $this->session->flashdata('sms_success') ?></p>
										 
										 <?php } ?>
										 <?php if($this->session->flashdata('sms_error')){ ?>
										 <p style="color:red;text-align:center;"><?php echo $this->session->flashdata('sms_error') ?></p>
										 
										 <?php } ?>
										</div>
                                    </div>
                  
                                    <div class="control-group">											
                                            <label class="control-label" for="coupontype">Customer Type</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="customer_type" id="customer_type_bulk" style="width: 350px;">
                                                    <option value="1">Regular</option>
                                                    <option value="0">One-time customer</option>
                                                    <?php foreach ($sms_groups as $sms_group) {?>
                                                     <option value="<?php echo $sms_group->sms_group_id;?>"><?php echo $sms_group->sms_group_name;?></option>   
                                                    <?php }?>
						</select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->


                                    <div class="control-group">											
											<label class="control-label" for="coupontype">Mobile number</label>
                                            <div class="controls">
                                            <input type="text" name="mobnumber_list" id="mobnumber_list" value="<?php echo $customer_mob_numbers ?>" data-role="tagsinput" /> 

                                       </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
									
								   <div class="control-group">											
											<label class="control-label" for="customertype">Message</label>
                                            <div class="controls">
                                             <textarea name="sms_message" id="sms_message" class="form-control" style="width:350px;"></textarea>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                   
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-info send_bulk e-new-but" value="Send" name="send_sms">

                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>