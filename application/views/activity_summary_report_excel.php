<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1">
    <thead>
        <tr>
            <th >Sl No</th>
            <th >Date</th>
            <th >Total Booking Hours(s)</th>
            <th colspan="2">One Day Booking(s)</th>
            <th colspan="2">Long Term Booking(s)</th>
            <th >Total Payment</th>
            <th >Total Invoice</th>
        </tr>
        <tr>
            <th ></th>
            <th ></th>
            <th ></th>
            <th >Booking(s)</th>
            <th >Booking Hrs</th>
            <th >Booking(s)</th>
            <th >Booking Hrs</th>
            <th ></th>
            <th ></th>
        </tr>
    </thead>
    <tbody>
                        <?php
                        $total_hrs = 0;
                        $total_OD = 0;
                        $total_OD_hrs = 0;
                        $total_WE = 0;
                        $total_WE_hrs = 0;
                        $total_payment = 0;
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $key => $report) {
                                $newDate = date("d/m/Y", strtotime($key));
                                $total_hrs += $report['hours'];
                                $total_OD += $report['OD'];
                                $total_OD_hrs += $report['OD_hrs'];
                                $total_WE += $report['WE'];
                                $total_WE_hrs += $report['WE_hrs'];
                                $total_payment += $report['payment'];
                                
                                $i++;
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td>
                                        <?php echo $newDate; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['hours']; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['OD']; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['OD_hrs']; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['WE']; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['WE_hrs']; ?>
                                    </td>
                                    <td>
                                        <?php echo $report['payment']; ?>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                                <tr>
                        <td ></td>
                        <td ><b>Total</b></td>
                        <td ><b><?php echo $total_hrs; ?></b></td>
                        <td ><b><?php echo $total_OD; ?></b></td>
                        <td ><b><?php echo $total_OD_hrs; ?></b></td>
                        <td ><b><?php echo $total_WE; ?></b></td>
                        <td ><b><?php echo $total_WE_hrs; ?></b></td>
                        <td ><b><?php echo $total_payment; ?></b></td>
                        <td > </td>
                                    
                    </tr>
                    </tbody>
    
</table>