<style type="text/css">
	 .btn-mm-success {
    background-color: #7eb216;
    background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
    border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
    color: white;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
}
.btn-mm-success:hover {
    background-color: #7eb216;
    background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
    border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
    color: white;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
}
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
				<form id="listcustomerss" class="form-horizontal" method="post" enctype="multipart/form-data">
					<i class="icon-th-list"></i>
					<h3>Customer List </h3>
					
					<input type="text" style="width: 150px;" id="cust_from_newdate" name="cust_from_newdate" placeholder="From" value="<?php echo $date_from; ?>" autocomplete="off" />
					<input type="text" style="width: 150px;" id="cust_to_newdate" name="cust_to_newdate" placeholder="To" value="<?php echo $date_to; ?>" autocomplete="off" />
					<!-- <div class="mm-drop"> -->
					
						<select style="margin-left : 10px;width:80px;" id="custstatus" name="custstatus">
							<option value="">All</option>
							<option value="1" <?php echo $status == '1' ? 'selected="selected"' : ''; ?>>Active</option>
							<option value="0" <?php echo $status == '0' ? 'selected="selected"' : ''; ?>>Inactive</option>
						</select>
					<!-- </div> -->
					<!-- <div class="mm-drop">  -->
					
						<select style="width: 150px;" id="servicetypenew" name="servicetypenew">
							<option value="">-Select Service-</option>
							<?php
							foreach($services as $servval)
							{
								if($servval->service_type_id == $service_id)
								{
									$selected = ' selected="selected"';
								} else {
									$selected = '';
								}
							?>
							<option value="<?php echo $servval->service_type_id; ?>"<?php echo $selected; ?>><?php echo $servval->service_type_name; ?></option>
							<?php
							}
							?>
						</select>
					<!-- </div> -->
					<!-- <div class="mm-drop"> -->
					
						<select style="width: 150px;" id="sort_sourcenew" name="sort_sourcenew">
							<option value="">-Select Source-</option>          
							<option value="Facebook" <?php echo $source == 'Facebook' ? 'selected="selected"' : ''; ?>>Facebook</option>
							<option value="Google" <?php echo $source == 'Google' ? 'selected="selected"' : ''; ?>>Google</option>
							<option value="Yahoo" <?php echo $source == 'Yahoo' ? 'selected="selected"' : ''; ?>>Yahoo</option>
							<option value="Bing" <?php echo $source == 'Bing' ? 'selected="selected"' : ''; ?>>Bing</option>
							<option value="Direct Call" <?php echo $source == 'Direct Call' ? 'selected="selected"' : ''; ?>>Direct Call</option>
							<option value="CrystalBlu" <?php echo $source == 'CrystalBlu' ? 'selected="selected"' : ''; ?>>CrystalBlu</option>
							<option value="Flyers" <?php echo $source == 'Flyers' ? 'selected="selected"' : ''; ?>>Flyers</option>
							<option value="Referral" <?php echo $source == 'Referral' ? 'selected="selected"' : ''; ?>>Referral</option>
							<option value="Watchman/Security Guard" <?php echo $source == 'Watchman/Security Guard' ? 'selected="selected"' : ''; ?>>Watchman/Security Guard</option>
							<option value="Maid" <?php echo $source == 'Maid' ? 'selected="selected"' : ''; ?>>Maid</option>
							<option value="Driver" <?php echo $source == 'Driver' ? 'selected="selected"' : ''; ?>>Driver</option>
							<option value="By email" <?php echo $source == 'By email' ? 'selected="selected"' : ''; ?>>By email</option>
							<option value="Schedule visit" <?php echo $source == 'Schedule visit' ? 'selected="selected"' : ''; ?>>Schedule visit</option>
							<option value="Website" <?php echo $source == 'Website' ? 'selected="selected"' : ''; ?>>Website</option>
							<option value="Referred by staff" <?php echo $source == 'Referred by staff' ? 'selected="selected"' : ''; ?>>Referred by staff</option>
							<option value="Referred by customer" <?php echo $source == 'Referred by customer' ? 'selected="selected"' : ''; ?>>Referred by customer</option>
							<option value="Justmop" <?php echo $source == 'Justmop' ? 'selected="selected"' : ''; ?>>Justmop</option>
							<option value="Matic" <?php echo $source == 'Matic' ? 'selected="selected"' : ''; ?>>Matic</option>
							<option value="ServiceMarket" <?php echo $source == 'ServiceMarket' ? 'selected="selected"' : ''; ?>>ServiceMarket</option>
						</select>
					<!-- </div> -->
					<input type="submit" class="btn" value="Go" name="listingcustomer" style="margin-bottom: 4px;">
					<?php
					$stat=(strlen($status)>0)?$status:'nil';
					$serviceid=(strlen($service_id)>0)?$service_id:'nil';
					$datefrom=(strlen($date_from)>0)?$date_from:'nil';
					$dateto=(strlen($date_to)>0)?$date_to:'nil';
					$sourcee=(strlen($source)>0)?$source:'nil';
					?>
					<div class="topiconnew"><a href="<?php echo base_url(); ?>customer/newExcel/<?php echo $stat; ?>/<?php echo $serviceid; ?>/<?php echo $datefrom; ?>/<?php echo $dateto; ?>/<?php echo $sourcee; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Export Customer"/></a></div>
				</form>
			</div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="customer-list" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Mobile</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Area</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Address</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Source</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Last Job</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Added Time</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
						if(!empty($customers))
						{
							$i = 1;
							foreach ($customers as $customers_val) 
							{
								if($customers_val->building != "")
								{
									$apartmnt_no = 'Apartment No:'.$customers_val->building.'<br/>';
								} else {
									$apartmnt_no = "";
								}
								//Payment Type
								if ($customers_val->payment_type == "D") {
									$paytype = "(D)";
									$paytext = "Daily";
								} else if ($customers_val->payment_type == "W") {
									$paytype = "(W)";
									$paytext = "Weekly";
								} else if ($customers_val->payment_type == "M") {
									$paytype = "(M)";
									$paytext = "Monthly";
								} else {
									$paytype = "";
									$paytext = "";
								}
								$last_jobdate = $this->customers_model->get_last_job_date_by_customerid($customers_val->customer_id);
								if(empty($last_jobdate))
								{
									$last_date = "";
								} else {
									$last_date = $last_jobdate->service_date;
								}
								
								if($customers_val->customer_address == "")
								{
									$a_address = 'Building - '.$customers_val->building.', '.$customers_val->unit_no.''.$customers_val->street;
								} else {
									$a_address = $customers_val->customer_address;
								}
								
								if($customers_val->is_flag == "Y") {
									$isflag = " -- Flagged (".$customers_val->flag_reason.")";
								} else if($customers_val->is_flag == "N") {
									$isflag = "";
								}
						?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><a href="<?php echo base_url(); ?>customer/view/<?php echo $customers_val->customer_id ?>" style="text-decoration: none;color:#333;" data-toggle="tooltip" title="<?php echo $paytext; ?>"><?php echo $customers_val->customer_name ?> <?php echo $paytype; ?><br/><span style="color: red;"><?php echo $isflag; ?></span></a></td>
							<td><?php echo $customers_val->mobile_number_1; ?></td>
							<td><?php echo $customers_val->zone_name . '-' . $customers_val->area_name ?></td>
							<td><?php echo $apartmnt_no; ?><?php echo wordwrap($a_address, 25, "<br>"); ?></td>
							<td><?php echo $customers_val->customer_source; ?></td>
							<td><?php echo $last_date; ?></td>
							<td><?php echo ($customers_val->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($customers_val->customer_added_datetime)) : ""; ?></td>
							<td><a class="btn btn-small btn-info" href="<?php echo base_url(); ?>customer/view/<?php echo $customers_val->customer_id ?>" title="View Customer"><i class="btn-icon-only fa fa-eye "> </i></a>
                                        <a class="btn btn-small btn-warning" href="<?php echo base_url(); ?>customer/edit/<?php echo $customers_val->customer_id ?>" title="Edit Customer"><i class="btn-icon-only icon-pencil"> </i></a>
										<?php $btn_class = $customers_val->customer_status == 1 ? 'btn btn-mm-success btn-small' : 'btn btn-danger btn-small'; ?>
										<a href="javascript:void(0)" class="<?php echo $btn_class ?>" title="Disable Customer" onclick="delete_customer(this, <?php echo $customers_val->customer_id ?>, <?php echo $customers_val->customer_status ?>);"><?php echo $customers_val->customer_status == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a></td>
						</tr>
						<?php
						$i++;
							}
						}
						else{
						?>
						<tr>
							<td >No Records Found</td>
						</tr>
						<?php } ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script  type="text/javascript">
     $('#listcustomerss').submit(function(e) {
        var from = document.getElementById("cust_from_newdate").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("cust_to_newdate").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
     </script>