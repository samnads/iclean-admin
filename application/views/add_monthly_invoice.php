<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Monthly invoice</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url();?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>invoice/list_customer_monthly_invoices"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Invoice List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                        
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Invoice added Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }else{
                                ?>
                                
                                    <?php }?>
                                
                                
                                <fieldset>            
                                    <div class="span10">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Customer&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customer" id="customers_vh_rep_new" class="span7" required>
                                                                <option value="">Select customer</option>
                                                                <?php
                                                                foreach($customerlist as $c_val)
                                                                {
                                                                    
                                                                ?>
                                                                <option value="<?php echo $c_val->customer_id; ?>"><?php echo $c_val->customer_name; ?></option>
                                                                <?php
                                                                }
                                                                ?>                                                                  
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Invoice Month&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="invoice_month" id="invoice_month" class="span7" required>
                                                                <option value="">Select Month</option>
                                                                <option value="01" <?php if(date("m")=='01'){echo 'selected';}?> >January</option>
                                                                <option value="02" <?php if(date("m")=='02'){echo 'selected';}?> >February</option>
                                                                <option value="03" <?php if(date("m")=='03'){echo 'selected';}?> >March</option>
                                                                <option value="04" <?php if(date("m")=='04'){echo 'selected';}?> >April</option>
                                                                <option value="05" <?php if(date("m")=='05'){echo 'selected';}?> >May</option>
                                                                <option value="06" <?php if(date("m")=='06'){echo 'selected';}?> >June</option>
                                                                <option value="07" <?php if(date("m")=='07'){echo 'selected';}?> >July</option>
                                                                <option value="08" <?php if(date("m")=='08'){echo 'selected';}?> >August</option>
                                                                <option value="09" <?php if(date("m")=='09'){echo 'selected';}?> >September</option>
                                                                <option value="10" <?php if(date("m")=='10'){echo 'selected';}?> >October</option>
                                                                <option value="11" <?php if(date("m")=='11'){echo 'selected';}?> >November</option>
                                                                <option value="12" <?php if(date("m")=='12'){echo 'selected';}?> >December</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Invoice Year&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="invoice_year" id="invoice_year" class="span7" required>
                                                                <option selected value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="control-group">											
                                                        <label class="control-label" for="firstname">Product/Service&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="product_name" name="product_name">
                                                        </div> 
                                                    </div> 	

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Description&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <textarea class="span7" rows="5" id="description" name="description"></textarea>    
                                                        </div>     
                                                    </div> 

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Quantity&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="quantity" name="quantity">
                                                        </div>  
                                                    </div> 

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Amount&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="unit_cost" name="unit_cost">
                                                        </div> 
                                                    </div>  -->

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Issue Date &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="doj" name="issue_date" value="<?php echo date('Y-m-d')?>" required>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Due Date &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="startdate" name="due_date" value="<?php echo date('Y-m-d',strtotime('+14 days'))?>" required>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->

                                                    <div class="form-actions" >
                                                        <input type="submit" class="btn mm-btn" value="Submit" name="maid_sub" onclick="return validate_cust_monthly_inv_add();"  >
                                                    </div> 



                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->




                                    

                                </fieldset>   
                                    
                            </div>

                            <div class="tab-pane" id="attachments">
                                <fieldset>            

                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Passport Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="passport_number" name="passport_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Passport Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="passport_expiry" name="passport_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Passport</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_passport" name="attach_passport">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Visa Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="visa_number" name="visa_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Visa Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="visa_expiry" name="visa_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Visa</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_visa" name="attach_visa">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->


                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Labour Card Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_number" name="labour_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Labour Card Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_expiry" name="labour_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Labour Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_labour" name="attach_labour">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Emirates Id</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_id" name="emirates_id">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Emirates Id Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_expiry" name="emirates_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Emirates Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_emirates" name="attach_emirates">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <br />
                                                </fieldset>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>            

<!--                                <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_sub" onclick="return validate_maid();">
                                    
                                    
                                </div>   -->

                                
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->

<script type="text/javascript">
function show() {
alert ("customer monthly invoice added");

}
</script>