<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'backpayment' ?>">
                    <i class="icon-th-list"></i>
                    <h3>Back Payment</h3>                   
                   
                    <input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="payment_date" name="payment_date" value="<?php echo $payment_date ?>">                       
                    
                    <input type="submit" class="btn" value="Go" name="add_payment" style="margin-bottom: 4px;">   
                    <a style="float:right ; margin-right:15px;" href="<?php echo base_url() . 'backpayment/add'?>" ><img src="<?php echo base_url() . 'img/add.png' ?>" title="Add Back Payment"></a>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Zone</th>
                            <th style="line-height: 18px;"> Collected Date</th>                            
                            <th style="line-height: 18px;"> Collected Time</th>     
                            <th style="line-height: 18px;"> Requested Amount</th>  
                            <th style="line-height: 18px;"> Collected Amount</th>    
                            <th style="line-height: 18px;"> Balance Amount</th>  
                        </tr>			

                    </thead>
                    <tbody>
                        <?php
                        if(!empty($back_payment))
                        {
                            $i = 0;
                            $total_req_amount = 0;
                            $total_collected_amount = 0;
                            $total_balance_amount = 0;
                            foreach ($back_payment as $payment)
                            {
                                //Payment Type
                                    if($payment->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($payment->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($payment->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                
                                $total_req_amount += $payment->requested_amount;
                                $total_collected_amount += $payment->collected_amount;
                                $total_balance_amount += $payment->balance;
                                
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->customer_name.' '.$paytype. '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_date . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->requested_amount . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_amount . '</td>'
                                        . '<td style="line-height: 18px;">' .  $payment->balance . '</td>'
                                    .'</tr>';
                            }
                            echo '<tr>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"><b>Total<b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_req_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_collected_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_balance_amount . '</b></td>'
                                    .'</tr>';
                        }
                        
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
