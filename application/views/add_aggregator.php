<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Aggregator</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url();?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>aggregators"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Maid List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Aggregator Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                        
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Aggregator added Successfully.!</strong> 
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                
                                
                                <fieldset>            
                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Aggregator Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="agg_name" name="agg_name" required>
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->				

                                                    <div class="control-group">											
                                                        <label class="control-label">Status&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <label class="radio inline">
                                                                <input type="radio" name="agg_status" value="1" checked="checked"> Active
                                                            </label>
                                                            <label class="radio inline">
                                                                <input type="radio" name="agg_status" value="0"> Inactive
                                                            </label>
                                                        </div>	<!-- /controls -->			
                                                    </div> <!-- /control-group -->


                                                      

                                                    <div class="form-actions">
                                    
                                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="agg_sub" >
                                                        
                                                        
                                                    </div> 

                                                    



                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->




                                     <!-- /span5 -->

                                </fieldset>   
                                    
                            </div>

                            <div class="tab-pane" id="attachments">
                                <fieldset>            

                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Name</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="passport_number" name="passport_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->
                                                    <div class="control-group">                                         
                                                        <label class="control-label">Status</label>
                                                        <div class="controls">
                                                            <label class="radio inline"><input type="radio" name="key" value="Y"> Yes</label>
                                                            <label class="radio inline"><input type="radio" name="key" checked="checked" value="N"> No</label>
                                                        </div>  <!-- /controls -->          
                                                    </div>
                                                    


                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->


                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Labour Card Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_number" name="labour_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Labour Card Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_expiry" name="labour_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Labour Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_labour" name="attach_labour">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Emirates Id</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_id" name="emirates_id">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Emirates Id Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_expiry" name="emirates_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Emirates Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_emirates" name="attach_emirates">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <br />
                                                    <div class="form-actions" >
                                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="agg_sub">
                                                    </div> 
                                                </fieldset>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>            

<!--                                <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_sub" onclick="return validate_maid();">
                                    
                                    
                                </div>   -->

                                
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
