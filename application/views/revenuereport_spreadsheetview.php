<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Cleaner</th>
            <th> Mobile</th>
            <th> Total Hours</th>
            <th> Total Revenue</th>
            <th> Avg Price/Hr</th>
        </tr>
    </thead>
    <tbody>
    <?php
	if(!empty($reports))
	{
		$i = 1;
		foreach($reports as $val)
		{
			$avg = number_format(($val['amount']/$val['hrs']),2);
	?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $val['maidname']; ?></td>
		<td><?php echo $val['mobile']; ?></td>
		<td><?php echo $val['hrs']; ?></td>
		<td><?php echo $val['amount']; ?></td>
		<td><?php echo $avg; ?></td>
	</tr>
	<?php
		$i++;
		}
	}
	?>
    </tbody>
</table>