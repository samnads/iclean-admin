<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" id="edit-profile">
                    <i class="icon-th-list"></i>
                    <h3>User Activity Report</h3>                   
                   
                    <input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="activity_date" value="<?php echo $activity_date ?>">                       
                    <input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="schedule_date" name="activity_date_to" value="<?php echo $activity_date_to ?>">                       
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">   
                    <?php
                    $ymd_act_date = str_replace('/', '-', $activity_date);
                    $ymd_act_date=date('Y-m-d', strtotime($ymd_act_date));

                    $ymd_act_to_date = str_replace('/', '-', $activity_date_to);
                    $ymd_act_to_date=date('Y-m-d', strtotime($ymd_act_to_date));
                    ?>  
                    <div class="topiconnew"><a href="<?php echo base_url();?>reports/user_activity_report_to_excel/<?php echo $ymd_act_date;?>/<?php echo $ymd_act_to_date;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div>               
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Type</th>
                            <th style="line-height: 18px;"> Booking Type</th>
                            <th style="line-height: 18px;"> Shift</th>                            
                            <th style="line-height: 18px;"> Action</th>     
                            <th style="line-height: 18px;"> Added By</th>  
                            <th style="line-height: 18px;"> Time</th>     
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($user_activity_report))
                        {
                            $i = 0;
                            
                            foreach ($user_activity_report as $activity)
                            {
                                $action_type = $activity->action_type;
                                switch ($activity->action_type)
                                {
                                    case 'Add' : $action_type = 'New Booking';
                                        break;
                                    case 'Update' : $action_type = 'Booking Update';
                                        break;
                                    case 'Delete' : $action_type = 'Booking Delete';
                                        break;
                                }
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . str_replace("_",' ',$action_type) . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->booking_type . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->shift . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->user_fullname . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->added . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<script  type="text/javascript">
     $('#edit-profile').submit(function(e) {
        var from = document.getElementById("vehicle_date").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("schedule_date").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
     </script>
