<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Master</title>
</head>
<style>
@page {
	margin: 0px 0px 0px 0px !important;
	padding: 0px 0px 0px 0px !important;
}


</style>
<body style="padding: 0px; margin: 0px;">

<div class="main" style="width:793px; height:auto; padding: 0px; margin: 0px auto;">

    <header style="height: 94px; overflow: hidden; position: fixed; left:0; top:0; z-index: 999;">
          
           
                 <div class="header" style="width:100%; height:auto; padding: 5px 0px 10px 0px; margin: 0px; background: #f4f4f4;">
       
                              
                <table width="100%" border="0">
                  <tr>
                    <td width="15%" align="right"><img src="https://iclean.bh/images/logo.png" width="70" height="70" /></td>
                    <td width="50%">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 10px; margin: 0px;">
                            <strong style="font-size: 13px;">I CLEAN SERVICES W.L.L</strong><br />
                            Road 31, Bldg. 1007/A. Block 635,<br />
                            Ma'ameer,<br />
							Sitra, Kingdom of Bahrain</p>
                    </td>
                    <td width="35%" valign="bottom">
                        <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                                      <tr>
                                        <td width="35%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                                        <td width="5%">:</td>
                                        <td width="60%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>+973 1770 0211-135</strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Email </td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px">info@iclean.bh</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px"></td>
                                      </tr>
                                      
                             </table>
                    </td>
                  </tr>
                </table>


     </div>
          
          
          </header>
    
  <section style="width:100%; height: 700px;  padding: 150px 30px 0px 30px;">
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
				<div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
					<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 18px; line-height: 16px; color: #333; padding: 0px 0px 30px 0px; margin: 0px; text-transform: uppercase; text-align: center;">Payment Receipt</p>
				</div>
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                      <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Recipient :
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                         <b><?php echo $payment_detail->customer_name; ?></b>,
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        <?php echo $payment_detail->customer_address; ?>
                     </p>
                 </div>
                 
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                   <div style="width: 100%; height:auto; margin: 0px; padding: 0px; background: #0097db;">
                           <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 10px 20px; margin: 0px;">
                         Payment Info :	 <?php echo str_replace("INV-", "", $invoice_detail[0]->invoice_num); ?>
                     </p>
                      </div>
                      
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 25px; color: #333; padding: 5px 0px 0px; margin: 0px; background: #eee;">
                          <tr>
                            <td width="50%" style="padding-left: 20px;">Payment Date</td>
                            <td width="50%" align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($payment_detail->paid_datetime)) ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;">Receipt No.</td>
                            <td align="right" style="padding-right: 20px;"><?php echo $payment_detail->payment_reference; ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;padding-bottom: 10px;">Payment Mode</td>
                            <td align="right" style="padding-right: 20px; line-height:15px; padding-bottom: 5px;">
							<?php
									if($payment_detail->payment_method == 0)
									{
										echo "Cash";
									} else {
										echo "Card";
									}
									?>  
							</td>
                          </tr>
                          
                          <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                            <td style="padding: 15px 20px; background: #8dc73f; ">Amount</td>
                            <td align="right" style="padding: 15px 20px; background: #8dc73f; "><span style="font-size: 14px;">BHD</span> <?php echo number_format($payment_detail->paid_amount,2); ?></td>
                          </tr>
                        </table>
               </div>
             <div style="clear:both;"></div>
             </div>
             
             
             <?php 
			 if(!empty($allocated_invoices))
			 {
			 ?>
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 50px 0px 10px 0px;">
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                       <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                           Payment For:
                       </p>
                  </div>
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                    <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; text-align: right; padding: 0px; margin: 0px;">
                      <!-- <span style="color: #777;">For the Month of</span> July 2020 -->
                    </p>
                  </div>
                  <div style="clear:both;"></div>
             </div>
             
             
             
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px 0px 0px 0px;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; line-height: 20px; color: #FFF; background: #0097db; ">
                      <td width="25%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">INVOIVE NO.</td>
                      <td width="25%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">INVOICE DATE</td>
                      <td width="25%" align="center" style="border-right: 1px solid; border-color: #FFF;">AMOUNT</td>
                      <!-- <td width="12%" align="center" style="border-right: 1px solid; border-color: #FFF;">Unit Cost</td> -->
                      <td width="25%" align="center">PAID AMOUNT</td>
                    </tr>
                    
                    



                    <?php
                    $i = 1;
                    foreach ($allocated_invoices as $invs_detail)
                    {
                    ?>
                    <tr>
                      <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-left: 1px solid; border-color: #CCC;">
                          <span><?php echo $invs_detail->invoice_num; ?>
                          </span>
                      </td>
                      <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo date('d/m/Y',strtotime($invs_detail->invoice_date)); ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">BHD</span> <?php echo number_format($invs_detail->invoice_net_amount,2); ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">BHD</span> <?php echo number_format($invs_detail->payment_amount,2); ?></td>
                    </tr>
                     <?php
                      $i++; } 
                    ?>
                    
                    
                  </table>
                  
             </div>
			 <?php
			 }
			 ?>
    
    </section>
    
    <footer style=" height: 190px; position: fixed; left:0; bottom:0; z-index: 999;">
    
    </footer>
    
</div>

</body>
</html>