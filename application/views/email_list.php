<style>
    #area select{ width: 15% !important;}
</style>

<div class="row">
        
      
    <div class="span6" id="edit_email_setting" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit Email Settings</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_email_setting();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="edit_header_name">Header Name </label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_header_name" name="edit_header_name" required="required">
                                            </div>   				
                                    </div>   
                                    <div class="control-group">											
					<label class="control-label" for="edit_subject">Email Subject </label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_subject" name="edit_subject" required="required">
                                            </div>   				
                                    </div>   
                                    
                                    
                                                <input type="hidden" class="span3" id="edit_email_id" name="edit_email_id">
                                    
                                     
										
                                    <div class="control-group">											
					<label class="control-label" for="edit_from">From Address</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_from" name="edit_from" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="email_edit_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Email Settings</h3>
              <!--<a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_hourly_price();"><img src="<?php echo base_url();?>img/add.png" title="Add Services"/></a>-->
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Header Name </th>
                            <th style="line-height: 18px"> Email Subject </th>
                            <th style="line-height: 18px"> From Address</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($email_settings) > 0) {
                            $i = 1;
                            foreach ($email_settings as $email) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $email['header_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $email['subject'] ?></td>
                            <td style="line-height: 18px"><?php echo $email['from_address'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_email_settings(<?php echo $email['id'] ?>);">
                                    <i class="btn-icon-only icon-pencil"> </i>
                                </a>
                                <!--<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_hourly_price(<?php echo $sms['id']  ?>);"><i class="btn-icon-only icon-remove"> </i></a>-->
                            </td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
  
</div>