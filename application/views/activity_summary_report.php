<?php
//        echo '<pre>';
//        print_r($reports);
//        exit();
?>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    <i class="icon-th-list"></i>
                    <h3>Activity Summary Report</h3>
                    <span style="margin-left:23px;">From :</span>
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="ActFromDate" name="from_date" value="<?php echo isset($from_date) ? $from_date : date('d/m/Y'); ?>">
                    <span style="margin-left:23px;">To :</span>
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="ActToDate" name="to_date" value="<?php echo isset($to_date) ? $to_date : date('d/m/Y'); ?>">
                    <input type="submit" class="btn" value="Go" name="activity_report" style="margin-bottom: 4px;">
                    <?php
                    $ymd_from_date = str_replace('/', '-', $from_date);
                    $ymd_from_date=date('Y-m-d', strtotime($ymd_from_date));

                    $ymd_to_date = str_replace('/', '-', $to_date);
                    $ymd_to_date=date('Y-m-d', strtotime($ymd_to_date));
                    ?>  

                    <div class="topiconnew"><a href="#"><img src="<?php echo base_url(); ?>images/fax-icon.png" id="ActivityPrint" title="Print"/></a></div>

                    <div class="topiconnew"><a href="<?php echo base_url();?>reports/activity_summary_report_toexcel/<?php echo $ymd_from_date;?>/<?php echo $ymd_to_date;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div> 
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;"><img src="<?php // echo base_url(); ?>img/printer.png" id="ActivityPrint" title="Print"/></a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Date</th>
                            <th style="line-height: 18px">Total Booking Hours(s)</th>
                            <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                            <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                            <th style="line-height: 18px">Total Payment</th>
                            <th style="line-height: 18px">Total Invoice</th>
                            <th style="line-height: 8px">View</th>
                        </tr>
                        <tr>
                            <th style="line-height: 18px;"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 8px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_hrs = 0;
                        $total_OD = 0;
                        $total_OD_hrs = 0;
                        $total_WE = 0;
                        $total_WE_hrs = 0;
                        $total_payment = 0;
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $key => $report) {
                                $newDate = date("d/m/Y", strtotime($key));
                                $total_hrs += $report['hours'];
                                $total_OD += $report['OD'];
                                $total_OD_hrs += $report['OD_hrs'];
                                $total_WE += $report['WE'];
                                $total_WE_hrs += $report['WE_hrs'];
                                $total_payment += $report['payment'];
                                
                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $newDate; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['hours']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['payment']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">

                                    </td>
                                    <td style="line-height: 8px;text-align: center;" class="td-actions ">
                                    <center>
                                        <a class="btn btn-small btn-info" target="_blank" href="<?php echo base_url() . 'reports/activity_summary_view/'. $key ?>" ><i class="btn-icon-only icon-search"> </i></a>
                                    </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                                <tr>
                        <td style="line-height: 18px;text-align: center;"></td>
                        <td style="line-height: 18px;text-align: center;"><b>Total</b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_OD; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_OD_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_WE; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_WE_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_payment; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"> </td>
                        <td style="line-height: 18px;text-align: center;"> </td>
                                    
                    </tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="ActivityReportPrint"> 
    <table cellpadding="0" border="1" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Date</th>
                <th style="line-height: 18px">Total Booking Hours(s)</th>
                <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                <th style="line-height: 18px">Total Payment</th>
                <th style="line-height: 18px">Total Invoice</th>
            </tr>
            <tr>
                <th style="line-height: 18px;"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total_hrs = 0;
            $total_OD = 0;
            $total_OD_hrs = 0;
            $total_WE = 0;
            $total_WE_hrs = 0;
            $total_payment = 0;
            
            if (!empty($reports)) {
                $i = 0;                            
                
                
                foreach ($reports as $key => $report) {
                    $newDate = date("d/m/Y", strtotime($key));
                    $total_hrs += $report['hours'];
                    $total_OD += $report['OD'];
                    $total_OD_hrs += $report['OD_hrs'];
                    $total_WE += $report['WE'];
                    $total_WE_hrs += $report['WE_hrs'];
                    $total_payment += $report['payment'];
                    
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $newDate; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['hours']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['payment']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">

                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
                    <tr>
                        <td style="line-height: 18px;text-align: center;"></td>
                        <td style="line-height: 18px;text-align: center;">Total</td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_OD; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_OD_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_WE; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_WE_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_payment; ?></td>
                    </tr>
        </tbody>
    </table>
</div>

<script  type="text/javascript">
     $('#edit-profile').submit(function(e) {
        var from = document.getElementById("ActFromDate").value;
       var fromdate = new Date($("from").val());
       var too =document.getElementById("ActToDate").value;
        var todate =new Date($("too").val());
      
    //      alert(fromdate);
    //      alert(todate);
    //    alert(Date.parse(fromdate));
    //    alert(Date.parse(todate));
    //     if(from < too){
    //          alert("End date should be greater than Start date.");
          
    //     }
         if(from > too){
             alert("Invalid Date Range");
             window.location.reload();
        }
    else{
   //alert("Valid date Range");
    }
    
     });
     </script>

