<!--<script src="<?php// echo base_url(); ?>js/jquery-1.7.2.min.js"></script>--> 

<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                    <div class="widget-header"> 
                        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>booking/locations">
                            <i class="icon-th-list"></i>
                            <h3>Maps</h3> 
                            <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">                      
                            <input type="hidden" id="formatted-date-job" value="<?php echo $formatted_date ?>"/>
                            <input type="submit" class="btn" value="Go" name="map_button" style="margin-bottom: 4px;">
                            <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                            <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                        </form> 
                    </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
                    <div id="job_map" style="height: 800px; width:100%;"></div>
            </div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->
<script type="text/javascript">
var map;
var mapOptions = {
    zoom: 12,
    center: {lat:26.0667, lng:50.5577}
}; // map options
var markers = [];
function initialize() {
    map = new google.maps.Map(document.getElementById('job_map'),
            mapOptions);
//            var marker = new google.maps.Marker({
//          map: map,
//          position: latlng,
//          draggable: false,
//          anchorPoint: new google.maps.Point(0, -29)
//       });
//alert(mapOptions.center);
//var marker = new google.maps.Marker({
//          position: mapOptions.center,
//          map: map,
//          title: 'Hello World!'
//        });

    jQuery.ajax({
        type: 'POST',
        url: '<?= base_url() ?>booking/closest_locations_job',
        dataType: "json",
        data:"data="+ '{ "latitude":"26.0667", "longitude": "50.5577", "servicedate" : "'+$('#formatted-date-job').val()+'" }',
        //data: {},
        success:function(data) {
            //alert(data);
            // when request is successed add markers with results
            //add_markers(data);
            var marker, i;
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();
        // display how many closest providers avialable
        //document.getElementById('info').innerHTML = ' Available:' + data.length + ' Providers<br>';

        for (i = 0; i < data.length; i++) {
            var htmlval = '<table style="margin-bottom:0px; color: #0756A5;"><tr><td><i class="fa fa-user"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][3]+'</td></tr><tr><td><i class="fa fa-female"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][6]+'</td></tr><tr><td><i class="fa fa-clock-o"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][0]+' - '+data[i][1]+'</td></tr></table>';
            //var infowindow = new google.maps.InfoWindow({content: htmlval});
//            var coordStr = data[i][5];
//            var coords = coordStr.split(',');
            //alert(data[i][4]);
            var pt = new google.maps.LatLng(data[i][5], data[i][4]);
            bounds.extend(pt);
            marker = new google.maps.Marker({
                position: pt,
                map: map,
                //icon: data[i][6],
                //icon: _base_url+'assets/images/38.png',
                //address: data[i][1],
                title: data[i][3],
                html: htmlval
            });
            markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(marker.html);
                    //prov.value = data[i][5];
                    infowindow.open(map, marker);
                }
            })
            (marker, i));

        }
        }
    });
}
google.maps.event.addDomListener(window, 'load', initialize);  
 
</script>
<!-- <script async defer src="//maps.googleapis.com/maps/api/js?key=AIzaSyDpDi6GKhcr4mTO180oPmDh3cFQ5ciBP94&libraries=places&callback=initialize" type="text/javascript"></script> -->
<script async defer src="//maps.googleapis.com/maps/api/js?key=AIzaSyDxbvfB_9QVZPent8erw6J68MFR2cya7gM&libraries=places&callback=initialize" type="text/javascript"></script>