<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/call-report' ?>">
                    <i class="icon-th-list"></i>
                    <h3>Call Report</h3> 
					<input type="text" readonly="readonly" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="OneDayDate" name="search_date" value="<?php echo $payment_date ?>">
					<select name="call_type" id="call_type" style="width: 160px;">
						<option value="0" <?php echo $call_type == 0 ? 'selected="selected"' : '' ?>>All</option>
						<option value="1" <?php echo $call_type == 1 ? 'selected="selected"' : '' ?>>New Number Calls</option>
						<option value="2" <?php echo $call_type == 2 ? 'selected="selected"' : '' ?>>Customer Calls</option>
					</select>
                    <input type="submit" class="btn" value="Go" name="add_payment" style="margin-bottom: 4px;">
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Customer</th>
                            <th style="line-height: 18px">Mobile</th>
                            <th style="line-height: 18px">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($reports))
                        {
                            $i = 0;
                            foreach ($reports as $report)
                            {
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->customer_name.' '.$paytype. '</td>'
                                        . '<td style="line-height: 18px;">' . $report->mobile_no . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->added_date_time . '</td>'
                                    .'</tr>';
                            }
                        }
                        
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

