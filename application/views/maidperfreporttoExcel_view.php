<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Cleaner</th>
            <th> Client Name</th>
            <th> Date</th>
            <th> Booking Type</th>
            <th> Source Code</th>
            <th> Timings</th>
            <th> Driver</th>
            <th> Notes</th>
        </tr>
    </thead>
    <tbody>
            <?php
                $sln = 1;
                foreach($performance_report as $performance)
                    {
                                            foreach ($performance as $key)
                                                    {
                            ?>
                            <tr>
                                <td style="line-height: 18px;"><?php echo $sln++; ?></td>
                                <td style="line-height: 18px;">
                                                                    <?php //echo html_escape($key->maid_name); ?>
                                                                    <?php
                                    $maid_names = explode(',', $key->maid_names);
                                    foreach ($maid_names as $maid_name) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $maid_name . '<br />';
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                                                </td>
                                <td style="line-height: 18px;">
                                    <?php
                                    $customers = explode(',', $key->customer_names);
                                    foreach ($customers as $customer) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $customer . '<br />';
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                                                <td style="line-height: 18px;">
                                    <?php
                                                                        
                                                                        $customers = explode(',', $key->customer_names);
                                    foreach ($customers as $customer) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $key->service_start_date . '<br />';
                                    }
                                                                        
                                    
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                                                <!--<td style="line-height: 18px;">
                                    <?php
                                    //$mobile_numbers = explode(',', $key->mobile_number);
                                    //foreach ($mobile_numbers as $mobile_number) {
                                    //  echo $mobile_number . '<br />';
                                    //}
                                    ?>
                                </td>-->
                                                                <td style="line-height: 18px;">
                                    <?php
                                    $booking_typ = explode(',', $key->booking_type);
                                    foreach ($booking_typ as $b_type) {
                                        // output each hobby and decorate/separate them however you'd like
                                                                                if($b_type == "OD")
                                                                                    {
                                                                                        $booktype = "One Time";
                                                                                    }else if($b_type == "WE")
                                                                                    {
                                                                                        $booktype = "Weekly";
                                                                                    }else if($b_type == "BW")
                                                                                    {
                                                                                        $booktype = "Bi-Weekly";
                                                                                    }
                                                                                    else {
                                                                                        $booktype = "";
                                                                                    }
                                        echo $booktype . '<br />';
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                <td style="line-height: 18px;">
                                    <?php
                                    $sources = explode(',', $key->sources);
                                    foreach ($sources as $source) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $source . '<br />';
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                <td style="line-height: 18px;">
                                    <?php
                                    $shifts = explode(',', $key->time_from);
                                    $shifts2 = explode(',', $key->time_to);
                                    $i = 0;
                                    foreach ($shifts as $shift) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $shift . ' - ' . $shifts2[$i]. '<br />';
                                        $i++;
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                
                                <td style="line-height: 18px;">
                                    <?php
                                    $drivers = explode(',', $key->drivers);
                                    foreach ($drivers as $driver) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $driver. '<br />';
                                    }
                                    //echo html_escape($performance->customer_name);
                                    ?>
                                </td>
                                
                                <!--<td style="line-height: 18px;">
                                    <?php
                                    //$totamt = explode(',', $key->totamt);
                                    //$total=0;
                                    //$total=array_sum($totamt);
                                    //foreach ($totamt as $totam) {
                                    //  echo $totam. '<br />';
                                    //}
                                    //echo "<br /><span style='font-style: oblique;'>Total - ".$total."</span>";
                                    ?>
                                </td>-->
                                
                                <td style="line-height: 18px;">
                                    <?php
                                    $notes = explode(',', $key->bookingnotes);
                                    foreach ($notes as $note) {
                                        // output each hobby and decorate/separate them however you'd like
                                        echo $note. '<br />';
                                    }
                                    ?>
                                </td>
                            <?php
                                
                                
                            }
                                
                        
                        }
                       
                        ?>
    </tbody>    
</table>