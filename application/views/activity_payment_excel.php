<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=payment_data_" . date('d-m-Y', strtotime($date)) . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1" align="center">
    <thead>
        <tr>
            <td colspan="5" align="center">Payment Details Dated <?= date('d-m-Y', strtotime($date)) ?></td>
        </tr>
        <tr>
            <th> Sl.No</th>
            <th> Client Name</th>
            <th> Client Mobile</th>
            <th> Maid Name</th>
            <th> Address</th>
            <th> Paid Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sumamt = 0;
        foreach ($data as $k => $d) { ?>
            <tr>
                <td><?php echo ($k + 1); ?></td>
                <td><?php echo $d['customer_name']; ?></td>
                <td><?php echo $d['mobile_number_1']; ?></td>
                <td><?php echo $d['maid_name']; ?></td>
                <td><?php echo $d['customer_address']; ?></td>
                <td><?php echo $d['paid_amount']; ?></td>
                <?php $sumamt += floatval($d['paid_amount']); ?>
            </tr>
        <?php
        } ?>
        <tr>
            <!-- <td colspan="5"> Total </td> -->
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total </td>
            <td><?php echo $sumamt ?></td>
        </tr>
    </tbody>
</table>