<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
    .card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 500px;
  margin: auto;
  text-align: center;
  font-family: arial;
}
.card2 {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  margin: auto;
  font-family: arial;padding: 5%;
}
.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background: rgba(0, 0, 0, 0) linear-gradient(135deg, rgb(58, 44, 112) 0%, rgb(106, 0, 91) 100%) repeat scroll 0 0;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}

.float-right{float: right;}

ul.timeline {
    list-style-type: none;
    position: relative;
    margin-left: -20px;
}
ul.timeline a {
    font-size: 16px;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 15px 0;
    padding-left: 50px;
}
ul.timeline > li a {
    margin: 0;
    padding: 0;
    border: 0;
    font: 13px/1.7em 'Open Sans';
    font-family: 'Open-Sans-regular',sans-serif;
    color: #777;
}
ul.timeline > li p {
    color: #000;
    font-size: 13px;
}
ul.timeline > li.success:before {
  border: 3px solid rgb(26,191,7);
}
ul.timeline > li.danger:before {
  border: 3px solid rgb(239,43,43);
}
ul.timeline > li.warning:before {
  border: 3px solid #ff6f00;
}
ul.timeline.closed > li:last-child:before {
  border: 3px solid rgb(26,191,7);
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid rgb(58,44,112);
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
.timeline_div{max-height: 250px;overflow-y: scroll;padding-right: 15px;}
.reply_btn{margin-top:5px;}
</style>
<div class="row">   
    <div class="col-md-6 grid-margin stretch-card face-pro-box text-center">
        <div class="card">
            <br/>
          <img src="<?php echo base_url();?>images/user-icon.png" alt="John" style="width:40%;margin:0px auto;border-radius: 50%;>
          <h1>Ticket No:<?php echo $complaints_details->cmp_ticket_ref;?></h1>
          <p class="text-center">User</p>
          <p class="title text-center"><?php echo $complaints_details->customer_name;?></p>
          <div style="margin: 24px 0;">
            <div class="col-md-4 text-center" style="padding: 25px 0px;">
                Date <br>
                <h2><?php echo $complaints_details->cmp_ticket_service_date;?></h2>
            </div>
            <div class="col-md-4 text-center" style="padding: 25px 0px;">
                Type <br>
                <h2><?php echo $complaints_details->category_name;?></h2>
            </div>
            <div class="col-md-4 text-center" style="padding: 25px 0px;">
                Status <br>
                <h2 id="comp_status_text"><?php echo $complaints_details->cmp_ticket_status;?></h2>
            </div>
          </div>
          <p>
            <button id="complaint_action_button" data-status="<?php echo $complaints_details->cmp_ticket_status;?>">
              <?php if($complaints_details->cmp_ticket_status=='PENDING'){
                        echo "Verify";$reply_style='style="display:none;"';
               }
               else if($complaints_details->cmp_ticket_status=='ACTIVE'){
                        echo 'Close Ticket';$reply_style='style="display:block;"';
               }
               else
               {
                        echo 'Closed';$reply_style='style="display:none;"';
               }
                ?>
            </button>
          </p>
        </div>
    </div><!-- /span12 --> 

    <div class="col-md-6 grid-margin stretch-card">
        <div class="card2">
            <div class="form-group">
              <label for="email"><h2>Description</h2></label>
              <div><?php echo $complaints_details->cmp_ticket_description;?></div>
            </div>
            <div class="form-group">
              <label for="email"><h2>Comments</h2></label>
              <div class="timeline_div">
                <ul class="timeline <?php if($complaints_details->cmp_ticket_status=='CLOSED'){echo 'closed';}?>">
                  <?php foreach ($complaint_comments as $comment) { ?>
                  <li >
                    <a target="_blank"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $comment->user_fullname;?></a>
                    <a href="#" class="float-right">
                      <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d F,Y",strtotime($comment->cmnt_added_datetime));?>
                      <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date("g:i a",strtotime($comment->cmnt_added_datetime));?>
                    </a>
                    <p><?php echo $comment->cmnt_text;?></p>
                  </li>
                  <?php } ?>
                </ul>
              </div>
              
              <div class="form-group replybox" <?php echo $reply_style;?>>
                <form method="POST" action="<?php echo base_url();?>settings/add_ticket_comment">
                  <label for="reply"><h2>Reply</h2></label>
                  <textarea id="reply" class="form-control" name="ticket_reply"></textarea>
                  <input type="hidden" id="hid_cmp_id" name="hid_cmp_id" value="<?php echo $complaints_details->cmp_id;?>">
                  <button class="reply_btn">Reply</button>
                </form>
              </div>
            </div>
            
              
            
        </div>
    </div>
</div>

<script>
$( document ).ready(function() {
    $("#complaint_action_button").click(function(){
        var complaint_id=$("#hid_cmp_id").val();
        var comp_stat=$("#comp_status_text").text();
        var update_stat='';
        if(comp_stat!='CLOSED')
        {
          update_stat='';
          if(comp_stat=='PENDING')
          {
            update_stat='ACTIVE';
          }
          else if(comp_stat=='ACTIVE')
          {
            update_stat='CLOSED';
          }

          $.ajax({
                    url: "<?php echo base_url();?>settings/update_complaint_status",
                    type: "post",
                    data: {complaint_id:complaint_id,complaint_status:update_stat} ,
                    success: function (response) {
                        if(response=='success')
                        {
                          $("#comp_status_text").html(update_stat);
                          if(update_stat=='ACTIVE')
                          {
                            $("#complaint_action_button").html('Close Ticket');
                            $(".replybox").show();
                          }
                          else if(update_stat=='CLOSED')
                          {
                            $("#complaint_action_button").html('Closed');
                            setTimeout(function(){ 
                              window.location = "<?php echo base_url();?>settings/complaints";
                            }, 1000);
                            $(".replybox").hide();
                          }

                        }
                        else
                        {
                          alert('Error!Try again later.');
                        }
                       
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                      alert('Error!Try again later.');
                    }
                });

        }
        
    });
});
</script>