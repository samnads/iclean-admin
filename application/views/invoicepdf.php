<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>title</title>
</head>
<style>
  @page {
    margin: 0px 0px 0px 0px !important;
    padding: 0px 0px 0px 0px !important;
  }

  < !--.header {
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
  }

  < !--.footer {
    position: fixed;
    bottom: 0px;
    left: 0px;
    right: 0px;
  }

  -->< !--.main-content {
    page-break-after: always;
  }

  -->*/
</style>

<body style="padding: 0px; margin: 0px;">


  <div class="main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">

    <header style="height: 94px; overflow: hidden; position: fixed; left:0; top:0; z-index: 999;">


      <div class="header" style="width:100%; height:auto; padding: 5px 0px 10px 0px; margin: 0px; background: #f4f4f4;">


        <!--   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15%" rowspan="2" align="right"><img src="http://servicemaster.ae/images/logo.png" width="70" height="70" /></td>
                  <td style=" padding: 5px 0px 0px 0px;"><img src="/var/www/booking.emaid.info/public_html/service-master/images/invoice-logo-name.png" width="130" height="14" /></td>
                  <td width="35%" rowspan="2" valign="bottom">
                  
                  
                  
                         <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                                      <tr>
                                        <td width="50%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                                        <td width="5%">:</td>
                                        <td width="45%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>054 459 0300</strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Email </td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px">info@servicemaster.ae</td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px">100243560800003</td>
                                      </tr>
                                      
                             </table>
                  
                  
                  
                  </td>
                </tr>
                <tr>
                  <td width="50%"><p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 0px; margin: 0px;">
                            <b>Service Master Cleaning Services L.L.C.</b><br />
                            ACICO Business Park, Office no. 603-A Rm. 265<br />
                            Al Hilal Al Arabi Business Centre Port Saeed Deira, Dubai, UAE.</p>
                  </td>
                </tr>
              </table>
              -->





        <table width="100%" border="0">
          <tr>
            <td width="15%" align="right"><img src="https://iclean.bh/images/logo.png" width="70" height="70" /></td>
            <td width="50%">
              <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 10px; margin: 0px;">
                <strong style="font-size: 13px;">I CLEAN SERVICES W.L.L</strong><br />
                Road 31, Bldg. 1007/A. Block 635,<br />
                Ma'ameer,<br />
                Sitra, Kingdom of Bahrain
              </p>
            </td>
            <td width="35%" valign="bottom">
              <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                <tr>
                  <td width="35%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                  <td width="5%">:</td>
                  <td width="60%" valign="middle" style="padding: 0px 25px 0px 0px"><strong>+973 1770 0211-135</strong></td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 0px">Email </td>
                  <td>:</td>
                  <td valign="middle" style="padding: 0px 25px 0px 0px">info@iclean.bh</td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 0px">TRN</td>
                  <td>:</td>
                  <td valign="middle" style="padding: 0px 25px 0px 0px">220009052000002</td>
                </tr>

              </table>
            </td>
          </tr>
        </table>


      </div>


    </header>





    <section style=" height: 1000px; padding-top: 100px;">

      <div class="main-content" style="width: 90%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">

        <div class="tax-head" style="width:100%; height:auto; padding: 50px 0px 20px 0px; margin: 0px;">
          <p style="font-family: Roboto, sans-serif; font-size: 30px; color: #555; line-height: 30px; padding: 0px; margin: 0px; text-align: center;">TAX INVOICE</p>
        </div>

        <div class="to-job" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px 0px 20px 0px;">

          <div class="to-address" style="width:100%; height:auto; background: #e5f6fc; padding: 20px 0px 25px; margin: 0px;">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="middle">
                  <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px; margin: 0px;"><strong>To</strong></p>

                  <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px 45px; margin: 0px;">
                    <strong> <?php echo $invoice_detail[0]->customer_name; ?></strong><br />
                    <?php echo $invoice_detail[0]->bill_address; ?><br />
                    <strong>Mobile : <?php echo $invoice_detail[0]->mobile_number_1; ?></strong><br />
                    <!--Apt No: 408, Dubai Internet City, Building 12 4th Floor.<br />-->
                  </p>


                  <!--<p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 10px 25px 0px 45px; margin: 0px; font-weight: bold;">Customer TRN &nbsp;&nbsp;&nbsp; :&nbsp; 0087267213283283</p>-->

                </td>

                <td align="right" valign="middle">




                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px;">
                    <tr>
                      <td style="padding: 0px 0px 0px 0px"><strong>Invoice Number</strong></td>
                      <td>:</td>
                      <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?php echo str_replace("INV-", "", $invoice_detail[0]->invoice_num); ?></strong></td>
                    </tr>
                    <tr>
                      <td style="padding: 0px 0px 0px 0px">Invoice Date</td>
                      <td>:</td>
                      <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo date('d F - Y', strtotime($invoice_detail[0]->invoice_date)) ?></td>
                    </tr>
                    <tr>
                      <td style="padding: 0px 0px 0px 0px">Due Date</td>
                      <td>:</td>
                      <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo date('d F - Y', strtotime($invoice_detail[0]->invoice_due_date)) ?></td>
                    </tr>
                    <tr>
                      <td style="padding: 0px 0px 0px 0px">Prepared By</td>
                      <td>:</td>
                      <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><?php echo $invoice_detail[0]->user_fullname; ?></td>
                    </tr>
                  </table>





                </td>
              </tr>
            </table>






          </div>



        </div>

        <div class="table-main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px;">


          <table width="100%" border="0" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 14px; padding: 0px; margin: 0px;">
            <tr class="table-head" bgcolor="#153a54" style=" font-size: 12px; line-height: 20px; font-weight: bold; color: #FFF;">
              <td width="8%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF; font-family:Arial, Helvetica, sans-serif; font-weight: bold;">Sl.No</td>
              <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Date & Time</td>
              <td width="31%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Description</td>
              <td width="7%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Hours</td>
              <td width="10%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Amount</td>
              <td width="10%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF; ">VAT-5%</td>
              <td width="14%" align="center" valign="middle" style="padding: 10px 10px;">Net Amount</td>
            </tr>
            <?php
            $i = 1;
            foreach ($invoice_detail as $jobs) {
              $tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time)) / 3600);
            ?>





              <tr bgcolor="<?php echo ($i % 2 ? "#FFF" : "#e5f6fc"); ?>">
                <td align="center" style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;"><?php echo $i; ?></td>
                <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; font-size: 11px;"><strong style="font-size: 12px;"><?php echo date('d/m/Y', strtotime($jobs->service_date)); ?></strong><br />
                  <?php echo date('H:i A', strtotime($jobs->service_from_time)); ?> - <?php echo date('H:i A', strtotime($jobs->service_to_time)); ?></td>

                <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 11px; line-height: 14px;"><?php echo $jobs->description; ?></td>
                <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $tot_hrs; ?></td>
                <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $jobs->line_amount; ?></td>
                <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $jobs->line_vat_amount; ?></td>
                <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $jobs->line_net_amount; ?></td>
              </tr>
            <?php
              $i++;
            } ?>
          </table>



        </div>



        <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">


          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e5f6fc" style="font-family: Roboto, sans-serif; font-size:18px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">


            <tr style="font-size: 14px; color: #333;">
              <td valign="middle" style="padding: 5px 0px 0px 20px;">Net Amount</td>
              <td align="right" valign="middle" style="padding: 5px 20px 0px 0px; font-size: 17px;"> <?php echo $invoice_detail[0]->invoice_total_amount; ?> <span style="font-size:11px; ">BHD</span></td>
            </tr>


            <tr style="font-size: 14px; color: #333;">
              <td valign="middle" style="padding: 0px 0px 10px 20px;">Vat 5%</td>
              <td align="right" valign="middle" style="padding: 0px 20px 10px 0px; font-size: 17px;"> <?php echo $invoice_detail[0]->invoice_tax_amount; ?> <span style="font-size:11px; ">BHD</span></td>
            </tr>


          </table>


        </div>



        <div class="table-main" style="width:72%; height:auto; padding: 0px 0px 0px 28%; margin: 0px;">


          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#00a8de" style="font-family: Roboto, sans-serif; font-size:14px; color: #FFF; line-height: 20px; padding: 0px; margin: 0px;">



            <tr>
              <td valign="middle" style="padding: 10px 0px 0px 20px;">Total Amount</td>
              <td align="right" valign="middle" style="font-size: 20px; padding: 10px 20px 0px 0px;"> <?php echo $invoice_detail[0]->invoice_net_amount; ?> <span style="font-size:11px; ">BHD</span></td>
            </tr>


            <tr>
              <td colspan="2" align="right" valign="middle" style="padding: 0px 20px 10px 20px; font-size: 13px;">BHD <strong><?php echo numberTowords($invoice_detail[0]->invoice_net_amount); ?></strong> Only</td>
            </tr>
          </table>




        </div>








      </div>

    </section>






    <footer style="height: 120px; overflow: hidden; position: fixed; left:0; bottom:0; z-index: 999;">

      <div style="">
        <table width="100%" border="0" style="padding-bottom: 10px;">
          <tr>
            <td width="23.3333%" align="center">
              <?php
              if ($invoice_detail[0]->signature_status == 1) {
                $imagepath = "/var/www/booking.emaid.info/public_html/iclean/invoice_customer_signature/" . $invoice_detail[0]->signature_image;
              ?>
                <img src="<?php echo $imagepath; ?>" width="60" height="34" style="padding-left: 50px;" />
              <?php
              }
              ?>
            </td>
            <td width="53.3333%">&nbsp;</td>
            <td width="23.3333%">
              <!--<img src="/var/www/booking.emaid.info/public_html/service-master/images/signature.png" width="60" height="34" />-->
            </td>
          </tr>
      </div>


      <table width="100%" border="0" style="border-top: 1px solid #888; margin-top: 10px;">
        <tr>
          <td width="23.3333%">
            <p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #333; line-height: 16px; padding: 0px 0px 0px 30px; text-align:right; margin: 0px; font-weight: 600;">Receiver's Signature</p>
          </td>
          <td width="53.3333%">&nbsp;</td>
          <td width="23.3333%">
            <p style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;color: #333; line-height: 16px; padding: 0px; margin: 0px; font-weight: 600;">Signature</p>
          </td>
        </tr>
      </table>


      <div style=" width: 100%; margin-top: 25px;">
        <p style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;color: #333; line-height: 16px; text-align: center; padding: 0px; margin: 0 auto; font-weight: 600; ">Thank you for your business with us</p>
      </div>


    </footer>

  </div>

</body>

</html>
<?php
function numberTowords($num)
{
  $ones = array(
    0 => "Zero",
    1 => "One",
    2 => "Two",
    3 => "Three",
    4 => "Four",
    5 => "Five",
    6 => "Six",
    7 => "Seven",
    8 => "Eight",
    9 => "Nine",
    10 => "Ten",
    11 => "Eleven",
    12 => "Twelve",
    13 => "Thirteen",
    14 => "Fourteen",
    15 => "Fifteen",
    16 => "Sixteen",
    17 => "Seventeen",
    18 => "Eighteen",
    19 => "Nineteen",
    "014" => "Fourteen"
  );
  $tens = array(
    0 => "Zero",
    1 => "Ten",
    2 => "Twenty",
    3 => "Thirty",
    4 => "Forty",
    5 => "Fifty",
    6 => "Sixty",
    7 => "Seventy",
    8 => "Eighty",
    9 => "Ninety"
  );
  $hundreds = array(
    "Hundred",
    "Thousand",
    "Million",
    "Billion",
    "Trillion",
    "Quardrillion"
  ); /*limit t quadrillion */
  $num = number_format($num, 2, ".", ",");
  $num_arr = explode(".", $num);
  $wholenum = $num_arr[0];
  $decnum = $num_arr[1];
  $whole_arr = array_reverse(explode(",", $wholenum));
  krsort($whole_arr, 1);
  $rettxt = "";
  foreach ($whole_arr as $key => $i) {
    while (substr($i, 0, 1) == "0")
      $i = substr($i, 1, 5);
    if ($i < 20) {
      /* echo "getting:".$i; */
      $rettxt .= $ones[$i];
    } elseif ($i < 100) {
      if (substr($i, 0, 1) != "0")  $rettxt .= $tens[substr($i, 0, 1)];
      if (substr($i, 1, 1) != "0") $rettxt .= " " . $ones[substr($i, 1, 1)];
    } else {
      if (substr($i, 0, 1) != "0") $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
      if (substr($i, 1, 1) != "0") $rettxt .= " " . $tens[substr($i, 1, 1)];
      if (substr($i, 2, 1) != "0") $rettxt .= " " . $ones[substr($i, 2, 1)];
    }
    if ($key > 0) {
      $rettxt .= " " . $hundreds[$key] . " ";
    }
  }
  if ($decnum > 0) {
    $rettxt .= " and ";
    if ($decnum < 20) {
      $rettxt .= $ones[$decnum];
      $rettxt .= " Fils";
    } elseif ($decnum < 100) {
      $rettxt .= $tens[substr($decnum, 0, 1)];
      if (substr($decnum, 1, 1) > 0) {
        $rettxt .= " " . $ones[substr($decnum, 1, 1)];
      }
      $rettxt .= " Fils";
    }
  }
  return $rettxt;
}
?>