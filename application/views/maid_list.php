<style type="text/css">
.widget .widget-header {
	margin-bottom: 0px;
}

table.da-table tr td { padding: 0px 6px;}
</style>
<div class="row">
  <div class="span12">
    <div class="widget widget-table action-table">
      <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Maids</h3>
        <div class="topiconnew"><a href="<?php echo base_url(); ?>maid/add"><img src="<?php echo base_url(); ?>images/maid-plus-icon.png" title="Add Maid"/></a></div>
        <div class="topiconnew"><a href="<?php echo base_url(); ?>maid/toExcel/<?php echo $active; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Export to Excel"/></a></div>
        <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url(); ?>maid/add"><img src="<?php// echo base_url(); ?>img/add_female_user.png" title="Add Maid"/></a>--> 
        <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url(); ?>maid/toExcel/<?php// echo $active; ?>"><img style="width: 32px; height: 32px;" src="<?php// echo base_url(); ?>images/upload-ex-icon.png" title="Export to Excel"/></a>-->
        <select style="margin-left : 10px; margin-top: 6px; width:150px;" id="all-maids">
          <option value="1" <?php echo $active == 1 ? 'selected="selected"' : ''; ?>>All</option>
          <option value="2" <?php echo $active == 2 ? 'selected="selected"' : ''; ?>>Active</option>
          <option value="3" <?php echo $active == 3 ? 'selected="selected"' : ''; ?>>Inactive</option>
          <option value="4" <?php echo $active == 4 ? 'selected="selected"' : ''; ?>>Deleted</option>
        </select>
      </div>
      <!-- /widget-header -->
      
      <div class="widget-content">
        <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 20px"><center>
                  No.
                </center>
              </th>
              
              <th colspan="2" style="line-height: 18px"> <center>
                  Maids
                </center></th>
              <th style="line-height: 18px"> <center>
                  Present Address
                </center></th>
              <th style="line-height: 18px"> <center>
                  Mobile Number
                </center></th>
              <!--                    <th style="line-height: 18px"> <center>Photo</center></th>-->
              <th style="line-height: 18px; width: 110px" class="td-actions"><center>
                  Actions
                </center></th>
            </tr>
          </thead>
          <tbody>
            <?php
                        if (count($maids) > 0) {
                            $i = 1;
                            foreach ($maids as $maids_val) {
                                ?>
            <tr>
              <?php
//                                    if ($maids_val['maid_photo_file'] == "") {
//                                        $image = base_url() . "img/no_image.jpg";
//                                    } else {
//                                        $image = base_url() . "maid_img/" . $maids_val['maid_photo_file'];
//                                    }
                                    ?>
              <td style="line-height: 18px; width: 20px"><center>
                  <?php echo $i; ?>
                </center></td>
                
                <td style="line-height: 18px; cursor: pointer;text-align: center;" onclick="view_maid(<?php echo $maids_val['maid_id'] ?>);"><?php
														if ($maids_val['maid_photo_file'] == "") {
                                                            $image = base_url() . "img/no_image.jpg";
                                                        } else {
                                                            $image = base_url() . "maidimg/" . $maids_val['maid_photo_file'];
                                                        }
                                                                        ?>
                                                                        
                                                                        <a href="<?php echo base_url(); ?>maid/view/<?php echo $maids_val['maid_id'] ?>" style="text-decoration: none;color:#333;"> <img src="<?php echo $image; ?>" style="height: 50px; width: 50px; margin: 3px auto 3px; border-radius: 50%;"/></a>
                </td>
                
                
                
                
              <td style="line-height: 18px; cursor: pointer;text-align: center;" onclick="view_maid(<?php echo $maids_val['maid_id'] ?>);">
			  
                <a href="<?php echo base_url(); ?>maid/view/<?php echo $maids_val['maid_id'] ?>" style="text-decoration: none;color:#333;"> <?php echo $maids_val['maid_name'] ?> </a></td>
                
                
                
                
                
              <td style="line-height: 18px; cursor: pointer"><center>
                  <?php echo $maids_val['maid_present_address'] ?>
                </center></td>
              <td style="line-height: 18px; cursor: pointer"><center>
                  <?php echo $maids_val['maid_mobile_1'] ?>
                </center></td>
              
              <!--                                    <td style="line-height: 18px"><center><img height="135" width="95" src="<?php //echo $image;      ?>"/></center></td>-->
              <td style="line-height: 18px; width: 158px" class="td-actions"><center>
                  <!--                                <a href="#myModal" role="button" class="btn btn-small btn-info" data-toggle="modal"><i class="btn-icon-only icon-search"></i></a>--> 
                  <!--                                            <a class="btn btn-small btn-info" href="javascript:void();" onclick="view_maid(<?php //echo $maids_val['maid_id']  ?>);"><i class="btn-icon-only icon-search"> </i></a>--> 
                  <a class="btn btn-small btn-info" href="<?php echo base_url(); ?>maid/view/<?php echo $maids_val['maid_id'] ?>" title="View Maid"><i class="btn-icon-only fa fa-eye"> </i></a>
                  <?php  if ($maids_val['maid_status']!=2) { ?>
                  <a class="btn btn-small btn-warning" href="<?php echo base_url(); ?>maid/edit/<?php echo $maids_val['maid_id'] ?>" title="Edit Maid"><i class="btn-icon-only icon-pencil"> </i></a>
                  <?php } ?>
                  <?php  if ($maids_val['maid_status']==2) { ?>
                  <p class="btn btn-danger" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                  <?php } ?>
                  <?php
                                        if(user_authenticate() == 1)
                                        {
                                        if ($maids_val['maid_status'] == 1) {
                                            ?>
                  <a href="javascript:void(0)" class="btn btn-success btn-small" title="Disbale Maid" onclick="change_status(<?php echo $maids_val['maid_id'] ?>,<?php echo $maids_val['maid_status'] ?>);"><i class="btn-icon-only fa fa-toggle-on "> </i></a>
                  <?php
                                        } if ($maids_val['maid_status'] == 0) { 
                                            ?>
                  <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $maids_val['maid_id'] ?>,<?php echo $maids_val['maid_status'] ?>);"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                  <?php
                                        }
//                                         if ($maids_val['maid_status']!=2) {
                                        ?>
                  
                  <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $maids_val['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->
                  
                  <?php  
//                                         }
                                        }
                                        ?>
                </center></td>
            </tr>
            <?php
        $i++;
    }
}
?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content --> 
    </div>
    <!-- /widget --> 
  </div>
  <!-- /span6 --> 
</div>

<!-- Modal -->
<div id="myModal" class="modal hide fade" style="width: 700px; left: 45%" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><span id="maid_name">Maid</span></h3>
  </div>
  <div class="modal-body"> <a style="float:right ; margin-right:20px; cursor:pointer;"><img id="maid_photo" src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 150px; width: 150px"/> </a>
    <table>
      <tbody>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Gender </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td style="line-height: 30px;"><span id="maid_gender"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Nationality </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_nationality"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Present Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_present_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Permanent Address </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_permanent_address"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 1 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_1"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Mobile Number 2 </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_mobile_2"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Flat </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="flat_name"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Passport Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_passport_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Visa Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_visa_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour Card Number </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_number"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Labour card Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_labour_card_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_id"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Emirates Id Expiry Date </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_emirates_expiry_date"></span></td>
        </tr>
        <tr>
          <td style="line-height: 30px; width: 200px"><b>Notes </b></td>
          <td>:&nbsp;&nbsp;</td>
          <td><span id="maid_notes"></span></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="modal-footer"> 
    <!--                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button class="btn btn-primary">Save changes</button>--> 
  </div>
</div>

<!--<style>

    .imge{
        display: none
    }

    td.dispimage:hover img{
        display: block;
    }
</style>-->