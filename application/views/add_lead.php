<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Add Lead</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" title="Lead List" href="<?php echo base_url(); ?>lead"><i class="icon-th-list"></i></a>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Lead Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                        
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data" >      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                <?php echo $errors; ?> 
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Lead added Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                
                                
                                <fieldset>            
                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="customername">Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
															<input type="text" class="span3" id="customername" name="customername" value="" required>
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

													<div class="control-group">											
														<label class="control-label" for="customeremail">Email&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="customeremail" name="customeremail" value="" required>
                                                            <?php echo form_error('customeremail'); ?>
														</div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
														<label class="control-label" for="customermobile">Phone&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="customermobile" name="customermobile"  value="" required >
                                                            <?php echo form_error('customermobile'); ?>
														</div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
														<label class="control-label" for="contactdate">Date of contact&nbsp;<font style="color: #C00">*</font></label>
														<div class="controls">
															<input type="text" class="span3" id="contactdate" name="contactdate" readonly="readonly" value="" required>
														</div> <!-- /controls -->				
													</div> <!-- /control-group -->
													
													<div class="control-group">											
                                                        <label class="control-label" for="customeraddress">Address&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="3" id="customeraddress" name="customeraddress" required></textarea>	
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->




                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
												<div class="control-group">
                                                    <label class="control-label" for="servicetype">Service Type&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="servicetype" id="servicetype" class="span3" required >
															<option value="">Select Service</option>
                                                            <?php
                                                            if(count($services)>0)
                                                            {
                                                               foreach ($services as $service)
                                                               {
                                                               ?>
                                                                       <option value="<?php echo $service->service_type_id;?>"><?php echo $service->service_type_name; ?></option>
                                                               <?php
                                                               }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
												<div class="control-group">
                                                    <label class="control-label" for="customersource">Source&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="customersource" id="customersource" class="span3" required >
                                                            <option value="">Select Option</option>
															<option value="Facebook">Facebook</option>
															<option value="Google">Google</option>
															<option value="Direct Call">Direct Call</option>
															<option value="Flyers">Flyers</option>
															<option value="Referral">Referral</option>
															<option value="Maid">Maid</option>
															<option value="Schedule visit">Schedule visit</option>
															<option value="Referred by staff">Referred by staff</option>
															<option value="Website">Website</option>
															<option value="Justmop">Justmop</option>
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="control-group">
                                                    <label class="control-label" for="leadtypes">Classification of lead&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="leadtypes" id="leadtypes" class="span3" required >
                                                            <option value="">Select type</option>
															<option value="Hot">Hot</option>
															<option value="Warm">Warm</option>
															<option value="Cold">Cold</option>
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="control-group">
                                                    <label class="control-label" for="servicetimeline">Timeline of service&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="servicetimeline" id="servicetimeline" class="span3" required >
                                                            <option value="">Select timeline</option>
															<option value="Within a week">Within a week</option>
															<option value="1 Month">1 Month</option>
															<option value="3 Months">3 Months</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">                                         
                                                        <label class="control-label" for="quote">Quote&nbsp;</label>
                                                        <div class="controls">
                                                            <input type="number" class="span3" id="quote" name="quote">  
                                                        </div> <!-- /controls -->               
                                                </div> <!-- /control-group -->
												<div class="control-group">                                         
                                                        <label class="control-label" for="customeraddress">Comments&nbsp;</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="3" id="comments" name="comments"></textarea>   
                                                        </div> <!-- /controls -->               
                                                </div> <!-- /control-group -->
												<div class="control-group">											
													<label class="control-label">Status&nbsp;<font style="color: #C00">*</font></label>
													<div class="controls">
														<label class="radio inline">
															<input type="radio" name="status" value="1" checked="checked"> Active
														</label>
														<label class="radio inline">
															<input type="radio" name="status" value="0"> Inactive
														</label>
													</div>	<!-- /controls -->			
												</div> <!-- /control-group -->
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>   
                                    <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="lead_sub">
                                
                                    
                                </div> 
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->

