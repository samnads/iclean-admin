<div class="row">
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Rate Reports</h3>
                    <?php
                    if ($search['search_date_from'] == "") {
                    ?>
                        <span style="color: #FFF;">From :</span> <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="search_date_from" name="search_date_from" value="<?php echo date('d/m/Y', strtotime('-7 days')); ?>" required>

                    <?Php
                    } else {
                    ?>
                        <span style="color: #FFF;">From</span> : <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="search_date_from" name="search_date_from" value="<?php echo $search['search_date_from'] ?>" required>
                    <?Php
                    }
                    ?>


                    <?php
                    if ($search['search_date_to'] == "") {
                    ?>
                        <span style="color: #FFF;">To </span> <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="search_date_to" name="search_date_to" value="<?php echo date('d/m/Y') ?>" required>

                    <?Php
                    } else {
                    ?>
                        <span style="color: #FFF;">To </span> <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="search_date_to" name="search_date_to" value="<?php echo $search['search_date_to'] ?>" required>
                    <?Php
                    }
                    ?>


                    <!-- <select name="maid_id" id="search-maid-id" style="margin-bottom: 4px;" class="sel2">  old code-->
                    <select name="maid_id" id="search-maid-id" style="margin-bottom: 4px;" class="">
                        <option value="0">All Maids</option>
                        <?php
                        $maid_id = $search['maid_id'];
                        foreach ($maids as $maid) {
                            $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                            echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                        }
                        ?>
                    </select>

                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day_from" value="<?php echo $search['search_day_from'] ?>">
                    <input type="hidden" name="day" id="day_to" value="<?php echo $search['search_day_to'] ?>">

                    <input type="submit" class="btn" value="Go" name="rate_report" style="margin-bottom: 4px;">
                    <?php
                    if ($search['search_date_from'] == "") {
                        $from_date = date('Y-m-d', strtotime('-7 days'));
                    } else {
                        list($day, $month, $year) = explode("/", $search['search_date_from']);
                        $from_date = "$year-$month-$day";
                    }

                    if ($search['search_date_to'] == "") {
                        $to_date = date('Y-m-d');
                    } else {
                        list($day, $month, $year) = explode("/", $search['search_date_to']);
                        $to_date = "$year-$month-$day";
                    }


                    ?>
                    <div class="topiconnew"><a href="<?php echo base_url(); ?>reports/ratereviewreporttoExcel/<?php echo $from_date; ?>/<?php echo $to_date; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Download to Excel"></a></div>
                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl.No</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Customer Mobile</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Service date</th>
                            <th style="line-height: 18px;"> Rating</th>
                            <th style="line-height: 18px;"> Comments</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        if ($rate_report != NULL) {
                            $i = 1;
                            foreach ($rate_report as $veh) {



                        ?>
                                <tr>
                                    <td style="line-height: 18px;"><?php echo $i; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $veh->customer_name; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $veh->mobile_number_1; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $veh->maid_name; ?> </td>
                                    <td style="line-height: 18px;"><?php echo date('d/m/Y', strtotime($veh->service_date)); ?> </td>
                                    <td><?php echo (int)$veh->rating; ?> stars</td>


                                    <td style="line-height: 18px;">
                                        <?php if ($veh->comments == "") {
                                            echo "";
                                        } else {
                                            echo $veh->comments;
                                        } ?>
                                    </td>
                                </tr>
                        <?php
                                $i++;
                            }
                        } ?>



                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>

<script type="text/javascript">
    $('#edit-profile').submit(function(e) {
        var from = document.getElementById("search_date_from").value;
        var fromdate = new Date($("from").val());
        var too = document.getElementById("search_date_to").value;
        var todate = new Date($("too").val());

        //      alert(fromdate);
        //      alert(todate);
        //    alert(Date.parse(fromdate));
        //    alert(Date.parse(todate));
        //     if(from < too){
        //          alert("End date should be greater than Start date.");

        //     }
        if (from > too) {
            alert("Invalid Date Range");
            window.location.reload();
        } else {
            //alert("Valid date Range");
        }

    });
</script>