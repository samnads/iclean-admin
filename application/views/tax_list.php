<style>
    #area select{ width: 15% !important;}
</style>

<div class="row">
        
      
    <div class="span6" id="edit_tax_setting" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit Tax Settings</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_tax_setting();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="edit_percentage">Percentage </label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_percentage" name="edit_percentage" required="required">
                                            </div>   				
                                    </div> 
                                
                                    <input type="hidden" class="span3" id="edit_taxt_id" name="edit_taxt_id">
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="tax_edit_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Tax Settings</h3>
              <!--<a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_hourly_price();"><img src="<?php// echo base_url();?>img/add.png" title="Add Services"/></a>-->
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Name </th>
                            <th style="line-height: 18px"> Percentage </th>
                            <th style="line-height: 18px"> Status </th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($tax_settings) > 0) {
                            $i = 1;
                            foreach ($tax_settings as $tax) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px">GST</td>
                            <td style="line-height: 18px"><?php echo $tax['percentage'] ?></td>
                            <td style="line-height: 18px">
                                <?php
                                    if($tax['status'] == 1)
                                    {
                                        $status = "Active";
                                    } else {
                                        $status = "Inactive";
                                    }
                                    echo $status; 
                                ?>
                            </td>
                            <td style="line-height: 18px" class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_tax_settings(<?php echo $tax['tax_id'] ?>);">
                                    <i class="btn-icon-only icon-pencil"> </i>
                                </a>
                                <!--<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_hourly_price(<?php// echo $sms['id']  ?>);"><i class="btn-icon-only icon-remove"> </i></a>-->
                            </td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
  
</div>