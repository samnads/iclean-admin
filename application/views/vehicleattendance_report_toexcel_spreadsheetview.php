<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
       <tr>                
          <?php            
            foreach($zones as $zone)
            {
                echo '<th>' . $zone->zone_name . '</th>';
            }
          ?>
      </tr>
    </thead>
    <tbody>
   <?php
        
            $i = 0;
            foreach ($reports as $rpt)
            {
                echo '<tr>';
                foreach($zones as $zone)
                {
                    echo '<td> ';
                    if(isset($rpt[$zone->zone_id]['maid_name']))
                    {
                        $color = $rpt[$zone->zone_id]['attendance_status'] == 'IN' ? 'green' : 'red';
                        
                        echo $rpt[$zone->zone_id]['maid_name'] 
                                . ' (<span style="color:'. $color . '">' . 
                                $rpt[$zone->zone_id]['attendance_status']  
                                . '</span>)';
                        
                    }
                    
                    
                    echo '</td>';
                }
                echo '</tr>';
                ++$i;
              
            }
           
        //}
        ?>
    </tbody>
</table>