<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No.</th>
            <th> ZOne</th>
            <th> Type</th>
            <th> Action</th>
            <th> Time</th>
        </tr>
    </thead>
    <tbody>
        <?php
                        if(!empty($driver_activity_report))
                        {
                            $i = 1;
                            foreach ($driver_activity_report as $activity)
                            {
                                switch ($activity->action_type)
                                {
                                    case 1 : $action_type = 'Vehicle IN';
                                        break;
                                    case 2 : $action_type = 'Vehicle OUT';
                                        break;
                                    case 3 : $action_type = 'Customer IN';
                                        break;
                                    case 4 : $action_type = 'Customer OUT';
                                        break;
                                    case 5 : $action_type = 'Transfer';
                                        break;
                                    case 6 : $action_type = 'Change Maid';
                                        break;
                                    case 7 : $action_type = 'Payment';
                                        break;
                                    case 8 : $action_type = 'Service Cancel';
                                        break;
                                    default : $action_type = 'Invalid';
                                        break;
                                }
                                
                                 echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $action_type . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->action_content . '</td>'
                                        . '<td style="line-height: 18px;">' . $activity->tracked_time . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
    </tbody>
</table>
