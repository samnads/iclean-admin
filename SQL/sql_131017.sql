ALTER TABLE `sms_settings` ADD `sender_id` VARCHAR(100) NULL AFTER `id`;   



CREATE TABLE `time_zone_settings` (
  `tz_id` int(15) NOT NULL,
  `timezone` varchar(154) DEFAULT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_zone_settings`
--

INSERT INTO `time_zone_settings` (`tz_id`, `timezone`, `last_updated_on`) VALUES
(1, 'Asia/Dubai', '2017-10-13 07:13:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `time_zone_settings`
--
ALTER TABLE `time_zone_settings`
  ADD PRIMARY KEY (`tz_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `time_zone_settings`
--
ALTER TABLE `time_zone_settings`
  MODIFY `tz_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;